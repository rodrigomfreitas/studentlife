-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09-Jun-2017 às 04:37
-- Versão do servidor: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `studentlife`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `dados_usuario`(IN `id` VARCHAR(250))
begin
select * from usuario where md5(cd_usuario) = id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_disciplinas_aluno`(IN `id` VARCHAR(250))
    NO SQL
begin
select * from disciplina_usuario
inner join disciplina on disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina
inner join semestre on semestre.cd_semestre = disciplina.SEMESTRE_cd_semestre
inner join curso on curso.cd_curso = semestre.CURSO_cd_curso
inner join turma on turma.cd_turma = disciplina_usuario.TURMA_cd_turma
inner join turno on turno.cd_turno = turma.TURNO_cd_turno
where md5(USUARIO_cd_usuario) = id;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `listar`(in id varchar(250))
begin

select * from usuario where md5(id_usuario) = id;

end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `mensagens_nao_lidas_count`(IN `id_sessao` INT)
BEGIN
SELECT COUNT(*) AS numrows
FROM mensagem
WHERE cd_recebe = id_sessao
AND ds_mensagem = 0;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verifica_login`(

		email varchar(45), senha varchar(45))
BEGIN



SELECT usuario.id_usuario, usuario.status, usuario.nome, usuario.sobrenome, usuario.senha, usuario.email, usuario.ativo, usuario.foto

FROM usuario

WHERE usuario.senha = senha

AND usuario.email = email

AND usuario.status >= 0;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `verifica_usuario`(IN `email` VARCHAR(45), IN `senha` VARCHAR(45))
BEGIN
SELECT usuario.cd_usuario, usuario.ds_email, usuario.ds_senha, usuario.TIPO_USUARIO_cd_tipo_usuario, usuario.nm_usuario, usuario.ds_foto, usuario.st_ativo, tipo_usuario.nm_tipo_usuario
from usuario
inner join tipo_usuario on usuario.TIPO_USUARIO_cd_tipo_usuario = tipo_usuario.cd_tipo_usuario
WHERE usuario.ds_email = email
AND usuario.ds_senha = senha
and usuario.TIPO_USUARIO_cd_tipo_usuario >= 0;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `acao`
--

CREATE TABLE IF NOT EXISTS `acao` (
`cd_acao` int(11) NOT NULL,
  `ds_acao` varchar(60) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `acao`
--

INSERT INTO `acao` (`cd_acao`, `ds_acao`) VALUES
(1, 'Entrada no Sistema'),
(2, 'Saída no Sistema'),
(3, 'Inclusão de Curso'),
(4, 'Exclusão de Curso'),
(5, 'Alteração de Curso');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ano`
--

CREATE TABLE IF NOT EXISTS `ano` (
`cd_ano` int(11) NOT NULL,
  `CURSO_cd_curso` int(11) NOT NULL,
  `dt_ano` date NOT NULL,
  `fl_excluido` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `ano`
--

INSERT INTO `ano` (`cd_ano`, `CURSO_cd_curso`, `dt_ano`, `fl_excluido`) VALUES
(1, 1, '2016-10-06', 0),
(2, 1, '2015-10-08', 0),
(3, 2, '2016-10-21', 0),
(4, 6, '2016-10-21', 0),
(6, 10, '2016-10-21', 0),
(7, 1, '2014-01-21', 0),
(8, 1, '2013-02-22', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aula`
--

CREATE TABLE IF NOT EXISTS `aula` (
`cd_aula` int(11) NOT NULL,
  `DISCIPLINA_cd_disciplina` int(11) NOT NULL,
  `nr_aula` int(11) NOT NULL,
  `fl_excluido` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Extraindo dados da tabela `aula`
--

INSERT INTO `aula` (`cd_aula`, `DISCIPLINA_cd_disciplina`, `nr_aula`, `fl_excluido`) VALUES
(1, 1, 1, 0),
(2, 1, 2, 0),
(3, 1, 3, 0),
(4, 1, 4, 0),
(5, 1, 5, 0),
(6, 1, 6, 0),
(7, 1, 7, 0),
(8, 1, 11, 0),
(9, 1, 8, 0),
(10, 1, 10, 0),
(11, 1, 9, 0),
(12, 1, 12, 0),
(13, 1, 13, 1),
(14, 1, 13, 1),
(15, 1, 13, 1),
(16, 1, 15, 0),
(17, 11, 1, 0),
(18, 11, 2, 0),
(19, 3, 1, 0),
(20, 2, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao`
--

CREATE TABLE IF NOT EXISTS `avaliacao` (
`cd_avaliacao` int(11) NOT NULL,
  `DISCIPLINA_cd_disciplina` int(11) NOT NULL,
  `TIPO_AVALIACAO_cd_avaliacao` int(11) NOT NULL,
  `nm_avaliacao` varchar(245) DEFAULT NULL,
  `ds_avaliacao` text NOT NULL,
  `palavras_chave_avaliacao` varchar(245) NOT NULL,
  `dt_data_cadastro` date DEFAULT NULL,
  `dt_data_final` date NOT NULL,
  `fl_excluido` int(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `avaliacao`
--

INSERT INTO `avaliacao` (`cd_avaliacao`, `DISCIPLINA_cd_disciplina`, `TIPO_AVALIACAO_cd_avaliacao`, `nm_avaliacao`, `ds_avaliacao`, `palavras_chave_avaliacao`, `dt_data_cadastro`, `dt_data_final`, `fl_excluido`) VALUES
(1, 1, 1, 'Desenvolver Sistemas Distribuídos', 'Neste trabalho vocês deverão desenvolver um sistema distribuído com o código muito bem documentado. Por exemplos, dois banco de dados comunicando em um sistema ou duas linguagens distintas rodando em plataformas diferentes.', 'sistema distribuido, comunicacao', '2016-07-30', '2016-08-25', 0),
(2, 2, 2, 'Prova presente na aula 12', 'Responder as perguntas montando o gráfico de Gantt utilizando o MS Project e os slides', 'gerencias de projetos, gráfico de gantt, ms project', '2016-06-07', '2016-08-02', 0),
(3, 5, 1, 'Resenha de livro', 'Escrever uma resenha do livro Psicologia Experimental - de Vera Lúcia Varanda Lombard.', '', '2016-06-08', '2016-06-08', 0),
(6, 1, 2, 'Sistemas Distribuídos', 'Esta prova de Sistemas Distribuídos aborda sincronização. Esta matéria consta nos slides das aulas 03 - 05', 'sistemas distribuidos, sincronizacao', '2016-07-01', '2016-10-25', 0),
(7, 2, 1, 'Gráfico de Gantt', 'Criar um Gráfico de Gantt', 'gerências de projetos, gráfico de gantt', '2016-06-30', '2016-07-08', 0),
(8, 8, 2, 'Banco de Dados', 'Estudar os slides 5 e 6. Sobre Procedures', 'banco de dados, procedures, triggers', '2016-06-30', '2016-07-28', 0),
(9, 1, 1, 'Desenvolver um sistema distribuído utilizando os conhecimentos adquiridos no decorrer do semestre.', 'Nesta atividade prática, caberá a você desenvolver um sistema distribuído, aplicando funcionalidades vistas em aulaem um ambiente multi-plataforma, podendo ter diferentes linguagens de desenvolvimento, diferentes bancos de dados, etc. Para esta prática você poderá desenvolver um sistema com base em webservice, SOAP, integração de sistemas, integração de bancos de dados', 'sistemas distribuidos, ambiente multi-plataforma, webservice, SOAP, integracao de sistemas, integracao de banco de dados', '2016-08-09', '2016-08-26', 0),
(10, 1, 1, 'Recuperar o trabalho sobre desenvolvimento de sistemas distribuídos.', 'Desenvolver uma resenha de no mínimo 5 páginas sobre desenvolvimento de sistemas distribuídos abordando segurança, formas de sincronização, formas de comunicação e arquiteturas a serem utilizadas', 'desenvolvimento de sistemas distribuidos, seguranca, formas de sincronizacao, formas de comunicacao, arquiteturas', '2016-08-17', '2016-11-01', 0),
(11, 3, 1, 'Desenvolver Sockets', 'Desenvolver uma aplicação utilizando sockets em qualquer linguagem(php, java c#)', 'sockets', '2016-10-11', '2016-10-31', 0),
(12, 11, 2, 'Algoritmos e Programação', 'Prova sobre todo o conteúdo passado neste semestre até o momento', 'algoritmos, programação', '2016-10-21', '2016-10-21', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao_turma`
--

CREATE TABLE IF NOT EXISTS `avaliacao_turma` (
  `AVALIACAO_cd_avaliacao` int(11) NOT NULL,
  `TURMA_cd_turma` int(11) NOT NULL,
  `DISCIPLINA_cd_disciplina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `avaliacao_turma`
--

INSERT INTO `avaliacao_turma` (`AVALIACAO_cd_avaliacao`, `TURMA_cd_turma`, `DISCIPLINA_cd_disciplina`) VALUES
(1, 1, 1),
(1, 2, 1),
(6, 1, 1),
(6, 2, 1),
(9, 1, 1),
(2, 1, 2),
(2, 2, 2),
(11, 1, 3),
(11, 2, 3),
(8, 3, 8),
(12, 11, 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacao_usuario`
--

CREATE TABLE IF NOT EXISTS `avaliacao_usuario` (
  `AVALIACAO_cd_avaliacao` int(11) NOT NULL,
  `USUARIO_cd_usuario` int(11) NOT NULL,
  `TURMA_cd_turma` int(11) NOT NULL,
  `ds_conceito` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `avaliacao_usuario`
--

INSERT INTO `avaliacao_usuario` (`AVALIACAO_cd_avaliacao`, `USUARIO_cd_usuario`, `TURMA_cd_turma`, `ds_conceito`) VALUES
(1, 1, 1, 'A'),
(1, 3, 1, 'D'),
(1, 4, 2, 'A'),
(1, 5, 1, 'D'),
(2, 1, 1, 'B'),
(6, 1, 1, 'D'),
(6, 3, 1, 'SC'),
(6, 4, 2, 'B'),
(6, 5, 1, 'A'),
(9, 1, 1, 'A'),
(9, 3, 1, 'A'),
(9, 5, 1, 'A'),
(11, 1, 1, 'A');

-- --------------------------------------------------------

--
-- Estrutura da tabela `backup`
--

CREATE TABLE IF NOT EXISTS `backup` (
`cd_backup` int(11) NOT NULL,
  `nm_backup` varchar(240) NOT NULL,
  `ds_caminho_backup` varchar(240) NOT NULL,
  `dt_criacao_backup` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fl_excluido` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Extraindo dados da tabela `backup`
--

INSERT INTO `backup` (`cd_backup`, `nm_backup`, `ds_caminho_backup`, `dt_criacao_backup`, `fl_excluido`) VALUES
(22, 'backup-studentlife-12-11-2016-12-13-50.zip', 'backupDB/backup-studentlife-12-11-2016-12-13-50.zip', '2016-11-12 12:13:50', 0),
(23, 'backup-studentlife-12-11-2016-12-34-33.zip', 'backupDB/backup-studentlife-12-11-2016-12-34-33.zip', '2016-11-12 12:34:33', 0),
(24, 'backup-studentlife-18-11-2016-09-21-06.zip', 'backupDB/backup-studentlife-18-11-2016-09-21-06.zip', '2016-11-18 09:21:06', 0),
(25, 'backup-studentlife-25-11-2016-20-58-56.zip', 'backupDB/backup-studentlife-25-11-2016-20-58-56.zip', '2016-11-25 20:58:56', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `chave_turma`
--

CREATE TABLE IF NOT EXISTS `chave_turma` (
`cd_chave_turma` int(11) NOT NULL,
  `ds_chave_turma` varchar(5) NOT NULL,
  `USUARIO_cd_usuario` int(11) NOT NULL,
  `TURMA_cd_turma` int(11) NOT NULL,
  `dt_criacao_chave_turma` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Extraindo dados da tabela `chave_turma`
--

INSERT INTO `chave_turma` (`cd_chave_turma`, `ds_chave_turma`, `USUARIO_cd_usuario`, `TURMA_cd_turma`, `dt_criacao_chave_turma`) VALUES
(2, '7QYS5', 1, 1, '2016-11-30 20:21:58'),
(6, 'eZfq8', 11, 2, '2016-12-04 18:10:55'),
(7, 'pxv9t', 11, 3, '2016-12-27 23:14:34');

-- --------------------------------------------------------

--
-- Estrutura da tabela `chave_turma_solicitacao_cad`
--

CREATE TABLE IF NOT EXISTS `chave_turma_solicitacao_cad` (
`cd_chave_turma_solicitacao_cad` int(11) NOT NULL,
  `DISCIPLINA_cd_disciplina` int(11) NOT NULL,
  `CHAVE_TURMA_solicitacao_cad_cd_chave_turma` int(11) NOT NULL,
  `chave_turma_solicitacao_cad_ds_chave_turma` varchar(5) NOT NULL,
  `USUARIO_cd_usuario` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=72 ;

--
-- Extraindo dados da tabela `chave_turma_solicitacao_cad`
--

INSERT INTO `chave_turma_solicitacao_cad` (`cd_chave_turma_solicitacao_cad`, `DISCIPLINA_cd_disciplina`, `CHAVE_TURMA_solicitacao_cad_cd_chave_turma`, `chave_turma_solicitacao_cad_ds_chave_turma`, `USUARIO_cd_usuario`) VALUES
(52, 1, 2, '7QYS5', 1),
(53, 2, 2, '7QYS5', 1),
(54, 3, 2, '7QYS5', 1),
(55, 6, 2, '7QYS5', 1),
(56, 7, 2, '7QYS5', 1),
(58, 2, 2, '7QYS5', 4),
(59, 3, 2, '7QYS5', 4),
(60, 6, 2, '7QYS5', 4),
(61, 7, 2, '7QYS5', 4),
(63, 2, 2, '7QYS5', 3),
(64, 3, 2, '7QYS5', 3),
(65, 6, 2, '7QYS5', 3),
(66, 7, 2, '7QYS5', 3),
(68, 2, 6, 'eZfq8', 5),
(69, 3, 6, 'eZfq8', 5),
(70, 6, 6, 'eZfq8', 5),
(71, 7, 6, 'eZfq8', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('fb7752451b2ae11223e491ad46f09ef31029bcd0', '::1', 1480893846, 0x6e6f6d657c733a31393a22536865696c612042616e6569726f204865636b223b69647c733a313a2235223b7374617475737c733a313a2233223b666f746f7c733a33363a2264343837376533363334383234353534373065656463666534353663636564612e706e67223b7469706f5f7573756172696f7c733a353a22416c756e6f223b6c6f6761646f7c623a313b5f5f63695f6c6173745f726567656e65726174657c693a313438303839333637353b),
('0d123a63e657c7b128af9abf4e357fc2ce3421fd', '::1', 1480893969, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839333734383b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('78caff31212e3030093593042c4d46981e7063be', '::1', 1480894312, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839343037323b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('5f06dd4005b86f7979f969b4099b5eb0ab8ade04', '::1', 1480894164, 0x6e6f6d657c4e3b69647c733a313a2235223b7374617475737c733a313a2233223b666f746f7c733a33363a2264343837376533363334383234353534373065656463666534353663636564612e706e67223b7469706f5f7573756172696f7c733a353a22416c756e6f223b6c6f6761646f7c623a303b5f5f63695f6c6173745f726567656e65726174657c693a313438303839343135343b),
('818c2c1e9757db462bffbe723e1a2e11195f437b', '::1', 1480895072, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839343638343b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('e68ed96513659b8da7e4b5511221369e2d192b6b', '::1', 1480895217, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839353039343b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('070951345d926a32fa92ae619603df67282bc988', '::1', 1480895757, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839353533353b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('fc4361bb3198fb3bb1ba347c614b3fcb4ea288ef', '::1', 1480896261, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839353939313b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('0da6f92ea2cbf80fe62948a92b830850ca786589', '::1', 1480896774, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839363538363b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('0fe40e5a9065539dd31c0c8ea774d8146f4b7b39', '::1', 1480897241, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839363936393b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('01e65d29bc84b127fe8726d806ef29fa739de25e', '::1', 1480897351, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839373238383b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('95f044ead6c42cd5e5153338f9653c9ade50f97e', '::1', 1480897995, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839373738373b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('60765e316221b6a5a94a26776028e0219fe43bcc', '::1', 1480898456, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839383135393b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('e001199fea563101829c65aefa8457c735337816', '::1', 1480898728, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839383531343b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('4f6bbebe5ae2cc9cacc2a36d1efb5fb6ec418191', '::1', 1480899404, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839393133323b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('6a7239a10f95a6b0253123fe4b1944760d27e84e', '::1', 1480899805, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839393531313b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('859f7ba7c62fb9086d11233a28df2db4c941b8ae', '::1', 1480901181, 0x6e6f6d657c733a32353a22526f647269676f204d616369656c2064652046726569746173223b69647c733a313a2231223b7374617475737c733a313a2233223b666f746f7c733a33363a2263393463663439323836313563653136346432616665343930306333393732652e706e67223b7469706f5f7573756172696f7c733a353a22416c756e6f223b6c6f6761646f7c623a313b5f5f63695f6c6173745f726567656e65726174657c693a313438303839393637383b),
('4e7d4f04b5f8a063bacbd2bc265d197e94392a8b', '::1', 1480900062, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303839393833333b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('babc694c9de990b62ca87fc9203aa9df8f670ccc', '::1', 1480900894, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303930303635313b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('1e769427b9873c9888a43443eb004bbc0ebd9bc6', '::1', 1480901199, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303930303935323b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('a49c3b5e5ef70e29f0a5dae8daf31246ae31ac85', '::1', 1480901197, 0x6e6f6d657c733a32353a224a6fc3a36f20446f6d696e676f732064652046726569746173223b69647c733a313a2233223b7374617475737c733a313a2233223b666f746f7c733a33363a2262653039393363383862663366303035663735306434653436313161663261642e706e67223b7469706f5f7573756172696f7c733a353a22416c756e6f223b6c6f6761646f7c623a313b5f5f63695f6c6173745f726567656e65726174657c693a313438303930313138333b),
('2ee378ee0c75f275a3b4ad901597f1b28f19a05e', '::1', 1480901283, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303930313238303b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('4275e52dc53a5a2eebe25263284c665222e94fde', '::1', 1480950459, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935303137333b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('42e435a15bf7e41824f42c2070f2ceaa33c173d2', '::1', 1480950761, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935303438363b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('3d3567855fb68e0f1d7113f93b80d37166581264', '::1', 1480950556, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935303531323b6e6f6d657c4e3b69647c733a313a2235223b7374617475737c733a313a2233223b666f746f7c733a33363a2264343837376533363334383234353534373065656463666534353663636564612e706e67223b7469706f5f7573756172696f7c733a353a22416c756e6f223b6c6f6761646f7c623a303b),
('976a7c0616f60d6a0cc7375f1800c68a8bcbe647', '::1', 1480951007, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935303832303b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('991c5a20436b3405416a221ae0c2cdf7bab2fe01', '::1', 1480955212, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935313437303b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('6469314fdd469b13dc1e6a9a27fe9fed17f2245f', '::1', 1480955431, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935353231333b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('77077be7caa3fb66a5f216f1be03f896458a769b', '::1', 1480955555, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935353533343b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('10c70a3f265838abac6149d38455b6e704315184', '::1', 1480956188, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935363138333b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('afbca168c609c296aa52ab5b3e57f1412c20e4a1', '::1', 1480956540, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935363533393b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('5bd9ffed901973024f3f1a7ba82e07181561a877', '::1', 1480956668, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935363534303b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('78c3da431f53dd09462a921a23cfc635bd61d78b', '::1', 1480957039, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438303935373033353b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('427d226b640063f52c40e905f2132969bd215deb', '::1', 1481027742, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438313032373532313b6e6f6d657c733a31333a2241646d696e6973747261646f72223b69647c733a313a2236223b7374617475737c733a313a2231223b666f746f7c733a33363a2238633639653235633663636136343330656531363734623339626436343633662e706e67223b7469706f5f7573756172696f7c733a353a2241646d696e223b6c6f6761646f7c623a313b),
('d31b56edddf7278a496081a5654561ede86ff0a2', '::1', 1481127421, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438313132373339363b6e6f6d657c733a31333a2241646d696e6973747261646f72223b69647c733a313a2236223b7374617475737c733a313a2231223b666f746f7c733a33363a2238633639653235633663636136343330656531363734623339626436343633662e706e67223b7469706f5f7573756172696f7c733a353a2241646d696e223b6c6f6761646f7c623a313b),
('4f4e3f4429b9e192f549baf9e0c3858bb7d4089e', '::1', 1481134501, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438313133343436323b6e6f6d657c733a31333a2241646d696e6973747261646f72223b69647c733a313a2236223b7374617475737c733a313a2231223b666f746f7c733a33363a2238633639653235633663636136343330656531363734623339626436343633662e706e67223b7469706f5f7573756172696f7c733a353a2241646d696e223b6c6f6761646f7c623a313b),
('f88431bab98ff89f314ea9fc5961b6c63179dd7f', '::1', 1481135256, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438313133353035323b6e6f6d657c733a31333a2241646d696e6973747261646f72223b69647c733a313a2236223b7374617475737c733a313a2231223b666f746f7c733a33363a2238633639653235633663636136343330656531363734623339626436343633662e706e67223b7469706f5f7573756172696f7c733a353a2241646d696e223b6c6f6761646f7c623a313b),
('e5dfc50e25c437bf7dec35163f477aeebee6f0fa', '::1', 1481137059, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438313133373035393b6e6f6d657c733a31333a2241646d696e6973747261646f72223b69647c733a313a2236223b7374617475737c733a313a2231223b666f746f7c733a33363a2238633639653235633663636136343330656531363734623339626436343633662e706e67223b7469706f5f7573756172696f7c733a353a2241646d696e223b6c6f6761646f7c623a313b),
('038dad401c8a8cf3b55df97c720ef08308c3e8cf', '::1', 1481208203, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438313230383035363b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('9220edf4b47280c1ed5e3adc8e68161c1943d691', '::1', 1481500270, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438313530303030323b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('880c2b9373a8da20f47b339988def1beee1eaac1', '::1', 1482887724, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438323838373536383b6e6f6d657c733a32353a22526f647269676f204d616369656c2064652046726569746173223b69647c733a313a2231223b7374617475737c733a313a2233223b666f746f7c733a33363a2263393463663439323836313563653136346432616665343930306333393732652e706e67223b7469706f5f7573756172696f7c733a353a22416c756e6f223b6c6f6761646f7c623a313b),
('824e6f337907211800f8a135025414bc7e5403fe', '::1', 1482887874, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438323838373538373b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b6c6f6761646f7c623a313b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b),
('545cd71afcf64635c10a00dc811f8f38e4607fc2', '::1', 1486947826, 0x5f5f63695f6c6173745f726567656e65726174657c693a313438363934373832363b),
('de14d0654401ac259a701ab78552ad64ca207881', '::1', 1496975489, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439363937353237373b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b),
('43d5d7dfca55612f898947fc724bd7d3e43a1baa', '::1', 1496975758, 0x5f5f63695f6c6173745f726567656e65726174657c693a313439363937353735383b6e6f6d657c733a32383a224361726c6f732056696ec3ad6369757320526173636820416c766573223b69647c733a323a223131223b7374617475737c733a313a2232223b666f746f7c4e3b7469706f5f7573756172696f7c733a393a2250726f666573736f72223b6c6f6761646f7c623a313b);

-- --------------------------------------------------------

--
-- Estrutura da tabela `curso`
--

CREATE TABLE IF NOT EXISTS `curso` (
`cd_curso` int(11) NOT NULL,
  `sg_curso` varchar(10) DEFAULT NULL,
  `nm_curso` varchar(45) NOT NULL,
  `dt_curso_inicio` date NOT NULL,
  `nr_semestre_total` int(11) NOT NULL,
  `ds_caminho_curso` varchar(245) NOT NULL,
  `fl_excluido` int(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `curso`
--

INSERT INTO `curso` (`cd_curso`, `sg_curso`, `nm_curso`, `dt_curso_inicio`, `nr_semestre_total`, `ds_caminho_curso`, `fl_excluido`) VALUES
(1, 'ADS', 'Análise e Desenvolvimento de Sistemas', '2016-05-01', 6, 'cursos_materiais/analise_e_desenvolvimento_de_sistemas_2016', 0),
(2, 'PSICO', 'Psicologia', '2015-04-06', 10, '', 0),
(3, 'REDES', 'Redes de Computadores da Faculdade Senac Pelo', '2014-02-18', 6, 'cursos_materiais/redes_de_computadores_da_faculdade_senac_pelotas', 0),
(4, 'PG', 'Processos Gerenciais', '2012-06-12', 5, 'cursos_materiais/processos_gerenciais', 0),
(5, 'DRT', 'Direito', '2016-05-23', 10, '', 1),
(6, 'CTB', 'Contabilidade', '2016-05-04', 6, '', 0),
(7, 'MD', 'Medicina', '2016-06-09', 10, '', 1),
(10, 'MKT', 'Marketing', '2016-07-08', 6, '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina`
--

CREATE TABLE IF NOT EXISTS `disciplina` (
`cd_disciplina` int(11) NOT NULL,
  `SEMESTRE_cd_semestre` int(11) NOT NULL,
  `ds_disciplina` varchar(180) NOT NULL,
  `nm_disciplina` varchar(45) DEFAULT NULL,
  `fl_excluido` int(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `disciplina`
--

INSERT INTO `disciplina` (`cd_disciplina`, `SEMESTRE_cd_semestre`, `ds_disciplina`, `nm_disciplina`, `fl_excluido`) VALUES
(1, 1, 'Entender o funcionamento de Sistemas Distribuídos (SD), de uma maneira abrangente, através da apresentação dos princípios e práticas que envolvem aplicações desta natureza.\r\nSincro', 'Sistemas Distribuídos', 0),
(2, 1, 'Aplicar conhecimentos gerenciais na administração da\r\ninformática', 'Gerencias de Projetos', 0),
(3, 1, 'Estudo dos conceitos fundamentais de redes de computadores, abrangendo os protocolos e serviços da arquitetura TCP/IP para o suporte as aplicações em rede.', 'Redes de Computadores', 0),
(4, 5, 'Esta disciplina tem caráter teórico prático, cuja\r\ncarga horária é distribuída de maneira eqüitativa\r\nentre teoria e prática, sendo que a prática será feita\r\nno laboratório de inf.', 'Psicologia Experimental', 0),
(5, 4, 'Apresentar e discutir a importância de se conhecer os psicofármacos; Compreender a importância do trabalho integrado entre psicólogo e psiquiatra.', 'Psicofarmacologia', 0),
(6, 1, 'Aplicar de forma sistêmica os princípios da gerência da\r\nqualidade no processo de desenvolvimento de software.', 'Qualidade de Software', 0),
(7, 1, 'Orientação na elaboração do projeto de trabalho de conclusão\r\nde curso, realizada em conjunto com o professor orientador,\r\ndesde o levantamento e fichamento bibliográfico para\r\nfun', 'Projeto de Conclusão de Curso', 0),
(8, 3, 'Testando...', 'Banco de Dados 2', 0),
(10, 14, 'Testando.....', 'Teste', 0),
(11, 20, 'Ementa da Unidade Curricular de Algoritmos e Programação I Conceitos Básicos Exemplos de Programas Exercícios.', 'Algoritmos e Programação', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina_avaliacao`
--

CREATE TABLE IF NOT EXISTS `disciplina_avaliacao` (
  `DISCIPLINA_cd_disciplina` int(11) NOT NULL,
  `AVALIACAO_cd_avaliacao` int(11) NOT NULL,
  `TURMA_cd_turma` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina_documento`
--

CREATE TABLE IF NOT EXISTS `disciplina_documento` (
  `DISCIPLINA_cd_disciplina` int(11) NOT NULL,
  `DOCUMENTO_cd_documento` int(11) NOT NULL,
  `TURMA_cd_turma` int(11) NOT NULL,
  `fl_visibilidade` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplina_usuario`
--

CREATE TABLE IF NOT EXISTS `disciplina_usuario` (
  `DISCIPLINA_cd_disciplina` int(11) NOT NULL,
  `USUARIO_cd_usuario` int(11) NOT NULL,
  `TURMA_cd_turma` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `disciplina_usuario`
--

INSERT INTO `disciplina_usuario` (`DISCIPLINA_cd_disciplina`, `USUARIO_cd_usuario`, `TURMA_cd_turma`) VALUES
(2, 1, 2),
(8, 1, 3),
(1, 3, 1),
(1, 4, 1),
(1, 5, 2),
(2, 5, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `documento`
--

CREATE TABLE IF NOT EXISTS `documento` (
`cd_documento` int(11) NOT NULL,
  `AULA_cd_aula` int(11) NOT NULL,
  `ds_arquivo` varchar(240) NOT NULL,
  `ds_caminho_documento` varchar(245) NOT NULL,
  `ds_nomenclatura` varchar(45) NOT NULL,
  `ds_descricao` varchar(45) NOT NULL,
  `ds_palavras_chave` varchar(245) NOT NULL,
  `ds_tipo_documento` varchar(10) NOT NULL,
  `fl_excluido` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Extraindo dados da tabela `documento`
--

INSERT INTO `documento` (`cd_documento`, `AULA_cd_aula`, `ds_arquivo`, `ds_caminho_documento`, `ds_nomenclatura`, `ds_descricao`, `ds_palavras_chave`, `ds_tipo_documento`, `fl_excluido`) VALUES
(21, 1, 'Aula_01.ppt', 'material/Aula_01.ppt', 'Sistemas Distribuídos', 'Introdução de Sistemas Distribuídos', 'sistemas distribuidos, Convergencia digital, sistema distribuido simples, desempenho de sistemas distribuidos, comvergencia digital', '.ppt', 0),
(26, 2, 'Aula_02.ppt', 'material/Aula_02.ppt', 'Sistemas DIstribuídos', 'Fundamentos dos Sistemas Distribuídos', 'fundamentos dos sistemas distribuidos, sistemas distribuidos', '.ppt', 0),
(27, 19, 'sockets1.pdf', 'material/sockets1.pdf', 'Sockets', 'Redes de Computadores', 'Sockets, redes de computadores', '.pdf', 0),
(28, 5, 'Aula_03_04.ppt', 'material/Aula_03_04.ppt', 'Sistemas Disitribuídos', 'Sistemas Distribuídos que aborda comunicação', 'sd, comunicacao', '.ppt', 0),
(29, 3, 'Aula_03_042.ppt', 'material/Aula_03_042.ppt', 'Sistemas Disitribuídos', 'Sistemas Distribuídos aborda comunicação', 'sistemas distribuidos, comunicacao, [sd', '.ppt', 0),
(30, 1, 'Aula_06_-_Conteúdo_-_Comunicação.doc', 'material/Aula_06_-_Conteúdo_-_Comunicação.doc', 'com', 'com', 'com', '.doc', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `documento_turma`
--

CREATE TABLE IF NOT EXISTS `documento_turma` (
  `DOCUMENTO_cd_documento` int(11) NOT NULL,
  `TURMA_cd_turma` int(11) NOT NULL,
  `DISCIPLINA_cd_disciplina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `instituicao`
--

CREATE TABLE IF NOT EXISTS `instituicao` (
`cd_instituicao` int(11) NOT NULL,
  `nr_instituicao` int(11) NOT NULL,
  `nm_instituicao` varchar(45) DEFAULT NULL,
  `sg_instituicao` varchar(45) NOT NULL,
  `ds_email` varchar(45) NOT NULL,
  `ds_endereco` varchar(45) DEFAULT NULL,
  `nr_endereco` int(11) DEFAULT NULL,
  `ds_complemento` varchar(45) DEFAULT NULL,
  `ds_cidade` varchar(45) DEFAULT NULL,
  `nr_cep` varchar(9) DEFAULT NULL,
  `ds_bairro` varchar(45) DEFAULT NULL,
  `sg_estado` varchar(2) DEFAULT NULL,
  `fl_excluido` int(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `instituicao`
--

INSERT INTO `instituicao` (`cd_instituicao`, `nr_instituicao`, `nm_instituicao`, `sg_instituicao`, `ds_email`, `ds_endereco`, `nr_endereco`, `ds_complemento`, `ds_cidade`, `nr_cep`, `ds_bairro`, `sg_estado`, `fl_excluido`) VALUES
(1, 78, 'Faculdade de Tecnologia Senac Pelotas', 'FATEC', 'fatecpelotas@senacrs.com.br', 'Rua Gonçalves Chaves', 602, '', 'Pelotas', '96015560', 'Centro', 'RS', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `USUARIO_cd_usuario` int(11) NOT NULL,
  `ACAO_cd_acao` int(11) NOT NULL,
  `dt_acao` date NOT NULL,
  `hr_acao` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `log`
--

INSERT INTO `log` (`USUARIO_cd_usuario`, `ACAO_cd_acao`, `dt_acao`, `hr_acao`) VALUES
(12, 2, '2016-11-10', '18:05:40'),
(12, 1, '2016-11-10', '18:05:44'),
(12, 2, '2016-11-10', '19:35:25'),
(6, 1, '2016-11-10', '19:35:29'),
(6, 1, '2016-11-11', '21:41:01'),
(12, 1, '2016-11-11', '22:17:14'),
(12, 1, '2016-11-12', '11:19:19'),
(6, 1, '2016-11-12', '11:19:28'),
(6, 1, '2016-11-12', '11:56:34'),
(12, 1, '2016-11-12', '17:55:15'),
(12, 2, '2016-11-12', '19:04:20'),
(6, 1, '2016-11-12', '19:04:25'),
(6, 2, '2016-11-12', '19:09:59'),
(12, 1, '2016-11-12', '19:10:07'),
(12, 1, '2016-11-13', '16:51:15'),
(6, 1, '2016-11-13', '16:51:24'),
(12, 1, '2016-11-16', '15:15:15'),
(1, 1, '2016-11-16', '19:13:34'),
(1, 2, '2016-11-16', '19:15:04'),
(6, 1, '2016-11-16', '19:15:09'),
(1, 1, '2016-11-16', '22:39:27'),
(1, 2, '2016-11-16', '22:42:52'),
(6, 1, '2016-11-16', '22:42:59'),
(6, 2, '2016-11-16', '22:43:18'),
(12, 1, '2016-11-16', '22:43:22'),
(12, 2, '2016-11-16', '22:43:31'),
(11, 1, '2016-11-16', '22:44:03'),
(11, 2, '2016-11-16', '22:46:31'),
(11, 1, '2016-11-18', '08:26:21'),
(11, 2, '2016-11-18', '08:26:34'),
(11, 1, '2016-11-18', '08:37:19'),
(1, 1, '2016-11-18', '08:38:56'),
(1, 2, '2016-11-18', '08:40:58'),
(1, 1, '2016-11-18', '08:46:15'),
(11, 2, '2016-11-18', '08:48:57'),
(1, 2, '2016-11-18', '08:49:00'),
(11, 2, '2016-11-18', '08:49:18'),
(11, 1, '2016-11-18', '09:15:07'),
(1, 1, '2016-11-18', '09:18:55'),
(1, 2, '2016-11-18', '09:20:33'),
(6, 1, '2016-11-18', '09:20:37'),
(11, 1, '2016-11-22', '16:47:46'),
(1, 1, '2016-11-23', '19:34:48'),
(1, 2, '2016-11-23', '20:11:00'),
(11, 1, '2016-11-23', '20:11:07'),
(11, 1, '2016-11-24', '13:51:08'),
(11, 1, '2016-11-24', '13:53:20'),
(11, 2, '2016-11-24', '15:04:12'),
(6, 1, '2016-11-24', '15:04:18'),
(6, 2, '2016-11-24', '15:33:18'),
(6, 1, '2016-11-24', '15:33:21'),
(6, 2, '2016-11-24', '15:34:22'),
(11, 1, '2016-11-24', '15:34:35'),
(11, 2, '2016-11-24', '15:54:32'),
(6, 1, '2016-11-24', '15:54:37'),
(6, 2, '2016-11-24', '16:31:17'),
(11, 1, '2016-11-24', '16:31:24'),
(6, 1, '2016-11-25', '20:56:01'),
(6, 2, '2016-11-25', '20:56:17'),
(6, 1, '2016-11-25', '20:56:27'),
(6, 2, '2016-11-25', '21:00:24'),
(6, 1, '2016-11-26', '17:43:33'),
(6, 2, '2016-11-26', '17:47:57'),
(1, 1, '2016-11-26', '17:48:03'),
(1, 1, '2016-11-26', '21:57:49'),
(1, 2, '2016-11-27', '18:00:59'),
(6, 1, '2016-11-27', '18:01:04'),
(6, 1, '2016-11-27', '18:01:07'),
(6, 1, '2016-11-27', '18:01:12'),
(6, 1, '2016-11-27', '18:02:07'),
(6, 2, '2016-11-27', '18:11:48'),
(1, 1, '2016-11-27', '18:11:51'),
(1, 2, '2016-11-27', '18:16:35'),
(6, 1, '2016-11-27', '18:16:39'),
(6, 2, '2016-11-27', '18:16:49'),
(1, 1, '2016-11-27', '18:16:56'),
(1, 2, '2016-11-27', '18:17:12'),
(6, 1, '2016-11-27', '18:17:15'),
(6, 2, '2016-11-27', '18:17:44'),
(6, 1, '2016-11-27', '18:17:47'),
(6, 2, '2016-11-27', '18:17:55'),
(6, 1, '2016-11-27', '18:17:58'),
(6, 2, '2016-11-27', '18:18:15'),
(6, 1, '2016-11-27', '18:18:44'),
(6, 2, '2016-11-27', '18:18:47'),
(6, 1, '2016-11-27', '18:19:15'),
(6, 2, '2016-11-27', '18:19:25'),
(6, 1, '2016-11-27', '18:19:27'),
(6, 2, '2016-11-27', '18:20:11'),
(6, 1, '2016-11-27', '18:20:41'),
(6, 2, '2016-11-27', '18:21:51'),
(1, 1, '2016-11-27', '18:21:58'),
(1, 2, '2016-11-27', '18:22:30'),
(6, 1, '2016-11-27', '18:22:33'),
(6, 2, '2016-11-27', '18:28:03'),
(11, 1, '2016-11-27', '18:28:07'),
(11, 2, '2016-11-27', '20:36:37'),
(6, 1, '2016-11-27', '22:36:36'),
(6, 2, '2016-11-27', '22:57:49'),
(6, 1, '2016-11-27', '22:57:56'),
(6, 1, '2016-11-30', '19:24:15'),
(6, 1, '2016-12-01', '13:32:39'),
(6, 1, '2016-12-01', '19:48:52'),
(6, 1, '2016-12-02', '08:39:49'),
(1, 2, '2016-12-02', '09:37:08'),
(1, 2, '2016-12-02', '09:41:45'),
(1, 1, '2016-12-02', '09:41:57'),
(1, 2, '2016-12-02', '09:42:49'),
(1, 1, '2016-12-02', '09:43:02'),
(1, 2, '2016-12-02', '09:45:14'),
(1, 1, '2016-12-02', '09:46:27'),
(1, 2, '2016-12-02', '10:03:54'),
(1, 2, '2016-12-02', '13:22:43'),
(6, 1, '2016-12-02', '19:40:58'),
(6, 2, '2016-12-02', '19:41:02'),
(11, 1, '2016-12-02', '19:41:13'),
(11, 1, '2016-12-03', '21:38:31'),
(4, 2, '2016-12-03', '21:46:48'),
(11, 1, '2016-12-03', '21:47:01'),
(11, 1, '2016-12-04', '14:57:29'),
(1, 2, '2016-12-04', '17:40:04'),
(11, 1, '2016-12-04', '17:40:17'),
(4, 2, '2016-12-04', '18:31:11'),
(11, 1, '2016-12-04', '21:15:57'),
(5, 1, '2016-12-04', '21:21:13'),
(5, 2, '2016-12-04', '21:21:19'),
(5, 2, '2016-12-04', '21:29:23'),
(11, 1, '2016-12-05', '13:03:02'),
(5, 2, '2016-12-05', '13:09:15'),
(11, 2, '2016-12-05', '13:24:30'),
(11, 1, '2016-12-05', '14:26:52'),
(11, 1, '2016-12-06', '10:32:11'),
(11, 2, '2016-12-06', '10:35:33'),
(6, 1, '2016-12-06', '10:35:36'),
(6, 1, '2016-12-07', '14:16:43'),
(11, 1, '2016-12-08', '12:41:06'),
(11, 1, '2016-12-11', '21:46:47'),
(11, 1, '2016-12-27', '23:13:30'),
(1, 1, '2016-12-27', '23:13:35'),
(1, 2, '2016-12-27', '23:14:55'),
(1, 1, '2017-06-08', '23:28:04'),
(1, 2, '2017-06-08', '23:28:35'),
(11, 1, '2017-06-08', '23:28:48'),
(11, 2, '2017-06-08', '23:31:02'),
(11, 1, '2017-06-08', '23:31:18');

-- --------------------------------------------------------

--
-- Estrutura da tabela `log_curso`
--

CREATE TABLE IF NOT EXISTS `log_curso` (
  `USUARIO_cd_usuario` int(11) NOT NULL,
  `ACAO_cd_acao` int(11) NOT NULL,
  `CURSO_cd_curso` int(11) NOT NULL,
  `dt_acao` date NOT NULL,
  `hr_acao` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `log_curso`
--

INSERT INTO `log_curso` (`USUARIO_cd_usuario`, `ACAO_cd_acao`, `CURSO_cd_curso`, `dt_acao`, `hr_acao`) VALUES
(6, 5, 4, '2016-11-24', '15:20:51'),
(11, 5, 3, '2016-12-11', '21:47:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagem`
--

CREATE TABLE IF NOT EXISTS `mensagem` (
`cd_mensagem` int(11) NOT NULL,
  `cd_envia` int(11) NOT NULL,
  `cd_recebe` int(11) NOT NULL,
  `ds_mensagem` text NOT NULL,
  `dt_data_cad` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fl_excluido` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `mensagem`
--

INSERT INTO `mensagem` (`cd_mensagem`, `cd_envia`, `cd_recebe`, `ds_mensagem`, `dt_data_cad`, `fl_excluido`) VALUES
(1, 6, 0, 'ola', '2016-12-01 19:49:09', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `notificacao`
--

CREATE TABLE IF NOT EXISTS `notificacao` (
`cd_notificacao_curso` int(11) NOT NULL,
  `USUARIO_cd_usuario` int(11) DEFAULT NULL,
  `CURSO_cd_curso` int(11) DEFAULT NULL,
  `TURMA_cd_turma` int(11) DEFAULT NULL,
  `DISCIPLINA_cd_disciplina` int(11) DEFAULT NULL,
  `ds_mensagem` text,
  `fl_excluido` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `professor_disciplina`
--

CREATE TABLE IF NOT EXISTS `professor_disciplina` (
  `USUARIO_cd_usuario` int(11) NOT NULL,
  `DISCIPLINA_cd_disciplina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `professor_disciplina`
--

INSERT INTO `professor_disciplina` (`USUARIO_cd_usuario`, `DISCIPLINA_cd_disciplina`) VALUES
(11, 1),
(12, 1),
(11, 2),
(12, 2),
(11, 3),
(11, 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `semestre`
--

CREATE TABLE IF NOT EXISTS `semestre` (
`cd_semestre` int(11) NOT NULL,
  `nr_semestre` int(2) NOT NULL,
  `fl_excluido` int(1) DEFAULT '0',
  `CURSO_cd_curso` int(11) NOT NULL,
  `ANO_cd_ano` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Extraindo dados da tabela `semestre`
--

INSERT INTO `semestre` (`cd_semestre`, `nr_semestre`, `fl_excluido`, `CURSO_cd_curso`, `ANO_cd_ano`) VALUES
(1, 5, 0, 1, 1),
(2, 1, 1, 1, 1),
(3, 3, 0, 1, 1),
(4, 5, 0, 2, 3),
(5, 2, 0, 2, 3),
(6, 1, 0, 2, 3),
(7, 2, 0, 1, 1),
(8, 4, 0, 1, 1),
(9, 1, 0, 6, 4),
(10, 2, 0, 6, 4),
(11, 3, 0, 6, 4),
(12, 2, 1, 1, 2),
(14, 1, 0, 10, 6),
(15, 1, 0, 1, 1),
(20, 1, 0, 1, 2),
(21, 6, 0, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `senhas_proibidas`
--

CREATE TABLE IF NOT EXISTS `senhas_proibidas` (
`cd_senha_proibidas` int(11) NOT NULL,
  `ds_senhas_proibidas` varchar(60) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `senhas_proibidas`
--

INSERT INTO `senhas_proibidas` (`cd_senha_proibidas`, `ds_senhas_proibidas`) VALUES
(1, 'admin'),
(2, 'teste'),
(4, 'abc'),
(5, 'xyz');

-- --------------------------------------------------------

--
-- Estrutura da tabela `telefone`
--

CREATE TABLE IF NOT EXISTS `telefone` (
`cd_telefone` int(11) NOT NULL,
  `INSTITUICAO_cd_instituicao` int(11) NOT NULL,
  `ds_setor` varchar(50) NOT NULL,
  `nr_telefone` varchar(11) NOT NULL,
  `fl_excluido` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `telefone`
--

INSERT INTO `telefone` (`cd_telefone`, `INSTITUICAO_cd_instituicao`, `ds_setor`, `nr_telefone`, `fl_excluido`) VALUES
(1, 1, 'Secretaria', '53982333211', 0),
(2, 1, 'Financeiro', '53981151235', 0),
(4, 1, 'Adminstração', '5399999999', 0),
(5, 1, 'Diretoria', '53981151235', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_avaliacao`
--

CREATE TABLE IF NOT EXISTS `tipo_avaliacao` (
`cd_avaliacao` int(11) NOT NULL,
  `nm_tipo_avaliacao` varchar(45) NOT NULL,
  `fl_excluido` int(1) DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `tipo_avaliacao`
--

INSERT INTO `tipo_avaliacao` (`cd_avaliacao`, `nm_tipo_avaliacao`, `fl_excluido`) VALUES
(1, 'Trabalho', 0),
(2, 'Prova', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_documento`
--

CREATE TABLE IF NOT EXISTS `tipo_documento` (
`cd_tipo` int(11) NOT NULL,
  `ds_tipo` varchar(45) NOT NULL,
  `fl_excluido` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_usuario`
--

CREATE TABLE IF NOT EXISTS `tipo_usuario` (
`cd_tipo_usuario` int(11) NOT NULL,
  `nm_tipo_usuario` varchar(45) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`cd_tipo_usuario`, `nm_tipo_usuario`) VALUES
(1, 'Admin'),
(2, 'Professor'),
(3, 'Aluno');

-- --------------------------------------------------------

--
-- Estrutura da tabela `turma`
--

CREATE TABLE IF NOT EXISTS `turma` (
`cd_turma` int(11) NOT NULL,
  `TURNO_cd_turno` int(11) NOT NULL,
  `CURSO_cd_curso` int(11) NOT NULL,
  `ANO_cd_ano` int(11) NOT NULL,
  `SEMESTRE_cd_semestre` int(11) NOT NULL,
  `fl_excluido` int(1) NOT NULL DEFAULT '0',
  `dt_turma` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `turma`
--

INSERT INTO `turma` (`cd_turma`, `TURNO_cd_turno`, `CURSO_cd_curso`, `ANO_cd_ano`, `SEMESTRE_cd_semestre`, `fl_excluido`, `dt_turma`) VALUES
(1, 3, 1, 1, 1, 0, '2016-07-11'),
(2, 1, 1, 1, 1, 0, '2016-06-20'),
(3, 1, 1, 1, 3, 0, '2016-02-18'),
(4, 3, 2, 3, 4, 0, '2016-06-20'),
(5, 1, 1, 1, 7, 0, '2016-06-24'),
(6, 1, 6, 4, 9, 0, '2016-06-02'),
(7, 1, 2, 3, 6, 0, '2016-06-08'),
(8, 1, 10, 6, 14, 0, '2016-07-11'),
(11, 3, 1, 2, 20, 0, '2015-02-22');

-- --------------------------------------------------------

--
-- Estrutura da tabela `turno`
--

CREATE TABLE IF NOT EXISTS `turno` (
`cd_turno` int(11) NOT NULL,
  `nm_turno` varchar(45) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `turno`
--

INSERT INTO `turno` (`cd_turno`, `nm_turno`) VALUES
(1, 'Manhã'),
(2, 'Tarde'),
(3, 'Noite');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
`cd_usuario` int(11) NOT NULL,
  `ds_email` varchar(45) NOT NULL,
  `ds_senha` varchar(45) NOT NULL,
  `nr_matricula` varchar(45) NOT NULL,
  `dt_data_cad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nm_usuario` varchar(245) DEFAULT NULL,
  `nr_cpf` varchar(11) DEFAULT NULL,
  `dt_data_nasc` date DEFAULT NULL,
  `ds_endereco` varchar(145) DEFAULT NULL,
  `nr_endereco` int(11) DEFAULT NULL,
  `ds_complemento` varchar(45) DEFAULT NULL,
  `ds_bairro` varchar(145) DEFAULT NULL,
  `ds_cidade` varchar(145) DEFAULT NULL,
  `sg_estado` varchar(2) DEFAULT NULL,
  `nr_cep` varchar(8) DEFAULT NULL,
  `nr_telefone` varchar(11) DEFAULT NULL,
  `nr_celular` varchar(11) DEFAULT NULL,
  `ds_foto` varchar(250) DEFAULT NULL,
  `st_ativo` int(11) NOT NULL,
  `fl_excluido` int(1) DEFAULT '0',
  `TIPO_USUARIO_cd_tipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`cd_usuario`, `ds_email`, `ds_senha`, `nr_matricula`, `dt_data_cad`, `nm_usuario`, `nr_cpf`, `dt_data_nasc`, `ds_endereco`, `nr_endereco`, `ds_complemento`, `ds_bairro`, `ds_cidade`, `sg_estado`, `nr_cep`, `nr_telefone`, `nr_celular`, `ds_foto`, `st_ativo`, `fl_excluido`, `TIPO_USUARIO_cd_tipo_usuario`) VALUES
(1, 'macielfreitas@hotmail.com.br', 'b74da2cb7c41cde5ba8c10c2ccea11c64eec28c6', '781410145', '2016-05-22 04:31:11', 'Rodrigo Maciel de Freitas', NULL, NULL, NULL, NULL, NULL, NULL, 'Pelotas', NULL, NULL, NULL, NULL, 'c94cf4928615ce164d2afe4900c3972e.png', 1, 0, 3),
(3, 'joaodomingos@hotmail.com.br', 'b74da2cb7c41cde5ba8c10c2ccea11c64eec28c6', '781010144', '2016-06-08 01:51:07', 'João Domingos de Freitas', '98192819281', '1954-11-07', ' Rua Argentina', 682, '', 'Palmeiras 1', 'Dracena', 'SP', '1790000', '18981212468', '1838214674', 'be0993c88bf3f005f750d4e4611af2ad.png', 1, 0, 3),
(4, 'sheilabheck@hotmail.com.br', 'b74da2cb7c41cde5ba8c10c2ccea11c64eec28c6', '781410000', '2016-06-21 07:27:20', 'Maria Aparecida', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '948d7df0da81193d40c1f95edcde130b.png', 1, 0, 3),
(5, 'sheilabheck@hotmail.com', 'b74da2cb7c41cde5ba8c10c2ccea11c64eec28c6', '781410144', '2016-06-29 21:16:49', 'Sheila Baneiro Heck', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd4877e363482455470eedcfe456cceda.png', 1, 0, 3),
(6, 'admin@admin.com', 'b74da2cb7c41cde5ba8c10c2ccea11c64eec28c6', '7800000000', '2016-07-04 20:34:00', 'Administrador', '33886469883', '0000-00-00', 'Rua Barão de Santa Tecla', 251, '', 'Centro', 'Pelotas', 'RS', '96010140', '', '', '8c69e25c6cca6430ee1674b39bd6463f.png', 1, 0, 1),
(11, 'carlos@gmail.com', 'b74da2cb7c41cde5ba8c10c2ccea11c64eec28c6', '781516277', '2016-07-12 22:07:39', 'Carlos Vinícius Rasch Alves', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 2),
(12, 'gladimir@gmail.com', 'b74da2cb7c41cde5ba8c10c2ccea11c64eec28c6', '7866161616', '2016-07-12 22:13:37', 'Gladlimir Catarino', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '609232bd4c37d2a1920d37b78c9b2cad.ico', 1, 0, 2),
(13, 'ma@gmail.com', '68bce21f6d55677148f7ca7cfc270abd4070b9b9', '78', '2016-11-25 22:54:50', 'Rodrigo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acao`
--
ALTER TABLE `acao`
 ADD PRIMARY KEY (`cd_acao`);

--
-- Indexes for table `ano`
--
ALTER TABLE `ano`
 ADD PRIMARY KEY (`cd_ano`), ADD KEY `CURSO_cd_curso` (`CURSO_cd_curso`);

--
-- Indexes for table `aula`
--
ALTER TABLE `aula`
 ADD PRIMARY KEY (`cd_aula`), ADD KEY `DISCIPLINA_cd_disciplina` (`DISCIPLINA_cd_disciplina`);

--
-- Indexes for table `avaliacao`
--
ALTER TABLE `avaliacao`
 ADD PRIMARY KEY (`cd_avaliacao`), ADD KEY `fk_AVALIACAO_DISCIPLINA1_idx` (`DISCIPLINA_cd_disciplina`), ADD KEY `fk_AVALIACAO_TIPO_AVALIACAO1_idx` (`TIPO_AVALIACAO_cd_avaliacao`);

--
-- Indexes for table `avaliacao_turma`
--
ALTER TABLE `avaliacao_turma`
 ADD PRIMARY KEY (`AVALIACAO_cd_avaliacao`,`TURMA_cd_turma`), ADD KEY `fk_avaliacao_has_turma_turma1_idx` (`TURMA_cd_turma`), ADD KEY `fk_avaliacao_has_turma_avaliacao1_idx` (`AVALIACAO_cd_avaliacao`), ADD KEY `DISCIPLINA_cd_disciplina` (`DISCIPLINA_cd_disciplina`);

--
-- Indexes for table `avaliacao_usuario`
--
ALTER TABLE `avaliacao_usuario`
 ADD PRIMARY KEY (`AVALIACAO_cd_avaliacao`,`USUARIO_cd_usuario`), ADD KEY `fk_AVALIACAO_has_USUARIO_USUARIO1_idx` (`USUARIO_cd_usuario`), ADD KEY `fk_AVALIACAO_has_USUARIO_AVALIACAO1_idx` (`AVALIACAO_cd_avaliacao`), ADD KEY `TURMA_cd_turma` (`TURMA_cd_turma`);

--
-- Indexes for table `backup`
--
ALTER TABLE `backup`
 ADD PRIMARY KEY (`cd_backup`);

--
-- Indexes for table `chave_turma`
--
ALTER TABLE `chave_turma`
 ADD PRIMARY KEY (`cd_chave_turma`), ADD KEY `TURMA_cd_turma` (`TURMA_cd_turma`), ADD KEY `USUARIO_cd_usuario` (`USUARIO_cd_usuario`);

--
-- Indexes for table `chave_turma_solicitacao_cad`
--
ALTER TABLE `chave_turma_solicitacao_cad`
 ADD PRIMARY KEY (`cd_chave_turma_solicitacao_cad`), ADD KEY `USUARIO_cd_usuario` (`USUARIO_cd_usuario`), ADD KEY `CHAVE_TURMA_solicitacao_cad_cd_chave_turma` (`CHAVE_TURMA_solicitacao_cad_cd_chave_turma`), ADD KEY `DISCIPLINA_cd_disciplina` (`DISCIPLINA_cd_disciplina`);

--
-- Indexes for table `curso`
--
ALTER TABLE `curso`
 ADD PRIMARY KEY (`cd_curso`);

--
-- Indexes for table `disciplina`
--
ALTER TABLE `disciplina`
 ADD PRIMARY KEY (`cd_disciplina`), ADD UNIQUE KEY `id_disciplina_UNIQUE` (`cd_disciplina`), ADD KEY `fk_DISCIPLINA_SEMESTRE1_idx` (`SEMESTRE_cd_semestre`);

--
-- Indexes for table `disciplina_avaliacao`
--
ALTER TABLE `disciplina_avaliacao`
 ADD PRIMARY KEY (`DISCIPLINA_cd_disciplina`,`AVALIACAO_cd_avaliacao`,`TURMA_cd_turma`), ADD KEY `fk_DISCIPLINA_has_AVALIACAO_AVALIACAO1_idx` (`AVALIACAO_cd_avaliacao`), ADD KEY `fk_DISCIPLINA_has_AVALIACAO_DISCIPLINA1_idx` (`DISCIPLINA_cd_disciplina`), ADD KEY `fk_DISCIPLINA_AVALIACAO_TURMA1_idx` (`TURMA_cd_turma`);

--
-- Indexes for table `disciplina_documento`
--
ALTER TABLE `disciplina_documento`
 ADD PRIMARY KEY (`DISCIPLINA_cd_disciplina`,`DOCUMENTO_cd_documento`,`TURMA_cd_turma`), ADD KEY `fk_DISCIPLINA_has_DOCUMENTO_DOCUMENTO1_idx` (`DOCUMENTO_cd_documento`), ADD KEY `fk_DISCIPLINA_has_DOCUMENTO_DISCIPLINA1_idx` (`DISCIPLINA_cd_disciplina`), ADD KEY `fk_DISCIPLINA_DOCUMENTO_TURMA1_idx` (`TURMA_cd_turma`);

--
-- Indexes for table `disciplina_usuario`
--
ALTER TABLE `disciplina_usuario`
 ADD PRIMARY KEY (`DISCIPLINA_cd_disciplina`,`USUARIO_cd_usuario`,`TURMA_cd_turma`), ADD KEY `fk_disciplinas_has_usuario_usuario1_idx` (`USUARIO_cd_usuario`), ADD KEY `fk_disciplinas_has_usuario_disciplinas1_idx` (`DISCIPLINA_cd_disciplina`), ADD KEY `fk_disciplina_usuario_turma1_idx` (`TURMA_cd_turma`);

--
-- Indexes for table `documento`
--
ALTER TABLE `documento`
 ADD PRIMARY KEY (`cd_documento`), ADD KEY `AULA_cd_aula` (`AULA_cd_aula`);

--
-- Indexes for table `documento_turma`
--
ALTER TABLE `documento_turma`
 ADD PRIMARY KEY (`DOCUMENTO_cd_documento`,`TURMA_cd_turma`,`DISCIPLINA_cd_disciplina`), ADD KEY `TURMA_cd_turma` (`TURMA_cd_turma`), ADD KEY `DISCIPLINA_cd_disciplina` (`DISCIPLINA_cd_disciplina`);

--
-- Indexes for table `instituicao`
--
ALTER TABLE `instituicao`
 ADD PRIMARY KEY (`cd_instituicao`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
 ADD KEY `USUARIO_cd_usuario` (`USUARIO_cd_usuario`,`ACAO_cd_acao`), ADD KEY `ACAO_cd_acao` (`ACAO_cd_acao`);

--
-- Indexes for table `log_curso`
--
ALTER TABLE `log_curso`
 ADD KEY `USUARIO_cd_usuario` (`USUARIO_cd_usuario`,`ACAO_cd_acao`,`CURSO_cd_curso`), ADD KEY `ACAO_cd_acao` (`ACAO_cd_acao`), ADD KEY `CURSO_cd_curso` (`CURSO_cd_curso`);

--
-- Indexes for table `mensagem`
--
ALTER TABLE `mensagem`
 ADD PRIMARY KEY (`cd_mensagem`);

--
-- Indexes for table `notificacao`
--
ALTER TABLE `notificacao`
 ADD PRIMARY KEY (`cd_notificacao_curso`), ADD UNIQUE KEY `id_notificacao_curso_UNIQUE` (`cd_notificacao_curso`), ADD KEY `fk_notificacao_curso_cursos_idx` (`CURSO_cd_curso`), ADD KEY `fk_notificacao_curso_usuario1_idx` (`USUARIO_cd_usuario`), ADD KEY `fk_NOTIFICACAO_TURMA1_idx` (`TURMA_cd_turma`), ADD KEY `fk_NOTIFICACAO_DISCIPLINA1_idx` (`DISCIPLINA_cd_disciplina`);

--
-- Indexes for table `professor_disciplina`
--
ALTER TABLE `professor_disciplina`
 ADD PRIMARY KEY (`USUARIO_cd_usuario`,`DISCIPLINA_cd_disciplina`), ADD KEY `DISCIPLINA_cd_disciplina` (`DISCIPLINA_cd_disciplina`);

--
-- Indexes for table `semestre`
--
ALTER TABLE `semestre`
 ADD PRIMARY KEY (`cd_semestre`), ADD KEY `fk_SEMESTRE_CURSO1_idx` (`CURSO_cd_curso`), ADD KEY `ANO_cd_ano` (`ANO_cd_ano`);

--
-- Indexes for table `senhas_proibidas`
--
ALTER TABLE `senhas_proibidas`
 ADD PRIMARY KEY (`cd_senha_proibidas`);

--
-- Indexes for table `telefone`
--
ALTER TABLE `telefone`
 ADD PRIMARY KEY (`cd_telefone`), ADD KEY `fk_TELEFONE_INSTITUICAO1_idx` (`INSTITUICAO_cd_instituicao`);

--
-- Indexes for table `tipo_avaliacao`
--
ALTER TABLE `tipo_avaliacao`
 ADD PRIMARY KEY (`cd_avaliacao`);

--
-- Indexes for table `tipo_documento`
--
ALTER TABLE `tipo_documento`
 ADD PRIMARY KEY (`cd_tipo`);

--
-- Indexes for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
 ADD PRIMARY KEY (`cd_tipo_usuario`);

--
-- Indexes for table `turma`
--
ALTER TABLE `turma`
 ADD PRIMARY KEY (`cd_turma`), ADD KEY `fk_TURMA_TURNO1_idx` (`TURNO_cd_turno`), ADD KEY `fk_TURMA_CURSO1_idx` (`CURSO_cd_curso`), ADD KEY `SEMESTRE_cd_semestre` (`SEMESTRE_cd_semestre`), ADD KEY `ANO_cd_ano` (`ANO_cd_ano`);

--
-- Indexes for table `turno`
--
ALTER TABLE `turno`
 ADD PRIMARY KEY (`cd_turno`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`cd_usuario`), ADD UNIQUE KEY `id_usuario_UNIQUE` (`cd_usuario`), ADD KEY `fk_USUARIO_TIPO_USUARIO1_idx` (`TIPO_USUARIO_cd_tipo_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acao`
--
ALTER TABLE `acao`
MODIFY `cd_acao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ano`
--
ALTER TABLE `ano`
MODIFY `cd_ano` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `aula`
--
ALTER TABLE `aula`
MODIFY `cd_aula` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `avaliacao`
--
ALTER TABLE `avaliacao`
MODIFY `cd_avaliacao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `backup`
--
ALTER TABLE `backup`
MODIFY `cd_backup` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `chave_turma`
--
ALTER TABLE `chave_turma`
MODIFY `cd_chave_turma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `chave_turma_solicitacao_cad`
--
ALTER TABLE `chave_turma_solicitacao_cad`
MODIFY `cd_chave_turma_solicitacao_cad` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `curso`
--
ALTER TABLE `curso`
MODIFY `cd_curso` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `disciplina`
--
ALTER TABLE `disciplina`
MODIFY `cd_disciplina` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `documento`
--
ALTER TABLE `documento`
MODIFY `cd_documento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `instituicao`
--
ALTER TABLE `instituicao`
MODIFY `cd_instituicao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `mensagem`
--
ALTER TABLE `mensagem`
MODIFY `cd_mensagem` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `notificacao`
--
ALTER TABLE `notificacao`
MODIFY `cd_notificacao_curso` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `semestre`
--
ALTER TABLE `semestre`
MODIFY `cd_semestre` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `senhas_proibidas`
--
ALTER TABLE `senhas_proibidas`
MODIFY `cd_senha_proibidas` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `telefone`
--
ALTER TABLE `telefone`
MODIFY `cd_telefone` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tipo_avaliacao`
--
ALTER TABLE `tipo_avaliacao`
MODIFY `cd_avaliacao` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tipo_documento`
--
ALTER TABLE `tipo_documento`
MODIFY `cd_tipo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
MODIFY `cd_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `turma`
--
ALTER TABLE `turma`
MODIFY `cd_turma` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `turno`
--
ALTER TABLE `turno`
MODIFY `cd_turno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
MODIFY `cd_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `ano`
--
ALTER TABLE `ano`
ADD CONSTRAINT `ano_ibfk_1` FOREIGN KEY (`CURSO_cd_curso`) REFERENCES `curso` (`cd_curso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `aula`
--
ALTER TABLE `aula`
ADD CONSTRAINT `aula_ibfk_1` FOREIGN KEY (`DISCIPLINA_cd_disciplina`) REFERENCES `disciplina` (`cd_disciplina`);

--
-- Limitadores para a tabela `avaliacao`
--
ALTER TABLE `avaliacao`
ADD CONSTRAINT `fk_AVALIACAO_DISCIPLINA1` FOREIGN KEY (`DISCIPLINA_cd_disciplina`) REFERENCES `disciplina` (`cd_disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_AVALIACAO_TIPO_AVALIACAO1` FOREIGN KEY (`TIPO_AVALIACAO_cd_avaliacao`) REFERENCES `tipo_avaliacao` (`cd_avaliacao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `avaliacao_turma`
--
ALTER TABLE `avaliacao_turma`
ADD CONSTRAINT `avaliacao_turma_ibfk_1` FOREIGN KEY (`DISCIPLINA_cd_disciplina`) REFERENCES `disciplina` (`cd_disciplina`),
ADD CONSTRAINT `fk_avaliacao_has_turma_avaliacao1` FOREIGN KEY (`AVALIACAO_cd_avaliacao`) REFERENCES `avaliacao` (`cd_avaliacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_avaliacao_has_turma_turma1` FOREIGN KEY (`TURMA_cd_turma`) REFERENCES `turma` (`cd_turma`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `avaliacao_usuario`
--
ALTER TABLE `avaliacao_usuario`
ADD CONSTRAINT `fk_AVALIACAO_USUARIO_AVALIACAO1` FOREIGN KEY (`AVALIACAO_cd_avaliacao`) REFERENCES `avaliacao` (`cd_avaliacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_AVALIACAO_USUARIO_USUARIO1` FOREIGN KEY (`USUARIO_cd_usuario`) REFERENCES `usuario` (`cd_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `chave_turma`
--
ALTER TABLE `chave_turma`
ADD CONSTRAINT `chave_turma_ibfk_1` FOREIGN KEY (`TURMA_cd_turma`) REFERENCES `turma` (`cd_turma`),
ADD CONSTRAINT `chave_turma_ibfk_2` FOREIGN KEY (`USUARIO_cd_usuario`) REFERENCES `usuario` (`cd_usuario`);

--
-- Limitadores para a tabela `chave_turma_solicitacao_cad`
--
ALTER TABLE `chave_turma_solicitacao_cad`
ADD CONSTRAINT `chave_turma_solicitacao_cad_ibfk_1` FOREIGN KEY (`USUARIO_cd_usuario`) REFERENCES `usuario` (`cd_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `chave_turma_solicitacao_cad_ibfk_2` FOREIGN KEY (`CHAVE_TURMA_solicitacao_cad_cd_chave_turma`) REFERENCES `chave_turma` (`cd_chave_turma`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `chave_turma_solicitacao_cad_ibfk_3` FOREIGN KEY (`DISCIPLINA_cd_disciplina`) REFERENCES `disciplina` (`cd_disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `disciplina`
--
ALTER TABLE `disciplina`
ADD CONSTRAINT `fk_DISCIPLINA_SEMESTRE1` FOREIGN KEY (`SEMESTRE_cd_semestre`) REFERENCES `semestre` (`cd_semestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `disciplina_avaliacao`
--
ALTER TABLE `disciplina_avaliacao`
ADD CONSTRAINT `fk_DISCIPLINA_AVALIACAO_AVALIACAO1` FOREIGN KEY (`AVALIACAO_cd_avaliacao`) REFERENCES `avaliacao` (`cd_avaliacao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_DISCIPLINA_AVALIACAO_DISCIPLINA1` FOREIGN KEY (`DISCIPLINA_cd_disciplina`) REFERENCES `disciplina` (`cd_disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_DISCIPLINA_AVALIACAO_TURMA1` FOREIGN KEY (`TURMA_cd_turma`) REFERENCES `turma` (`cd_turma`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `disciplina_documento`
--
ALTER TABLE `disciplina_documento`
ADD CONSTRAINT `fk_DISCIPLINA_DOCUMENTO_DISCIPLINA1` FOREIGN KEY (`DISCIPLINA_cd_disciplina`) REFERENCES `disciplina` (`cd_disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_DISCIPLINA_DOCUMENTO_DOCUMENTO1` FOREIGN KEY (`DOCUMENTO_cd_documento`) REFERENCES `documento` (`cd_documento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_DISCIPLINA_DOCUMENTO_TURMA1` FOREIGN KEY (`TURMA_cd_turma`) REFERENCES `turma` (`cd_turma`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `disciplina_usuario`
--
ALTER TABLE `disciplina_usuario`
ADD CONSTRAINT `fk_disciplina_usuario_turma1` FOREIGN KEY (`TURMA_cd_turma`) REFERENCES `turma` (`cd_turma`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_disciplinas_usuario_disciplinas1` FOREIGN KEY (`DISCIPLINA_cd_disciplina`) REFERENCES `disciplina` (`cd_disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_disciplinas_usuario_usuario1` FOREIGN KEY (`USUARIO_cd_usuario`) REFERENCES `usuario` (`cd_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `documento`
--
ALTER TABLE `documento`
ADD CONSTRAINT `documento_ibfk_1` FOREIGN KEY (`AULA_cd_aula`) REFERENCES `aula` (`cd_aula`);

--
-- Limitadores para a tabela `documento_turma`
--
ALTER TABLE `documento_turma`
ADD CONSTRAINT `documento_turma_ibfk_1` FOREIGN KEY (`DOCUMENTO_cd_documento`) REFERENCES `documento` (`cd_documento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `documento_turma_ibfk_2` FOREIGN KEY (`TURMA_cd_turma`) REFERENCES `turma` (`cd_turma`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `documento_turma_ibfk_3` FOREIGN KEY (`DISCIPLINA_cd_disciplina`) REFERENCES `disciplina` (`cd_disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `log`
--
ALTER TABLE `log`
ADD CONSTRAINT `log_ibfk_1` FOREIGN KEY (`ACAO_cd_acao`) REFERENCES `acao` (`cd_acao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `log_ibfk_2` FOREIGN KEY (`USUARIO_cd_usuario`) REFERENCES `usuario` (`cd_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `log_curso`
--
ALTER TABLE `log_curso`
ADD CONSTRAINT `log_curso_ibfk_1` FOREIGN KEY (`ACAO_cd_acao`) REFERENCES `acao` (`cd_acao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `log_curso_ibfk_2` FOREIGN KEY (`USUARIO_cd_usuario`) REFERENCES `usuario` (`cd_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `log_curso_ibfk_3` FOREIGN KEY (`CURSO_cd_curso`) REFERENCES `curso` (`cd_curso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `notificacao`
--
ALTER TABLE `notificacao`
ADD CONSTRAINT `fk_NOTIFICACAO_DISCIPLINA1` FOREIGN KEY (`DISCIPLINA_cd_disciplina`) REFERENCES `disciplina` (`cd_disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_NOTIFICACAO_TURMA1` FOREIGN KEY (`TURMA_cd_turma`) REFERENCES `turma` (`cd_turma`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_notificacao_curso_cursos` FOREIGN KEY (`CURSO_cd_curso`) REFERENCES `curso` (`cd_curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_notificacao_curso_usuario1` FOREIGN KEY (`USUARIO_cd_usuario`) REFERENCES `usuario` (`cd_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `professor_disciplina`
--
ALTER TABLE `professor_disciplina`
ADD CONSTRAINT `professor_disciplina_ibfk_1` FOREIGN KEY (`DISCIPLINA_cd_disciplina`) REFERENCES `disciplina` (`cd_disciplina`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `professor_disciplina_ibfk_2` FOREIGN KEY (`USUARIO_cd_usuario`) REFERENCES `usuario` (`cd_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `semestre`
--
ALTER TABLE `semestre`
ADD CONSTRAINT `semestre_ibfk_1` FOREIGN KEY (`CURSO_cd_curso`) REFERENCES `curso` (`cd_curso`),
ADD CONSTRAINT `semestre_ibfk_2` FOREIGN KEY (`ANO_cd_ano`) REFERENCES `ano` (`cd_ano`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `telefone`
--
ALTER TABLE `telefone`
ADD CONSTRAINT `fk_TELEFONE_INSTITUICAO1` FOREIGN KEY (`INSTITUICAO_cd_instituicao`) REFERENCES `instituicao` (`cd_instituicao`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `turma`
--
ALTER TABLE `turma`
ADD CONSTRAINT `fk_TURMA_CURSO1` FOREIGN KEY (`CURSO_cd_curso`) REFERENCES `curso` (`cd_curso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_TURMA_TURNO1` FOREIGN KEY (`TURNO_cd_turno`) REFERENCES `turno` (`cd_turno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `turma_ibfk_1` FOREIGN KEY (`SEMESTRE_cd_semestre`) REFERENCES `semestre` (`cd_semestre`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `turma_ibfk_2` FOREIGN KEY (`ANO_cd_ano`) REFERENCES `ano` (`cd_ano`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
ADD CONSTRAINT `fk_USUARIO_TIPO_USUARIO1` FOREIGN KEY (`TIPO_USUARIO_cd_tipo_usuario`) REFERENCES `tipo_usuario` (`cd_tipo_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
