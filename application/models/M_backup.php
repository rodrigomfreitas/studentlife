<?php

class M_backup extends CI_Model {
    
    /**
     * Método para inserção dos dados do Backup
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert('backup', $data);
    }
    
    /**
     * Método para atualização dos dados de Backup
     * @param type $data
     * @return type
     */
    public function update($data) {
        $this->db->where('cd_backup', $data['cd_backup']);
        return $this->db->update('backup', $data);
    }
    
    /**
     * Método que retorna dos dados de um determinado Backup
     * @param type $cd_backup
     * @return type
     */
    public function getBackup($cd_backup) {
        $this->db->where('cd_backup', $cd_backup);
        $query = $this->db->get('backup');
        return $query->result();
    }
    
    /**
     * Método que retorna dos Backups gerados
     * @return type
     */
    public function getBackups() {
        $this->db->where('fl_excluido', 0);
        $query = $this->db->get('backup');
        return $query->result();
    }
}

