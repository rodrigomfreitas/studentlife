<?php

class M_disciplina_usuario extends CI_Model {
    
    /**
     * Método de inserção de alunos à disciplina
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert_batch('disciplina_usuario', $data);
    }
    
}

