<?php

class M_semestre extends CI_Model {

    public function insert($data) {
        return $this->db->insert('semestre', $data);
    }

    public function update($data) {
        $this->db->where('cd_semestre', $data['cd_semestre']);
        return $this->db->update('semestre', $data);
    }

    public function getSemestres($cd_ano) {
        $this->db->join('ano', 'ano.cd_ano = semestre.ANO_cd_ano', 'inner');
        $this->db->join('curso', 'semestre.CURSO_cd_curso = curso.cd_curso', 'inner');
        $this->db->where('cd_ano', $cd_ano);
        $this->db->where('semestre.fl_excluido', 0);
        $this->db->order_by('nr_semestre');
        $query = $this->db->get('semestre');
        return $query->result();
    }
    
    /**
     * retorna os semestres no qual o aluno está matriculado que corresponde o curso
     * @param type $cd_usuario
     * @param type $cd_curso
     * @return type
     */
    public function getSemestresAluno($cd_usuario, $cd_ano) {
        $this->db->select('cd_semestre, nr_semestre, cd_curso');
        $this->db->distinct('cd_semestre');
        $this->db->join('curso', 'semestre.CURSO_cd_curso = curso.cd_curso', 'inner');
        $this->db->join('ano', 'ano.cd_ano = semestre.ANO_cd_ano', 'inner');
        $this->db->join('disciplina', 'disciplina.SEMESTRE_cd_semestre = semestre.cd_semestre');
        $this->db->join('disciplina_usuario', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina');
        $this->db->where('disciplina_usuario.USUARIO_cd_USUARIO', $cd_usuario);
        $this->db->where('cd_ano', $cd_ano);
        $this->db->where('semestre.fl_excluido', 0);
        $this->db->order_by('nr_semestre');
        $query = $this->db->get('semestre');
        return $query->result();
    }

    public function getSemestre($cd_semestre) {
        $this->db->join('curso', 'semestre.CURSO_cd_curso = curso.cd_curso', 'inner');
        $this->db->where('cd_semestre', $cd_semestre);
        $this->db->where('semestre.fl_excluido', 0);
        $query = $this->db->get('semestre');
        return $query->result();
    }

    public function getSemestrePorCurso($cd_curso) {
        $this->db->select('cd_semestre, nr_semestre');
        $this->db->where('CURSO_cd_curso', $cd_curso);
        $this->db->where('fl_excluido', 0);
        $this->db->order_by('nr_semestre');
        $query = $this->db->get('semestre');
        return $query->result();
    }
    
    public function getSemestrePorAno($cd_ano) {
        $this->db->select('cd_semestre, nr_semestre');
        $this->db->where('ANO_cd_ano', $cd_ano);
        $this->db->where('fl_excluido', 0);
        $this->db->order_by('nr_semestre');
        $query = $this->db->get('semestre');
        return $query->result();
    }

    public function verificarSemestreCadastrado($cd_ano, $nr_semestre) {
//        $this->db->select('cd_semestre');
        $this->db->where('ANO_cd_ano', $cd_ano);
        $this->db->where('nr_semestre', $nr_semestre);
        $this->db->where('semestre.fl_excluido', 0);
        $query = $this->db->get('semestre');
        return $query->result();
    }

}
