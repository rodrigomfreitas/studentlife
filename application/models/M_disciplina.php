<?php

class M_disciplina extends CI_Model {

    public function insert($data) {
        return $this->db->insert('disciplina', $data);
    }

    public function update($data) {
        $this->db->where('cd_disciplina', $data['cd_disciplina']);
        return $this->db->update('disciplina', $data);
    }

    public function getDisciplinas($cd_semestre) {
        $this->db->where('SEMESTRE_cd_semestre', $cd_semestre);
        $this->db->where('fl_excluido', 0);
        $this->db->order_by('nm_disciplina');
        $query = $this->db->get('disciplina');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }

    public function getDisciplinasAluno($cd_semestre, $cd_usuario) {
        $this->db->select('cd_disciplina, nm_disciplina, ds_disciplina');
        $this->db->distinct('cd_disciplina');
//        $this->db->join('curso', 'semestre.CURSO_cd_curso = curso.cd_curso', 'inner');
        $this->db->join('semestre', 'disciplina.SEMESTRE_cd_semestre = semestre.cd_semestre');
        $this->db->join('disciplina_usuario', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina');
        $this->db->where('disciplina_usuario.USUARIO_cd_USUARIO', $cd_usuario);
        $this->db->where('cd_semestre', $cd_semestre);
        $this->db->where('disciplina.fl_excluido', 0);
        $query = $this->db->get('disciplina');
        return $query->result();
    }

    public function getDisciplina($cd_disciplina) {
        $this->db->where('cd_disciplina', $cd_disciplina);
        $this->db->where('disciplina.fl_excluido', 0);
        $query = $this->db->get('disciplina');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }

    public function getDadosDisciplina($cd_disciplina) {
        $this->db->join('semestre', 'semestre.cd_semestre = disciplina.SEMESTRE_cd_semestre');
        $this->db->join('curso', 'curso.cd_curso = semestre.CURSO_cd_curso');
        $this->db->where('disciplina.fl_excluido', 0);
        $this->db->where('disciplina.cd_disciplina', $cd_disciplina);
        $query = $this->db->get('disciplina');
        return $query->row();
    }

    public function getDisciplinasAlunoMatriculado($cd_usuario) {
        $stored_procedure = "call get_disciplinas_aluno(?)";
        $query = $this->db->query($stored_procedure, array('cd_usuario' => $cd_usuario));
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }

    public function getDisciplinaProfessor($cd_usuario) {
//        $this->db->join('disciplina', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina');
//        $this->db->where('USUARIO_cd_usuario', $cd_usuario);
//        $query = $this->db->get('disciplina_usuario');
//        return $query->result();

        $this->db->join('disciplina', 'disciplina.cd_disciplina = professor_disciplina.DISCIPLINA_cd_disciplina');
        $this->db->where('USUARIO_cd_usuario', $cd_usuario);
        $this->db->order_by('disciplina.nm_disciplina');
        $query = $this->db->get('professor_disciplina');
        return $query->result();
    }

}
