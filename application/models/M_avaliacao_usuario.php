<?php

class M_avaliacao_usuario extends CI_Model {

    /**
     * Método para inserção da avaliação do Aluno
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert_batch('avaliacao_usuario', $data);
    }

//    public function insert($data, $cd_avaliacao) {
//        $this->db->where('AVALIACAO_cd_avaliacao', $cd_avaliacao);
//        $query = $this->db->get('avaliacao_usuario');
//        
//        if ($query->num_rows() > 0) {
//            $this->db->where('AVALIACAO_cd_avaliacao', $cd_avaliacao);
//            return $this->db->update_batch('avaliacao_usuario', $data, 'USUARIO_cd_usuario');
//        } else {
//            return $this->db->insert_batch('avaliacao_usuario', $data);
//        }
//    }
    
    /**
     * Método para atualização da avaliação do Aluno
     * @param type $data
     * @return type
     */
    public function update($data) {
        $this->db->where('AVALIACAO_cd_avaliacao', $data['AVALIACAO_cd_avaliacao']);
        $this->db->where('USUARIO_cd_usuario', $data['USUARIO_cd_usuario']);
        return $this->db->update('avaliacao_usuario', $data);
    }
    
    /**
     * Método para a exclusão da avaliação do Aluno
     * @param type $cd_avaliacao
     * @return type
     */
    public function delete($cd_avaliacao) {
        $this->db->where('AVALIACAO_cd_avaliacao', $cd_avaliacao);
        return $this->db->delete('avaliacao_usuario');
    }

    /**
     * Método que retorna todos os conceitos de uma determinada Avaliação
     * @param type $cd_avaliacao
     * @return type
     */
    public function carregarConceitos($cd_avaliacao) {
        $this->db->where('AVALIACAO_cd_avaliacao', $cd_avaliacao);
        $query = $this->db->get('avaliacao_usuario');
        return $query->result();
    }
    
    /**
     * Método que retorna os conceitos do Aluno em uma determinada Disciplina
     * @param type $cd_disciplina
     * @param type $cd_usuario
     * @return type
     */
    public function listaConceitosAluno($cd_disciplina, $cd_usuario) {
        $this->db->select('usuario.nm_usuario, usuario.cd_usuario, avaliacao.nm_avaliacao, avaliacao_usuario.ds_conceito');
        $this->db->join('usuario', 'usuario.cd_usuario = avaliacao_usuario.USUARIO_cd_usuario', 'inner');
        $this->db->join('avaliacao', 'avaliacao.cd_avaliacao = avaliacao_usuario.AVALIACAO_cd_avaliacao', 'inner');
        $this->db->where('avaliacao.DISCIPLINA_cd_disciplina', $cd_disciplina);
        $this->db->where('avaliacao_usuario.USUARIO_cd_usuario', $cd_usuario);
        $query = $this->db->get('avaliacao_usuario');
        return $query->result();
    }
    
    /**
     * Método que retorna os conceitos da Turma em uma determinada Disciplina
     * @param type $cd_disciplina
     * @return type
     */
    public function listaConceitosTurma($cd_disciplina) {
        $this->db->select('usuario.nm_usuario, usuario.cd_usuario, avaliacao.nm_avaliacao, avaliacao_usuario.ds_conceito');
        $this->db->join('usuario', 'usuario.cd_usuario = avaliacao_usuario.USUARIO_cd_usuario', 'inner');
        $this->db->join('avaliacao', 'avaliacao.cd_avaliacao = avaliacao_usuario.AVALIACAO_cd_avaliacao', 'inner');
        $this->db->where('avaliacao.DISCIPLINA_cd_disciplina', $cd_disciplina);
        $query = $this->db->get('avaliacao_usuario');
        return $query->result();
    }
    
    /**
     * Método que retorna os conceitos de uma determinada Disciplina
     * @param type $cd_disciplina
     * @return type
     */
    public function conceitosGrafico($cd_disciplina) {
        $sql = array(
            "avaliacao.nm_avaliacao,
            count(if (ds_conceito = 'A',1,null)) as conceitoA,
            count(if (ds_conceito = 'B',1,null)) as conceitoB,
            count(if (ds_conceito = 'C',1,null)) as conceitoC,
            count(if (ds_conceito = 'D',1,null)) as conceitoD,
            count(if (ds_conceito = 'SC',1,null)) as semConceito",
        );
        
    
        $this->db->select($sql);
        $this->db->join('avaliacao', 'avaliacao.cd_avaliacao = avaliacao_usuario.AVALIACAO_cd_avaliacao', 'inner');
        $this->db->where('avaliacao.DISCIPLINA_cd_disciplina', $cd_disciplina);
        
        $this->db->group_by('cd_avaliacao');
        $query = $this->db->get('avaliacao_usuario');
        return $query->result_array();
    }
    
    /**
     * Método que retorna os conceitos do Aluno em uma determinada Disciplina
     * @param type $cd_disciplina
     * @param type $cd_usuario
     * @return type
     */
    public function conceitosGraficoAluno($cd_disciplina, $cd_usuario) {
        $this->db->select('nm_avaliacao, ds_conceito');
        $this->db->join('avaliacao', 'avaliacao.cd_avaliacao = avaliacao_usuario.AVALIACAO_cd_avaliacao', 'inner');
        $this->db->where('avaliacao.DISCIPLINA_cd_disciplina', $cd_disciplina);
        $this->db->where('avaliacao_usuario.USUARIO_cd_usuario', $cd_usuario);
        
        $this->db->group_by('cd_avaliacao');
        $query = $this->db->get('avaliacao_usuario');
        return $query->result_array();
    }
    
    /**
     * Método que retorna os conceitos de uma determinada Turma
     * @param type $cd_turma
     * @return type
     */
    public function conceitosGraficoTurma($cd_turma) {
        $sql = array(
            "nm_disciplina,
            count(if (ds_conceito = 'A',1,null)) as conceitoA,
            count(if (ds_conceito = 'B',1,null)) as conceitoB,
            count(if (ds_conceito = 'C',1,null)) as conceitoC,
            count(if (ds_conceito = 'D',1,null)) as conceitoD,
            count(if (ds_conceito = 'SC',1,null)) as semConceito",
        );
        
        $this->db->select($sql);
        $this->db->join('avaliacao', 'avaliacao.cd_avaliacao = avaliacao_usuario.AVALIACAO_cd_avaliacao', 'inner');
        $this->db->join('disciplina', 'disciplina.cd_disciplina = avaliacao.DISCIPLINA_cd_disciplina', 'inner');
        $this->db->join('semestre', 'semestre.cd_semestre = disciplina.SEMESTRE_cd_semestre', 'inner');
        $this->db->join('turma', 'turma.cd_turma = avaliacao_usuario.TURMA_cd_turma', 'inner');
        $this->db->where('avaliacao_usuario.TURMA_cd_turma', $cd_turma);
        $this->db->group_by('cd_disciplina');
        $query = $this->db->get('avaliacao_usuario');
        return $query->result_array();
    }
    
    /**
     * Método que retorna todos os conceitos de todos os Alunos
     * @param type $cd_usuario
     * @return type
     */
    public function listaTodosConceitosAlunoWebservice($cd_usuario) {
        $this->db->join('usuario', 'cd_usuario = USUARIO_cd_usuario');
        $this->db->join('avaliacao', 'cd_avaliacao = AVALIACAO_cd_avaliacao');
        $this->db->join('disciplina', 'cd_disciplina = DISCIPLINA_cd_disciplina');
        $this->db->where('USUARIO_cd_usuario', $cd_usuario);
        $query = $this->db->get('avaliacao_usuario');
        return $query->result();
    }
    
    /**
     * Método que retorna os alunos cadastrados no sistemas
     * @return type
     */
    public function listaAlunosWebservice() {
//        $this->db->select('usuario.nm_usuario, usuario.ds_email, usuario.ds_foto, curso.nm_curso');
//        $this->db->distinct('disciplina_usuario.USUARIO_cd_usuario');
//        $this->db->join('disciplina_usuario', 'disciplina_usuario.USUARIO_cd_usuario = usuario.cd_usuario', 'inner');
//        $this->db->join('disciplina', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina', 'inner');
//        $this->db->join('semestre', 'semestre.cd_semestre = disciplina.SEMESTRE_cd_semestre', 'inner');
//        $this->db->join('curso', 'curso.cd_curso = semestre.CURSO_cd_curso');
        $this->db->where('TIPO_USUARIO_cd_tipo_usuario', 3);
        $this->db->order_by('nm_usuario');
        $query = $this->db->get('usuario');
        return $query->result();
    }
    
    /**
     * Método que retorna a(s) Disciplina(s) e Avaliacao(oes) no qual o Aluno foi reprovado
     * @param type $cd_usuario
     * @return type
     */
    public function getConceitosRecomendacoes($cd_usuario) {
        $conceitos = array('D', 'SC');
        
        $this->db->select('cd_usuario, nm_usuario, nm_tipo_avaliacao, avaliacao.cd_avaliacao,  nm_disciplina, avaliacao.nm_avaliacao, ds_conceito');
        $this->db->join('usuario', 'usuario.cd_usuario = avaliacao_usuario.USUARIO_cd_usuario', 'inner');
        $this->db->join('avaliacao', 'avaliacao.cd_avaliacao = avaliacao_usuario.AVALIACAO_cd_avaliacao', 'inner');
        $this->db->join('tipo_avaliacao', 'tipo_avaliacao.cd_avaliacao = avaliacao.TIPO_AVALIACAO_cd_avaliacao', 'inner');
        $this->db->join('disciplina', 'disciplina.cd_disciplina = avaliacao.DISCIPLINA_cd_disciplina', 'inner');
//        $this->db->join('aula', 'aula.DISCIPLINA_cd_disciplina = disciplina.cd_disciplina', 'inner');
//        $this->db->join('documento', 'documento.AULA_cd_aula = aula.cd_aula', 'inner');
        $this->db->where('avaliacao_usuario.USUARIO_cd_usuario', $cd_usuario);
        $this->db->where('usuario.st_ativo', 1);
        $this->db->where('usuario.TIPO_USUARIO_cd_tipo_usuario', 3);
        $this->db->where_in('ds_conceito', $conceitos);
        $query = $this->db->get('avaliacao_usuario');
        return $query->result();
    }
    
}
