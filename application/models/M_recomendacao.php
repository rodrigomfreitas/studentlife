<?php

class M_recomendacao extends CI_Model {
    
    /**
     * Método que retorna os dados de um determinado Documento
     * @param type $cd_documento
     * @return type
     */
    public function getRecomendacao($cd_documento) {
        $this->db->where('cd_documento', $cd_documento);
        $this->db->where('documento.fl_excluido', 0);
        $query = $this->db->get('documento');
        return $query->result();
    }
    
    /**
     * Método que retorna materiais como recomendação através das palavras_chave da Avaliação
     * @param type $palavras_chave_avaliacao
     * @return type
     */
    public function getRecomendacaoMaterial($palavras_chave_avaliacao) {
        $this->db->select('*');
        $this->db->from('documento');
        foreach ($palavras_chave_avaliacao as $p) {
            $this->db->or_like('ds_palavras_chave', $p);
        }
        $this->db->where('documento.fl_excluido', 0);
        $query = $this->db->get();
        return $query->result();
    }
    
    /**
     * Método que retorna os dados de uma determinada Avaliação
     * @param type $cd_avaliacao
     * @return type
     */
    public function getAvaliacao($cd_avaliacao) {
        $this->db->select('curso.nm_curso, disciplina.nm_disciplina, avaliacao.cd_avaliacao, avaliacao.DISCIPLINA_cd_disciplina, avaliacao.TIPO_AVALIACAO_cd_avaliacao, avaliacao.nm_avaliacao, avaliacao.ds_avaliacao, avaliacao.palavras_chave_avaliacao, avaliacao.dt_data_cadastro, avaliacao.dt_data_final, tipo_avaliacao.cd_avaliacao as cd_tipo_avaliacao, nm_tipo_avaliacao');
        $this->db->join('tipo_avaliacao', 'tipo_avaliacao.cd_avaliacao = avaliacao.TIPO_AVALIACAO_cd_avaliacao', 'inner');
        $this->db->join('disciplina', 'disciplina.cd_disciplina = avaliacao.DISCIPLINA_cd_disciplina', 'inner');
        $this->db->join('semestre', 'semestre.cd_semestre = disciplina.SEMESTRE_cd_semestre', 'inner');
        $this->db->join('ano', 'ano.cd_ano = semestre.ANO_cd_ano', 'inner');
        $this->db->join('curso', 'curso.cd_curso = ano.CURSO_cd_curso', 'inner');
        $this->db->where('avaliacao.fl_excluido', 0);
        $this->db->where('avaliacao.cd_avaliacao', $cd_avaliacao);
        $query = $this->db->get('avaliacao');

        return $query->result();
    }
}