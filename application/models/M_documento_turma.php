<?php

class M_documento_turma extends CI_Model {
    
    /**
     * Método de inserção de dados de Documentos que serão visualizados por, uma ou mais, turmas
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert('documento_turma', $data);
    }
    
    /**
     * Método que exclui, uma ou mais, visibilidades às turmas
     * @param type $cd_documento
     * @return type
     */
    public function delete($cd_documento) {
       $this->db->where('DOCUMENTO_cd_documento', $cd_documento);
       return $this->db->delete('documento_turma');
   }
    
    /**
     * Método que retorna os documentos que estão sendo visualizados por, uma ou mais, turmas
     * @param type $cd_documento
     * @return type
     */
    public function getDocumentoTurma($cd_documento) {
       $this->db->select('TURMA_cd_turma');
       $this->db->where_in('DOCUMENTO_cd_documento', $cd_documento);
       $query = $this->db->get('documento_turma');
       return $query->result();
   }
}