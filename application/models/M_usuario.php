<?php

class M_usuario extends CI_Model {

    // variável para encriptar a senha
    private $salt = 'St4d3ntL1f3-G3r3nc14m3nt03st4nt1l';

    /**
     * Método para inserção de dados de Usuários
     * @param type $data
     * @return type
     */
//    public function insert($data) {
//        return $this->db->insert('usuario', $data) ? 1 : 0;
//    }

    public function insert($data) {
        $data['ds_senha'] = sha1($data['ds_senha'] . $this->salt);
        return $this->db->insert('usuario', $data);
    }

    /**
     * Método para atualização de dados de Usuários
     * @param type $data
     * @return type
     */
    public function update($data) {
        $this->db->where('cd_usuario', $data['cd_usuario']);
        return $this->db->update('usuario', $data);
    }

    /**
     * Método para atualização da senha de acesso
     * @param type $cd_usuario
     * @param type $senha
     * @return type
     */
    public function updateSenha($cd_usuario, $senha) {
        $this->db->set('ds_senha', $senha);
        $this->db->where('cd_usuario', $cd_usuario);
        $this->db->where("st_ativo", 1);
        return $this->db->update('usuario');
    }

    /**
     *  Método para setar como ativo o Usuário no Sistema
     * @param type $hashEmail
     * @param type $dados
     * @return type
     */
    public function updateAtivo($hashEmail, $dados) {
        $this->db->where('md5(ds_email)', $hashEmail);
        return $this->db->update('usuario', $dados);
    }

//    public function getAlunosCadastrados($id_disciplina) {
//        $query = $this->db->query("select usuario.nm_usuario, usuario.nr_matricula, usuario.cd_usuario, usuario.TIPO_USUARIO_cd_tipo_usuario from usuario where usuario.TIPO_USUARIO_cd_tipo_usuario = 1 and usuario.cd_usuario not in (select distinct disciplina_usuario.USUARIO_cd_usuario from disciplina_usuario where disciplina_usuario.DISCIPLINA_cd_disciplina = '$id_disciplina')");
//        return ($query->num_rows() != 0) ? $query->result() : array();
//    }

    /**
     * Lista todos os Usuários que estão ativos no Sistema
     * @param type $cd_usuario
     * @param type $senha
     * @return type
     */
    public function getUsuario($cd_usuario, $senha) {
        $this->db->where("ds_senha", $senha);
        $this->db->where("cd_usuario", $cd_usuario);
        $this->db->where("st_ativo", 1);
        return $this->db->get('usuario')->result();
    }

    /**
     * Lista todos os Usuários ativos no Sistema
     * @return type
     */
    public function getUsuarios() {
        $this->db->select('cd_usuario, nm_usuario');
        $this->db->where("st_ativo", 1);
        $this->db->order_by('nm_usuario');
        return $this->db->get('usuario')->result();
    }

    /**
     * Método chama uma procedure que verifica se consta o Usuário no Sistema
     * @param type $email
     * @param type $senha
     * @return type
     */
    public function verificaUsuario($email, $senha) {
        $stored_procedure = "call verifica_usuario(?,?)";
        $query = $this->db->query($stored_procedure, array('ds_email' => $email, 'ds_senha' => $senha));
        mysqli_next_result($this->db->conn_id);
        return ($query->num_rows() != 0) ? $query->result() : array();
    }

    /**
     * Método chama uma procedure que lista dos dados de um Usuário
     * @param type $cd_usuario
     * @return type
     */
    public function getDadosUsuarioProcedure($cd_usuario) {
        $stored_procedure = "call dados_usuario(?)";
        $query = $this->db->query($stored_procedure, array('cd_usuario' => $cd_usuario));
        mysqli_next_result($this->db->conn_id);
        return ($query->num_rows() != 0) ? $query->row() : array();
    }

    /**
     * Método que verifica se o e-mail do Usuário consta no Sistema
     * @param type $email
     * @return type
     */
    public function verificaEmail($email) {
        $this->db->select('usuario.cd_usuario, usuario.ds_email, usuario.nm_usuario');
        $this->db->where("ds_email", $email);
        return $this->db->get('usuario')->result();
    }

    public function verificaMatricula($matricula) {
        $this->db->select('usuario.cd_usuario, usuario.ds_email, usuario.nm_usuario');
        $this->db->where("nr_matricula", $matricula);
        return $this->db->get('usuario')->result();
    }

    /**
     * Método que retorna uma lista dos Professores Inscritos em uma Disciplina
     * @param type $cd_disciplina
     * @return type
     */
    public function professoresInscritosDisciplina($cd_disciplina) {
//        $this->db->select('usuario.nm_usuario, disciplina_usuario.DISCIPLINA_cd_disciplina, disciplina.cd_disciplina');
//        $this->db->join('disciplina', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina', 'inner');
//        $this->db->join('usuario', 'usuario.cd_usuario = disciplina_usuario.USUARIO_cd_usuario', 'inner');
//        $this->db->where('usuario.TIPO_USUARIO_cd_tipo_usuario', 2);
//        $this->db->where('usuario.fl_excluido', 0);
//        $this->db->where_in('DISCIPLINA_cd_disciplina', $cd_disciplina);
//        $query = $this->db->get('disciplina_usuario');
//        return $query->result();

        $this->db->select('usuario.nm_usuario, professor_disciplina.DISCIPLINA_cd_disciplina, disciplina.cd_disciplina');
        $this->db->join('disciplina', 'disciplina.cd_disciplina = professor_disciplina.DISCIPLINA_cd_disciplina', 'inner');
        $this->db->join('usuario', 'usuario.cd_usuario = professor_disciplina.USUARIO_cd_usuario', 'inner');
        $this->db->where('usuario.TIPO_USUARIO_cd_tipo_usuario', 2);
        $this->db->where('usuario.fl_excluido', 0);
        $this->db->where_in('DISCIPLINA_cd_disciplina', $cd_disciplina);
        $query = $this->db->get('professor_disciplina');
        return $query->result();
    }

    /**
     * Método que retorna uma lista dos Alunos matriculados em uma Disciplina
     * @param type $cd_disciplina
     * @return type
     */
    public function getAlunosMatriculadosDisciplina($cd_disciplina) {
        $this->db->select('usuario.cd_usuario, usuario.nm_usuario, usuario.nr_matricula, turno.nm_turno, semestre.nr_semestre, curso.sg_curso, turma.dt_turma');
        $this->db->select('DATE_FORMAT(dt_ano, "%Y") AS ano_curso', false);
        $this->db->join('disciplina_usuario', 'disciplina_usuario.USUARIO_cd_usuario = usuario.cd_usuario', 'inner');
        $this->db->join('turma', 'turma.cd_turma = disciplina_usuario.TURMA_cd_turma', 'inner');
        $this->db->join('turno', 'turno.cd_turno = turma.TURNO_cd_turno', 'inner');
        $this->db->join('semestre', 'semestre.cd_semestre = turma.SEMESTRE_cd_semestre', 'inner');
        $this->db->join('ano', 'ano.cd_ano = semestre.ANO_cd_ano', 'inner');
        $this->db->join('curso', 'curso.cd_curso = turma.CURSO_cd_curso', 'inner');
        $this->db->where('disciplina_usuario.DISCIPLINA_cd_disciplina', $cd_disciplina);
        $this->db->where('usuario.TIPO_USUARIO_cd_tipo_usuario', 3);
        $this->db->where('usuario.fl_excluido', 0);
        $query = $this->db->get('usuario');
        return $query->result();
    }

    /**
     * Método que retorna o total de Alunos matriculados em uma Disciplina
     * @param type $cd_disciplina
     * @return type
     */
    public function totalAlunosMatriculadosDisciplina($cd_disciplina) {
        $this->db->join('disciplina', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina', 'inner');
        $this->db->join('usuario', 'usuario.cd_usuario = disciplina_usuario.USUARIO_cd_usuario', 'inner');
        $this->db->where('usuario.TIPO_USUARIO_cd_tipo_usuario', 3);
        $this->db->where('usuario.fl_excluido', 0);
        $this->db->where_in('DISCIPLINA_cd_disciplina', $cd_disciplina);
        $query = $this->db->from('disciplina_usuario');
        return $query->count_all_results();
    }

    /**
     * Método que retorna o total de Alunos inscritos em uma Turma
     * @param type $cd_turma
     * @return type
     */
    public function totalAlunosInscritosTurma($cd_turma) {
        $this->db->select('usuario.cd_usuario');
        $this->db->distinct('usuario.cd_usuario');
        $this->db->join('disciplina', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina', 'inner');
        $this->db->join('usuario', 'usuario.cd_usuario = disciplina_usuario.USUARIO_cd_usuario', 'inner');
        $this->db->where('usuario.TIPO_USUARIO_cd_tipo_usuario', 3);
        $this->db->where('usuario.fl_excluido', 0);
        $this->db->where_in('TURMA_cd_turma', $cd_turma);
        $query = $this->db->from('disciplina_usuario');
        return $query->count_all_results();
    }

    /**
     * Método que lista os Alunos das Turmas que estão com visibilidades nas Avaliações
     * @param type $cd_disciplina
     * @param type $cd_turmas
     * @return type
     */
    public function alunosDasTurmasVisivilidades($cd_disciplina, $cd_turmas) {
        $this->db->select('usuario.cd_usuario, usuario.nm_usuario, disciplina_usuario.TURMA_cd_turma, usuario.nr_matricula, turno.nm_turno, semestre.nr_semestre, curso.sg_curso, turma.dt_turma');
        $this->db->distinct('usuario.cd_usuario');
        $this->db->join('disciplina_usuario', 'disciplina_usuario.USUARIO_cd_usuario = usuario.cd_usuario', 'inner');
        $this->db->join('avaliacao_turma', 'avaliacao_turma.TURMA_cd_turma = disciplina_usuario.TURMA_cd_turma', 'inner');
        $this->db->join('turma', 'turma.cd_turma = disciplina_usuario.TURMA_cd_turma', 'inner');
        $this->db->join('turno', 'turno.cd_turno = turma.TURNO_cd_turno', 'inner');
        $this->db->join('semestre', 'semestre.cd_semestre = turma.SEMESTRE_cd_semestre', 'inner');
        $this->db->join('curso', 'curso.cd_curso = turma.CURSO_cd_curso', 'inner');
        $this->db->where('disciplina_usuario.DISCIPLINA_cd_disciplina', $cd_disciplina);
        $this->db->where_in('avaliacao_turma.TURMA_cd_turma', $cd_turmas);
//        $this->db->where('avaliacao_turma.TURMA_cd_turma', $cd_avaliacao);
        $this->db->where('usuario.TIPO_USUARIO_cd_tipo_usuario', 3);
        $this->db->where('usuario.fl_excluido', 0);
        $this->db->where('usuario.st_ativo', 1);
//        $this->db->group_by('disciplina_usuario.TURMA_cd_turma');
        $query = $this->db->get('usuario');
        return $query->result();
    }

    public function getProfessores() {
        $this->db->select('cd_usuario, nm_usuario');
        $this->db->where('TIPO_USUARIO_cd_tipo_usuario', 2);
        $query = $this->db->get('usuario');
        return $query->result();
    }

    /**
     * Método que retorna as senhas que são proibidas para o usuário se cadastrar no sistema
     * @param type $str
     * @return type
     */
    public function getSenhasProibidas($str) {
        $query = $this->db->query("SELECT ds_senhas_proibidas FROM senhas_proibidas WHERE " . "'$str'" . " LIKE CONCAT('%', ds_senhas_proibidas, '%')");

        return $query->result();
    }

}
