<?php

class M_turma extends CI_Model {

    /**
     * Método para inserção de dados de Turmas
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert('turma', $data) ? 1 : 0;
    }
    
    /**
     * Método para atualização dos dados de Turmas
     * @param type $data
     * @return type
     */
    public function update($data) {
        $this->db->where('cd_turma', $data['cd_turma']);
        return $this->db->update('turma', $data);
    }
    
    /**
     * Método que retorna uma lista das Turmas referente a um Curso
     * e retorna as Turmas que contenham os semestres cadastrados ou não
     * @param type $cd_curso
     * @return type
     */
    public function getTurmas($cd_ano) {
        $this->db->select('turma.cd_turma, turma.CURSO_cd_curso, curso.nm_curso, turma.SEMESTRE_cd_semestre, curso.sg_curso, semestre.nr_semestre, turno.nm_turno, turma.dt_turma, disciplina.cd_disciplina');
        $this->db->distinct('turma.cd_turma');
        $this->db->from('turma');
        $this->db->join('curso', 'turma.CURSO_cd_curso = curso.cd_curso', 'inner');
        $this->db->join('turno', 'turno.cd_turno = turma.TURNO_cd_turno', 'inner');
        $this->db->join('semestre', 'turma.SEMESTRE_cd_semestre = semestre.cd_semestre', 'inner');
        $this->db->join('disciplina', 'disciplina.SEMESTRE_cd_semestre = semestre.cd_semestre', 'left');
        $this->db->where('turma.ANO_cd_ano', $cd_ano);
        $this->db->where('turma.fl_excluido', 0);
        $this->db->where('disciplina.cd_disciplina', null);
//        $this->db->order_by('cd_turma');
        $this->db->get();
        $query1 = $this->db->last_query();

        $this->db->select('turma.cd_turma, turma.CURSO_cd_curso, curso.nm_curso, turma.SEMESTRE_cd_semestre, curso.sg_curso, semestre.nr_semestre, turno.nm_turno, turma.dt_turma, semestre.CURSO_cd_curso');
        $this->db->distinct('turma.cd_turma, disciplina.cd_disciplina');
        $this->db->from('turma');
        $this->db->join('curso', 'turma.CURSO_cd_curso = curso.cd_curso', 'inner');
        $this->db->join('turno', 'turno.cd_turno = turma.TURNO_cd_turno', 'inner');
        $this->db->join('semestre', 'turma.SEMESTRE_cd_semestre = semestre.cd_semestre', 'inner');
        $this->db->join('disciplina', 'disciplina.SEMESTRE_cd_semestre = semestre.cd_semestre', 'left');
        $this->db->where('turma.ANO_cd_ano', $cd_ano);
        $this->db->where('turma.fl_excluido', 0);
        $this->db->where('disciplina.cd_disciplina !=', null);
        $this->db->order_by('cd_turma');
        $this->db->get();
        $query2 = $this->db->last_query();
        $query = $this->db->query($query1 . " UNION " . $query2);
        return $query->result();
    }

    /**
     * Método que retorna os dados de uma Turma
     * @param type $cd_turma
     * @return type
     */
    public function getTurma($cd_turma) {
        $this->db->join('curso', 'turma.CURSO_cd_curso = curso.cd_curso', 'inner');
        $this->db->join('turno', 'turno.cd_turno = turma.TURNO_cd_turno', 'inner');
        $this->db->join('semestre', 'semestre.cd_semestre = turma.SEMESTRE_cd_semestre', 'inner');
        $this->db->where('cd_turma', $cd_turma);
        $this->db->where('turma.fl_excluido', 0);
        $query = $this->db->get('turma');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }
    
    /**
     * Método que retorna as Turmas que estão cadastradas em um Curso
     * @param type $cd_curso
     * @return type
     */
    public function getTurmasPorCurso($cd_curso) {
        $this->db->join('curso', 'turma.CURSO_cd_curso = curso.cd_curso', 'inner');
        $this->db->join('turno', 'turno.cd_turno = turma.TURNO_cd_turno', 'inner');
        $this->db->where_in('cd_curso', $cd_curso);
        $this->db->where('turma.fl_excluido', 0);
        $this->db->where('curso.fl_excluido', 0);
        $query = $this->db->get('turma');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }

    /**
     * Método que retorna uma lista dos alunos inscritos em uma Turma
     * @param type $cd_turma
     * @return type
     */
    public function getAlunosPorTurma($cd_turma) {
        $this->db->select('usuario.cd_usuario, usuario.nm_usuario, usuario.ds_foto');
        $this->db->distinct('usuario.cd_usuario, usuario.nm_usuario');
        $this->db->join('disciplina_usuario', 'disciplina_usuario.TURMA_cd_turma = turma.cd_turma', 'inner');
        $this->db->join('usuario', 'usuario.cd_usuario = disciplina_usuario.USUARIO_cd_usuario', 'inner');
        $this->db->where('usuario.TIPO_USUARIO_cd_tipo_usuario', 3);
        $this->db->where('usuario.st_ativo', 1);
        $this->db->where('turma.cd_turma', $cd_turma);
        $query = $this->db->get('turma');
        return $query->result();
    }

    /**
     * Método que retorna os dados correspondente a uma Turma
     * @param type $cd_turma
     * @return type
     */
    public function getDisciplinasPorTurma($cd_turma) {
        $this->db->join('turno', 'turno.cd_turno = turma.TURNO_cd_turno', 'inner');
        $this->db->join('semestre', 'semestre.cd_semestre = turma.SEMESTRE_cd_semestre', 'inner');
        $this->db->join('disciplina', 'disciplina.SEMESTRE_cd_semestre = semestre.cd_semestre', 'inner');
        $this->db->join('curso', 'curso.cd_curso = semestre.CURSO_cd_curso', 'inner');
        $this->db->where('disciplina.fl_excluido', 0);
        $this->db->where('turma.cd_turma', $cd_turma);
        $query = $this->db->get('turma');
        return $query->result();
    }

    /**
     * Método que retorna as Turmas de uma Disciplina
     * @param type $cd_disciplina
     * @return type
     */
    public function getTurmasNaDisciplina($cd_disciplina) {
        $this->db->select('cd_turma, nm_turno, nr_semestre');
        $this->db->select('DATE_FORMAT(dt_ano, "%Y") AS ano_curso', false);
        $this->db->join('turno', 'turno.cd_turno = turma.TURNO_cd_turno', 'inner');
        $this->db->join('semestre', 'semestre.cd_semestre = turma.SEMESTRE_cd_semestre', 'inner');
        $this->db->join('ano', 'ano.cd_ano = semestre.ANO_cd_ano', 'inner');
        $this->db->join('disciplina', 'disciplina.SEMESTRE_cd_semestre = semestre.cd_semestre', 'inner');
        $this->db->where('disciplina.cd_disciplina', $cd_disciplina);
        $this->db->where('turma.fl_excluido', 0);
        $query = $this->db->get('turma');
        return $query->result();
    }
    
    /**
     * Método que retorna uma lista dos Turnos
     * @return type
     */
    public function getTurnos() {
        $query = $this->db->get('turno');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }

}
