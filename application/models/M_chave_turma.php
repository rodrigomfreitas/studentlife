<?php

class M_chave_turma extends CI_Model {

    /**
     * Método para a inserção das chaves para enviá-las aos alunos
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert('chave_turma', $data);
    }

    /**
     * Método que retorna as chaves geradas
     * @return type
     */
    public function getChaves($cd_turma) {
        $this->db->select('nm_usuario, nm_tipo_usuario, cd_chave_turma, ds_chave_turma, TURMA_cd_TURMA, dt_criacao_chave_turma');
        $this->db->join('usuario', 'usuario.cd_usuario = chave_turma.USUARIO_cd_usuario', 'inner');
        $this->db->join('tipo_usuario', 'tipo_usuario.cd_tipo_usuario = usuario.TIPO_USUARIO_cd_tipo_usuario', 'inner');
        $this->db->where('TURMA_cd_turma', $cd_turma);
        $query = $this->db->get('chave_turma');
        return $query->result();
    }

    /**
     * Método que retorna os dados de uma determinada chave
     * @param type $chave_nova
     * @return type
     */
    public function getChave($chave_nova) {
        $this->db->where('ds_chave_turma', $chave_nova);
        $query = $this->db->get('chave_turma');
        return $query->result();
    }

    /**
     * Método para inserção dos dados para a solicitação de cadastro do aluno à turma
     * @param type $data
     * @return type
     */
    public function insertSolicitacaoCadTurma($data) {
        return $this->db->insert_batch('chave_turma_solicitacao_cad', $data);
    }

    public function deleteSolicitacaoPosCadAluno($data) {
        $this->db->where_in('USUARIO_cd_usuario', $data['USUARIO_cd_usuario']);
        $this->db->where('DISCIPLINA_cd_disciplina', $data['DISCIPLINA_cd_disciplina']);
        return $this->db->delete('chave_turma_solicitacao_cad');
    }

//    public function getSolicitacoesCadTurma($ds_chave_turma) {
//        $this->db->select('cd_chave_turma_solicitacao_cad, chave_turma_solicitacao_cad_ds_chave_turma, chave_turma_solicitacao_cad.USUARIO_cd_usuario, cd_chave_turma, TURMA_cd_turma, nm_usuario');
//        $this->db->join('chave_turma', 'chave_turma.ds_chave_turma = chave_turma_solicitacao_cad.chave_turma_solicitacao_cad_ds_chave_turma', 'inner');
//        $this->db->join('usuario', 'usuario.cd_usuario = chave_turma_solicitacao_cad.USUARIO_cd_usuario');
//        $this->db->where('chave_turma_solicitacao_cad_ds_chave_turma', $ds_chave_turma);
//        $query = $this->db->get('chave_turma_solicitacao_cad');
//        return $query->result();
//    }

    /**
     * Método que retorna o total de solicitações
     * @param type $ds_chave_turma
     * @return type
     */
//    public function getSolicitacaoCadTurma($ds_chave_turma) {
//        $this->db->select('cd_chave_turma_solicitacao_cad, chave_turma_solicitacao_cad_ds_chave_turma, chave_turma_solicitacao_cad.USUARIO_cd_usuario, cd_chave_turma, TURMA_cd_turma, nm_usuario');
//        $this->db->join('chave_turma', 'chave_turma.ds_chave_turma = chave_turma_solicitacao_cad.chave_turma_solicitacao_cad_ds_chave_turma', 'inner');
//        $this->db->join('usuario', 'usuario.cd_usuario = chave_turma_solicitacao_cad.USUARIO_cd_usuario');
//        $this->db->where('chave_turma_solicitacao_cad_ds_chave_turma', $ds_chave_turma);
//        $this->db->from('chave_turma_solicitacao_cad');
//        return $this->db->count_all_results();
//    }
    public function getTotalSolicitacoesCadTurma($cd_disciplina) {
//        $this->db->select('cd_chave_turma_solicitacao_cad, chave_turma_solicitacao_cad_ds_chave_turma, chave_turma_solicitacao_cad.USUARIO_cd_usuario, cd_chave_turma, TURMA_cd_turma, nm_usuario');
        $this->db->join('chave_turma', 'chave_turma.ds_chave_turma = chave_turma_solicitacao_cad.chave_turma_solicitacao_cad_ds_chave_turma', 'inner');
        $this->db->join('turma', 'turma.cd_turma = chave_turma.TURMA_cd_turma', 'inner');
        $this->db->join('turno', 'turno.cd_turno = turma.TURNO_cd_turno', 'inner');
        $this->db->join('semestre', 'semestre.cd_semestre = turma.SEMESTRE_cd_semestre', 'inner');
        $this->db->join('ano', 'ano.cd_ano = semestre.ANO_cd_ano', 'inner');
        $this->db->join('disciplina', 'disciplina.cd_disciplina = chave_turma_solicitacao_cad.DISCIPLINA_cd_disciplina', 'inner');
        $this->db->join('usuario', 'usuario.cd_usuario = chave_turma_solicitacao_cad.USUARIO_cd_usuario');
        $this->db->where('chave_turma_solicitacao_cad.DISCIPLINA_cd_disciplina', $cd_disciplina);
        $this->db->order_by('nm_usuario');
        $this->db->order_by('cd_turma');
        $this->db->from('chave_turma_solicitacao_cad');
        return $this->db->count_all_results();
    }

    /**
     * Método que retorna as solicitações de cadastro de aluno de uma determinada disciplina
     * @param type $cd_disciplina
     * @return type
     */
    public function getSolicitacoesCadTurma($cd_disciplina) {
        $this->db->select('cd_chave_turma_solicitacao_cad, chave_turma_solicitacao_cad_ds_chave_turma, '
                . 'chave_turma_solicitacao_cad.USUARIO_cd_usuario, cd_chave_turma, TURMA_cd_turma, nm_usuario, '
                . 'nr_matricula, nr_semestre, nm_turno, chave_turma_solicitacao_cad.DISCIPLINA_cd_disciplina');
        $this->db->select('DATE_FORMAT(dt_ano, "%Y") AS ano_curso', false);
        $this->db->join('chave_turma', 'chave_turma.ds_chave_turma = chave_turma_solicitacao_cad.chave_turma_solicitacao_cad_ds_chave_turma', 'inner');
        $this->db->join('turma', 'turma.cd_turma = chave_turma.TURMA_cd_turma', 'inner');
        $this->db->join('turno', 'turno.cd_turno = turma.TURNO_cd_turno', 'inner');
        $this->db->join('semestre', 'semestre.cd_semestre = turma.SEMESTRE_cd_semestre', 'inner');
        $this->db->join('ano', 'ano.cd_ano = semestre.ANO_cd_ano', 'inner');
        $this->db->join('disciplina', 'disciplina.cd_disciplina = chave_turma_solicitacao_cad.DISCIPLINA_cd_disciplina', 'inner');
        $this->db->join('usuario', 'usuario.cd_usuario = chave_turma_solicitacao_cad.USUARIO_cd_usuario');
        $this->db->where('chave_turma_solicitacao_cad.DISCIPLINA_cd_disciplina', $cd_disciplina);
        $this->db->order_by('nm_usuario');
        $this->db->order_by('cd_turma');
        $query = $this->db->get('chave_turma_solicitacao_cad');
        return $query->result();
    }

}
