<?php

class M_aula extends CI_Model {
    
    /**
     * Método para inserção de dados de Aula
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert('aula', $data);
    }
    
    /**
     * Método para atualização de dados de Aula
     * @param type $data
     * @return type
     */
    public function update($data) {
        $this->db->where('cd_aula', $data['cd_aula']);
        return $this->db->update('aula', $data);
    }
    
    /**
     * Método que retorna as Aulas de uma determinada Disciplina
     * @param type $cd_disciplina
     * @return type
     */
    public function getAulas($cd_disciplina) {
        $this->db->where('DISCIPLINA_cd_disciplina', $cd_disciplina);
        $this->db->where('fl_excluido', 0);
        $this->db->order_by('nr_aula');
        $query = $this->db->get('aula');
        return $query->result();
    }
    
    /**
     * Método que retorna os dados de uma determinada aula referente o código
     * @param type $cd_aula
     * @return type
     */
    public function getAula($cd_aula) {
        $this->db->where('cd_aula', $cd_aula);
        $query = $this->db->get('aula');
        return $query->result();
    }
    
    /**
     * Método que retorna a aula cadastrada referente o código da aula
     * @param type $cd_aula
     * @return type
     */
    public function verificaAulaCadastrada($nr_aula) {
        $this->db->where('nr_aula', $nr_aula);
        $this->db->where('fl_excluido', 0);
        $query = $this->db->get('aula');
        return $query->result();
    }
   
}
