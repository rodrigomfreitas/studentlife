<?php

class M_instituicao extends CI_Model {

    public function update($data) {
        $this->db->where('cd_instituicao', $data['cd_instituicao']);
        return $this->db->update('instituicao', $data);
    }

    public function getInstituicao() {
        $this->db->where('fl_excluido', 0);
        $query = $this->db->get('instituicao');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }

    public function getInstituicaoEditar($cd_instituicao) {
        $this->db->where('cd_instituicao', $cd_instituicao);
        $this->db->where('fl_excluido', 0);
        $query = $this->db->get('instituicao');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }

}
