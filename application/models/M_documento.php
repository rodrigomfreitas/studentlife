<?php

class M_documento extends CI_Model {
    
    /**
     * Método para inserção de dados de Documento
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert('documento', $data);
    }
    
    /**
     * Método para atualização de Documentos
     * @param type $data
     * @return type
     */
    public function update($data) {
        $this->db->where('cd_documento', $data['cd_documento']);
        return $this->db->update('documento', $data);
    }
    
    /**
     * Método que retorna todos os Documentos de uma determinada Aula
     * @param type $cd_aula
     * @return type
     */
    public function getDocumentos($cd_aula) {
        $this->db->join('aula', 'aula.cd_aula = documento.AULA_cd_aula', 'inner');
        $this->db->where('AULA_cd_aula', $cd_aula);
        $this->db->where('documento.fl_excluido', 0);
        $query = $this->db->get('documento');
        return $query->result();
    }
    
    /**
     * Método que retorna os dados de um Documento
     * @param type $cd_documento
     * @return type
     */
    public function getDocumento($cd_documento) {
        $this->db->where('cd_documento', $cd_documento);
        $this->db->where('documento.fl_excluido', 0);
        $query = $this->db->get('documento');
        return $query->result();
    }
}

