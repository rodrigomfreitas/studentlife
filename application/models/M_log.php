<?php

class M_log extends CI_Model {

    public function insert($data) {
        return $this->db->insert('log', $data);
    }

    public function ultimoAcesso($cd_usuario) {
        $this->db->where('USUARIO_cd_usuario', $cd_usuario);
        $this->db->where('ACAO_cd_acao', 1);
        $this->db->limit(2);
        $this->db->order_by('dt_acao', 'DESC');
        $this->db->order_by('hr_acao', 'DESC');
        $query = $this->db->get('log');
        return $query->result();
    }

    public function listaAcoes() {
        $query = $this->db->get('acao');
        return $query->result();
    }

    public function listaPorAcao($cd_acao) {
        $this->db->where('cd_acao', $cd_acao);
        $query = $this->db->get('acao');
        return $query->result();
    }

    public function listaAcao($cd_acao, $data = NULL) {
        $this->db->select('usuario.cd_usuario, log.USUARIO_cd_usuario, usuario.nm_usuario, log.dt_acao, log.hr_acao, acao.ds_acao, "null" as nm_curso');
        $this->db->from('log');
        $this->db->join('acao', 'acao.cd_acao = log.ACAO_cd_acao', 'inner');
        $this->db->join('usuario', 'usuario.cd_usuario = log.USUARIO_cd_usuario', 'inner');
        $this->db->where('log.ACAO_cd_acao', $cd_acao);
        if ($data['dt_acao_inicio'] != NULL) {
            $this->db->where('log.dt_acao >=', $data['dt_acao_inicio']);
        }
        if ($data['dt_acao_final'] != NULL) {
            $this->db->where('log.dt_acao <=', $data['dt_acao_final']);
        }
        $this->db->get();
        $query1 = $this->db->last_query();

        $this->db->select('usuario.cd_usuario, log_curso.USUARIO_cd_usuario, usuario.nm_usuario, log_curso.dt_acao, log_curso.hr_acao, acao.ds_acao, nm_curso');
        $this->db->from('log_curso');
        $this->db->join('acao', 'acao.cd_acao = log_curso.ACAO_cd_acao', 'inner');
        $this->db->join('usuario', 'usuario.cd_usuario = log_curso.USUARIO_cd_usuario', 'inner');
        $this->db->join('curso', 'curso.cd_curso  = log_curso.CURSO_cd_curso', 'inner');
        $this->db->where('log_curso.ACAO_cd_acao', $cd_acao);
        if ($data['dt_acao_inicio'] != NULL) {
            $this->db->where('log_curso.dt_acao >=', $data['dt_acao_inicio']);
        }
        if ($data['dt_acao_final'] != NULL) {
            $this->db->where('log_curso.dt_acao <=', $data['dt_acao_final']);
        }
        $this->db->get();
        $query2 = $this->db->last_query();

        $query = $this->db->query($query1 . " UNION " . $query2);
        return $query->result();
    }

    public function listaAcaoUsuarios($cd_usuario, $data = NULL) {
        $this->db->select('log.USUARIO_cd_usuario, usuario.nm_usuario, acao.ds_acao, log.dt_acao, log.hr_acao');
        $this->db->from('log');
        $this->db->join('acao', 'acao.cd_acao = log.ACAO_cd_acao', 'inner');
        $this->db->join('usuario', 'usuario.cd_usuario = log.USUARIO_cd_usuario', 'inner');
        $this->db->where_in('USUARIO_cd_usuario', $cd_usuario);
        if ($data['dt_acao_inicio'] != NULL) {
            $this->db->where('log.dt_acao >=', $data['dt_acao_inicio']);
        }
        if ($data['dt_acao_final'] != NULL) {
            $this->db->where('log.dt_acao <=', $data['dt_acao_final']);
        }
        $this->db->get();
        $query1 = $this->db->last_query();

        $this->db->select('log_curso.USUARIO_cd_usuario, usuario.nm_usuario, acao.ds_acao, log_curso.dt_acao, log_curso.hr_acao');
        $this->db->from('log_curso');
        $this->db->join('acao', 'acao.cd_acao = log_curso.ACAO_cd_acao', 'inner');
        $this->db->join('usuario', 'usuario.cd_usuario = log_curso  .USUARIO_cd_usuario', 'inner');
        $this->db->where_in('USUARIO_cd_usuario', $cd_usuario);
        if ($data['dt_acao_inicio'] != NULL) {
            $this->db->where('log_curso.dt_acao >=', $data['dt_acao_inicio']);
        }
        if ($data['dt_acao_final'] != NULL) {
            $this->db->where('log_curso.dt_acao <=', $data['dt_acao_final']);
        }
        $this->db->get();
        $query2 = $this->db->last_query();

        $query = $this->db->query($query1 . " UNION " . $query2);
        return $query->result();
    }

}
