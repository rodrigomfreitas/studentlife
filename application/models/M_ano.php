<?php

class M_ano extends CI_Model {
    
    /**
     * Método para inserção de dados do Ano
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert('ano', $data);
    }
    
    /**
     * Método para atualização de dados do Ano
     * @param type $data
     * @return type
     */
    public function update($data) {
        $this->db->where('cd_ano', $data['cd_ano']);
        return $this->db->update('ano', $data);
    }
    
    /**
     * Método que retorna os Anos de um determinado Curso
     * @param type $cd_curso
     * @return type
     */
    public function getAnos($cd_curso) {
        $this->db->select('cd_ano, CURSO_cd_curso, curso.cd_curso, curso.nm_curso, curso.sg_curso');
        $this->db->select('DATE_FORMAT(dt_ano, "%Y") AS ano_curso', false);
        $this->db->join('curso', 'curso.cd_curso = ano.CURSO_cd_curso', 'inner');
        $this->db->where('CURSO_cd_curso', $cd_curso);
        $this->db->where('ano.fl_excluido', 0);
        $this->db->order_by('ano_curso');
        $query = $this->db->get('ano');
        return $query->result();
    }
    
    /**
     * Método que retorna os dados de um determinado ano
     * @param type $cd_ano
     * @return type
     */
    public function getAno($cd_ano) {
        $this->db->where('cd_ano', $cd_ano);
        $this->db->where('ano.fl_excluido', 0);
        $query = $this->db->get('ano');
        return $query->result();
    }
    
    /**
     * Método que retorna o Ano no qual o alunos está matriculado
     * @param type $cd_curso
     * @param type $cd_usuario
     * @return type
     */
    public function getAnosAlunoMatriculado($cd_curso, $cd_usuario) {
        $this->db->select('cd_ano');
        $this->db->distinct('cd_ano');
        $this->db->select('DATE_FORMAT(dt_ano, "%Y") AS ano_curso', false);
        $this->db->join('curso', 'curso.cd_curso = ano.CURSO_cd_curso');
        $this->db->join('semestre', 'semestre.ANO_cd_ano = ano.cd_ano');
        $this->db->join('disciplina', 'disciplina.SEMESTRE_cd_semestre = semestre.cd_semestre');
        $this->db->join('disciplina_usuario', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina');
        $this->db->where('disciplina_usuario.USUARIO_cd_USUARIO', $cd_usuario);
        $this->db->where('cd_curso', $cd_curso);
        $this->db->where('ano.fl_excluido', 0);
       $this->db->order_by('ano_curso');
        $query = $this->db->get('ano');
        return $query->result();
    }
    
    /**
     * Método que retorna os Semestres que estão ou não cadastrados em um ano de um determinado Curso
     * @param type $cd_curso
     * @return type
     */
    public function getAnosComSemestres($cd_curso) {
        $this->db->select('cd_ano, semestre.CURSO_cd_curso');
        $this->db->select('DATE_FORMAT(ano.dt_ano, "%Y") AS ano_curso', false);
        $this->db->distinct('semestre.CURSO_cd_curso');
        $this->db->join('semestre', 'semestre.ANO_cd_ano = ano.cd_ano', 'left');
        $this->db->join('curso', 'curso.cd_curso = semestre.CURSO_cd_curso', 'left');
        $this->db->where('ano.CURSO_cd_curso', $cd_curso);
        $this->db->where('ano.fl_excluido', 0);
        $this->db->order_by('ano_curso');
        $query = $this->db->get('ano');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }
}

