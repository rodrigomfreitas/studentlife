<?php

class M_telefone extends CI_Model {
    
    /**
     * Método para inserção dos Telefones da Instituição
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert('telefone', $data);
    }
    
    /**
     * Método para atualização dos Telefones da Instituição
     * @param type $data
     * @return type
     */
    public function update($data) {
        $this->db->where('cd_telefone', $data['cd_telefone']);
        return $this->db->update('telefone', $data);
    }

    /**
     * Método que retorna os Telefones da Instituição
     * @return type
     */
    public function getTelefones() {
        $this->db->join('instituicao', 'INSTITUICAO_cd_instituicao = cd_instituicao', 'inner');
        $this->db->where('telefone.fl_excluido', 0);
        $query = $this->db->get('telefone');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }
    
    /**
     * Método que retorna os dados de um Telefone da Instituição
     * @param type $cd_telefone
     * @return type
     */
    public function getTelefonesEditar($cd_telefone) {
        $this->db->join('instituicao', 'INSTITUICAO_cd_instituicao = cd_instituicao', 'inner');
        $this->db->where('cd_telefone', $cd_telefone);
        $this->db->where('telefone.fl_excluido', 0);
        $query = $this->db->get('telefone');

        return $query->result();
    }

}
