<?php

class M_conceito extends CI_Model {

  public function insert($data) {
    return $this->db->insert('avaliacao_usuario', $data);
  }
  
  public function get_conceitos($id_aluno){
  $this->db->select('avaliacao_usuario.avaliacao_id, avaliacao_usuario.conceito, avaliacao.nome');
  $this->db->join('avaliacao_usuario', 'avaliacao_id = id_avaliacao', 'inner');
  $this->db->where('avaliacao_usuario.usuario_id', $id_aluno);
  $query = $this->db->get('avaliacao_usuario');
  return ($query->num_rows() != 0) ? $query->result() : array();
  }

  
  
  
  public function get_conceitos_aluno($id_aluno)
  {
    $this->db->select('avaliacao.nome as avnome, perfil.nome, perfil.sobrenome,avaliacao_usuario.conceito, avaliacao_usuario.usuario_id');
    $this->db->join('avaliacao_usuario', 'avaliacao_id = id_avaliacao', 'inner');
    $this->db->join('perfil', 'perfil.usuario_id = avaliacao_usuario.usuario_id', 'inner');
    $this->db->where('avaliacao_usuario.usuario_id', $id_aluno);
    $query = $this->db->get('avaliacao');
    return ($query->num_rows() != 0) ? $query->result() : array();
  }

  public function get_avaliacoes_aluno($id_disciplina)
  {
    $query = $this->db->query("select perfil.nome, perfil.sobrenome, usuario.matricula, usuario.id_usuario, usuario.status from usuario inner join perfil on perfil.usuario_id = usuario.id_usuario where usuario.status = 1 and usuario.id_usuario in (select distinct disciplinas_usuario.usuario_id from disciplinas_usuario where disciplinas_usuario.disciplinas_id = '$id_disciplina')");
        return ($query->num_rows() != 0) ? $query->result() : array();
  }

  public function get_avaliacao($id_disciplina)
  {
    $this->db->where('disciplina_id', $id_disciplina);
    $this->db->order_by('disciplina_id', 'asc');
    $query = $this->db->get('avaliacao');
    return ($query->num_rows() != 0) ? $query->result() : array();
  }


  function a_publish($avaliacao_id, $usuario_id, $conceito)
  {
    $data['usuario_id'] = $usuario_id;
    $data['avaliacao_id'] = $avaliacao_id;
    $data['conceito'] = $conceito;

    $this->db->where('avaliacao_id', $avaliacao_id);
    $this->db->where('usuario_id', $usuario_id);
    $this->db->from('avaliacao_usuario');
    $result =  $this->db->count_all_results();
    if($result == 0) {
      $this->db->insert('avaliacao_usuario', $data);
    } else {
      $this->db->where('avaliacao_id', $data['avaliacao_id']);
      $this->db->where('usuario_id', $data['usuario_id']);
      $this->db->update('avaliacao_usuario', $data);
    }
    echo '<script type="text/javascript">';
    echo 'window.close();';
    echo '</script>';

  }






}
