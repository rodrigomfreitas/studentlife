<?php

class M_avaliacao_turma extends CI_Model {

    public function insert($data) {
        return $this->db->insert('avaliacao_turma', $data);
    }
        
    public function update($data) {
        $this->db->where('AVALIACAO_cd_avaliacao', $data['AVALIACAO_cd_avaliacao']);
        $this->db->where('TURMA_cd_turma', $data['TURMA_cd_turma']);
        return $this->db->update('avaliacao_turma', $data);
    }

   public function delete($cd_avaliacao) {
       $this->db->where('AVALIACAO_cd_avaliacao', $cd_avaliacao);
       return $this->db->delete('avaliacao_turma');
   }
   
   public function getAvaliacaoTurma($cd_avaliacao) {
       $this->db->select('TURMA_cd_turma');
       $this->db->where_in('AVALIACAO_cd_avaliacao', $cd_avaliacao);
       $query = $this->db->get('avaliacao_turma');
       return $query->result();
   }
   
   public function turmasSetadas($cd_disciplina) {
       $this->db->select('AVALIACAO_cd_avaliacao');
       $this->db->distinct('AVALIACAO_cd_avaliacao');
       $this->db->where('DISCIPLINA_cd_disciplina', $cd_disciplina);
       $query = $this->db->get('avaliacao_turma');
       return $query->result();
   }
   
}
