<?php

class M_chat extends CI_Model {

    public function get_mensagem($cd_usuario) {

        $id = $this->session->userdata('id');

        $query = $this->db->query("SELECT mensagem.cd_mensagem, mensagem.dt_data_cad, mensagem.cd_envia,mensagem.cd_recebe,
                                   mensagem.ds_mensagem, mensagem.fl_excluido, usuario.cd_usuario, usuario.nm_usuario,
                                   usuario.ds_foto FROM mensagem INNER JOIN usuario ON mensagem.cd_envia = usuario.cd_usuario
                                   WHERE mensagem.cd_recebe = '$id' AND mensagem.cd_envia = '$cd_usuario'
                                   UNION SELECT mensagem.cd_mensagem, mensagem.dt_data_cad, mensagem.cd_envia,
                                   mensagem.cd_recebe, mensagem.ds_mensagem, mensagem.fl_excluido, usuario.cd_usuario,
                                   usuario.nm_usuario, usuario.ds_foto FROM mensagem INNER JOIN
                                   usuario ON mensagem.cd_envia = usuario.cd_usuario WHERE
                                   mensagem.cd_recebe = '$cd_usuario' AND mensagem.cd_envia = '$id' ORDER BY dt_data_cad");
        
        return $query->result();
    }

    public function get_usuario() {
        $id = $this->session->userdata('id');
        $this->db->select('usuario.cd_usuario, usuario.nm_usuario, usuario.ds_foto, usuario.TIPO_USUARIO_cd_tipo_usuario, tipo_usuario.cd_tipo_usuario');
        $this->db->join('tipo_usuario', 'tipo_usuario.cd_tipo_usuario = usuario.TIPO_USUARIO_cd_tipo_usuario', 'inner');
        $this->db->where('usuario.cd_usuario !=', $id);
        $this->db->where('tipo_usuario.cd_tipo_usuario !=', 1);
        $this->db->order_by("tipo_usuario.nm_tipo_usuario", "desc");
        $query = $this->db->get('usuario');
        return $query->result();
    }

    public function set_mensagem($data) {
        return $this->db->insert('mensagem', $data);
    }

    public function update_status($cd_envia, $cd_recebe) {
        $this->db->set('fl_excluido', 1);
        $this->db->where('cd_envia', $cd_envia);
        $this->db->where('cd_recebe', $cd_recebe);
        $this->db->where('fl_excluido', 0);
        return $this->db->update('mensagem');
    }

    public function get_mensagens_nao_lidas_count($id) {
        $stored_procedure = "call mensagens_nao_lidas_count(?)";
        $query = $this->db->query($stored_procedure, array('id_recebe' => $id));
        mysqli_next_result($this->db->conn_id);
        return $query->result();
    }

    public function get_mensagens_nao_lidas() {
        $id = $this->session->userdata('id');
        $query = $this->db->query("SELECT mensagem.cd_mensagem,  mensagem.dt_data_cad,  mensagem.cd_envia,
                                   mensagem.cd_recebe, mensagem.ds_mensagem,  mensagem.fl_excluido,
                                   usuario.cd_usuario,  usuario.nm_usuario, usuario.ds_foto FROM
                                   mensagem INNER JOIN usuario ON mensagem.cd_envia = usuario.cd_usuario
                                   WHERE mensagem.cd_recebe = '$id' AND mensagem.fl_excluido = 0 ORDER BY dt_data_cad");
        return $query->result();
    }

}
