<?php

class M_professor_disciplina extends CI_Model {
    
    /**
     * Método de inserção de dados de Backup
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert('professor_disciplina', $data);
    }
    
    /**
     * Método para exclusão de dados de um Professor que ministra em uma determinada Disciplina
     * @param type $data
     * @return type
     */
    public function delete($data) {
        $this->db->where('USUARIO_cd_usuario', $data['USUARIO_cd_usuario']);
        $this->db->where('DISCIPLINA_cd_disciplina', $data['DISCIPLINA_cd_disciplina']);
        return $this->db->delete('professor_disciplina', $data);
    }
    
    /**
     * Método que retorna os Professores que ministram em uma determinada Disciplina
     * @param type $cd_disciplina
     * @return type
     */
    public function getProfessoresDisciplinas($cd_disciplina) {
        $this->db->select('USUARIO_cd_usuario, usuario.nm_usuario, usuario.ds_email');
        $this->db->join('usuario', 'usuario.cd_usuario = professor_disciplina.USUARIO_cd_usuario', 'inner');
        $this->db->where('DISCIPLINA_cd_disciplina', $cd_disciplina);
        $query = $this->db->get('professor_disciplina');
        return $query->result();
    }
    
    /**
     * Método que retorna os dados de um determinado Professor
     * @param type $cd_usuario
     * @param type $cd_disciplina
     * @return type
     */
    public function getProfessorDisciplina($cd_usuario, $cd_disciplina) {
        $this->db->select('USUARIO_cd_usuario, usuario.nm_usuario, usuario.ds_email');
        $this->db->join('usuario', 'usuario.cd_usuario = professor_disciplina.USUARIO_cd_usuario', 'inner');
        $this->db->where('DISCIPLINA_cd_disciplina', $cd_disciplina);
        $this->db->where('USUARIO_cd_usuario', $cd_usuario);
        $query = $this->db->get('professor_disciplina');
        return $query->result();
    }
}

