<?php

class M_avaliacao extends CI_Model {

    /**
     * Método para inserção de Avaliação
     * @param type $data
     * @return type
     */
    public function insert($data) {
        return $this->db->insert('avaliacao', $data);
    }
    
    /**
     * Método para atualização de Avaliação
     * @param type $data
     * @return type
     */
    public function update($data) {
        $this->db->where('cd_avaliacao', $data['cd_avaliacao']);
        return $this->db->update('avaliacao', $data);
    }
    
    /**
     * Método que retorna todas as Avaliacões de uma determinada Disciplina
     * @param type $cd_disciplina
     * @return type
     */
    public function getAvaliacaoDisciplina($cd_disciplina) {
        $this->db->select('avaliacao.cd_avaliacao, avaliacao.nm_avaliacao, DISCIPLINA_cd_disciplina, TIPO_AVALIACAO_cd_avaliacao, nm_tipo_avaliacao, avaliacao.ds_avaliacao, avaliacao.dt_data_cadastro, avaliacao.dt_data_final');
        $this->db->join('tipo_avaliacao', 'tipo_avaliacao.cd_avaliacao = avaliacao.TIPO_AVALIACAO_cd_avaliacao', 'inner');
        $this->db->where('avaliacao.DISCIPLINA_cd_disciplina', $cd_disciplina);
        $this->db->where('avaliacao.fl_excluido', 0);
        $this->db->order_by('dt_data_final', 'desc');
        $query = $this->db->get('avaliacao');
        return $query->result();
    }
    
    /**
     * Método que retorna as Avaliações com os conceitos, no qual o Aluno está matriculado
     * @param type $cd_disciplina
     * @param type $cd_usuario
     * @return type
     */
    public function getAvaliacaoConceitoAluno($cd_disciplina, $cd_usuario) {
        $query = $this->db->query("select distinct avaliacao.cd_avaliacao, avaliacao.nm_avaliacao, ds_avaliacao, dt_data_cadastro, dt_data_final, ds_conceito, USUARIO_cd_usuario
            from avaliacao_usuario
            INNER JOIN avaliacao on avaliacao.cd_avaliacao = avaliacao_usuario.AVALIACAO_cd_avaliacao
            INNER JOIN `tipo_avaliacao` ON `tipo_avaliacao`.`cd_avaliacao` = `avaliacao`.`TIPO_AVALIACAO_cd_avaliacao` 
            WHERE md5(USUARIO_cd_usuario) = " . "'$cd_usuario'" .
            " and avaliacao_usuario.AVALIACAO_cd_avaliacao in (select distinct cd_avaliacao from avaliacao where `avaliacao`.`DISCIPLINA_cd_disciplina` = " . "'$cd_disciplina'" . ")");
         return $query->result();
    }
    
    /**
     * Método que retorna os Tipo de Avaliações
     * @return type
     */
    public function getTipoAvaliacao() {
        $this->db->where('fl_excluido', 0);
        $query = $this->db->get('tipo_avaliacao');
        return $query->result();
    }
    
    /**
     * Método que retorna os dados de uma determinada Avaliação
     * @param type $cd_avaliacao
     * @return type
     */
    public function getAvaliacao($cd_avaliacao) {
        $this->db->select('avaliacao.cd_avaliacao, avaliacao.DISCIPLINA_cd_disciplina, avaliacao.TIPO_AVALIACAO_cd_avaliacao, avaliacao.nm_avaliacao, avaliacao.ds_avaliacao, avaliacao.palavras_chave_avaliacao, avaliacao.dt_data_cadastro, avaliacao.dt_data_final, tipo_avaliacao.cd_avaliacao as cd_tipo_avaliacao, nm_tipo_avaliacao');
        $this->db->join('tipo_avaliacao', 'tipo_avaliacao.cd_avaliacao = avaliacao.TIPO_AVALIACAO_cd_avaliacao', 'inner');
        $this->db->where('avaliacao.fl_excluido', 0);
        $this->db->where('avaliacao.cd_avaliacao', $cd_avaliacao);
        $query = $this->db->get('avaliacao');

        return $query->result();
    }
    
    /**
     * Método que retorna dos dados das Avaliações que estão pendentes de um determinado Aluno
     * @param type $cd_usuario
     * @return type
     */
    public function getAvaliacaoPendente($cd_usuario) {
        date_default_timezone_set('America/Sao_Paulo');
        $data = date('Y-m-d');
        
        $this->db->join('disciplina_usuario', 'disciplina_usuario.DISCIPLINA_cd_disciplina = avaliacao.DISCIPLINA_cd_disciplina');
        $this->db->where('disciplina_usuario.USUARIO_cd_usuario', $cd_usuario);
        $this->db->where('avaliacao.dt_data_final >=', $data);
        $this->db->where('avaliacao.fl_excluido', 0);
        $query = $this->db->get('avaliacao');
        return $query->result();
    }
}
