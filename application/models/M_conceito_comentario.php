<?php

class M_conceito_comentario extends CI_Model {

    public function get_alunos() {
        $this->db->where('TIPO_USUARIO_cd_tipo_usuario', 3);
        $this->db->order_by('nm_usuario');
        $query = $this->db->get('usuario');
        return $query->result();
    }
    
    public function get_aluno($cd_usuario) {
        $this->db->where('TIPO_USUARIO_cd_tipo_usuario', 3);
        $this->db->where('cd_usuario', $cd_usuario);
        $query = $this->db->get('usuario');
        return $query->result();
        
    }
    
    public function get_alunos_de_curso($cd_curso) {
        $this->db->select('usuario.nm_usuario, usuario.cd_usuario');
        $this->db->distinct('usuario.nm_usuario');
        $this->db->join('disciplina_usuario', 'disciplina_usuario.USUARIO_cd_usuario = usuario.cd_usuario');
        $this->db->join('disciplina', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina');
        $this->db->join('semestre', 'semestre.cd_semestre = disciplina.SEMESTRE_cd_semestre');
        $this->db->join('curso', 'curso.cd_curso = semestre.CURSO_cd_curso');
        $this->db->where('curso.cd_curso', $cd_curso);
        $query = $this->db->get('usuario');
        return $query->result();
    }
    
    public function get_aluno_de_curso($cd_curso, $cd_usuario) {
        $this->db->select('usuario.nm_usuario, usuario.cd_usuario');
        $this->db->distinct('usuario.nm_usuario');
        $this->db->join('disciplina_usuario', 'disciplina_usuario.USUARIO_cd_usuario = usuario.cd_usuario');
        $this->db->join('disciplina', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina');
        $this->db->join('semestre', 'semestre.cd_semestre = disciplina.SEMESTRE_cd_semestre');
        $this->db->join('curso', 'curso.cd_curso = semestre.CURSO_cd_curso');
        $this->db->where('curso.cd_curso', $cd_curso);
        $this->db->where_in('usuario.cd_usuario', $cd_usuario);
        $query = $this->db->get('usuario');
        return $query->result();
    }
    
    public function get_cursos() {
        $query = $this->db->get('curso');
        return $query->result();
    }
    
    public function get_curso($cd_curso) {
        $this->db->where('cd_curso', $cd_curso);
        $query = $this->db->get('curso');
        return $query->result();
    }
    
    public function get_conceito_comentario($cd_curso, $cd_aluno = NULL) {
        $this->db->select('studentlife.curso.nm_curso, studentlife.curso.sg_curso, studentlife.semestre.nr_semestre,studentlife.usuario.nm_usuario, studentlife.tipo_avaliacao.nm_tipo_avaliacao, studentlife.disciplina.nm_disciplina, studentlife.avaliacao.ds_avaliacao, studentlife.avaliacao.nm_avaliacao, studentlife.avaliacao_usuario.ds_conceito, sd.comentarios.ds_conceito_comentario');
        $this->db->join('usuario', 'studentlife.usuario.cd_usuario = studentlife.avaliacao_usuario.USUARIO_cd_usuario');
        $this->db->join('avaliacao', 'studentlife.avaliacao.cd_avaliacao = studentlife.avaliacao_usuario.AVALIACAO_cd_avaliacao');
        $this->db->join('tipo_avaliacao', 'studentlife.tipo_avaliacao.cd_avaliacao = studentlife.avaliacao.TIPO_AVALIACAO_cd_avaliacao');
        $this->db->join('disciplina', 'studentlife.disciplina.cd_disciplina = studentlife.avaliacao.DISCIPLINA_cd_disciplina');
        $this->db->join('semestre', 'studentlife.semestre.cd_semestre = studentlife.disciplina.SEMESTRE_cd_semestre');
        $this->db->join('curso', 'studentlife.curso.cd_curso = studentlife.semestre.CURSO_cd_curso');
        $this->db->join('sd.comentarios', 'sd.comentarios.cd_aluno = studentlife.usuario.cd_usuario');
        $this->db->where('sd.comentarios.cd_avaliacao = studentlife.avaliacao_usuario.AVALIACAO_cd_avaliacao');
        $this->db->where('studentlife.semestre.CURSO_cd_curso', $cd_curso);
        $this->db->where_in('studentlife.avaliacao_usuario.USUARIO_cd_usuario', $cd_aluno);
        $this->db->order_by('studentlife.usuario.nm_usuario');
        $query = $this->db->get('studentlife.avaliacao_usuario');
        return $query->result();
    }
    
}
