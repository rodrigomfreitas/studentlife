<?php

class M_curso extends CI_Model {

    public function insert($data) {
        if ($this->db->insert('curso', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function update($data) {
        $this->db->where('cd_curso', $data['cd_curso']);
        return $this->db->update('curso', $data);
    }

    public function getCursos() {
        $this->db->select('curso.cd_curso, curso.nm_curso, curso.sg_curso, curso.dt_curso_inicio, curso.nr_semestre_total');
        $this->db->select('DATE_FORMAT(curso.dt_curso_inicio, "%Y") AS ano', false);
        $this->db->where('fl_excluido', 0);
        $this->db->order_by('nm_curso');
        $query = $this->db->get('curso');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }

    public function getCurso($cd_curso) {
        $this->db->where('fl_excluido', 0);
        $this->db->where('cd_curso', $cd_curso);
        $query = $this->db->get('curso');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }
    
    public function getCursoPorAno($cd_ano) {
        $this->db->select('curso.cd_curso, sg_curso, nm_curso, nr_semestre_total, cd_ano');
        $this->db->join('curso', 'curso.cd_curso = ano.CURSO_cd_curso', 'inner');
        $this->db->where('curso.fl_excluido', 0);
        $this->db->where('cd_ano', $cd_ano);
        $query = $this->db->get('ano');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }

    public function getCursosComAnos() {
        $this->db->select('curso.cd_curso, curso.nm_curso, curso.sg_curso, curso.dt_curso_inicio, curso.nr_semestre_total, semestre.CURSO_cd_curso');
        $this->db->distinct('semestre.CURSO_cd_curso');
        $this->db->select('DATE_FORMAT(curso.dt_curso_inicio, "%Y") AS ano', false);
        $this->db->join('semestre', 'semestre.CURSO_cd_curso = curso.cd_curso', 'left');
        $this->db->join('ano', 'ano.cd_ano = semestre.ANO_cd_ano', 'left');
        $this->db->where('curso.fl_excluido', 0);
        $this->db->order_by('nm_curso');
        $query = $this->db->get('curso');
        return ($query->num_rows() != 0) ? $query->result() : array();
    }
    
    public function getCursosAlunoMatriculado($cd_usuario) {
        $this->db->select('cd_curso, nm_curso, sg_curso');
        $this->db->select('DATE_FORMAT(curso.dt_curso_inicio, "%Y") AS ano', false);
        $this->db->distinct('cd_curso');
        $this->db->join('semestre', 'curso.cd_curso = semestre.CURSO_cd_curso');
        $this->db->join('disciplina', 'disciplina.SEMESTRE_cd_semestre = semestre.cd_semestre');
        $this->db->join('disciplina_usuario', 'disciplina.cd_disciplina = disciplina_usuario.DISCIPLINA_cd_disciplina');
        $this->db->where('disciplina_usuario.USUARIO_cd_USUARIO', $cd_usuario);
        $this->db->where('curso.fl_excluido', 0);
        $query = $this->db->get('curso');
        return $query->result();
    }

//    public function getSemestreTotalCurso($cd_curso) {
//        $this->db->select('curso.nr_semestre_total');
//        $this->db->where('fl_excluido', 0);
//        $this->db->where('cd_curso', $cd_curso);
//        $query = $this->db->get('curso');
//        return ($query->num_rows() != 0) ? $query->result() : array();
//    }

}
