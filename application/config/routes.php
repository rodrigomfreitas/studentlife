<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['logout'] = 'login/logout';
$route['esqueci-minha-senha'] = 'login/esqueci_minha_senha';

$route['turmas'] = 'turmas/index';
$route['turmas/ano/(:num)'] = 'turmas/turmasAno/$1';
$route['turmas/curso/(:num)'] = 'turmas/turmasCurso/$1';
$route['turmas/turma-informacoes/(:num)'] = 'turmas/turmasInformacoes/$1';

$route['cursos'] = 'cursos/index';

$route['anos/(:num)'] = 'anos/index/$1';

$route['semestres/(:num)'] = 'semestres/index/$1';

$route['backup-sistema'] = 'backup/index';

$route['disciplinas/(:num)'] = 'disciplinas/index/$1';
$route['disciplina/(:num)'] = 'disciplinas/disciplinaInterna/$1';

$route['usuario/(:any)'] = 'usuario/index/$1';
$route['usuario/inc_foto'] = 'usuario/inc_foto/index/';
$route['editar-perfil/(:any)'] = 'usuario/formAlterar/$1';
$route['editar-foto/(:any)'] = 'usuario/formAlterarFoto/$1';
$route['usuario/inserirFoto'] = 'usuario/inserirFoto/';

$route['mensagem/(:num)'] = 'mensagem/index/$1';

$route['disciplina/inserir-aluno/(:num)'] = 'aluno_disciplina/index/$1';
$route['disciplina/avaliacao/(:num)'] = 'avaliacao/index/$1';
$route['disciplina/conceitos/(:num)/(:num)'] = 'avaliacao_usuario/index/$1/$2';
$route['disciplina/desempenho-aluno/(:num)/(:num)'] = 'graficos/index/$1/$2';
$route['disciplina/informacoes-aluno/(:num)/(:any)'] = 'usuario/informacoesAluno/$1/$2';
$route['disciplina/material/aulas/(:num)'] = 'aulas/index/$1';
$route['disciplina/material/(:num)/(:num)'] = 'documentos/index/$1/$2';

$route['disciplina/professor/(:num)'] = 'professorDisciplina/index/$1';

$route['disciplina/enviar-email/(:num)'] = 'enviaEmail/index/$1';

$route['conceitos/(:num)'] = 'conceitos/index/$1';
$route['conceito-comentario'] = 'conceito_comentario/index';

$route['recomendacoes/(:num)'] = 'recomendacoes/index/$1';
$route['recomendacoes/download/(:num)'] = 'recomendacoes/download/$1';

$route['webservice/(:num)'] = 'webservice/index/$1';

$route['enviar-solicitacao-registrar-turma'] = 'login/enviarSolicitacao';