{extends file="templates/template.tpl"}

{block name="tittle"}<title>Pagina Inicial | Student Life</title>{/block}

{block name="imports"}  {/block}

{block name="content"}

    <div id="wrapper">

        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="#">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h2 class="font-light m-b-xs">
                        Dashboard
                    </h2>
                </div>
            </div>
        </div>

        <div class="content animate-panel" data-effect="fadeIn" data-child="hpanel">
            <div class="row">
                <!-- Exibe as recomendações de material para o aluno, as mensagens não lidas e as atividades pendentes -->
                {if $sessao_dados['status'] == 3}
                    <div class="col-md-9 border-right pull-right">
                        <div class="hpanel">
                            <!-- TimeLine das notificações -->
                            {foreach $avisoReprovacao as $aviso}
                                <div class="v-timeline vertical-container animate-panel" data-child="vertical-timeline-block" data-delay="1">
                                    <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon navy-bg">
                                            <i class="fa fa-book"></i>
                                        </div>
                                        <div class="vertical-timeline-content">
                                            <div class="p-sm">
                                                {*                                                <span class="vertical-date pull-right"> Saturday <br> <small>12:17:43 PM</small> </span>*}
                                                <h2>Recomendação de conteúdo para estudo</h2>
                                                <p>
                                                    Olá <strong>{$aviso->nm_usuario}</strong>, você foi reprovado em uma avaliação (<strong>{$aviso->nm_tipo_avaliacao}</strong>).<br>
                                                    <strong>Avaliação:</strong> {$aviso->nm_avaliacao}.<br>
                                                    <strong>Disciplina:</strong> {$aviso->nm_disciplina} no qual {if $aviso->ds_conceito == SC} ficou <strong>Sem Conceito (S/C)</strong>.{else}ficou com o <strong>conceito {$aviso->ds_conceito}</strong>.{/if}<br>
                                                    Estamos recomendando materiais para reforçar seus estudos.
                                                    Clique no botão abaixo para visualizar os materiais.                                                
                                                </p>
                                            </div>
                                            <div class="panel-footer text-right">
                                                <div class="btn-group">
                                                    <a href="{base_url}recomendacoes/{$aviso->cd_avaliacao}" class="btn btn-sm btn-primary">Visualizar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                            <!-- Fim da TimeLine das notificações --> 
                        </div>
                    </div>
                {/if}
                <div class="col-md-3">
                    <div class="hpanel hblue">
                        <div class="panel-body text-center h-200">
                            <i class="pe-7s-mail fa-4x"></i>
                            {if $nao_lidas[0]->numrows > 0}
                                <h1 class="m-xs">{$nao_lidas[0]->numrows}</h1>

                                <h5 class="font-light no-margins">
                                    {if $nao_lidas[0]->numrows == 1}
                                        Mensagem não lida
                                    {else}
                                        Mensagens não lidas
                                    {/if}
                                </h5>
                                <small>Neste box você fica sabendo de quantas mensagens ainda não leu.</small>
                            </div>
                            <div class="panel-footer text-center">
                                <a href="{base_url}mensagem" class="btn-info btn btn-sm">Ler mensagem</a>
                            </div>
                        {else}
                            <h1 class="m-xs">{$nao_lidas[0]->numrows}</h1>

                            <h5 class="font-light no-margins">
                                Nenhuma nova mensagem
                            </h5>
                            <small>Neste box você fica sabendo de quantas mensagens ainda não leu.</small>
                        </div>
                    {/if}
                </div>
                {if $sessao_dados['status'] == 3}
                    <div class="hpanel hred">
                        <div class="panel-body text-center h-200">
                            <i class="pe-7s-mail fa-4x"></i>
                            {if $avaliacoesPendentes|count > 0}
                                <h1 class="m-xs">{$avaliacoesPendentes|count}</h1>

                                <h5 class="font-light no-margins">
                                    {if $avaliacoesPendentes > 0}
                                        Atividade à realizar
                                    {else}
                                        Atividades à realizarem
                                    {/if}
                                </h5>
                                <small>Neste box você fica sabendo de quantas atividades ainda não realizou.</small>
                            </div>
                            <div class="panel-footer text-center">
                                <a href="{base_url}" class="btn-danger btn btn-sm">Visualizar Atividades Pendentes</a>
                            </div>
                        {else}
                            <h1 class="m-xs">{$avaliacoesPendentes|count}</h1>

                            <h5 class="font-light no-margins">
                                Nenhuma nova atividade
                            </h5>
                            <small>Neste box você fica sabendo de quantas atividades ainda não realizou.</small>
                        </div>
                    {/if}
                {/if}
                {if $sessao_dados['status'] == 2}
                    <div class="hpanel hyellow">
                        <div class="panel-heading hbuilt text-center">
                            Disciplinas no qual você está ministrando as aulas
                        </div>
                        <div class="panel-body no-padding" style="display: block;">
                            <ul class="list-group">
                                {if $disciplinas|count > 0}
                                    {foreach $disciplinas as $d}
                                        <li class="list-group-item">
                                            {$d->nm_disciplina}
                                            <a href="{base_url}disciplina/{$d->cd_disciplina}" class="label label-warning pull-right">Entrar</a>
                                        </li>
                                    {/foreach}
                                {else}
                                    <li class="list-group-item">
                                        Nenhuma Disciplina até o momento
                                    </li>
                                {/if}
                            </ul>
                        </div>
                    </div>
                {/if}
                <!-- Fim da exibição as recomendações de material para o aluno, as mensagens não lidas e as atividades pendentes -->
            </div>
        </div>
        {include file="templates/footer.tpl"}
    </div>

{/block}
{block name="validation-script"}
    <script>
        $(document).ready(function () {
            $('.home').addClass('active');
        });
    </script>
{/block}
