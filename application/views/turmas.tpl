{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Turmas </h3>
                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">  <i class="fa fa-plus"></i> &nbsp; Adicionar </button>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>

                            <li>
                                <a href="{base_url}turmas">Turmas - Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}turmas/ano/{$curso[0]->cd_curso}">Anos</a>
                            </li>
                            <li>
                                <span>Turmas do {$curso[0]->sg_curso}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$curso[0]->nm_curso}</h5>
                </div>
            </div>
        </div>

        <div class="content animate-panel" data-effect="fadeIn" data-child="hpanel">

            <div class="row">
                {if $turmas|@count < 1}
                    <h1>Nenhuma Turma Cadastrada</h1>

                {/if}

                {foreach $turmas as $turma}
                    <div class="col-md-3">
                        <div class="hpanel hgreen">
                            <div class="panel-body">
                                <!-- Botão de ação -->
                                <div class="text-right">
                                    <div class="input-group-btn">
                                        <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"><i class="fa fa-gear"></i></button>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:;" class="editar-turma" data-id="{$turma->cd_turma}" ><i class="fa fa-pencil"></i>&nbsp;Editar</a></li>
                                            <li><a href="javascript:;" class="excluir-turma" data-id="{$turma->cd_turma}"><i class="fa fa-trash"></i>&nbsp;Remover</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Fim do Botão de ação -->
                                <div class="text-uppercase pull-left">
                                    <h4>{$turma->sg_curso}/{$turma->nr_semestre}{$turma->nm_turno|truncate:1:"":true}/{$turma->dt_turma|date_format:"%Y"}</h4>
                                </div>
                                <div class="stats-icon pull-right">
                                    <i class="pe-7s-study fa-4x"></i>
                                </div>
                                <div class="m-t-xl">
                                    <h2 class="text-success">
                                        {if $alunos_count[$turma->cd_turma] > 0}
                                            {$alunos_count[$turma->cd_turma]} <small> Alunos</small>
                                        {else}
                                            {$alunos_count[$turma->cd_turma]} <small> Aluno</small>
                                        {/if}
                                    </h2>
                                    <p>
                                        <strong>{$turma->nr_semestre}&ordm; semestre</strong>. Turno: <strong>{$turma->nm_turno}</strong>
                                    </p>
                                </div>
                                <a {if $turma->cd_disciplina == NULL} class="btn btn-sm btn-success btn-block btn-default info-turma" data-toggle="modal" data-target="#info-turma" data-id="{$turma->SEMESTRE_cd_semestre}"{else}href="{base_url}turmas/turma-informacoes/{$turma->cd_turma}" class="btn btn-success btn-block"{/if} >Entrar</a>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>

    <!-- Modal de incluir turma -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Incluir Turma</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-incluir-turma" action="{base_url}turmas/incluir" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_curso" id="cd_curso" value="{$curso[0]->cd_curso}">
                    <input type="hidden" name="cd_ano" value="{$curso[0]->cd_ano}">
                    <div class="modal-body">
                        <div id="validation-error"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label control-label-required">Semestre Atual</label>
                            <div class="col-sm-4">
                                <select name="semestre" id="semestre" class="form-control">
                                    <option></option>
                                    {foreach $nrsemestre as $semestre}
                                        <option value="{$semestre->cd_semestre}">{$semestre->nr_semestre}&ordm; Semestre </option>
                                    {/foreach}
                                </select>
                            </div>
                            <label class="col-sm-1 control-label control-label-required">Turno</label>
                            <div class="col-sm-4">
                                <select name="turno" id="turno" class="form-control">
                                    <option></option>
                                    {foreach $turnos as $turno}
                                        <option value="{$turno->cd_turno}">{$turno->nm_turno}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label control-label-required">Ano da Turma</label>
                            <div class="col-sm-5"><input type="date" name="ano_turma" id="ano_turma" class="form-control"></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Todos os campos são obrigatórios</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal de incluir turma -->

    <!-- Modal de editar turma -->
    <div class="modal fade" id="editar-turma-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Editar Turma</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-editar-turma" action="{base_url}turmas/atualizar" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_curso" value="{$curso[0]->cd_curso}">
                    <input type="hidden" name="cd_ano" value="{$curso[0]->cd_ano}">
                    <div class="modal-body">
                        <div id="validation-error-edit"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label control-label-required">Semestre Atual</label>
                            <div class="col-sm-4">
                                <select name="semestre" id="semestre" class="form-control">
                                    <option></option>
                                    {foreach $nrsemestre as $semestre}
                                        <option value="{$semestre->cd_semestre}">{$semestre->nr_semestre}&ordm; Semestre </option>
                                    {/foreach}
                                </select>
                            </div>
                            <label class="col-sm-1 control-label control-label-required">Turno</label>
                            <div class="col-sm-4">
                                <select name="turno" class="form-control">
                                    <option></option>
                                    {foreach $turnos as $turno}
                                        <option value="{$turno->cd_turno}">{$turno->nm_turno}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-3 control-label control-label-required">Ano da Turma</label>
                            <div class="col-sm-5"><input type="date" name="ano_turma" id="ano_turma" class="form-control"></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Todos os campos são obrigatórios</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                    <input type="hidden" name="cd_turma" value="">
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal de editar turma -->

    <!-- Modal de Informação caso não não exista disciplina em um determinado semestre -->
    <div class="modal fade hmodal-info" id="info-turma" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <h4 class="modal-title">Informações para o próximo passo</h4>
                    <small class="font-bold">Para seguir em frente leia as instruções abaixo.</small>
                </div>
                <div class="modal-body">
                    <p>Não foi possível prosseguir ao próximo passo, porque é preciso primeiramente cadastrar, pelo menos, uma <strong>disciplina</strong>.<br>
                        Clique no botão <strong>Cadastrar</strong> para entrar no módulo de cadastrar disciplina.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <a id="url-turma" href="" class="btn btn-primary">Cadastrar</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Fim do modal de Informação caso não não exista disciplina em um determinado semestre -->
{/block}

{block name="validation-script"}
    <script>
        $(document).ready(function () {

            var modal = $("#info-turma");
            $('.info-turma').on("click", function () {
                var cd_semestre = $(this).data("id");
                $("#url-turma").attr("href", "{base_url}disciplinas/" + cd_semestre);
            });
        });
    </script>
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-turma').submit(function () {
                $('button[type=submit], input[type=submit]').prop('disabled', true);

                $.post($('#form-incluir-turma').attr('action'), $('#form-incluir-turma').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 1500);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <script>
        $(document).ready(function () {

            var modal = $("#editar-turma-modal");
            $('.editar-turma').on("click", function () {
                var cd_turma = $(this).data("id");
                $.get(
                        "{base_url}turmas/editar/" + cd_turma,
                        function (turma) {
                            modal.find('[name="semestre"]').val(turma.SEMESTRE_cd_semestre);
                            modal.find('[name="turno"]').val(turma.TURNO_cd_turno);
                            modal.find('[name="ano_turma"]').val(turma.dt_turma);
                            modal.find('[name="cd_turma"]').val(turma.cd_turma);

                            modal.modal("show");

                        }
                );
            });
            modal.on("hide.bs.modal", function () {
                modal.find('[name="semestre"]').val("");
                modal.find('[name="turno"]').val("");
                modal.find('[name="ano_turma"]').val("");
                modal.find('[name="cd_turma"]').val("");
                modal.find('[id="validation-error-edit"]').removeClass('alert alert-warning');
                modal.find('[id="validation-error-edit"]').find('p').remove();
            });

            modal.find("#form-editar-turma").on("submit", function (e) {
                e.preventDefault();
                $.post($('#form-editar-turma').attr('action'), $('#form-editar-turma').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error-edit').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error-edit').removeClass('alert alert-warning');
                        $('#validation-error-edit > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });

            $("#myModal").on("hide.bs.modal", function () {
                $(this).find('[name="semestre"]').val("");
                $(this).find('[name="turno"]').val("");
                $(this).find('[name="ano_turma"]').val("");
                $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
                $(this).find('[id="validation-error"]').find('p').remove();
            });
        });
    </script>

    <script>
        $(function () {
            $('.excluir-turma').click(function () {
                var cd_curso = $(this).data("id");
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá a turma com suas dependências",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}turmas/excluir/" + cd_curso,
                                function () {
                                    swal("Removida!", "A turma foi removida com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "A turma não foi removida :)", "error");
                    }
                });
            });

            $('.homerDemo4').click(function () {
                toastr.error('Error - This is a Homer error notification');
            });

        });
    </script>
{/block}
