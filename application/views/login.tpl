<html><head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Student Life Login</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="{base_url}assets/vendor/fontawesome/css/font-awesome.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/metisMenu/dist/metisMenu.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/animate.css/animate.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/toastr/build/toastr.min.css">

        <!-- App styles -->
        <link rel="stylesheet" href="{base_url}assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
        <link rel="stylesheet" href="{base_url}assets/fonts/pe-icon-7-stroke/css/helper.css">
        <link rel="stylesheet" href="{base_url}assets/styles/style.css">
        <link rel="stylesheet" href="{base_url}assets/styles/static_custom.css">

        <style type="text/css"></style><style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>
    <body class="blank">



        <div class="color-line"></div>


        <div class="login-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center m-b-md">
                        <p><img src="{base_url}assets/images/gallery/logo1.png" alt=""></p>
                        <h6>Gerenciamento Estudantil</h6>
                    </div>
                    <div class="hpanel">
                        <div class="panel-body">
                            <form id="login" method="post" action="{base_url}login/logar">
                                <div class="form-group">
                                    <label class="control-label" for="username">E-mail</label>
                                    <input type="email" placeholder="exemplo@gmail.com" title="Por favor insira seu e-mail" value="" name="email" id="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Senha</label>
                                    <input type="password" title="Por favor, insira sua senha" placeholder="******" value="" name="senha" id="senha" class="form-control">
                                </div>
                                <button class="btn btn-success btn-block" type="submit">Logar</button>
                                <a class="btn btn-default btn-block" href="{base_url}register">Registrar</a>
                            </form>
                            <a class="pull-right btn btn-link" href="{base_url}esqueci-minha-senha">Esqueci minha senha</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <strong>Student Life</strong>  <br>  Gerenciamento Estudantil
                </div>
            </div>
        </div>


        <!-- Vendor scripts -->
        <script src="{base_url}assets/vendor/jquery/dist/jquery.min.js"></script>
        <script src="{base_url}assets/vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="{base_url}assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="{base_url}assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="{base_url}assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="{base_url}assets/vendor/iCheck/icheck.min.js"></script>
        <script src="{base_url}assets/vendor/sparkline/index.js"></script>
        <script src="{base_url}assets/vendor/toastr/build/toastr.min.js"></script>

        <script type="text/javascript">
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            /*
             $('#form-login').submit(function (event) {
             base_url = '{base_url}';
             event.preventDefault();
             var email = $('#email').val();
             var senha = $('#senha').val();
             
             if (email == '' || senha == '') {
             
             Alert("error", "Atenção", "O campo Usuário e Senha são obrigatórios.");
             
             } else {
             
             var dados = jQuery(this).serialize();
             console.log(dados);
             jQuery.ajax({
             type: "POST",
             url: base_url + "login/logar",
             data: dados,
             success: function (data)
             {
             if (data == 1) {
             setTimeout(function () {
             window.open(base_url + "home", '_self');
             }, 2000, Alert("success", "Sucesso", "Você foi logado com sucesso."));
             
             
             } else if (data == 2) {
             $("#form-login").each(function () {
             this.reset();
             });
             Alert("error", "Atenção", "E-mail ou Senha inválidos.");
             
             
             } else if (data == 3) {
             setTimeout(function () {
             window.open(base_url + "login", '_self');
             }, 7000, Alert("info", "Informação", "Seu cadastro consta em nosso site, mas não está liberado para acesso. Verifique seu e-mail para a liberação."));
             
             } else {
             Alert("error", "ERRO", "Ocorreu um Erro, por favor, tente novamente");
             }
             
             }
             });
             return false;
             }
             });
             */
            $(document).ready(function () {
                base_url = '{base_url}';
                $('#login').submit(function () {
                    $.post($('#login').attr('action'), $('#login').serialize(), function (data) {
                        if (data.st == 0)
                        {
                            toastr.error(data.msg);
                            $("#senha").val("");
                        }
                        else if (data.st == 1)
                        {
                            setTimeout(function () {
                                window.open(base_url + "home", '_self');
                            }, 1000, toastr.success(data.msg));
                        }
                        else if (data.st == 2)
                        {
                            toastr.info(data.msg);
                            $("#senha").val("");
                            $('#senha').focus();
                        }
                    }, 'json');
                    return false;

                });
            });
        </script>




    </body></html>
