{extends file="templates/template.tpl"}
{block name="imports"}
    <link rel="stylesheet" href="{base_url}assets/vendor/bootstrap-tokenfield/dist/css/bootstrap-tokenfield.min.css">
    <link rel="stylesheet" href="{base_url}assets/vendor/bootstrap-tokenfield/dist/css/tokenfield-typeahead.min.css">
{/block}
{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplina de <strong>{$dados_disciplina->nm_disciplina}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}anos/{$dados_disciplina->CURSO_cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}semestres/{$dados_disciplina->ANO_cd_ano}">Semestres</a>
                            </li>
                            <li>
                                <a href="{base_url}disciplinas/{$dados_disciplina->cd_semestre}">Disciplinas</a>
                            </li>
                            <li>
                                <span>{$dados_disciplina->nm_disciplina}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados_disciplina->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row projects">
                {include file="templates/disciplina_menu.tpl"}
                <!-- Conteúdo do material -->
                <div class="col-md-9">
                    <div class="hpanel">
                        <div class="panel-heading hbuilt">
                            <div class="p-xs h4">
                                <small class="pull-right">
                                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus"></i> &nbsp;Adicionar</button>
                                </small>                               
                                <strong>Materiais da Aula {$aula[0]->nr_aula}</strong>
                            </div>
                        </div>
                        <div class="panel-body">
                            {if $sessao_dados['status'] != 3}
                                {if $materiais|@count < 1}
                                    <div class="col-md-12">
                                        <h1>Nenhum Material Cadastrado</h1>
                                    </div>
                                {/if}
                            {else}
                                {if $materiais|@count < 1}
                                    <div class="col-md-12">
                                        <h1>Nenhum Material inserido até o momento.</h1>
                                    </div>
                                {/if}
                            {/if}
                            <div class="col-lg-12">
                                <div class="hpanel">
                                    <div class="panel-body" style="display: block;">
                                        <div class="table-responsive">
                                            <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Arquivo</th>
                                                        <th>Nome</th>
                                                        <th>Descrição</th>
                                                        <th>Palavras-chave</th>
                                                        <th>Tipo do Documento</th>
                                                        <th>Ação</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {foreach $materiais as $material}
                                                        <tr>
                                                            <td>{$material->ds_arquivo}</td>
                                                            <td>{$material->ds_nomenclatura}</td>
                                                            <td>{$material->ds_descricao}</td>
                                                            <td>{$material->ds_palavras_chave}</td>
                                                            <td>{$material->ds_tipo_documento}</td>
                                                            <td style="width: 15%" class="text-center">
                                                                <a class="btn btn-primary2 btn-xs visibilidade-material" data-id="{$material->cd_documento}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visibilidade"><i class="fa fa-eye"></i></a>
                                                                <a class="btn btn-info btn-xs editar-material" data-id="{$material->cd_documento}" data-toggle="tooltip" data-placement="left" title="" data-original-title="Editar"><i class="fa fa-edit"></i></a>
                                                                <a class="btn btn-danger btn-xs excluir-material" data-id="{$material->cd_documento}" data-toggle="tooltip" data-placement="left" title="" data-original-title="Remover"><i class="fa fa-remove"></i></a>
                                                            </td>
                                                        </tr>
                                                    {/foreach}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="panel-footer" style="display: block;">
                                        Total de linhas: <strong>{$materiais|count} </strong>
                                    </div>
                                </div>
                            </div>
                            <!-- Fim do conteúdo do material -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
    <!-- Modal incluir material -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Incluir Material</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-incluir-material" action="{base_url}documentos/incluir" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="cd_aula" id="cd_aula" value="{$aula[0]->cd_aula}">
                    <div class="modal-body">
                        <div id="validation-error"></div>
                        <div class="form-group"><label for="arquivo" class="col-sm-4 control-label control-label-required">Carregue o Arquivo</label>
                            <div class="col-sm-8"><input type="file" name="arquivo" id="arquivo" class="form-control"></div>
                        </div>
                        <div class="form-group"><label for="ds_nomenclatura" class="col-sm-4 control-label control-label-required">Nome do material</label>
                            <div class="col-sm-8"><input type="text" name="ds_nomenclatura" id="ds_nomenclatura" class="form-control"></div>
                        </div>
                        <div class="form-group"><label for="ds_descricao" class="col-sm-4 control-label control-label-required">Descrição</label>
                            <div class="col-sm-8"><textarea type="text" rows="5" name="ds_descricao" id="ds_descricao" class="form-control"></textarea></div>
                        </div>
                        <div class="form-group"><label for="ds_palavras_chave" class="col-sm-4 control-label control-label-required">Palavras-chave</label>
                            <div class="col-sm-8">
                                <div><input type="text" class="form-control" name="ds_palavras_chave" id="tokenfieldInsert" class="form-control"/></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* O campo é obrigatório</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal incluir material -->
    <!-- Modal editar material -->
    <div class="modal fade" id="editar-material-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Editar Disciplina</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-editar-material" action="{base_url}documentos/atualizar" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="cd_aula" id="cd_aula" value="{$aula[0]->cd_aula}">
                    <div class="modal-body">
                        <div id="validation-error-edit"></div>
                        <div class="form-group"><label for="arquivo" class="col-sm-4 control-label control-label-required">Carregue o Arquivo</label>
                            <div class="col-sm-8"><input type="file" name="arquivo" id="arquivo" class="form-control"></div>
                        </div>
                        <div class="form-group"><label for="ds_nomenclatura" class="col-sm-4 control-label control-label-required">Nome do material</label>
                            <div class="col-sm-8"><input type="text" name="ds_nomenclatura" id="ds_nomenclatura" class="form-control"></div>
                        </div>
                        <div class="form-group"><label for="ds_descricao" class="col-sm-4 control-label control-label-required">Descrição</label>
                            <div class="col-sm-8"><textarea type="text" rows="5" name="ds_descricao" id="ds_descricao" class="form-control"></textarea></div>
                        </div>
                        <div class="form-group"><label for="ds_palavras_chave" class="col-sm-4 control-label control-label-required">Palavras-chave</label>
                            <div class="col-sm-8">
                                <div><input type="text" class="form-control" name="ds_palavras_chave" id="tokenfieldEdit" class="form-control"/></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* O campo é obrigatório</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                    <input type="hidden" name="cd_documento" value="">
                    <input type="hidden" name="ds_arquivo" id="ds_arquivo" value="">
                    <input type="hidden" name="ds_tipo_documento" id="ds_tipo_documento" value="">
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal editar material -->
    <!-- Modal incluir Turma(s) a um material -->
    <div class="modal fade" id="visibilidade-material-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Dar Visibilidade</h4>
                    <small class="font-bold">Escolha uma ou mais Turmas que irão visualizar este Material</small>
                </div>
                <form id="form-incluir-turma-material" action="{base_url}documentos/incluirVisibilidadeDocumento" method="post" class="form-horizontal">
                    <div class="modal-body">
                        <div class="form-group"><label class="col-sm-4 control-label">Turmas</label>
                            {foreach $turmas as $turma}
                                <div class="checkbox checkbox-success checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox{$turma->cd_turma}" value="{$turma->cd_turma}" name="cd_turmas[]">
                                    <label for="cd_turma"> {$turma->nr_semestre}{$turma->nm_turno|truncate:1:"":true}/{$turma->ano_curso} </label>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                    <input type="hidden" name="cd_disciplina" value="{$dados_disciplina->cd_disciplina}">
                    <input type="hidden" name="cd_documento" value="">
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim da Modal incluir Turma(s) a um material -->
{/block}

{block name="validation-script"}
    <!-- DataTable Scripts -->
    <script src="{base_url}assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- TokenField Script -->
    <script src="{base_url}assets/vendor/bootstrap-tokenfield/dist/js/bootstrap-tokenfield.min.js"></script>
    
    <script src="{base_url}assets/vendor/jqueryform/jquery.form.js"></script>
    <script src="{base_url}assets/vendor/iCheck/icheck.min.js"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#tokenfieldInsert').tokenfield();
        });
    </script>
    <!-- Script para incluir um material e o Alert toastr -->
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        $(document).ready(function () {
            $('#form-incluir-material').submit(function () {
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $('#form-incluir-material').ajaxSubmit(function (data) {
                    if (data.st == 0) {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 3)
                    {
                        toastr.error(data.msg);
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <!-- Script para incluir uma aula e o Alert toastr -->
    <!-- Script para editar um material -->
    <script>
        $(document).ready(function () {

            var modal = $("#editar-material-modal");
            $('.editar-material').on("click", function () {
                var cd_documento = $(this).data("id");
                $.get(
                        "{base_url}documentos/editar/" + cd_documento,
                        function (documento) {
                            modal.find('[name="ds_arquivo"]').val(documento.ds_arquivo);
                            modal.find('[name="ds_nomenclatura"]').val(documento.ds_nomenclatura);
                            modal.find('[name="ds_descricao"]').val(documento.ds_descricao);
                            modal.find('[name="ds_palavras_chave"]').val(documento.ds_palavras_chave);
                            modal.find('[name="ds_tipo_documento"]').val(documento.ds_tipo_documento);
                            modal.find('[name="cd_documento"]').val(documento.cd_documento);
                            $('#tokenfieldEdit').tokenfield();
                            
                            modal.modal("show");

                        }
                );
            });
            modal.on("hide.bs.modal", function () {
                modal.find('[name="arquivo"]').val("");
                modal.find('[name="ds_nomenclatura"]').val("");
                modal.find('[name="ds_descricao"]').val("");
                modal.find('[name="ds_palavras_chave"]').val("");
                modal.find('[name="ds_tipo_documento"]').val("");
                modal.find('[name="cd_documento"]').val("");
                $('#tokenfieldEdit').tokenfield('destroy');
                modal.find('[id="validation-error-edit"]').removeClass('alert alert-warning');
                modal.find('[id="validation-error-edit"]').find('p').remove();
            });

            modal.find("#form-editar-material").on("submit", function (e) {
                e.preventDefault();
                $('#form-editar-material').ajaxSubmit(function (data) {
                    if (data.st == 0) {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error-edit').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error-edit').removeClass('alert alert-warning');
                        $('#validation-error-edit > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error-edit').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 3)
                    {
                        toastr.error(data.msg);
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                    }
                }, 'json');
                return false;

            });

            $("#myModal").on("hide.bs.modal", function () {
                $(this).find('[name="arquivo"]').val("");
                $(this).find('[name="ds_nomenclatura"]').val("");
                $(this).find('[name="ds_descricao"]').val("");
                $(this).find('[name="ds_palavras_chave"]').val("");
                $(this).find('[class="token"]').remove();
                $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
                $(this).find('[id="validation-error"]').find('p').remove();
            });
        });
    </script>
    <!-- Script para editar um material -->
    <!-- Script de visibilidade para incluir turma ao material -->
    <script>
        {*        $(document).ready(function () {*}
        var modalVisibilidade = $("#visibilidade-material-modal");
        $('.visibilidade-material').on("click", function () {
            modalVisibilidade.find('input[type="checkbox"]').iCheck('uncheck');

            var cd_documento = $(this).data("id");
            modalVisibilidade.find('[name="cd_documento"]').val(cd_documento);
            $.get(
                    "{base_url}documentos/atualizarVisibilidadeDocumento/" + cd_documento,
                    function (turmasRelacionadas) {
                        for (var i in turmasRelacionadas) {
                            modalVisibilidade.find('#inlineCheckbox' + turmasRelacionadas[i]).iCheck("check");
                        }
                        modalVisibilidade.modal("show");

                    }
            );
        });
        {*        });*}
    </script>
    <script>
        $(document).ready(function () {
            $('#form-incluir-turma-material').submit(function () {
                $.post($('#form-incluir-turma-material').attr('action'), $('#form-incluir-turma-material').serialize(), function (data) {
                    if (data.st == 1)
                    {
                        $('#visibilidade-material-modal').modal('hide');
                        toastr.success(data.msg);
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;
            });
        });
    </script>
    <!-- Fim do Script de visibilidade para incluir turma ao material -->
    <!-- Script para excluir um material -->
    <script>
        $(function () {
            $('.excluir-material').click(function () {
                var cd_documento = $(this).data("id");
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá o arquivo permanentemente",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}documentos/excluir/" + cd_documento,
                                function () {
                                    swal("Removida!", "O arquivo foi removido com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "O arquivo não foi removido :)", "error");
                    }
                });
            });

            $('.homerDemo4').click(function () {
                toastr.error('Error - This is a Homer error notification');
            });

        });
    </script>
    <!-- Fim do Script para excluir um material -->

{/block}
