{extends file="templates/template.tpl"}
{block name="imports"}
    <link rel="stylesheet" href="{base_url}assets/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css">
{/block}
{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplina de <strong>{$dados_disciplina->nm_disciplina}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}anos/{$dados_disciplina->CURSO_cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}semestres/{$dados_disciplina->ANO_cd_ano}">Semestres</a>
                            </li>
                            <li>
                                <a href="{base_url}disciplinas/{$dados_disciplina->cd_semestre}">Disciplinas</a>
                            </li>
                            <li>
                                <span>{$dados_disciplina->nm_disciplina}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados_disciplina->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row projects">
                {include file="templates/disciplina_menu.tpl"}
                <!-- Conteúdo da aula -->
                <div class="col-md-9">
                    <div class="hpanel">
                        <div class="panel-heading hbuilt">
                            <div class="p-xs h4">
                                <small class="pull-right">
                                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus"></i> &nbsp;Adicionar</button>
                                </small>                               
                                <strong>Aulas</strong>
                            </div>
                        </div>
                        <div class="panel-body">
                            {if $sessao_dados['status'] != 3}
                                {if $aulas|@count < 1}
                                    <div class="col-md-12">
                                        <h1>Nenhuma Aula Cadastrada</h1>
                                    </div>
                                {/if}
                            {else}
                                {if $aulas|@count < 1}
                                    <div class="col-md-12">
                                        <h1>Nenhuma Aula inserida até o momento.</h1>
                                    </div>
                                {/if}
                            {/if}
                            {foreach $aulas as $aula}
                                <div class="col-md-3">
                                    <div class="hpanel hgreen">
                                        <div class="panel-body">
                                            <div class="text-center">
                                                <!-- Botão de ação -->
                                                {if $sessao_dados['status'] != 3}
                                                    <div class="dropdown text-right">
                                                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
                                                            <button class="text-muted btn btn-default"><i class="fa fa-gear text-primary"></i></button>
                                                        </a>
                                                        <ul class="dropdown-menu animated flipInX m-t-xs pull-right">
                                                            <li><a href="javascript:;" class="editar-aula" data-id="{$aula->cd_aula}" ><i class="fa fa-pencil"></i>&nbsp;Editar</a></li>
                                                            <li><a href="javascript:;" class="excluir-aula" data-id="{$aula->cd_aula}"><i class="fa fa-trash"></i>&nbsp;Remover</a></li>
                                                        </ul>
                                                    </div>
                                                {/if}
                                                <!-- Fim do Botão de ação -->
                                                <div class="stats-title text-center">
                                                    <h3>Aula {$aula->nr_aula}</h3>
                                                </div>
                                                <a href="{base_url}disciplina/material/{$dados_disciplina->cd_disciplina}/{$aula->cd_aula}" class="btn btn-sm btn-success btn-block">Entrar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                            <!-- Fim do conteúdo da aula -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
    <!-- Modal incluir aula -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Incluir Aula</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-incluir-aula" action="{base_url}aulas/incluir" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_disciplina" id="cd_disciplina" value="{$dados_disciplina->cd_disciplina}">
                    <div class="modal-body">
                        <div id="validation-error"></div>
                        <div class="form-group"><label class="col-sm-4 control-label control-label-required">Número da Aula</label>
                            <div class="col-sm-3"><input type="number" min="1" name="nr_aula" id="nr_aula" class="form-control"></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* O campo é obrigatório</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal incluir aula -->
    <!-- Modal editar aula -->
    <div class="modal fade" id="editar-aula-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Editar Disciplina</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-editar-aula" action="{base_url}aulas/atualizar" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_disciplina" id="cd_disciplina" value="{$dados_disciplina->cd_disciplina}">
                    <div class="modal-body">
                        <div id="validation-error-edit"></div>
                        <div class="form-group"><label class="col-sm-4 control-label control-label-required">Número da Aula</label>
                            <div class="col-sm-3"><input type="number" min="1" name="nr_aula" id="nr_aula" class="form-control"></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* O campo é obrigatório</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                    <input type="hidden" name="cd_aula" value="">
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal editar aula -->
{/block}

{block name="validation-script"}
    <script src="{base_url}assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <!-- Script para incluir uma aula e o Alert toastr -->
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-aula').submit(function () {
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $.post($('#form-incluir-aula').attr('action'), $('#form-incluir-aula').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                    else if (data.st == 3)
                    {
                        toastr.info(data.msg);
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <!-- Script para incluir uma aula e o Alert toastr -->
    <!-- Script para editar uma aula -->
    <script>
        $(document).ready(function () {

            var modal = $("#editar-aula-modal");
            $('.editar-aula').on("click", function () {
                var cd_aula = $(this).data("id");
                $.get(
                        "{base_url}aulas/editar/" + cd_aula,
                        function (semestre) {
                            modal.find('[name="nr_aula"]').val(semestre.nr_aula);
                            modal.find('[name="cd_aula"]').val(semestre.cd_aula);

                            modal.modal("show");

                        }
                );
            });
            modal.on("hide.bs.modal", function () {
                modal.find('[name="nr_aula"]').val("");
                modal.find('[name="cd_aula"]').val("");
                modal.find('[id="validation-error-edit"]').removeClass('alert alert-warning');
                modal.find('[id="validation-error-edit"]').find('p').remove();
            });

            modal.find("#form-editar-aula").on("submit", function (e) {
                e.preventDefault();
                $.post($('#form-editar-aula').attr('action'), $('#form-editar-aula').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error-edit').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error-edit').removeClass('alert alert-warning');
                        $('#validation-error-edit > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                    else if (data.st == 3)
                    {
                        toastr.info(data.msg);
                    }
                }, 'json');
                return false;

            });

            $("#myModal").on("hide.bs.modal", function () {
                $(this).find('[name="nr_aula"]').val("");
                $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
                $(this).find('[id="validation-error"]').find('p').remove();
            });
        });
    </script>
    <!-- Script para editar uma aula -->
    <!-- Script para excluir uma aula -->
    <script>
        $(function () {
            $('.excluir-aula').click(function () {
                var cd_aula = $(this).data("id");
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá a aula com suas dependências",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}aulas/excluir/" + cd_aula,
                                function () {
                                    swal("Removida!", "A aula foi removida com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "A aula não foi removida :)", "error");
                    }
                });
            });

            $('.homerDemo4').click(function () {
                toastr.error('Error - This is a Homer error notification');
            });

        });
    </script>
    <!-- Fim do Script para excluir uma aula -->

{/block}
