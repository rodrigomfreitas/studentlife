{extends file="templates/template.tpl"}
{block name="imports"}
    <link rel="stylesheet" href="{base_url}assets/vendor/select2-3.5.2/select2.css">
    <link rel="stylesheet" href="{base_url}assets/vendor/select2-bootstrap/select2-bootstrap.css">
{/block}
{block name="content"}
    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Conceitos e Pareceres</h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Conceitos e Pareceres</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row">
                {validation_errors('<p class="panel-body text-danger"><i class="fa fa-exclamation-circle"></i>  ', '</p>')}
                <form id="form-conceito" action="{base_url}conceito_comentario/validar" method="post">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="cursos">Cursos</label>
                            <select class="form-control" id="cd_curso" name="cd_curso">
                                <option value="">Escolha um curso...</option>
                                {foreach $cursos as $curso}
                                    <option value="{$curso->cd_curso}">{$curso->nm_curso}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <!-- Teste -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="alunos">Alunos</label>
                             <select class="js-source-states-2" name="alunos[]" id="alunos" style="width: 100%" multiple="multiple">
                            
                            </select>
                        </div>
                    </div>
                    <!-- Fim do Teste -->
                    <div class="col-md-2">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Pesquisar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}
{block name="validation-script"}

    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-').submit(function () {
                $.post($('#form-conceito').attr('action'), $('#form-conceito').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        toastr.error(data.msg);
                    } else if (data.st == 1) {
                        toastr.info(data.msg);
                        setTimeout(function () {
                            window.open("{base_url}conceito_comentario/listar_conceito_aluno/" + data.cd_aluno, '_self');
                        }, 1500);
                    } else if (data.st == 2) {
                        toastr.info(data.msg);
                        setTimeout(function () {
                            window.open("{base_url}conceito_comentario/listar_conceito_curso/" + data.cd_curso, '_self');
                        }, 1500);
                    } else if (data.st == 3) {
                        toastr.info(data.msg);
                        setTimeout(function () {
                            window.open("{base_url}conceito_comentario/listar_conceito_aluno_curso/" + data.cd_aluno + "/" + data.cd_curso, '_self');
                        }, 1500);
                    }
                }, 'json');
                return false;
            });
        });
    </script>
    <script src="{base_url}assets/vendor/select2-3.5.2/select2.min.js"></script>
    <script type="text/javascript">
        $(".js-source-states-2").select2();
    </script>
    <script type="text/javascript">
        var path = '{base_url}'
    </script>

    <script>
        {literal}
            $(function () {
                $("select[name=cd_curso]").change(function () {
                    cd_curso = $(this).val();
                    if (cd_curso === '')
                            return false;
                    resetaCombo('alunos[]');
             
                    $.getJSON(path + 'conceito_comentario/get_alunos_de_curso/' + cd_curso, function (data) {
 
                        var option = new Array();
 
                        $.each(data, function (i, obj) {
                            option[i] = document.createElement('option');
                            $(option[i]).attr({value: obj.cd_usuario});
                            $(option[i]).append(obj.nm_usuario);
                            $("select[name='alunos[]']").append(option[i]);
                        });
                    });
                });
            });
 
            function resetaCombo(el) {
                   $("select[name='" + el + "']").empty();
                   var option = document.createElement('option');                                  
                   //$(option).attr({value: ''});
                   //$(option).append('Escolha');
                   $("select[name='" + el + "']").append(option);
            }
        {/literal}
    </script>
{/block}
