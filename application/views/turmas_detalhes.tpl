{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Turma do <strong>{$dados[0]->nr_semestre}&ordm; Semestre</strong> do Turno da <strong>{$dados[0]->nm_turno}</strong></h3>
                    {if $sessao_dados['status'] != 3}
                        <button class="btn btn-primary pull-right gerar-chave" data-id="{$dados[0]->cd_turma}"> <i class="fa fa-key"></i> &nbsp;Gerar Chave</button>
                    {/if}
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>

                            <li>
                                <a href="{base_url}turmas">Turmas - Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}turmas/ano/{$dados[0]->cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}turmas/curso/{$dados[0]->cd_curso}">Turmas do {$dados[0]->sg_curso}</a>
                            </li>
                            <li>
                                <span>{$dados[0]->sg_curso}/{$dados[0]->nr_semestre}{$dados[0]->nm_turno|truncate:1:"":true}/{$dados[0]->dt_turma|date_format:"%Y"}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados[0]->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row">

                <div class="col-md-12">

                    <div class="font-bold m-b-sm">

                    </div>

                    <div class="hpanel">
                        <div class="panel-body">
                            <h3 class="m-t-none">Total dos Conceitos por Disciplina</h3>
                            <small>
                                {*A wonderful serenity has taken possession of my entire soul, like these sweet mornings of
                                spring which I enjoy with my whole heart.
                                I am alone, and feel the charm of existence in this spot, which was created for the bliss of
                                souls like mine.*}
                            </small>
                            {if $conceitos|count > 0}
                                <div class="m-t-md">
                                    <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>
                                </div>
                            {else}
                                <h5 class="text-left text-info">Não foi possível gerar o gráfico, porque deve ter, pelo menos, um conceito inserido.</h5>
                            {/if}
                        </div>
                    </div>
                </div>
                {*<div class="col-md-8"> 
                <div class="hpanel">

                <div class="panel-body">

                <p>
                <strong>These sweet mornings of spring</strong> which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.
                </p>

                <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped">
                <tbody>
                <tr>
                <td>
                <span class="label label-success">Added</span>
                </td>
                <td class="issue-info">
                <a href="#">
                Message
                </a>
                <br>
                <small>
                This is issue with the coresponding note
                </small>
                </td>
                <td>
                Adrian Novak
                </td>
                <td>
                12.02.2015
                </td>
                <td class="text-right">
                <button class="btn btn-default btn-xs"> Action</button>
                </td>
                </tr>
                </tbody>
                </table>
                </div>
                </div>

                </div>
                </div>*}
                <!-- Criar chave para os alunos acessarem para se cdastrarem nas disciplinas -->
                <div class="col-md-8">
                    <div class="font-bold m-b-sm">
                        Chave para enviar aos alunos
                    </div>
                    <div class="hpanel hblue">
                        <div class="panel-body">
                            {if $chaves|count == 0}
                                <span class="font-bold no-margins">
                                    Nenhuma chave gerada até o momento.
                                </span>
                                <br>
                                <small>
                                    Para gerar uma chave, clica-se no botão que encontra-se no canto superior direito desta página, denominado "Gerar Chave".
                                </small>
                            {else}
                                <p>
                                    <strong>Professor</strong>, após gerar a chave, envia-o aos alunos para que se registram às disciplinas que constam nesta turma.
                                    Disponibilizar a url: <strong>{base_url}enviar-solicitacao-registrar-turma</strong> e a chave gerada.
                                </p>
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Data da Geração</th>
                                                <th>Nível</th>
                                                <th>Gerado Por</th>
                                                <th>Chave</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {foreach $chaves as $chave}
                                                <tr>
                                                    <td>{$chave->dt_criacao_chave_turma|date_format:"%d/%m/%Y"}</td>
                                                    <td>{$chave->nm_tipo_usuario}</td>
                                                    <td>{$chave->nm_usuario}</td>
                                                    <td>{$chave->ds_chave_turma}</td>
                                                </tr>
                                            {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            {/if}
                        </div>
                    </div>
                </div>
                <!-- Fim do criar chave para os alunos acessarem para se cdastrarem nas disciplinas -->
                <div class="col-md-4 border-left">
                    <div class="font-bold m-b-sm">
                        <span>Alunos matriculados</span>
                        <span class="pull-right">Total: {$alunos|count}</span>
                    </div>
                    
                    <div class="row">
                        {if $alunos|@count == 0}
                            <div class="col-md-12">
                                <p class="text-info">Nenhum aluno inserido nesta turma</p>
                            </div>
                        {else}
                            <div class="col-md-12">
                                <div class="hpanel hgreen">
                                    <div class="panel-body">
                                        <div class="users-list">
                                            {foreach $alunos as $aluno}
                                                <div class="chat-user">
                                                    <img class="chat-avatar" src="{if $aluno->ds_foto == ""}{base_url}assets/images/profile.png{else}{base_url}fotos/{$aluno->ds_foto}{/if}" alt="">
                                                    <div class="chat-user-name">
                                                        {$aluno->nm_usuario}
                                                    </div>
                                                </div>
                                            {/foreach}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}

                    </div>
                </div>
            </div>
        </div>

        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}
{block name="validation-script"}
    {*    <script src="{base_url}assets/vendor/chartjs/Chart.min.js"></script>*}
    <script src="{base_url}assets/vendor/highcharts/highcharts.js"></script>
    <script src="{base_url}assets/vendor/highcharts/modules/exporting.js"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $(function () {
            $('.gerar-chave').click(function () {
                var cd_turma = $(this).data("id");
                swal({
                    title: "Gerar Chave?",
                    text: "Você realmente gostaria de gerar a chave para enviar aos alunos? Após gerar, disponibilize-a aos alunos.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#3498db",
                    confirmButtonText: "Sim, gostaria!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}chave_turma/incluir/" + cd_turma,
                                function (data) {
                                    if (data.st == 1) {
                                        swal("Parabéns!", "Chave gerada com sucesso.", "success");
                                        setTimeout(function () {
                                            location.reload();
                                        }, 1000);
                                    }
                                    else if (data.st == 3) {
                                        swal("Cancelado", "A chave criada já existe, por favor, tente novamente.", "error");
                                    }
                                }
                        );
                    } else {
                        swal("Cancelado", "A chave não foi gerada :)", "error");
                    }
                });
            });
        });
    </script>
    {if $conceitos|count > 0}
        <script>
            $(function () {
                $('#container').highcharts({
                    title: {
                        text: 'Gráfico combinado'
                    },
                    xAxis: {
                        title: {
                            text: 'Conceitos'
                        },
                        categories: {$nmDisciplina}
                    },
                    yAxis: {
                        title: {
                            text: 'Total de Alunos'
                        },
                    },
                    labels: {
                        items: [{
                                html: 'Total de Alunos referente aos Conceitos',
                                style: {
                                    left: '50px',
                                    top: '18px',
                                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                                }
                            }]
                    },
                    series: [{
                            type: 'column',
                            name: 'A',
                            data: [{join($conceitoA, ',')}]
                        }, {
                            type: 'column',
                            name: 'B',
                            data: [{join($conceitoB, ',')}]
                        }, {
                            type: 'column',
                            name: 'C',
                            data: [{join($conceitoC, ',')}]
                        }, {
                            type: 'column',
                            name: 'D',
                            data: [{join($conceitoD, ',')}]
                        }, {
                            type: 'column',
                            name: 'SC',
                            data: [{join($conceitoSC, ',')}]
                        },
                        {
                            type: 'pie',
                            name: 'Total de Alunos',
                            data: [{
                                    name: 'A',
                                    y: {$conceitoA|array_sum},
                                    color: Highcharts.getOptions().colors[0]
                                }, {
                                    name: 'B',
                                    y: {$conceitoB|array_sum},
                                    color: Highcharts.getOptions().colors[1]
                                }, {
                                    name: 'C',
                                    y: {$conceitoC|array_sum},
                                    color: Highcharts.getOptions().colors[2]
                                }, {
                                    name: 'D',
                                    y: {$conceitoD|array_sum},
                                    color: Highcharts.getOptions().colors[3]
                                }, {
                                    name: 'S/C',
                                    y: {$conceitoSC|array_sum},
                                    color: Highcharts.getOptions().colors[4]
                                }],
                            center: [100, 80],
                            size: 100,
                            showInLegend: false,
                            dataLabels: {
                                enabled: false
                            }
                        }]
                });
            });


        </script>
    {/if}
    {*<script>

    $(function () {

    /**
    * Options for Line chart
    */
    var lineData = {
    // labels: ["January", "February", "March", "April"],
    labels:
    [
    {foreach $dados as $dado}
    "{$dado->nm_disciplina}",
    {/foreach}
    ],
    datasets: [
    {
    label: "Example dataset",
    fillColor: "rgba(98,203,49,0.5)",
    strokeColor: "rgba(98,203,49,0.7)",
    pointColor: "rgba(98,203,49,1)",
    pointStrokeColor: "#fff",
    pointHighlightFill: "#fff",
    pointHighlightStroke: "rgba(26,179,148,1)",
    data: [{if "A" == "A"}("A", 10){/if}, 21, 19, 24, 20, 30]
    }
    ]
    };

    var lineOptions = {
    scaleShowGridLines: true,
    scaleGridLineColor: "rgba(0,0,0,.05)",
    scaleGridLineWidth: 1,
    bezierCurve: false,
    pointDot: true,
    pointDotRadius: 3,
    pointDotStrokeWidth: 1,
    pointHitDetectionRadius: 20,
    datasetStroke: false,
    datasetStrokeWidth: 1,
    datasetFill: true,
    responsive: true,
    tooltipTemplate: "<%= value %>",
    showTooltips: true,
    onAnimationComplete: function ()
    {
    this.showTooltip(this.datasets[0].points, true);
    },
    tooltipEvents: []
    };


    var ctx = document.getElementById("lineOptions").getContext("2d");
    var myNewChart = new Chart(ctx).Line(lineData, lineOptions);


    });


    </script>*}
{/block}
