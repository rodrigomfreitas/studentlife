{extends file="templates/template.tpl"}
{block name="content"}

    <div id="wrapper">
        
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Perfil</h3>
                    {if $uri == $sessao_dados['id']|md5}
                        <a href="{base_url}editar-perfil/{$sessao_dados['id']|md5}" class="btn btn-success pull-right"><i class="fa fa-paste"></i>&nbsp;Editar</a>
                        {/if}
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Perfil</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-4">
                    <div class="hpanel hgreen">
                        <div class="panel-body">
                            <div class="pull-right text-right">
                                <div class="btn-group">
                                    <i class="fa fa-facebook btn btn-default btn-xs"></i>
                                    <i class="fa fa-twitter btn btn-default btn-xs"></i>
                                    <i class="fa fa-linkedin btn btn-default btn-xs"></i>
                                </div>
                            </div>
                                <img alt="logo" class="img-circle m-b m-t-md" width="76" height="76" src="{if empty($dados->ds_foto)}{base_url}assets/images/profile.png{else}{base_url}fotos/{$dados->ds_foto}{/if}">
                                <h3><a href="">{$dados->nm_usuario}</a></h3>
                                <div class="text-muted font-bold m-b-xs">{if empty($dados->ds_cidade) && empty($dados->sg_estado)}{""}{else}{$dados->ds_cidade}, {$dados->sg_estado}{/if}</div>
                                <p>{$dados->ds_email}</p>
                                <p>{if empty($dados->nr_cep)}{""}{else}CEP: <span class="cep">{$dados->nr_cep}</span> - {/if}
                                   {if empty($dados->ds_endereco)}{""}{else}{$dados->ds_endereco}, {/if}
                                   {if empty($dados->nr_endereco)}{""}{else}{$dados->nr_endereco} {/if}
                                   {if empty($dados->ds_complemento)}{""}{else}{$dados->ds_complemento}, {/if}
                                   {if empty($dados->ds_bairro)}{""}{else}{$dados->ds_bairro}{/if}
                                   {if empty($dados->sg_estado)}{""}{else}, {$dados->sg_estado}{/if}</p>
                            </div>
                    </div>
                </div>
                {literal}
                    <!--
                    {if {$dados_perfil[0]->endereco} != "" && {$dados_perfil[0]->cep} != ""}
                    <div class='col-lg-8'>
                        <div class='hpanel hblue'>
                            <div class='panel-body'>

                                {$map['html']}

                            </div>
                        </div>
                    </div>
                    {/if}
                    -->
                {/literal}
            </div>
        </div>
            {include file="templates/footer.tpl"}
    </div>
    {literal}
        <!-- {$map['js']} -->
    {/literal}



{/block}
