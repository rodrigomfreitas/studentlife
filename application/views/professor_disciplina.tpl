{extends file="templates/template.tpl"}
{block name="imports"}
{/block}
{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplina de <strong>{$dados_disciplina->nm_disciplina}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}anos/{$dados_disciplina->CURSO_cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}semestres/{$dados_disciplina->ANO_cd_ano}">Semestres</a>
                            </li>
                            <li>
                                <a href="{base_url}disciplinas/{$dados_disciplina->cd_semestre}">Disciplinas</a>
                            </li>
                            <li>
                                <span>{$dados_disciplina->nm_disciplina}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados_disciplina->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row projects">
                {include file="templates/disciplina_menu.tpl"}
                <!-- Conteúdo -->
                <div class="col-md-9">
                    <div class="hpanel">
                        <div class="panel-heading hbuilt">
                            <div class="p-xs h4">
                                <small class="pull-right">
                                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus"></i> &nbsp;Adicionar</button>
                                </small>                               
                                <strong>Professores</strong>
                            </div>
                        </div>
                        <div class="panel-body">
                            {if $professoresDisciplina|@count < 1}
                                <div class="col-md-12">
                                    <h4>Nenhum Professor Inserido à Disciplina até o momento.</h4>
                                </div>
                            {/if}
                            <div class="col-lg-12">
                                <div class="hpanel">
                                    <div class="panel-body" style="display: block;">
                                        <div class="table-responsive">
                                            <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Nome</th>
                                                        <th>E-mail</th>
                                                        <th>Ação</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {foreach $professoresDisciplina as $professor}
                                                        <tr>
                                                            <td>{$professor->nm_usuario}</td>
                                                            <td>{$professor->ds_email}</td>
                                                            <td style="width: 15%" class="text-center">
                                                                <a class="btn btn-danger btn-xs excluir-professor" data-id="{$professor->USUARIO_cd_usuario}" data-toggle="tooltip" data-placement="left" title="" data-original-title="Remover"><i class="fa fa-remove"></i></a>
                                                            </td>
                                                        </tr>
                                                    {/foreach}
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="panel-footer" style="display: block;">
                                        Total de linhas: <strong>{$professoresDisciplina|count} </strong>
                                    </div>
                                </div>
                            </div>
                            <!-- Fim do conteúdo -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
    <!-- Modal incluir professor -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Incluir Professor</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-incluir-professor" action="{base_url}professorDisciplina/incluir" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_disciplina" id="cd_disciplina" value="{$dados_disciplina->cd_disciplina}">
                    <div class="modal-body">
                        <div id="validation-error"></div>
                        <div class="form-group">
                            <label for="usuario" class="col-sm-2 control-label control-label-required">Professor</label>
                            <div class="col-sm-10"><select class="form-control m-b" name="cd_usuario">
                                    <option value="" class="text-muted">Selecione um professor...</option>
                                    {foreach $professores as $professor}
                                        <option value="{$professor->cd_usuario}">{$professor->nm_usuario}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* O campo é obrigatório</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal incluir professor -->
{/block}

{block name="validation-script"}
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <!-- Script para incluir uma aula e o Alert toastr -->
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-professor').submit(function () {
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $.post($('#form-incluir-professor').attr('action'), $('#form-incluir-professor').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                    else if (data.st == 3)
                    {
                        toastr.info(data.msg);
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <!-- Script para incluir uma aula e o Alert toastr -->
    <!-- Script para editar uma aula -->
    <script>
        $(document).ready(function () {

            $("#myModal").on("hide.bs.modal", function () {
                $(this).find('[name="cd_usuario"]').val("");
                $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
                $(this).find('[id="validation-error"]').find('p').remove();
            });
        });
    </script>
    <!-- Script para editar uma aula -->
    <!-- Script para excluir uma aula -->
    <script>
        $(function () {
            $('.excluir-professor').click(function () {
                var cd_usuario = $(this).data("id");
                var cd_disciplina = {$dados_disciplina->cd_disciplina};
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá o professor que ministra esta disciplina",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}professorDisciplina/excluir/" + cd_usuario + "/" + cd_disciplina,
                                function () {
                                    swal("Removida!", "O professor foi removido com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "O professor não foi removido :)", "error");
                    }
                });
            });
        });
    </script>
    <!-- Fim do Script para excluir uma aula -->

{/block}
