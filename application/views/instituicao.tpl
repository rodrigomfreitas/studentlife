{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página-->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Instituição </h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Instituição</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->

        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-7">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Dados da Instituição</h4>
                                </div>
                                {if $instituicao|count == 0}
                                    
                                {else}
                                    {if $sessao_dados['status'] == 1}
                                        <div class="col-md-6">
                                            <div class="text-right">
                                                <button class="btn btn-default btn-sm editar-instituicao" data-id="{$instituicao[0]->cd_instituicao}"><i class="fa fa-pencil"></i> Editar </button>
                                            </div>
                                        </div>
                                    {/if}
                                {/if}

                            </div>
                        </div>
                        <div class="panel-body p-xl">
                            <div class="row m-b-xl">
                                <div class="col-sm-12">
                                    {if $instituicao|count == 0}
                                        <h4>Nenhuma Instituição cadastrada</h4>
                                    {else}
                                        {foreach $instituicao as $inst}
                                            <h3>{$inst->nm_instituicao}</h3>
                                            <address>
                                                <h3>{$inst->sg_instituicao}</h3>
                                                CEP: <span class='cep'>{$inst->nr_cep}</span><br>
                                                {$inst->ds_endereco}, {$inst->nr_endereco} {$inst->ds_complemento} - {$inst->ds_bairro}<br>
                                                {$inst->ds_cidade}/{$inst->sg_estado}<br>
                                                E-mail: {$inst->ds_email}<br>
                                            </address>
                                        {/foreach}
                                    {/if}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Telefones</h4>
                                </div>
                                {if $sessao_dados['status'] == 1}
                                    <div class="col-md-6">
                                        <div class="text-right">
                                            <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#incluir-telefone-modal"><i class="fa fa-plus"></i> Adicionar Telefones </button>
                                        </div>
                                    </div>
                                {/if}
                            </div>
                        </div>
                        <div class="panel-body p-xl">
                            <div class="row m-b-xl">
                                <div class="table-responsive">
                                    <table id="tabela-tarefa" cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Setor</th>
                                                <th>Número</th>
                                                    {if $sessao_dados['status'] == 1}
                                                    <th>Ações</th>
                                                    {/if}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {foreach $telefones as $tel}
                                                <tr>
                                                    <td>{$tel->ds_setor}</td>
                                                    <td class="telefone">{$tel->nr_telefone}</td>
                                                    {if $sessao_dados['status'] == 1}
                                                        <td style="width: 21%" class="text-center">
                                                            <a class="btn btn-default btn-xs editar-telefone" data-id="{$tel->cd_telefone}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"><i class="fa fa-edit"></i></a>
                                                            <a class="btn btn-default btn-xs excluir-telefone" data-id="{$tel->cd_telefone}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Excluir"><i class="fa fa-remove"></i></a>
                                                        </td>
                                                    {/if}
                                                </tr>
                                            {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {include file="templates/footer.tpl"}
    </div>

    <!-- Modal incluir telefone(s) a uma Instituição -->
    <div class="modal fade" id="incluir-telefone-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Incluir Telefone</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-incluir-telefone" action="{base_url}instituicao/inserir_telefone" method="post" class="form-horizontal">
                    <div class="modal-body">
                        <div id="validation-error"></div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Setor</label>
                            <div class="col-sm-10"><input type="text" name="setor" id="setor" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Número</label>
                            <div class="col-sm-10"><input type="text" name="numero" id="numero" class="form-control telefone" {literal}pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}"{/literal}></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Todos os campos são obrigatórios</strong></p>
                        </div>
                    </div>
                    <input type='hidden' name='cd_instituicao' value='{$instituicao[0]->cd_instituicao}'>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do Modal incluir telefone(s) a uma Instituição -->
    <!-- Modal editar telefone(s) a uma Instituição -->
    <div class="modal fade" id="editar-telefone-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Editar Telefone</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-editar-telefone" action="{base_url}instituicao/atualizar_telefone" method="post" class="form-horizontal">
                    <div class="modal-body">
                        <div id="validation-error-edit"></div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Setor</label>
                            <div class="col-sm-10"><input type="text" name="setor" id="setor" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Número</label>
                            <div class="col-sm-10"><input type="text" name="numero" id="numero" class="form-control" {literal}pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}"{/literal}></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Todos os campos são obrigatórios</strong></p>
                        </div>
                    </div>
                    <input type="hidden" name="cd_telefone" value="">
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do Modal editar telefone(s) a uma Instituição -->
    <!-- Modal editar telefone(s) a uma Instituição -->
    <div class="modal fade" id="editar-instituicao-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Editar Instituição</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-editar-instituicao" action="{base_url}instituicao/atualizar" method="post" class="form-horizontal">
                    <div class="modal-body">
                        <div id="validation-error-edit-inst"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label-required">Código</label>
                            <div class="col-sm-3"><input type="text" name="codigo" id="codigo" class="form-control"></div>
                            <label class="col-sm-1 control-label control-label-required">Sigla</label>
                            <div class="col-sm-6"><input type="text" name="sigla" id="sigla" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label-required">Nome</label>
                            <div class="col-sm-10"><input type="text" name="nome" id="nome" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label-required">E-mail</label>
                            <div class="col-sm-10"><input type="text" name="email" id="email" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label-required">CEP</label>
                            <div class="col-sm-4"><input type="text" name="cep" id="cep" class="form-control"></div>
                            <label class="col-sm-2 control-label control-label-required">Bairro</label>
                            <div class="col-sm-4"><input type="text" name="bairro" id="bairro" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Complemento</label>
                            <div class="col-sm-10"><input type="text" name="complemento" id="complemento" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label-required">Endereço</label>
                            <div class="col-sm-6"><input type="text" name="endereco" id="endereco" class="form-control"></div>
                            <label class="col-sm-2 control-label control-label-required">Número</label>
                            <div class="col-sm-2"><input type="text" name="numero" id="numero" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label-required">Cidade</label>
                            <div class="col-sm-6"><input type="text" name="cidade" id="cidade" class="form-control"></div>
                            <label class="col-sm-2 control-label control-label-required">Estado</label>
                            <div class="col-sm-2">
                                <select class="form-control" id="estado" name="estado" 
                                        placeholder="Estado...">
                                    <option value="" ></option>
                                    <option value="AC">AC</option>
                                    <option value="AL">AL</option>
                                    <option value="AP">AP</option>
                                    <option value="AM">AM</option>
                                    <option value="BA">BA</option>
                                    <option value="CE">CE</option>
                                    <option value="DF">DF</option>
                                    <option value="ES">ES</option>
                                    <option value="GO">GO</option>
                                    <option value="MA">MA</option>
                                    <option value="MT">MT</option>
                                    <option value="MS">MS</option>
                                    <option value="MG">MG</option>
                                    <option value="PA">PA</option>
                                    <option value="PB">PB</option>
                                    <option value="PR">PR</option>
                                    <option value="PE">PE</option>
                                    <option value="PI">PI</option>
                                    <option value="RJ">RJ</option>
                                    <option value="RN">RN</option>
                                    <option value="RS">RS</option>
                                    <option value="RO">RO</option>
                                    <option value="RR">RR</option>
                                    <option value="SC">SC</option>
                                    <option value="SP">SP</option>
                                    <option value="SE">SE</option>
                                    <option value="TO">TO</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Campos obrigatórios</strong></p>
                        </div>
                    </div>
                    <input type="hidden" name="cd_instituicao" value="">
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do Modal editar telefone(s) a uma Instituição -->

{/block}
{block name="validation-script"}
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-telefone').submit(function () {
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $.post($('#form-incluir-telefone').attr('action'), $('#form-incluir-telefone').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 1500);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <!-- Script para editar telefone -->
    <script>
        $(document).ready(function () {
            var modal = $("#editar-telefone-modal");
            $('.editar-telefone').on("click", function () {
                var cd_telefone = $(this).data("id");
                $.get(
                        "{base_url}instituicao/editar_telefone/" + cd_telefone,
                        function (telefone) {
                            modal.find('[name="setor"]').val(telefone.ds_setor);
                            modal.find('[name="numero"]').val(telefone.nr_telefone);
                            modal.find('[name="cd_telefone"]').val(telefone.cd_telefone);
                            modal.find('[name="numero"]').addClass('telefone');

                            modal.modal("show");

                        }
                );
            });
            modal.on("hide.bs.modal", function () {
                modal.find('[name="setor"]').val("");
                modal.find('[name="numero"]').val("");
                modal.find('[name="cd_instituicao"]').val("");
                modal.find('[id="validation-error-edit"]').removeClass('alert alert-warning');
                modal.find('[id="validation-error-edit"]').find('p').remove();
            });

            modal.find("#form-editar-telefone").on("submit", function (e) {
                e.preventDefault();
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $.post($('#form-editar-telefone').attr('action'), $('#form-editar-telefone').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error-edit').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error-edit').removeClass('alert alert-warning');
                        $('#validation-error-edit > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });

            $("#incluir-telefone-modal").on("hide.bs.modal", function () {
                $(this).find('[name="setor"]').val("");
                $(this).find('[name="numero"]').val("");
                $(this).find('[name="cd_instituicao"]').val("");
                $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
                $(this).find('[id="validation-error"]').find('p').remove();
            });
        });
    </script>
    <!-- Fim do Script para editar telefone -->
    <!-- Script para excluir telefone -->
    <script>
        $(function () {
            $('.excluir-telefone').click(function () {
                var cd_telefone = $(this).data("id");
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá o telefone",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}instituicao/excluir_telefone/" + cd_telefone,
                                function () {
                                    swal("Removido!", "O telefone foi removido com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "O telefone não foi removido :)", "error");
                    }
                });
            });

            $('.homerDemo4').click(function () {
                toastr.error('Error - This is a Homer error notification');
            });
        });
    </script>
    <!-- Fim doScript para excluir telefone -->
    <!-- Script para editar Instituição -->
    <script>
        $(document).ready(function () {
            var modal = $("#editar-instituicao-modal");
            $('.editar-instituicao').on("click", function () {
                var cd_instituicao = $(this).data("id");
                $.get(
                        "{base_url}instituicao/editarInstituicao/" + cd_instituicao,
                        function (instituicaoEditar) {
                            modal.find('[name="cd_instituicao"]').val(instituicaoEditar.cd_instituicao);
                            modal.find('[name="nome"]').val(instituicaoEditar.nm_instituicao);
                            modal.find('[name="sigla"]').val(instituicaoEditar.sg_instituicao);
                            modal.find('[name="codigo"]').val(instituicaoEditar.nr_instituicao);
                            modal.find('[name="email"]').val(instituicaoEditar.ds_email);
                            modal.find('[name="endereco"]').val(instituicaoEditar.ds_endereco);
                            modal.find('[name="numero"]').val(instituicaoEditar.nr_endereco);
                            modal.find('[name="complemento"]').val(instituicaoEditar.ds_complemento);
                            modal.find('[name="cidade"]').val(instituicaoEditar.ds_cidade);
                            modal.find('[name="cep"]').val(instituicaoEditar.nr_cep);
                            modal.find('[name="bairro"]').val(instituicaoEditar.ds_bairro);
                            modal.find('[name="estado"]').val(instituicaoEditar.sg_estado);
                            modal.find('[name="cep"]').addClass('cep');

                            modal.modal("show");

                        }
                );
            });
            modal.on("hide.bs.modal", function () {
                modal.find('[name="cd_instituicao"]').val("");
                modal.find('[name="nome"]').val("");
                modal.find('[name="sigla"]').val("");
                modal.find('[name="codigo"]').val("");
                modal.find('[name="email"]').val("");
                modal.find('[name="endereco"]').val("");
                modal.find('[name="numero"]').val("");
                modal.find('[name="complemento"]').val("");
                modal.find('[name="cidade"]').val("");
                modal.find('[name="cep"]').val("");
                modal.find('[name="bairro"]').val("");
                modal.find('[name="estado"]').val("");
                modal.find('[id="validation-error-edit-inst"]').removeClass('alert alert-warning');
                modal.find('[id="validation-error-edit-inst"]').find('p').remove();
            });

            modal.find("#form-editar-instituicao").on("submit", function (e) {
                e.preventDefault();
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $.post($('#form-editar-instituicao').attr('action'), $('#form-editar-instituicao').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error-edit-inst').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error-edit-inst').removeClass('alert alert-warning');
                        $('#validation-error-edit-inst > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <!-- Fim do Script para editar Instituição -->
{/block}
