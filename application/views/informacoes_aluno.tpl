{extends file="templates/template.tpl"}

{block name="content"}
    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplina de <strong>{$dados_disciplina->nm_disciplina}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}anos/{$dados_disciplina->CURSO_cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}semestres/{$dados_disciplina->ANO_cd_ano}">Semestres</a>
                            </li>
                            <li>
                                <a href="{base_url}disciplinas/{$dados_disciplina->cd_semestre}">Disciplinas</a>
                            </li>
                            <li>
                                <span>{$dados_disciplina->nm_disciplina}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados_disciplina->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row projects">
                {include file="templates/disciplina_menu.tpl"}
                <!-- Conteúdo do conceito do aluno -->
                <div class="col-md-9">
                    <div class="hpanel">
                        <div class="panel-heading hbuilt">
                            <div class="p-xs h4">
                                <small class="pull-right">
                                    <button class="btn btn-primary pull-right"> <i class="fa fa-print"></i> &nbsp;Imprimir</button>
                                </small>  
                                <strong>Detalhes do Aluno</strong>
                            </div>
                        </div>
                        <div class="hpanel">
                            <div class="panel-body">
                                <div class="col-lg-4 border-right">
                                    <div class="pull-right text-right">
                                        <div class="btn-group">
                                            <i class="fa fa-facebook btn btn-default btn-xs"></i>
                                            <i class="fa fa-twitter btn btn-default btn-xs"></i>
                                            <i class="fa fa-linkedin btn btn-default btn-xs"></i>
                                        </div>
                                    </div>
                                    <img alt="logo" class="img-circle m-b m-t-md" width="76" height="76" src="{if empty($dados_aluno->ds_foto)}{base_url}assets/images/profile.png{else}{base_url}fotos/{$dados_aluno->ds_foto}{/if}">
                                <h3><a href="">{$dados_aluno->nm_usuario}</a></h3>
                                <div class="text-muted font-bold m-b-xs">{if empty($dados_aluno->ds_cidade) && empty($dados_aluno->sg_estado)}{""}{else}{$dados_aluno->ds_cidade}, {$dados_aluno->sg_estado}{/if}</div>
                                <p>{$dados_aluno->ds_email}</p>
                                <p>{if empty($dados_aluno->nr_cep)}{""}{else}CEP: <span class="cep">{$dados_aluno->nr_cep}</span> - {/if}
                                   {if empty($dados_aluno->ds_endereco)}{""}{else}{$dados_aluno->ds_endereco}, {/if}
                                   {if empty($dados_aluno->nr_endereco)}{""}{else}{$dados_aluno->nr_endereco} {/if}
                                   {if empty($dados_aluno->ds_complemento)}{""}{else}{$dados_aluno->ds_complemento}, {/if}
                                   {if empty($dados_aluno->ds_bairro)}{""}{else}{$dados_aluno->ds_bairro}{/if}
                                   {if empty($dados_aluno->sg_estado)}{""}{else}, {$dados_aluno->sg_estado}{/if}</p>
                                </div>
                                <div class="col-lg-8">
                                    <div class="text-center m-b-md">
                                        <p class="text-primary">Clique na Turma abaixo para exibir a lista de Disciplinas que o aluno está matriculado.</p>
                                    </div>
                                    {foreach $ordenar as $index=>$disciplinas}
                                        <div class="panel-group {if $ordenar|count == 1}col-lg-12{else}col-lg-6{/if}" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <a aria-expanded="true" aria-controls="collapseOne" href='#collapseOne{$index|replace:"/":""}' data-toggle="collapse" data-parent="#accordion">
                                                    <div class="panel-heading" id="headingOne" role="tab">
                                                        <h4 class="panel-title">
                                                            {$index}
                                                        </h4>
                                                    </div>
                                                </a>
                                                <div class="panel-collapse collapse in" id='collapseOne{$index|replace:"/":""}' role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        <div class="col-lg-12">
                                                            <h4>Disciplinas</h4>
                                                            {foreach $disciplinas as $d}
                                                                <p>{$d->nm_disciplina}</p>
                                                            {/foreach}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                        </div>
                   <!-- Tabela das tarefas -->             
                    <div class="hpanel">
                        <div class="panel-body">
                            <div class="col-md-12 animated-panel zoomIn" style="animation-delay: 0.2s;"> 
                                <p>
                                    <strong>Tabela de Avaliações</strong> com os conceitos.
                                </p>
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Situação</th>
                                            <th>Nome</th>
                                            <th>Descrição</th>
                                            <th>Data Final</th>
                                            <th>Conceito</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach $avaliacoes as $avaliacao}
                                        <tr>
                                            <td>
                                                <span class="label {if $avaliacao->ds_conceito == D || $avaliacao->ds_conceito == SC}label-danger{else}label-success{/if}">{if $avaliacao->ds_conceito == D || $avaliacao->ds_conceito == SC}Reprovado{else}Aprovado{/if}</span>
                                            </td>
                                            <td class="issue-info">
                                                {$avaliacao->nm_avaliacao}
                                            </td>
                                            <td>
                                                {$avaliacao->ds_avaliacao}
                                            </td>
                                            <td class="text-right">
                                                {$avaliacao->dt_data_final|date_format:"%d/%m/%Y"}
                                            </td>
                                            <td class="text-center">
                                                {$avaliacao->ds_conceito}
                                            </td>
                                        </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                   <!-- Fim da Tabela das tarefas -->  
                    </div>
                </div>
            </div>
        </div>
<!-- Fim do conteúdo da página -->
<!-- Footer -->
{include file="templates/footer.tpl"}
<!-- Fim do footer -->
</div>
{/block}

{block name="validation-script"}
{/block}
