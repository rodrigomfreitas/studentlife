{extends file="templates/template.tpl"}

{block name="content"}
    <div id="wrapper">
        <div class="content animate-panel">

            <div class="row">
                <div class="col-md-12">
                    <div class="hpanel email-compose">
                        <div class="panel-heading hbuilt">
                            <a class="btn btn-success pull-right" href="{base_url}cursos">Voltar</a>
                            <div class="p-xs h4">
                                Inserir Avaliação
                            </div>
                        </div>
                        <div class="border-top border-left border-right bg-light">
                            <div class="p-m">

                                <form id="incluir_avaliacao" action="{base_url}avaliacao/incluir" method="post" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="modal-body">
                                        <input type="hidden" name="disciplina_id" id="disciplina_id" value="{$id}">
                                        <div class="form-group"><label class="col-sm-2 control-label">Nome</label>
                                            <div class="col-sm-10"><input type="text" name="nome" id="nome" class="form-control"></div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Tipo</label>
                                            <div class="col-sm-10">
                                                <select class="form-control" name="tipo" id="tipo">
                                                    <option value="1">Prova</option>
                                                    <option value="2">Trabalho</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group"><label class="col-sm-2 control-label">Data de Entrega</label>
                                            <div class="col-sm-10"><input type="date" name="data_final" id="data_final" class="form-control"></div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        <p>Descrição</p>
                        <textarea style="width: 100%; min-height: 150px;" id="descricao" name="descricao"></textarea>
                        <button type="submit"  class="btn btn-success btn-block" name="button">Inserir Avaliação</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}
