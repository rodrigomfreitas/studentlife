<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
    {block name="tittle"}{/block}



    <link rel="stylesheet" href="{base_url}assets/vendor/fontawesome/css/font-awesome.css" />
    <link rel="stylesheet" href="{base_url}assets/vendor/metisMenu/dist/metisMenu.css" />
    <link rel="stylesheet" href="{base_url}assets/vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="{base_url}assets/vendor/bootstrap/dist/css/bootstrap.css" />
    <link rel="stylesheet" href="{base_url}assets/vendor/toastr/build/toastr.min.css">
    <link rel="stylesheet" href="{base_url}assets/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css">
    <link rel="stylesheet" href="{base_url}assets/vendor/sweetalert/lib/sweet-alert.css">
    {block name="imports"}  {/block}
    <!-- App styles -->
    <link rel="stylesheet" href="{base_url}assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" />
    <link rel="stylesheet" href="{base_url}assets/fonts/pe-icon-7-stroke/css/helper.css" />
    <link rel="stylesheet" href="{base_url}assets/styles/style.css">
    <link rel="stylesheet" href="{base_url}assets/styles/static_custom.css">
    <!-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script> -->
</head>
<body>

    <!-- Simple splash screen-->
    <div class="splash"> <div class="color-line"></div><div class="splash-title"><h1>Student Life</h1><p>Aguarde enquanto preparamos tudo</p><img src="{base_url}assets/images/loading-bars.svg" width="64" height="64" /> </div> </div>
    <!--[if lt IE 7]>
    <p class="alert alert-danger">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


    <div id="header">
        <div class="color-line">
        </div>
        <div id="logo" class="light-version">

            <a href="{base_url}"><img src="{base_url}assets/images/gallery/logo.png" alt=""></a>

        </div>
        <nav role="navigation">
            <div class="header-link hide-menu"><i class="fa fa-bars"></i></div>
            <div class="small-logo">
                <span class="text-primary">Student Life</span>
            </div>
            <form role="search" class="navbar-form-custom" method="post" action="#">
                <div class="form-group">
                </div>
            </form>
            <div class="navbar-right">
                <ul class="nav navbar-nav no-borders">

                    <li class="dropdown">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                            <i class="pe-7s-keypad"></i>
                        </a>

                        <div class="dropdown-menu hdropdown bigmenu animated flipInX">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="{base_url}home">
                                                <i class="pe pe-7s-home text-danger"></i>
                                                <h5>Home</h5>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{base_url}instituicao">
                                                <i class="pe pe-7s-portfolio text-info"></i>
                                                <h5>Instituição</h5>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{base_url}cursos">
                                                <i class="pe pe-7s-notebook text-warning"></i>
                                                <h5>Cursos</h5>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="{base_url}turmas">
                                                <i class="pe pe-7s-study text-success"></i>
                                                <h5>Turmas</h5>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle label-menu-corner" href="{base_url}mensagem">
                            <i class="pe-7s-mail"></i>
                        </a>
                    </li>

                    <li class="dropdown">
                        <a href="{base_url}login/logout">
                            <i class="pe-7s-upload pe-rotate-90"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!-- Navigation -->
    <aside id="menu">
        <div id="navigation">
            <div class="profile-picture">
                <p>
                    <small class="text-muted">{if $ultimoAcesso|count == 1}Primeiro acesso no site{else}Último acesso em: {$ultimoAcesso[1]->dt_acao|date_format:"%d/%m/%Y"} às {$ultimoAcesso[1]->hr_acao}{/if}</small>
                </p>
                <a href="{base_url}">
                    <img src="{if $sessao_dados['foto'] == ""}{base_url}assets/images/profile.png{else}{base_url}fotos/{$sessao_dados['foto']}{/if}" class="img-circle m-b" width="76" height="76" alt="logo">
                </a>

                <div class="stats-label text-color">
                    <span class="font-extra-bold font-uppercase">{$sessao_dados['nome']}</span>
                    <p>
                        {$sessao_dados['tipo_usuario']}
                    </p>
                </div>
                <!-- Menu dropdown de Profile -->
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
                        <p class="text-muted"><i class="fa fa-gear text-primary"></i>&nbsp;Editar <b class="caret"></b></p>
                    </a>
                    <ul class="dropdown-menu animated flipInX m-t-xs">
                        <li><a href="{base_url}usuario/{$sessao_dados['id']|md5}">Perfil</a></li>
                        <li><a href="{base_url}editar-foto/{$sessao_dados['id']|md5}">Foto</a></li>
                        <li class="divider"></li>
                        <li><a href="{base_url}login/logout">Logout</a></li>
                    </ul>
                </div>
                <!-- Fim do Menu dropdown de Profile -->
            </div>
            <ul class="nav" id="side-menu">
                <li class="{active_link('home')}"><a href="{base_url}"> <span class="nav-label">Página Inicial</span> </a></li>
                <li class="{active_link('instituicao')}"><a href="{base_url}instituicao"> <span class="nav-label">Instituição</span></a></li>
                <li class="{active_link('cursos')} {active_link('semestres')} {active_link('disciplinas')} {active_link('avaliacao')}">
                    <a href="{base_url}cursos"> <span class="nav-label">Cursos</span></a>
                </li>
                <!--
                {if $sessao_dados['status'] == 1 || $sessao_dados['status'] == 2}
                <li><a href="{base_url}avaliacao"><span class="nav_label">Avaliações</span></a></li>
                {/if}
                {if $sessao_dados['status'] == 1}
                <li><a href="{base_url}conceitos/{$sessao_dados['id']}"><span class="nav_label">Conceitos</span></a></li>
                {/if}
                -->
                {if $sessao_dados['status'] == 1 || $sessao_dados['status'] == 2}
                    <li class="{active_link('turmas')}"><a href="{base_url}turmas"><span class="nav_label">Turmas</span></a></li>
                    {/if}
                <li class="{active_link('conceito_comentario')}"><a href={base_url}conceito-comentario><span class="nav-label">Conceitos e Pareceres</span></a></li>
                {if $sessao_dados['status'] == 1}
                    <li class="{active_link('logs')}"><a href="{base_url}logs"><span class="nav_label">Logs</span></a></li>
                    {/if}
                {if $sessao_dados['status'] == 1}
                    <li class="{active_link('backup')}"><a href="{base_url}backup-sistema"><span class="nav_label">Backup</span></a></li>
                    {/if}
            </ul>
        </div>
    </aside>

{block name="content"}{/block}

{block name="scripts"}

    <!-- Vendor scripts -->
    <script src="{base_url}assets/vendor/jquery/dist/jquery.min.js"></script>
    <script src="{base_url}assets/vendor/jquery-ui/jquery-ui.min.js"></script>
    <script src="{base_url}assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="{base_url}assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{base_url}assets/vendor/jquery-flot/jquery.flot.js"></script>
    <script src="{base_url}assets/vendor/jquery-flot/jquery.flot.resize.js"></script>
    <script src="{base_url}assets/vendor/jquery-flot/jquery.flot.pie.js"></script>
    <script src="{base_url}assets/vendor/flot.curvedlines/curvedLines.js"></script>
    <script src="{base_url}assets/vendor/jquery.flot.spline/index.js"></script>
    <script src="{base_url}assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
    <script src="{base_url}assets/vendor/iCheck/icheck.min.js"></script>
    <script src="{base_url}assets/vendor/peity/jquery.peity.min.js"></script>
    <script src="{base_url}assets/vendor/toastr/build/toastr.min.js"></script>
    <script src="{base_url}assets/vendor/sparkline/index.js"></script>
    <script src="{base_url}assets/vendor/sweetalert/lib/sweet-alert.min.js"></script>




    <!-- App scripts -->

    <script src="{base_url}assets/scripts/jquery.mask.js"></script>
    <script src="{base_url}assets/scripts/homer.js"></script>
    <!-- <script src="{base_url}assets/scripts/forms.js"></script> -->
    <script src="{base_url}assets/scripts/charts.js"></script>
    <script>
        //var BASE_URL = '{base_url}';
    </script>

</body>
</html>

{/block}

{block name="validation-script"}{/block}
