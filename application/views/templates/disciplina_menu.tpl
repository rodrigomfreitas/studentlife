<div class="col-md-3">
    <div class="hpanel">
        <div class="panel-body">
            <ul class="mailbox-list">
                <li class="{active_link('disciplinas')} {active_link('usuario')} {active_link('graficos')} ">
                    <a href="{base_url}disciplina/{$dados_disciplina->cd_disciplina}">
                        <i class="fa fa-mortar-board text-danger"></i> Alunos
                    </a>
                </li>
                <li class="{active_link('aluno_disciplina')}">
                    <a href="{base_url}disciplina/inserir-aluno/{$dados_disciplina->cd_disciplina}"><i class="fa fa-plus text-success"></i>
                        Inserir Aluno
                        {if $totalAlunos > 0}<span class="badge badge-info pull-right">{$totalAlunos}</span>{/if}
                    </a>
                </li>
                <li class="{active_link('professorDisciplina')}">
                    <a href="{base_url}disciplina/professor/{$dados_disciplina->cd_disciplina}"><i class="fa fa-child text-info"></i> Professores</a>
                </li>
                <li class="{active_link('avaliacao')} {active_link('avaliacao_usuario')}">
                    <a href="{base_url}disciplina/avaliacao/{$dados_disciplina->cd_disciplina}"><i class="fa fa-book text-primary"></i> Avaliações</a>
                </li>
                <li class="{active_link('aulas')} {active_link('documentos')}">
                    <a href="{base_url}disciplina/material/aulas/{$dados_disciplina->cd_disciplina}"><i class="fa fa-file-text text-primary-2"></i> Material</a>
                </li>
                <li class="{active_link('enviaEmail')}">
                    <a href="{base_url}disciplina/enviar-email/{$dados_disciplina->cd_disciplina}"><i class="fa fa-envelope text-warning"></i> Enviar E-mail</a>
                </li>
            </ul>
        </div>
    </div>
</div>