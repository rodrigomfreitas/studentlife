{extends file="templates/template.tpl"}
{block name="imports"} <link rel="stylesheet" href="{base_url}assets/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css"> {/block}
{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplina de <strong>{$dados_disciplina->nm_disciplina}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}anos/{$dados_disciplina->CURSO_cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}semestres/{$dados_disciplina->ANO_cd_ano}">Semestres</a>
                            </li>
                            <li>
                                <a href="{base_url}disciplinas/{$dados_disciplina->cd_semestre}">Disciplinas</a>
                            </li>
                            <li>
                                <span>{$dados_disciplina->nm_disciplina}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados_disciplina->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row projects">
                {include file="templates/disciplina_menu.tpl"}
                <!-- Conteúdo do conceito do aluno -->
                <form id="form-incluir-conceitos" method="post" action="{base_url}avaliacao_usuario/incluir">
                    <div class="col-md-9">
                        <div class="hpanel">
                            <div class="panel-heading hbuilt">
                                <div class="p-xs h4">
                                    <small class="pull-right">
                                        <button class="btn btn-primary pull-right"> <i class="fa fa-save"></i> &nbsp;Salvar</button>
                                    </small>  
                                    <strong>Conceitos</strong>
                                </div>
                            </div>
                            <div class="panel-body">
                                {foreach $avaliacao as $a}
                                    <div class="hpanel {$mensagem['hpanel-color']}">
                                        <div class="panel-body">
                                            <span class="label {$mensagem['label-color']} pull-right">{$mensagem['msg']}</span>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h5> {$a->nm_avaliacao}</h5>
                                                    <h6>
                                                        {$a->ds_avaliacao}
                                                    </h6>
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <div class="project-label">Tipo</div>
                                                            <small>{$a->nm_tipo_avaliacao}</small>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="project-label">Data da Criação</div>
                                                            <small>{$a->dt_data_cadastro|date_format:"%d/%m/%Y"}</small>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="project-label">Data Entrega Final</div>
                                                            <small>{$a->dt_data_final|date_format:"%d/%m/%Y"}</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="cd_avaliacao" name="cd_avaliacao" value="{$a->cd_avaliacao}">
                                {/foreach}
                                <div class="text-center m-b-md">
                                    <small class="text-primary">Clique na Turma abaixo para exibir a lista dos alunos.</small><br>
                                </div>
                                {if $ordenar|@count < 1}
                                    <h2><small>Não há alunos inscritos nesta Disciplina</small></h2>
                                {else}
                                    {foreach $ordenar as $index=>$alunos}
                                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <a aria-expanded="true" aria-controls="collapseOne" href='#collapseOne{$index|replace:"/":""}' data-toggle="collapse" data-parent="#accordion">
                                                    <div class="panel-heading" id="headingOne" role="tab">
                                                        <h4 class="panel-title">
                                                            {$index}
                                                        </h4>
                                                    </div>
                                                </a>
                                                <div class="panel-collapse collapse" id='collapseOne{$index|replace:"/":""}' role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        <div class="panel-body">
                                                            <div class="table-responsive">
                                                                <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Matrícula</th>
                                                                            <th>Nome</th>
                                                                            <th>Conceitos</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        {foreach $alunos as $aluno}
                                                                            <tr>
                                                                        <input type="hidden" id="cd_usuario" name="cd_usuario[]" value="{$aluno->cd_usuario}">
                                                                        <input type="hidden" id="cd_turma" name="cd_turma[]" value="{$aluno->TURMA_cd_turma}">
                                                                        <td>{$aluno->nr_matricula} <br></td>
                                                                        <td>{$aluno->nm_usuario} <br></td>
                                                                        <td><select id="conceitos" name="conceitos[]">
                                                                                {if isset($ordenar_conceito[$aluno->cd_usuario])}
                                                                                    <option value="" {if $ordenar_conceito[$aluno->cd_usuario]['ds_conceito'] == ''} selected="selected"{/if}></option>
                                                                                    <option value="A" {if $ordenar_conceito[$aluno->cd_usuario]['ds_conceito'] == 'A'} selected="selected"{/if}>A</option>
                                                                                    <option value="B" {if $ordenar_conceito[$aluno->cd_usuario]['ds_conceito'] == 'B'} selected="selected"{/if}>B</option>
                                                                                    <option value="C" {if $ordenar_conceito[$aluno->cd_usuario]['ds_conceito'] == 'C'} selected="selected"{/if}>C</option>
                                                                                    <option value="D" {if $ordenar_conceito[$aluno->cd_usuario]['ds_conceito'] == 'D'} selected="selected"{/if}>D</option>
                                                                                    <option value="SC" {if $ordenar_conceito[$aluno->cd_usuario]['ds_conceito'] == 'SC'} selected="selected"{/if}>S/C</option>
                                                                                {elseif !isset($ordenar_conceito[$aluno->cd_usuario])}
                                                                                    <option value=""></option>
                                                                                    <option value="A">A</option>
                                                                                    <option value="B">B</option>
                                                                                    <option value="C">C</option>
                                                                                    <option value="D">D</option>
                                                                                    <option value="SC">S/C</option>
                                                                                {/if}
                                                                            </select> <br></td>
                                                                        </tr>
                                                                    {/foreach}
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                {/if}
                            </div>
                        </div>
                    </div>
                </form>
                <!-- Fim do conteúdo do conceito do aluno -->
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}

{block name="validation-script"}
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-conceitos').submit(function () {
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $.post($('#form-incluir-conceitos').attr('action'), $('#form-incluir-conceitos').serialize(), function (data) {
                    if (data.st == 1)
                    {
                        setTimeout(function () {
                            location.reload();
                        }, 1500);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
{/block}
