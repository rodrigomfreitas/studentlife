
{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
                    Conceitos
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nome do Aluno</th>
                                    <th>Avaliação </th>
                                    <th>Conceito </th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $conceitos as $conceito}
                                    <tr>

                                        <td>{$conceito->nome} {$conceito->sobrenome}</td>
                                        <td>{$conceito->avnome}</td>
                                        <td>{$conceito->conceito}</td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        {include file="templates/footer.tpl"}
    </div>
{/block}
