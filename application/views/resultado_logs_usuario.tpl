{extends file="templates/template.tpl"}
{block name="imports"}
    <link rel="stylesheet" href="{base_url}assets/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{base_url}assets/vendor/datatables.net-bs/css/buttons.dataTables.min.css">
{/block}
{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">{if $usuariosAcoes|count > 0}Logs da ação dos Usuários{else}Nenhum Registro de Log. {/if}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}logs">Logs</a>
                            </li>
                            <li>
                                <span>Tabela dos Logs</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            Tabela dos Logs.
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="tabela-acao" cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Código do Usuário</th>
                                            <th>Nome do Usuário</th>
                                            <th>Descrição da Ação</th>
                                            <th>Data da Ação</th>
                                            <th>Hora da Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach $usuariosAcoes as $acao}
                                            <tr>
                                                <td>{$acao->USUARIO_cd_usuario}</td>
                                                <td>{$acao->nm_usuario}</td>
                                                <td>{$acao->ds_acao}</td>
                                                <td>{$acao->dt_acao|date_format:"%d/%m/%Y"}</td>
                                                <td>{$acao->hr_acao}</td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}
{block name="validation-script"}
    <!-- DataTables -->
    {*    <script src="{base_url}assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>*}
    <script src="{base_url}assets/vendor/datatables.net-bs/js/jquery.dataTables.min.js"></script>
    <script src="{base_url}assets/vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- DataTables buttons scripts -->
    <script src="{base_url}assets/vendor/pdfmake/build/pdfmake.min.js"></script>
    <script src="{base_url}assets/vendor/pdfmake/build/vfs_fonts.js"></script>
    {*    <script src="{base_url}assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>*}
    <script src="{base_url}assets/vendor/datatables.net-bs/js/buttons.html5.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/buttons.print.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/dataTables.buttons.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/buttons.bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#tabela-acao').DataTable({
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                buttons: [
                    {
                        extend: 'copy', className: 'btn-sm', text: 'Copiar'
                    },
                    {
                        extend: 'csv', title: 'Registro de Log da {$usuariosAcoes[0]->ds_acao}', className: 'btn-sm'
                    },
                    {
                        extend: 'pdf', download: 'open', title: 'Registro de Log da {$usuariosAcoes[0]->ds_acao}', className: 'btn-sm'
                    },
                    {
                        extend: 'print',
                        message: 'Relatório de Controle de Logs',
                        className: 'btn-sm', text: 'Imprimir'
                    }
                ],
                "oLanguage": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "order": [[3, "desc"]]
            });
        });
    </script>
{/block}
