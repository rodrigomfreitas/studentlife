<div class="modal fade" id="myModalAddCurso" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Incluir Curso</h4>
                <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente :D</small>
            </div>
            <div class="modal-body">

                <form id="inc_curso" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class="form-group"><label class="col-sm-2 control-label">Nome</label>
                        <div class="col-sm-10"><input type="text" name="nome_curso" id="nome_curso" class="form-control"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Sigla</label>
                        <div class="col-sm-10"><input type="text" name="sigla_curso" id="sigla_curso" class="form-control"></div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Instituição</label>
                        <div class="col-sm-10">
                            <select name="instituicao_id" id="instituicao_id" class="form-control">
                                {foreach $instituicao as $insti}
                                    <option value="{$insti->id_instituicao}">{$insti->nome}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Semestre Total</label>
                        <div class="col-sm-4"><input type="number" name="semestre_total" id="semestre_total" class="form-control"></div>
                        <label class="col-sm-2 control-label">Semestre Atual</label>
                        <div class="col-sm-4"><input type="number" name="semestre_atual" id="semestre_atual" class="form-control"></div>
                    </div>

            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Salvar</button>

            </div>
            </form>
        </div>
    </div>
</div>