{block name="alt_instituicao_telefone"}
<div class="modal fade" id="myModalEditTelefone" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Alterar Telefone</h4>
                <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente :D</small>
            </div>
            <div class="modal-body">
                <form id="alt_instituicao_telefone" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="id_instituicao_telefone" value="{$instituicao_telefone[0]->id_instituicao_telefone}">
                    <div class="form-group"><label class="col-sm-2 control-label">Instituição</label>
                        <div class="col-sm-10">
                            <select name="edit_instituicao_id" id="edit_instituicao_id" class="form-control">
                                    <option value="{$instituicao_telefone[0]->instituicao_id}">{$instituicao_telefone[0]->nome}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Telefone</label>
                        <div class="col-sm-10"><input type="text" name="edit_telefone" id="edit_telefone" class="form-control" value="{$instituicao_telefone[0]->telefone}"></div>
                    </div>
            </div>

            <div class="modal-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
{/block}