<div class="modal fade" id="myModalAddTelefone" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header text-center">
                <h4 class="modal-title">Incluir Telefone</h4>
                <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente :D</small>
            </div>
            <div class="modal-body">
                <form id="inc_instituicao_telefone" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class="form-group"><label class="col-sm-2 control-label">Instituição</label>
                        <div class="col-sm-10">
                            <select name="instituicao_id" id="instituicao_id" class="form-control">
                                {foreach $instituicao as $insti}
                                    <option value="{$insti->id_instituicao}">{$insti->nome}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="form-group"><label class="col-sm-2 control-label">Telefone</label>
                        <div class="col-sm-10"><input type="text" name="telefone" id="telefone" class="form-control"></div>
                    </div>
            </div>

            <div class="modal-footer">
                <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>
