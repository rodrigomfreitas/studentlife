<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header text-center">
                        <h4 class="modal-title">Alterar Instituição</h4>
                        <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente :D</small>
                    </div>
                    <div class="modal-body">

                        <form id="alt_instituicao" method="post" enctype="multipart/form-data" class="form-horizontal">
                            <input type="hidden" name="id_instituicao" value="{$instituicao[0]->id_instituicao}">
                            <div class="form-group"><label class="col-sm-2 control-label">Nome</label>
                                <div class="col-sm-10"><input type="text" name="nome_instituicao" id="nome_instituicao" class="form-control" value="{$instituicao[0]->nome}"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Sigla</label>
                                <div class="col-sm-10"><input type="text" name="sigla_instituicao" id="sigla_instituicao" class="form-control" value="{$instituicao[0]->sigla}"></div>
                            </div>
                            <div class="form-group"><label class="col-sm-2 control-label">Endereço</label>
                                <div class="col-sm-10"><input type="text" name="endereco_instituicao" id="endereco_instituicao" class="form-control" value="{$instituicao[0]->endereco}"></div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>