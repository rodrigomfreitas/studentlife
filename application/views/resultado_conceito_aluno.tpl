{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Conceitos e Pareceres de <strong>{$dados[0]->nm_usuario}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}conceito_comentario">Conceitos e Pareceres</a>
                            </li>
                            <li>
                                <span>{$dados[0]->nm_usuario}</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            Tabela dos conceitos do aluno e seus pareceres.
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Curso</th>
                                            <th>Tipo</th>
                                            <th>Disciplina</th>
                                            <th>Avaliação</th>
                                            <th>Descrição da Avaliação</th>
                                            <th>Conceito</th>
                                            <th>Pareceres</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach $dados as $dado}
                                            <tr>
                                                <td>{$dado->nm_curso}</td>
                                                <td>{$dado->tipo_nm_avaliacao}</td>
                                                <td>{$dado->nm_disciplina}</td>
                                                <td>{$dado->nm_avaliacao}</td>
                                                <td>{$dado->ds_avaliacao}</td>
                                                <td class="text-center">{$dado->ds_conceito}</td>
                                                <td>{$dado->ds_conceito_comentario}</td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}
{block name="validation-script"}
    
{/block}
