{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplinas do {$semestre[0]->nr_semestre}&ordm; Semestre</h3>
                    {if $sessao_dados['status'] != 3}
                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus"></i> &nbsp;Adicionar</button>
                    {/if}
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}anos/{$semestre[0]->CURSO_cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}semestres/{$semestre[0]->ANO_cd_ano}">Semestres</a>
                            </li>
                            <li>
                                <span>Disciplinas</span>
                            </li>

                        </ol>
                    </div>
                    <h5>Curso de {$semestre[0]->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel" data-effect="fadeIn" data-child="hpanel">
            <div class="row projects">
                {if $sessao_dados['status'] != 3}
                    {if $disciplinas|@count < 1}
                        <div class="col-md-12">
                            <h1>Nenhum Disciplina Cadastrada</h1>
                        </div>
                    {/if}
                {else}
                    {if $disciplinas|@count < 1}
                        <div class="col-md-12">
                            <h1>Você não está matriculado em nenhuma disciplina até o momento</h1>
                        </div>
                    {/if}
                {/if}
                {foreach $disciplinas as $disciplina}
                    <div class="col-lg-6">
                        <div class="hpanel hgreen">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-10">
                                        <h4>{$disciplina->nm_disciplina}</h4>
                                        <p>
                                            {$disciplina->ds_disciplina}
                                        </p>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="project-label">TOTAL DE ALUNOS</div>
                                                <small>{if $alunos_count[$disciplina->cd_disciplina] == 0}Nenhum Aluno Inserido{else}{$alunos_count[$disciplina->cd_disciplina]}{/if}</small>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="project-label">PROFESSOR(ES)</div>
                                                {if $professores[$disciplina->cd_disciplina]|@count == 0}
                                                    <small>Nenhum Professor Inserido</small>
                                                {else} 
                                                    {foreach $professores[$disciplina->cd_disciplina] as $prof}
                                                        <small>{$prof->nm_usuario}</small>
                                                    {/foreach} 
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 project-info">
                                        <div class="project-action m-t-md">
                                            <div class="btn-group">
                                                <!-- Botão de ação -->
                                                {if $sessao_dados['status'] != 3}
                                                    <div class="dropdown text-right">
                                                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
                                                            <button class="text-muted btn btn-default"><i class="fa fa-gear text-primary"></i></button>
                                                        </a>
                                                        <ul class="dropdown-menu animated flipInX m-t-xs pull-right">
                                                            <li><a href="javascript:;" class="editar-disciplina" data-id="{$disciplina->cd_disciplina}" ><i class="fa fa-pencil"></i>&nbsp;Editar</a></li>
                                                            <li><a href="javascript:;" class="excluir-disciplina" data-id="{$disciplina->cd_disciplina}"><i class="fa fa-trash"></i>&nbsp;Remover</a></li>
                                                        </ul>
                                                    </div>
                                                {/if}
                                                <!-- Fim do Botão de ação -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer text-right">
                                <div class="btn-group">
                                    <a href="{base_url}disciplina/{$disciplina->cd_disciplina}" class="btn btn-sm btn-success">Entrar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
    <!-- Modal incluir disciplina -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Incluir Disciplina</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-incluir-disciplina" action="{base_url}disciplinas/incluir" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_semestre" id="cd_semestre" value="{$semestre[0]->cd_semestre}">
                    <div class="modal-body">
                        <div id="validation-error"></div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Disciplina</label>
                            <div class="col-sm-10"><input type="text" name="disciplina" id="disciplina" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Descrição</label>
                            <div class="col-sm-10"><textarea type="text" rows="5" maxlength="180" name="descricao" id="descricao" class="form-control" placeholder="Máximo 180 caracteres"></textarea></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Todos os campos são obrigatórios</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal incluir disciplina -->
    <!-- Modal editar disciplina -->
    <div class="modal fade" id="editar-disciplina-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Editar Disciplina</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-editar-disciplina" action="{base_url}disciplinas/atualizar" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_semestre" id="cd_semestre" value="{$disciplinas[0]->SEMESTRE_cd_semestre}">
                    <div class="modal-body">
                        <div id="validation-error-edit"></div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Disciplina</label>
                            <div class="col-sm-10"><input type="text" name="disciplina" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Descrição</label>
                            <div class="col-sm-10"><textarea type="text" rows="5" maxlength="180" name="descricao" class="form-control" placeholder="Máximo 180 caracteres"></textarea></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Todos os campos são obrigatórios</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                    <input type="hidden" name="cd_disciplina" value="">
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal editar disciplina -->
{/block}

{block name="validation-script"}
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-disciplina').submit(function () {
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $.post($('#form-incluir-disciplina').attr('action'), $('#form-incluir-disciplina').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <script>
        $(document).ready(function () {
            var modal = $("#editar-disciplina-modal");
            $('.editar-disciplina').on("click", function () {
                var cd_disciplina = $(this).data("id");
                $.get(
                        "{base_url}disciplinas/editar/" + cd_disciplina,
                        function (disciplina) {
                            modal.find('[name="disciplina"]').val(disciplina.nm_disciplina);
                            modal.find('[name="descricao"]').val(disciplina.ds_disciplina);
                            modal.find('[name="cd_disciplina"]').val(disciplina.cd_disciplina);

                            modal.modal("show");

                        }
                );
            });
            modal.on("hide.bs.modal", function () {
                modal.find('[name="disciplina"]').val("");
                modal.find('[name="descricao"]').val("");
                modal.find('[name="cd_disciplina"]').val("");
                modal.find('[id="validation-error-edit"]').removeClass('alert alert-warning');
                modal.find('[id="validation-error-edit"]').find('p').remove();
            });

            modal.find("#form-editar-disciplina").on("submit", function (e) {
                e.preventDefault();
                $.post($('#form-editar-disciplina').attr('action'), $('#form-editar-disciplina').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error-edit').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error-edit').removeClass('alert alert-warning');
                        $('#validation-error-edit > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });

            $("#myModal").on("hide.bs.modal", function () {
                $(this).find('[name="disciplina"]').val("");
                $(this).find('[name="descricao"]').val("");
                $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
                $(this).find('[id="validation-error"]').find('p').remove();
            });
        });
    </script>

    <script>
        $(function () {
            $('.excluir-disciplina').click(function () {
                var cd_disciplina = $(this).data("id");
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá a disciplina com suas dependências",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}disciplinas/excluir/" + cd_disciplina,
                                function () {
                                    swal("Removido!", "A disciplina foi removida com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "A disciplina não foi removido :)", "error");
                    }
                });
            });

            $('.homerDemo4').click(function () {
                toastr.error('Error - This is a Homer error notification');
            });

        });


    </script>
{/block}
