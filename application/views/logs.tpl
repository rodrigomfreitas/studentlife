{extends file="templates/template.tpl"}
{block name="imports"}
    <link rel="stylesheet" href="{base_url}assets/vendor/select2-3.5.2/select2.css">
    <link rel="stylesheet" href="{base_url}assets/vendor/select2-bootstrap/select2-bootstrap.css">
{/block}
{block name="content"}
    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Logs</h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Logs</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row">
                {$mensagem}
                <form id="form-conceito" action="{base_url}logs/validar" method="post">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="acao">Ação</label>
                            <select class="form-control" id="cd_acao" name="cd_acao">
                                <option value="">Escolha uma ação...</option>
                                {foreach $acoes as $acao}
                                    <option value="{$acao->cd_acao}">{$acao->ds_acao}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="usuario">Usuário</label>
                            <select class="js-source-states-2" name="cd_usuario[]" id="cd_usuario" style="width: 100%" multiple="multiple">
                                <option value="">Escolha um Usuário</option>
                                {foreach $usuarios as $usuario}
                                    <option value="{$usuario->cd_usuario}">{$usuario->nm_usuario}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="dt_incio">Data Início</label>
                            <input type="date" name="dt_inicio" id="dt_inicio" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="dt_final">Data Final</label>
                            <input type="date" name="dt_final" id="dt_final" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Pesquisar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}
{block name="validation-script"}

    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    </script>
    <script src="{base_url}assets/vendor/select2-3.5.2/select2.min.js"></script>
    <script type="text/javascript">
        $(".js-source-states-2").select2();
    </script>
{/block}
