{extends file="templates/template.tpl"}
{block name="imports"}
    <link rel="stylesheet" href="{base_url}assets/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{base_url}assets/vendor/datatables.net-bs/css/buttons.dataTables.min.css"> 
    <link rel="stylesheet" href="{base_url}assets/vendor/bootstrap-tokenfield/dist/css/bootstrap-tokenfield.min.css">
    <link rel="stylesheet" href="{base_url}assets/vendor/bootstrap-tokenfield/dist/css/tokenfield-typeahead.min.css">
{/block}
{block name="content"}
    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplina de <strong>{$dados_disciplina->nm_disciplina}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}anos/{$dados_disciplina->CURSO_cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}semestres/{$dados_disciplina->ANO_cd_ano}">Semestres</a>
                            </li>
                            <li>
                                <a href="{base_url}disciplinas/{$dados_disciplina->cd_semestre}">Disciplinas</a>
                            </li>
                            <li>
                                <span>{$dados_disciplina->nm_disciplina}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados_disciplina->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel" data-effect="fadeIn" data-child="hpanel">
            <div class="row">
                {include file="templates/disciplina_menu.tpl"}
                <!-- Conteúdo da disciplina interna -->
                <div class="col-md-9">
                    <div class="hpanel">
                        <div class="panel-heading hbuilt">
                            <div class="p-xs h4">
                                <small class="pull-right">
                                    <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus"></i> &nbsp;Adicionar</button>
                                </small>                               
                                <strong>Avaliações</strong>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="tabela-tarefa" cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Tipo</th>
                                            <th>Nome</th>
                                            <th>Descrição</th>
                                            <th>Data Cadastro</th>
                                            <th>Data Final</th>
                                            <th>Ação</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach $avaliacoes as $avaliacao}
                                            <tr>
                                                <td>{$avaliacao->nm_tipo_avaliacao}</td>
                                                <td>{$avaliacao->nm_avaliacao}</td>
                                                <td>{$avaliacao->ds_avaliacao}</td>
                                                <td>{$avaliacao->dt_data_cadastro|date_format:"%d/%m/%Y"}</td>
                                                <td>{$avaliacao->dt_data_final|date_format:"%d/%m/%Y"}</td>
                                                <td style="width: 21%" class="text-center">
                                                    <a class="btn btn-primary2 btn-xs visibilidade-avaliacao" data-id="{$avaliacao->cd_avaliacao}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Visibilidade"><i class="fa fa-eye"></i></a>
                                                        {if isset($turmas_visibilidade[$avaliacao->cd_avaliacao])}
                                                        <a href="{base_url}disciplina/conceitos/{$dados_disciplina->cd_disciplina}/{$avaliacao->cd_avaliacao}" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="" data-original-title="Conceito"><i class="fa fa-star"></i></a>
                                                        {else}
                                                        <a type="button" class="btn btn-success btn-xs" data-container="body" data-toggle="popover" data-placement="left" data-title="Atenção!" data-content="Para entrar na página de Conceitos é preciso selecionar, pelo menos, visibilidade para uma turma." data-original-title="" title=""><i class="fa fa-star"></i></a>
                                                        {/if}
                                                    <a class="btn btn-info btn-xs editar-avaliacao" data-id="{$avaliacao->cd_avaliacao}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"><i class="fa fa-edit"></i></a>
                                                    <a class="btn btn-danger btn-xs excluir-avaliacao" data-id="{$avaliacao->cd_avaliacao}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remover"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fim do conteúdo da disciplina interna -->
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
    <!-- Modal incluir Tarefa -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Incluir Avaliação</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-incluir-avaliacao" action="{base_url}avaliacao/incluir" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_disciplina" id="cd_disciplina" value="{$dados_disciplina->cd_disciplina}">
                    <div class="modal-body">
                        <div id="validation-error"></div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Tipo da Avaliação</label>
                            <div class="col-sm-3">
                                <select name="tipo_avaliacao" id="tipo_avaliacao" class="form-control">
                                    <option></option>
                                    {foreach $tipo_avaliacao as $tipo}
                                        <option value="{$tipo->cd_avaliacao}">{$tipo->nm_tipo_avaliacao} </option>
                                    {/foreach}
                                </select>
                            </div>
                            <label class="col-sm-1 control-label control-label-required">Nome</label>
                            <div class="col-sm-6"><input type="text" name="nome" id="nome" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Descrição</label>
                            <div class="col-sm-10"><textarea type="text" rows="5" name="descricao" id="descricao" class="form-control"></textarea></div>
                        </div>
                        <div class="form-group"><label for="palavras_chave" class="col-sm-2 control-label control-label-required">Palavras-chave</label>
                            <div class="col-sm-10">
                                <div><input type="text" class="form-control" name="palavras_chave" id="tokenfieldInsert" class="form-control"/></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Data do Cadastro</label>
                            <div class="col-sm-4"><input type="date" name="dt_cadastro" id="dt_cadastro" class="form-control"></div>

                            <label class="col-sm-2 control-label control-label-required">Data Final</label>
                            <div class="col-sm-4"><input type="date" name="dt_final" id="dt_final" class="form-control"></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Todos os campos são obrigatórios</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim da Modal incluir Tarefa -->
    <!-- Modal editar Tarefa -->
    <div class="modal fade" id="editar-avaliacao-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Editar Avaliação</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-editar-avaliacao" action="{base_url}avaliacao/atualizar" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_disciplina" id="cd_disciplina" value="{$dados_disciplina->cd_disciplina}">
                    <div class="modal-body">
                        <div id="validation-error-edit"></div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Tipo da Avaliação</label>
                            <div class="col-sm-3">
                                <select name="tipo_avaliacao" id="tipo_avaliacao" class="form-control">
                                    <option></option>
                                    {foreach $tipo_avaliacao as $tipo}
                                        <option value="{$tipo->cd_avaliacao}">{$tipo->nm_tipo_avaliacao} </option>
                                    {/foreach}
                                </select>
                            </div>
                            <label class="col-sm-1 control-label control-label-required">Nome</label>
                            <div class="col-sm-6"><input type="text" name="nome" id="nome" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Descrição</label>
                            <div class="col-sm-10"><textarea type="text" rows="5" name="descricao" id="descricao" class="form-control"></textarea></div>
                        </div>
                        <div class="form-group"><label for="palavras_chave" class="col-sm-2 control-label control-label-required">Palavras-chave</label>
                            <div class="col-sm-10">
                                <div><input type="text" class="form-control" name="palavras_chave" id="tokenfieldEdit" class="form-control"/></div>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Data do Cadastro</label>
                            <div class="col-sm-4"><input type="date" name="dt_cadastro" id="dt_cadastro" class="form-control"></div>

                            <label class="col-sm-2 control-label control-label-required">Data Final</label>
                            <div class="col-sm-4"><input type="date" name="dt_final" id="dt_final" class="form-control"></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Todos os campos são obrigatórios</strong></p>
                        </div>
                    </div>
                    <input type="hidden" name="cd_avaliacao" value="">
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal editar Tarefa -->
    <!-- Modal incluir Turma(s) a uma Avaliação -->
    <div class="modal fade" id="visibilidade-avaliacao-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Dar Visibilidade</h4>
                    <small class="font-bold">Escolha uma ou mais Turmas que irão visualizar esta Avaliação</small>
                </div>
                <form id="form-incluir-turma-avaliacao" action="{base_url}avaliacao/incluir_visibilidade_avaliacao" method="post" class="form-horizontal">
                    <div class="modal-body">
                        <div class="form-group"><label class="col-sm-4 control-label">Turmas</label>
                            {foreach $turmas as $turma}
                                <div class="checkbox checkbox-success checkbox-inline">
                                    <input type="checkbox" id="inlineCheckbox{$turma->cd_turma}" value="{$turma->cd_turma}" name="cd_turmas[]">
                                    <label for="cd_turma"> {$turma->nr_semestre}{$turma->nm_turno|truncate:1:"":true}/{$turma->ano_curso} </label>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                    <input type="hidden" name="cd_disciplina" value="{$dados_disciplina->cd_disciplina}">
                    <input type="hidden" name="cd_avaliacao" value="">
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim da Modal incluir Turma(s) a uma Avaliação -->
{/block}

{block name="validation-script"}
    <script src="{base_url}assets/vendor/datatables.net-bs/js/jquery.dataTables.min.js"></script>
    <script src="{base_url}assets/vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="{base_url}assets/vendor/iCheck/icheck.min.js"></script>
    <script src="{base_url}assets/vendor/bootstrap-tokenfield/dist/js/bootstrap-tokenfield.min.js"></script>
    <!-- DataTables buttons scripts -->
    <script src="{base_url}assets/vendor/pdfmake/build/pdfmake.min.js"></script>
    <script src="{base_url}assets/vendor/pdfmake/build/vfs_fonts.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/buttons.html5.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/buttons.print.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/dataTables.buttons.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/buttons.bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $(document).ready(function () {
            $('#tokenfieldInsert').tokenfield();
            //$('#tokenfieldEdit').tokenfield();
        });
    </script>
    <!-- Script para traduzir o DataTables para português -->
    <script>
        $(document).ready(function () {
            $('#tabela-tarefa').DataTable({
                dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
                buttons: [
                    {
                        extend: 'copy',
                        className: 'btn-sm',
                        text: 'Copiar'
                    },
                    {
                        extend: 'csv',
                        title: 'Avaliações da disciplina {$dados_disciplina->nm_disciplina}',
                        className: 'btn-sm'
                    },
                    {
                        extend: 'pdf',
                        download: 'open',
                        title: 'Avaliações da disciplina {$dados_disciplina->nm_disciplina}',
                        className: 'btn-sm'
                    },
                    {
                        extend: 'print',
                        className: 'btn-sm',
                        text: 'Imprimir'
                    }
                ],
                "oLanguage": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "order": [[4, "asc"]],
                "columnDefs": [
                    {
                        "visible": false, "targets": 2
                    }
                ]
            });
        });
    </script>
    <!-- Fim do Script para traduzir o DataTables para português -->
    <!-- Script para incluir uma avaliação e o Alert toastr -->
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-avaliacao').submit(function () {
                $.post($('#form-incluir-avaliacao').attr('action'), $('#form-incluir-avaliacao').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <!-- Fim do Script para incluir uma avaliação e o Alert toastr -->
    <!-- Script para editar uma avaliação -->
    <script>
        {*        $(document).ready(function () {*}
        var modalEditar = $("#editar-avaliacao-modal");
        $('.editar-avaliacao').on("click", function () {
            var cd_avaliacao = $(this).data("id");
            $.get(
                    "{base_url}avaliacao/editar/" + cd_avaliacao,
                    function (avaliacao) {
                        modalEditar.find('[name="tipo_avaliacao"]').val(avaliacao.TIPO_AVALIACAO_cd_avaliacao);
                        modalEditar.find('[name="nome"]').val(avaliacao.nm_avaliacao);
                        modalEditar.find('[name="descricao"]').val(avaliacao.ds_avaliacao);
                        modalEditar.find('[name="palavras_chave"]').val(avaliacao.palavras_chave_avaliacao);
                        modalEditar.find('[name="dt_cadastro"]').val(avaliacao.dt_data_cadastro);
                        modalEditar.find('[name="dt_final"]').val(avaliacao.dt_data_final);
                        modalEditar.find('[name="cd_avaliacao"]').val(avaliacao.cd_avaliacao);
                        $('#tokenfieldEdit').tokenfield();

                        modalEditar.modal("show");

                    }
            );
        });
        modalEditar.on("hide.bs.modal", function () {
            modalEditar.find('[name="tipo_avaliacao"]').val("");
            modalEditar.find('[name="nome"]').val("");
            modalEditar.find('[name="descricao"]').val("");
            $('#tokenfieldEdit').tokenfield('destroy');
            modalEditar.find('[name="dt_cadastro"]').val("");
            modalEditar.find('[name="dt_final"]').val("");
            modalEditar.find('[name="cd_avaliacao"]').val("");
            modalEditar.find('[id="validation-error-edit"]').removeClass('alert alert-warning');
            modalEditar.find('[id="validation-error-edit"]').find('p').remove();
        });

        modalEditar.find("#form-editar-avaliacao").on("submit", function (e) {
            e.preventDefault();
            $.post($('#form-editar-avaliacao').attr('action'), $('#form-editar-avaliacao').serialize(), function (data) {
                if (data.st == 0)
                {
                    $('#validation-error-edit').addClass('alert alert-warning').html(data.msg);
                }
                else if (data.st == 1)
                {
                    $('#validation-error-edit').removeClass('alert alert-warning');
                    $('#validation-error-edit > p').remove();
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                    toastr.success(data.msg);
                }
                else if (data.st == 2)
                {
                    toastr.error(data.msg);
                }
            }, 'json');
            return false;

        });

        $("#myModal").on("hide.bs.modal", function () {
            $(this).find('[name="tipo_avaliacao"]').val("");
            $(this).find('[name="nome"]').val("");
            $(this).find('[name="descricao"]').val("");
            $(this).find('[name="palavras_chave"]').val("");
            $(this).find('[class="token"]').remove();
            $(this).find('[name="dt_final"]').val("");
            $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
            $(this).find('[id="validation-error"]').find('p').remove();
        });
        {*        });*}
    </script>
    <!-- Fim do Script para editar uma avaliação -->
    <!-- Script de visibilidade para incluir turma à avaliação -->
    <script>
        {*        $(document).ready(function () {*}
        var modalVisibilidade = $("#visibilidade-avaliacao-modal");
        $('.visibilidade-avaliacao').on("click", function () {
            modalVisibilidade.find('input[type="checkbox"]').iCheck('uncheck');

            var cd_avaliacao = $(this).data("id");
            modalVisibilidade.find('[name="cd_avaliacao"]').val(cd_avaliacao);
            $.get(
                    "{base_url}avaliacao/atualizar_visibilidade_avaliacao/" + cd_avaliacao,
                    function (turmasRelacionadas) {
                        for (var i in turmasRelacionadas) {
                            modalVisibilidade.find('#inlineCheckbox' + turmasRelacionadas[i]).iCheck("check");
                        }
                        modalVisibilidade.modal("show");

                    }
            );
        });
        {*        });*}
    </script>
    <script>
        $(document).ready(function () {
            $('#form-incluir-turma-avaliacao').submit(function () {
                $.post($('#form-incluir-turma-avaliacao').attr('action'), $('#form-incluir-turma-avaliacao').serialize(), function (data) {
                    if (data.st == 1)
                    {
                        $('#visibilidade-avaliacao-modal').modal('hide');
                        toastr.success(data.msg);
                        setTimeout(function () {
                            location.reload();
                        }, 500);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;
            });
        });
    </script>
    <!-- Fim do Script de visibilidade para incluir turma à avaliação -->
    <!-- Script para excluir uma avaliação -->
    <script>
        {*        $(function () {*}
        $('.excluir-avaliacao').click(function () {
            var cd_avaliacao = $(this).data("id");
            swal({
                title: "Você tem certeza?",
                text: "Se não você removerá a avaliação",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, quero remover!",
                cancelButtonText: "Não, cancelar!",
                closeOnConfirm: false,
                closeOnCancel: false},
            function (isConfirm) {
                if (isConfirm) {
                    $.get(
                            "{base_url}avaliacao/excluir/" + cd_avaliacao,
                            function () {
                                swal("Removido!", "A avaliacao foi removida com sucesso.", "success");
                                setTimeout(function () {
                                    location.reload();
                                }, 1000);
                            }
                    );
                } else {
                    swal("Cancelado", "A avaliação não foi removido :)", "error");
                }
            });
        });

        $('.homerDemo4').click(function () {
            toastr.error('Error - This is a Homer error notification');
        });

        {*        });*}
    </script>
    <!-- Fim do Script para excluir uma avaliação -->
{/block}
