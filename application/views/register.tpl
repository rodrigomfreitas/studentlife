<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Student Life | Gerenciamento Estudantil</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="{base_url}assets/vendor/fontawesome/css/font-awesome.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/metisMenu/dist/metisMenu.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/animate.css/animate.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/toastr/build/toastr.min.css">

        <!-- App styles -->
        <link rel="stylesheet" href="{base_url}assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
        <link rel="stylesheet" href="{base_url}assets/fonts/pe-icon-7-stroke/css/helper.css">
        <link rel="stylesheet" href="{base_url}assets/styles/style.css">
        <link rel="stylesheet" href="{base_url}assets/styles/static_custom.css">
        <style type="text/css"></style><style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>

    <body>

        <div class="color-line"></div>
        <div class="register-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center m-b-md">
                        <p><img src="{base_url}assets/images/gallery/logo1.png" alt=""></p>
                        <h6>Gerenciamento Estudantil</h6>
                    </div>
                    <div class="text-center m-b-md">
                        <h3>Registro de Usuário</h3>
                        <p>Crie sua conta agora e começe a gerenciar seus estudos</p>
                    </div>
                    <div class="hpanel">
                        <div id="validation-error"></div>
                        <div class="panel-body">
                            <form id="form-registrar-usuario" action="{base_url}register/incluir_usuario" method="post">
                                <div class="row">
                                    <div class="form-group col-lg-4">
                                        <label>Matrícula*</label>
                                        <input type="text" id="matricula" class="form-control" name="matricula">
                                    </div>

                                    <div class="form-group col-lg-8">
                                        <label>Nome Completo*</label>
                                        <input type="text" id="nome" class="form-control" name="nome">
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <label>E-mail*</label>
                                        <input type="text" id="email" class="form-control" name="email">
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Confirmar E-mail*</label>
                                        <input type="text" id="confirmar_email" class="form-control" name="confirmar_email">
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Senha*</label>
                                        <div class="input-group">
                                            <input input type="password" id="senha" class="form-control" name="senha">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default fa fa-question" data-toggle="modal" data-target="#politicas-senhas" type="button" data-show="tip" data-original-title="Política de Senha"></button>
                                            </div>
                                            <div class="input-group-btn">
                                                <a class="btn btn-default fa fa-lightbulb-o sugestao-senha" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sugestão para senha" type="button"></a>
                                            </div>
                                        </div>
                                        <div id="sugestao-senha"></div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label>Confirmar Senha*</label>
                                        <input type="password" id="confirmar_senha" class="form-control" name="confirmar_senha">
                                    </div>
                                    <div class="checkbox col-lg-12">
                                        <p><strong>* Todos os campos são obrigatórios</strong></p>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-success">Registrar</button>
                                    <a href="{base_url}login" class="btn btn-default">Cancelar</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <strong>Student Life</strong> - Gerenciamento Estudantil <br> 2015 Copyright Student Life
                </div>
            </div>
        </div>

        <!-- Modal de política de senhas -->
        <div class="modal fade" id="politicas-senhas" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header text-center">
                        <h4 class="modal-title">Política de Senha</h4>
                        <small class="font-bold">A política de senha da Student Life exige que você use senhas difíceis.</small>
                    </div>
                    <div class="modal-body">
                        <p>A política de senha da Student Life exige que você use <strong>senhas difíceis</strong>. A senha deve ser composta de uma combinação de:</p>
                        <ul>
                            <li>No mínino <strong>seis caracteres</strong>;</li>
                            <li>incluindo <strong>letras maiúsculas e minúsculas</strong>;</li>
                            <li>pelo menos, <strong>um número</strong>.</li>
                                {*<li>e <strong>um símbolo</strong>.</li>*}
                        </ul>
                        <p>Qualquer dúvidas, o sistema sugere algumas senhas como exemplo. Basta <strong>clicar no botão com o ícone da lâmpada</strong> e gerar uma senha.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do modal políticas de senhas -->

        <script src="{base_url}assets/vendor/jquery/dist/jquery.min.js"></script>
        <script src="{base_url}assets/vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="{base_url}assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="{base_url}assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="{base_url}assets/vendor/jquery-flot/jquery.flot.js"></script>
        <script src="{base_url}assets/vendor/jquery-flot/jquery.flot.resize.js"></script>
        <script src="{base_url}assets/vendor/jquery-flot/jquery.flot.pie.js"></script>
        <script src="{base_url}assets/vendor/flot.curvedlines/curvedLines.js"></script>
        <script src="{base_url}assets/vendor/jquery.flot.spline/index.js"></script>
        <script src="{base_url}assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="{base_url}assets/vendor/peity/jquery.peity.min.js"></script>
        <script src="{base_url}assets/vendor/sparkline/index.js"></script>
        <script src="{base_url}assets/scripts/jquery.mask.js"></script>
        <script src="{base_url}assets/scripts/homer.js"></script>
        <script src="{base_url}assets/scripts/forms.js"></script>
        <script src="{base_url}assets/vendor/toastr/build/toastr.min.js"></script>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();

                $("body").tooltip({
                    selector: '[data-show=tip]'
                });
            });
        </script>
        <script type="text/javascript">
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            $(document).ready(function () {
                var base_url = '{base_url}';
                $('#form-registrar-usuario').submit(function () {
                    $('button[type=submit], input[type=submit]').prop('disabled', true);
                    $.post($('#form-registrar-usuario').attr('action'), $('#form-registrar-usuario').serialize(), function (data) {
                        if (data.st == 0)
                        {
                            $('button[type=submit], input[type=submit]').prop('disabled', false);
                            $('#validation-error').addClass('alert alert-warning').html(data.msg);
                        }
                        else if (data.st == 1)
                        {
                            $('#validation-error').removeClass('alert alert-warning');
                            $('#validation-error > p').remove();
                            setTimeout(function () {
                                window.open(base_url + "register/cadastro_confirmado", '_self');
                            }, 1000);

                            //alerta provisória somente para apresentação do TCC I
            {*toastr.success(data.msg);
            setTimeout(function () {
            window.open(base_url + "login", '_self');
            }, 2000);*}
                        }
                        else if (data.st == 2)
                        {
                            $('button[type=submit], input[type=submit]').prop('disabled', false);
                            $('#validation-error').removeClass('alert alert-warning');
                            $('#validation-error > p').remove();
                            toastr.error(data.msg);
                        }
                        else if (data.st == 3)
                        {
                            $('button[type=submit], input[type=submit]').prop('disabled', false);
                            $('#validation-error').removeClass('alert alert-warning');
                            $('#validation-error > p').remove();
                            toastr.error(data.msg);
                        }
                    }, 'json');
                    return false;
                });
            });
        </script>
        <script>
            $(document).ready(function () {
                $('.sugestao-senha').on("click", function () {
                    $.get(
                            "{base_url}register/gerarSenha/",
                            function (senha) {
                                $('#sugestao-senha').addClass('alert alert-info text-center').css("fontSize", "15px").html(senha);
                            }
                    );
                });

            });
        </script>
    </body>
</html>