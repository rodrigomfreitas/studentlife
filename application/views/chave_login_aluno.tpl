<html><head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Student Life Login</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="{base_url}assets/vendor/fontawesome/css/font-awesome.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/metisMenu/dist/metisMenu.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/animate.css/animate.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/toastr/build/toastr.min.css">

        <!-- App styles -->
        <link rel="stylesheet" href="{base_url}assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
        <link rel="stylesheet" href="{base_url}assets/fonts/pe-icon-7-stroke/css/helper.css">
        <link rel="stylesheet" href="{base_url}assets/styles/style.css">
        <link rel="stylesheet" href="{base_url}assets/styles/static_custom.css">

        <style type="text/css"></style><style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>
    <body class="blank">
        <div class="color-line"></div>
        <div class="login-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center m-b-md">
                        <p><img src="{base_url}assets/images/gallery/logo1.png" alt=""></p>
                        <h6>Gerenciamento Estudantil</h6>
                    </div>
                    <span class="text-center m-b-md">
                        <h4>Enviar Solicitação para Matricular-se à Turma</h4>
                    </span>
                    <div class="hpanel">
                        <div class="panel-body">
                            <form id="incluir-solicitacao-cadastro" method="post" action="{base_url}login/incluirSolicitacaoCad">
                                <div class="form-group">
                                    <label class="control-label" for="username">E-mail</label>
                                    <input type="email" placeholder="exemplo@gmail.com" title="Por favor insira seu e-mail" value="" name="email" id="email" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Senha</label>
                                    <input type="password" title="Por favor, insira sua senha" placeholder="******" value="" name="senha" id="senha" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Chave</label>
                                    <input type="text" title="Por favor, insira a chave" value="" name="chave_turma" id="chave_turma" class="form-control">
                                </div>
                                <button class="btn btn-success btn-block" type="submit">Logar</button>
                                <a class="btn btn-default btn-block" href="{base_url}register">Registrar</a>
                            </form>
                            <a class="pull-right btn btn-link" href="{base_url}esqueci-minha-senha">Esqueci minha senha</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <strong>Student Life</strong>  <br>  Gerenciamento Estudantil
                </div>
            </div>
        </div>

        <!-- Vendor scripts -->
        <script src="{base_url}assets/vendor/jquery/dist/jquery.min.js"></script>
        <script src="{base_url}assets/vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="{base_url}assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="{base_url}assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="{base_url}assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="{base_url}assets/vendor/iCheck/icheck.min.js"></script>
        <script src="{base_url}assets/vendor/sparkline/index.js"></script>
        <script src="{base_url}assets/vendor/toastr/build/toastr.min.js"></script>

        <script type="text/javascript">
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            $(document).ready(function () {
                base_url = '{base_url}';
                $('#incluir-solicitacao-cadastro').submit(function () {
                    $('button[type=submit], input[type=submit]').prop('disabled', true);
                    $.post($('#incluir-solicitacao-cadastro').attr('action'), $('#incluir-solicitacao-cadastro').serialize(), function (data) {
                        if (data.st == 0)
                        {
                            $('button[type=submit], input[type=submit]').prop('disabled', false);
                            toastr.error(data.msg);
                            $("#senha").val("");
                        }
                        else if (data.st == 1)
                        {
                            setTimeout(function () {
                                window.open(base_url + "login/mensagemEnvioSolicitacao", '_self');
                            }, 1500, toastr.success(data.msg));
                        }
                        else if (data.st == 2)
                        {
                            $('button[type=submit], input[type=submit]').prop('disabled', false);
                            toastr.info(data.msg);
                            $("#senha").val("");
                            $('#senha').focus();
                        }
                        else if (data.st == 3)
                        {
                            $('button[type=submit], input[type=submit]').prop('disabled', false);
                            toastr.error(data.msg);
                            $("#chave_turma").val("");
                            $("#chave_turma").focus();
                        }
                    }, 'json');
                    return false;
                });
            });
        </script>
    </body>
</html>
