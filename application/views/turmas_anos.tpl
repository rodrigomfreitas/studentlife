{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página-->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Anos </h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <span>Anos</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$cursos[0]->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel" data-effect="fadeIn" data-child="hpanel">
            <div class="row">
                {if $sessao_dados['status'] != 3}
                    {if $anos|@count < 1}
                        <div class="col-md-12">
                            <h1>Nenhum Ano Cadastrado</h1>
                        </div>
                    {/if}
                {else}
                    {if $anos|@count < 1}
                        <div class="col-md-12">
                            <h1>Você não está matriculado em nenhum ano até o momento</h1>
                        </div>
                    {/if}
                {/if}
                {foreach $anos as $ano}
                    <div class="col-md-3">
                        <div class="hpanel hgreen">
                            <div class="panel-body text-center">
                                <!-- Botão de ação -->
                                {if $sessao_dados['status'] != 3}
                                    <div class="dropdown text-right">
                                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
                                            <button class="text-muted btn btn-default"><i class="fa fa-gear text-primary"></i></button>
                                        </a>
                                        <ul class="dropdown-menu animated flipInX m-t-xs pull-right">
                                            <li><a href="javascript:;" class="editar-semestre" data-id="{$ano->cd_ano}" ><i class="fa fa-pencil"></i>&nbsp;Editar</a></li>
                                            <li><a href="javascript:;" class="excluir-semestre" data-id="{$ano->cd_ano}"><i class="fa fa-trash"></i>&nbsp;Remover</a></li>
                                        </ul>
                                    </div>
                                {/if}        
                                <!-- Fim do Botão de ação -->
                                <div class="stats-title text-center">
                                    <h3>{$ano->ano_curso}</h3>
                                </div>
                                <a {if $ano->CURSO_cd_curso == NULL} class="btn btn-sm btn-block btn-default info-ano" data-toggle="modal" data-target="#info-ano" data-id="{$ano->cd_ano}"{else} href="{base_url}turmas/curso/{$ano->cd_ano}" class="btn btn-sm btn-success btn-block"{/if}>Entrar</a>
{*                                <a href="{base_url}turmas/curso/{$ano->cd_ano}" class="btn btn-sm btn-success btn-block">Entrar</a>*}
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        
        <!-- Modal de Informação caso não não exista semestre em um determinado curso -->
        <div class="modal fade hmodal-info" id="info-ano" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header">
                        <h4 class="modal-title">Informações para o próximo passo</h4>
                        <small class="font-bold">Para seguir em frente leia as instruções abaixo.</small>
                    </div>
                    <div class="modal-body">
                        <p>Não foi possível prosseguir ao próximo passo, porque é preciso primeiramente cadastrar, pelo menos, um <strong>semestre</strong>.<br>
                            Clique no botão <strong>Cadastrar</strong> para entrar no módulo de cadastrar semestre.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <a id="url-curso" href="" class="btn btn-primary">Cadastrar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do modal de Informação caso não não exista semestre em um determinado curso -->
        
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}

{block name="validation-script"}
    <script>
        $(document).ready(function () {

            var modal = $("#info-curso");
            $('.info-ano').on("click", function () {
                var cd_ano = $(this).data("id");
                 $("#url-curso").attr("href", "{base_url}semestres/" + cd_ano);
            });
        });
    </script>
{/block}
