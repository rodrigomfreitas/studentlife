<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="<?= $num % 2 == 1 ? 'alert alert-success' : 'alert alert-danger' ?>" role="alert">
        <?php
        $mensagem = "";
        if ($num == 1) {
            $mensagem = 'Ok! Registro Inserido com Sucesso';
        } else if ($num == 2) {
            $mensagem = 'Erro... Não foi possível inserir o registro';
        } else if ($num == 3) {
            $mensagem = 'Ok! Registro Alterado';
        } else if ($num == 4) {
            $mensagem = 'Erro... Alteração não realizada';
        } else if ($num == 5) {
            $mensagem = 'Ok! Registro Excluído Corretamente';
        } else if ($num == 6) {
            $mensagem = 'Erro... Não foi possível excluir o registro';
        }
        echo $mensagem;
        ?>
    </div>
</div>