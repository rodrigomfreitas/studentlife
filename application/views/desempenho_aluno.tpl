{extends file="templates/template.tpl"}
{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplina de <strong>{$dados[0]->nm_disciplina}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}anos/{$dados[0]->CURSO_cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}semestres/{$dados[0]->ANO_cd_ano}">Semestres</a>
                            </li>
                            <li>
                                <a href="{base_url}disciplinas/{$dados[0]->cd_semestre}">Disciplinas</a>
                            </li>
                            <li>
                                <span>{$dados[0]->nm_disciplina}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados[0]->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row projects">
                {include file="templates/disciplina_menu.tpl"}
                <!-- Conteúdo do conceito do aluno -->
                <div class="col-md-9">
                    <div class="hpanel">
                        <div class="panel-heading hbuilt">
                            <div class="p-xs h4">
                                <strong>Desempenho</strong>
                            </div>
                        </div>
                        <div class="hpanel">
                            <div class="panel-body">
                                <div id="container" style="min-width: 350px; height: 400px; margin: 0 auto"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fim do conteúdo do conceito do aluno -->
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}

{block name="validation-script"}
    <script src="{base_url}assets/vendor/highcharts/highcharts.js"></script>
    <script src="{base_url}assets/vendor/highcharts/modules/exporting.js"></script>
    {*    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>*}

    {* <script>
    $(function () {
    $('#container').highcharts({
    chart: {
    type: 'bar'
    },
    title: {
    text: 'Total de Alunos referente aos Conceitos das Avaliações'
    },
    subtitle: {
    text: 'Página das Avaliações de {$dados[0]->nm_disciplina}: <a href="{base_url}disciplina/avaliacao/{$dados[0]->cd_disciplina}">Clique aqui</a>'
    },
    xAxis: {
    categories: {$nmAvaliacao},
    title: {
    text: null
    }
    },
    yAxis: {
    min: 0,
    title: {
    text: 'Alunos (Total)',
    align: 'high'
    },
    labels: {
    overflow: 'justify'
    }
    },
    tooltip: {
    valueSuffix: ' no total'
    },
    plotOptions: {
    bar: {
    dataLabels: {
    enabled: true
    }
    }
    },
    legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'top',
    x: -40,
    y: 80,
    floating: true,
    borderWidth: 1,
    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
    shadow: true
    },
    credits: {
    enabled: false
    },
    series: [{
    name: 'A',
    data: [{join($conceitoA, ',')}]
    }, {
    name: 'B',
    data: [{join($conceitoB, ',')}]
    }, {
    name: 'C',
    data: [{join($conceitoC, ',')}]
    }, {
    name: 'D',
    data: [{join($conceitoD, ',')}]
    }, {
    name: 'SC',
    data: [{join($conceitoSC, ',')}]
    }]
    });
    });
    </script>*}
    {*<script>
    $(function () {
    $('#container').highcharts({
    title: {
    text: 'Combination chart'
    },
    xAxis: {
    categories: {$nmAvaliacao}
    },
    labels: {
    items: [{
    html: 'Total de Alunos referente aos Conceitos',
    style: {
    left: '50px',
    top: '18px',
    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
    }
    }]
    },
    series: [{
    type: 'column',
    name: 'A',
    data: [{join($conceitoA, ',')}]
    }, {
    type: 'column',
    name: 'B',
    data: [{join($conceitoB, ',')}]
    }, {
    type: 'column',
    name: 'C',
    data: [{join($conceitoC, ',')}]
    },{
    type: 'column',
    name: 'D',
    data: [{join($conceitoD, ',')}]
    },{
    type: 'column',
    name: 'SC',
    data: [{join($conceitoSC, ',')}]
    }, {
    type: 'spline',
    name: 'Average',
    data: [3, 2.67, 3],
    marker: {
    lineWidth: 2,
    lineColor: Highcharts.getOptions().colors[3],
    fillColor: 'white'
    }
    }, {
    type: 'pie',
    name: 'Total de Alunos',
    data: [{
    name: 'A',
    y: {$conceitoA|array_sum},
    color: Highcharts.getOptions().colors[0] // Jane's color
    }, {
    name: 'B',
    y: {$conceitoB|array_sum},
    color: Highcharts.getOptions().colors[1] // John's color
    }, {
    name: 'C',
    y: {$conceitoC|array_sum},
    color: Highcharts.getOptions().colors[2] // Joe's color
    },{
    name: 'D',
    y: {$conceitoD|array_sum},
    color: Highcharts.getOptions().colors[3] // Joe's color
    },{
    name: 'S/C',
    y: {$conceitoSC|array_sum},
    color: Highcharts.getOptions().colors[4] // Joe's color
    }],
    center: [100, 80],
    size: 100,
    showInLegend: false,
    dataLabels: {
    enabled: false
    }
    }]
    });
    });
    </script>*}
    <script>
        $(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly Average Rainfall'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
        xAxis: {
            categories: [
                {nmAvaliacao},
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rainfall (mm)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Tokyo',
            data: [{join($conceitoA, ',')}]

        }, {
            name: 'New York',
            data: [{join($conceitoB, ',')}]

        }, {
            name: 'London',
            data: [{join($conceitoC, ',')}]

        }, {
            name: 'Berlin',
            data: [{join($conceitoSC, ',')}]

        }]
    });
});
    </script>
{/block}
