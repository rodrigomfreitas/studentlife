{extends file="templates/template.tpl"}
{block name="imports"}
    <link rel="stylesheet" href="{base_url}assets/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{base_url}assets/vendor/summernote/dist/summernote.css">
{/block}
{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplina de <strong>{$dados_disciplina->nm_disciplina}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}anos/{$dados_disciplina->CURSO_cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}semestres/{$dados_disciplina->ANO_cd_ano}">Semestres</a>
                            </li>
                            <li>
                                <a href="{base_url}disciplinas/{$dados_disciplina->cd_semestre}">Disciplinas</a>
                            </li>
                            <li>
                                <span>{$dados_disciplina->nm_disciplina}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados_disciplina->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row projects">
                {include file="templates/disciplina_menu.tpl"}
                <!-- Conteúdo da disciplina interna -->
                <div class="col-md-9">
                    <div class="hpanel">
                        <div class="panel-heading hbuilt">
                            <div class="p-xs h4">
                                <strong>Enviar E-mail</strong>
                            </div>
                        </div>
                        <div class="panel-heading hbuilt">
                            <div class="p-xs">
                                <form method="get" class="form-horizontal">
                                    <div class="form-group"><label class="col-sm-1 control-label text-left">Para:</label>

                                        <div class="col-sm-11"><input type="text" class="form-control input-sm" placeholder="exemplo@email.com"></div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-1 control-label text-left">Cc:</label>

                                        <div class="col-sm-11"><input type="text" class="form-control input-sm"></div>
                                    </div>
                                    <div class="form-group"><label class="col-sm-1 control-label text-left">Assunto:</label>
                                        <div class="col-sm-11"><input type="text" class="form-control input-sm" placeholder="Digite um assunto ao E-mail"></div>
                                    </div>
                                    <div class="panel-body no-padding">
                                        <textarea class="summertext form-control" rows="8"></textarea>
                                    </div>
                                    <div class="panel-footer">
                                        <div class="text-right">
                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-primary">Enviar E-mail</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- Fim do conteúdo da disciplina interna -->
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}

{block name="validation-script"}
    <script src="{base_url}assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="{base_url}assets/vendor/summernote/dist/summernote.min.js"></script>
    <script src="{base_url}assets/vendor/summernote/lang/summernote-pt-br.js"></script>

    <script>
        $(document).ready(function () {
            $('.summertext').summernote({
                height: 200,
                lang: 'pt-BR'
            });
        });
    </script>
{/block}
