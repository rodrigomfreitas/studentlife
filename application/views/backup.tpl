{extends file="templates/template.tpl"}
{block name="imports"}
    <link rel="stylesheet" href="{base_url}assets/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="{base_url}assets/vendor/datatables.net-bs/css/buttons.dataTables.min.css"> 
    <link rel="stylesheet" href="{base_url}assets/vendor/bootstrap-tokenfield/dist/css/bootstrap-tokenfield.min.css">
{/block}
{block name="content"}
    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Backup</h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Backup</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row">
                <div class="hpanel">
                    <div class="panel-heading hbuilt">
                        <div class="p-xs h4">
                            <small class="pull-right">
                                <a class="btn btn-primary gerar-backup"><i class="fa fa-database"></i>&nbsp;Gerar Backup</a>
                            </small>
                            {if $backups|@count > 0}
                                <strong>Backups gerados</strong>
                            {else}
                                <strong>Nenhum backup até o momento</strong>
                            {/if}
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="tabela-backup" cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Descrição</th>
                                        <th>Data Criação</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {foreach $backups as $backup}
                                        <tr>
                                            <td>{$backup->nm_backup}</td>
                                            <td>{$backup->dt_criacao_backup|date_format:"%d/%m/%Y - %H:%M:%S"}</td>
                                            <td  style="width: 15%" class="text-center">
                                                <a href="{base_url}backup/download/{$backup->cd_backup}" class="btn btn-info btn-xs download-backup" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download"><i class="fa fa-download"></i></a>
                                                <a class="btn btn-primary2 btn-xs restaurar-backup" data-toggle="tooltip" data-placement="top" title="" data-original-title="Restaurar"><i class="fa fa-refresh"></i></a>
                                                <a class="btn btn-danger btn-xs excluir-backup" data-id="{$backup->cd_backup}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Remover"><i class="fa fa-remove"></i></a>
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}
{block name="validation-script"}
    <script src="{base_url}assets/vendor/datatables.net-bs/js/jquery.dataTables.min.js"></script>
    <script src="{base_url}assets/vendor/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <!-- DataTables buttons scripts -->
    <script src="{base_url}assets/vendor/pdfmake/build/pdfmake.min.js"></script>
    <script src="{base_url}assets/vendor/pdfmake/build/vfs_fonts.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/buttons.html5.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/buttons.print.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/dataTables.buttons.min.js"></script>
    <script src="{base_url}assets/vendor/datatables.net-bs/js/buttons.bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    
    <!-- Script da DataTable -->
    <script>
        $('#tabela-backup').DataTable({
            dom: "<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
            buttons: [
                {
                    extend: 'copy',
                    className: 'btn-sm',
                    text: 'Copiar'
                },
                {
                    extend: 'csv',
                    title: 'Registros dos Backups do Sistema',
                    className: 'btn-sm'
                },
                {
                    extend: 'pdf',
                    download: 'open',
                    title: 'Registros dos Backups do Sistema',
                    className: 'btn-sm'
                },
                {
                    extend: 'print',
                    className: 'btn-sm',
                    text: 'Imprimir'
                }
            ],
            "oLanguage": {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "_MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },
            "order": [[1, "desc"]]
        });
    </script>
    <!-- Fim do script da DataTable -->
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }


    </script>
    <!-- Script para gerar backup -->
    <script>
        $(function () {
            $('.gerar-backup').click(function () {
                swal({
                    title: "Gerar Backup?",
                    text: "Você realmente gostaria de gerar um Backup do sistema?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, gostaria!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}backup/criarBackup/",
                                function () {
                                    swal("Parabéns!", "Backup gerado com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "O Backup não foi gerado :)", "error");
                    }
                });
            });
        });
    </script>
    <!-- Script para restaurar backup -->
    <script>
        $(function () {
            $('.restaurar-backup').click(function () {
                swal({
                    title: "Restaurar Backup?",
                    text: "Você realmente gostaria de restaurar este Backup do sistema?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, gostaria!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}backup/restaurarBackup/",
                                function () {
                                    swal("Parabéns!", "Backup restaurado com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "O Backup não foi restaurado :)", "error");
                    }
                });
            });
        });
    </script>
    <!-- Fim do script para restaurar backup -->
    <!-- Script para exclusão de backup -->
    <script>
        $(function () {
            $('.excluir-backup').click(function () {
                var cd_backup = $(this).data("id");
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá o Backup",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}backup/excluir/" + cd_backup,
                                function () {
                                    swal("Removido!", "O Backup foi removido com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "O Backup não foi removido :)", "error");
                    }
                });
            });
        });
    </script>
    <!-- Fim do script para exclusão de backup -->
{/block}
