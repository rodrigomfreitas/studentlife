{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplina de <strong>{$dados[0]->nm_disciplina}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>

                            <li>
                                <a href="{base_url}semestres/{$dados[0]->cd_semestre}">Semestres</a>
                            </li>
                            <li>
                                <a href="{base_url}disciplinas/{$dados[0]->cd_curso}">Disciplinas</a>
                            </li>
                            <li>
                                <span>{$dados[0]->nm_disciplina}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados[0]->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row projects">
                {include file="templates/disciplina_menu.tpl"}
                <!-- Conteúdo da disciplina interna -->
                <div class="col-md-9">
                    <div class="hpanel">
                        <div class="panel-heading hbuilt">
                            <div class="p-xs h4">
                                <small class="pull-right">
                                    <strong>Total:</strong> {$alunos|@count}
                                </small>
                                {if $alunos|@count > 0}
                                    <strong>Alunos matriculados nesta Disciplina</strong>
                                {else}
                                    <strong>Nenhum aluno matriculado nesta Disciplina</strong>
                                {/if}
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Matrícula</th>
                                            <th>Nome do Aluno</th>
                                            <th>Turma</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach $alunos as $aluno}
                                            <tr>
                                                <td>{$aluno->nr_matricula}</td>
                                                <td>{$aluno->nm_usuario}</td>
                                                <td>{$aluno->sg_curso}/{$aluno->nr_semestre}{$aluno->nm_turno|truncate:1:"":true}/{$aluno->dt_turma|date_format:"%Y"}</td>
                                                <td style="width: 21%" class="text-center">
                                                    <a class="btn btn-info btn-xs " data-toggle="tooltip" data-placement="left" title="" data-original-title="Detalhe"><i class="fa fa-info"></i></a>
                                                    <a class="btn btn-warning btn-xs " data-toggle="tooltip" data-placement="top" title="" data-original-title="Enviar e-mail"><i class="fa fa-envelope"></i></a>
                                                    <a class="btn btn-warning2 btn-xs " data-toggle="tooltip" data-placement="top" title="" data-original-title="Enviar notificação"><i class="fa fa-bell"></i></a>
                                                    <a class="btn btn-success btn-xs " data-toggle="tooltip" data-placement="top" title="" data-original-title="Desempenho"><i class="fa fa-area-chart"></i></a>
                                                    <a class="btn btn-danger btn-xs " data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Excluir aluno"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Fim do conteúdo da disciplina interna -->
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
    <!-- Modal incluir disciplina -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Incluir Disciplina</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-incluir-disciplina" action="{base_url}disciplinas/incluir" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_semestre" id="cd_semestre" value="{$semestre[0]->cd_semestre}">
                    <div class="modal-body">
                        <div id="validation-error"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Disciplina</label>
                            <div class="col-sm-10"><input type="text" name="disciplina" id="disciplina" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-10"><textarea type="text" rows="5" maxlength="180" name="descricao" id="descricao" class="form-control" placeholder="Máximo 180 caracteres"></textarea></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal incluir disciplina -->
    <!-- Modal editar disciplina -->
    <div class="modal fade" id="editar-disciplina-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Editar Disciplina</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-editar-disciplina" action="{base_url}disciplinas/atualizar" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_semestre" id="cd_semestre" value="{$disciplinas[0]->SEMESTRE_cd_semestre}">
                    <div class="modal-body">
                        <div id="validation-error-edit"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Disciplina</label>
                            <div class="col-sm-10"><input type="text" name="disciplina" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-10"><textarea type="text" rows="5" maxlength="180" name="descricao" class="form-control" placeholder="Máximo 180 caracteres"></textarea></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                    <input type="hidden" name="cd_disciplina" value="">
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal editar disciplina -->
{/block}

{block name="validation-script"}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-disciplina').submit(function () {
                $.post($('#form-incluir-disciplina').attr('action'), $('#form-incluir-disciplina').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <script>
        $(document).ready(function () {
            var modal = $("#editar-disciplina-modal");
            $('.editar-disciplina').on("click", function () {
                var cd_disciplina = $(this).data("id");
                $.get(
                        "{base_url}disciplinas/editar/" + cd_disciplina,
                        function (disciplina) {
                            modal.find('[name="disciplina"]').val(disciplina.nm_disciplina);
                            modal.find('[name="descricao"]').val(disciplina.ds_disciplina);
                            modal.find('[name="cd_disciplina"]').val(disciplina.cd_disciplina);

                            modal.modal("show");

                        }
                );
            });
            modal.on("hide.bs.modal", function () {
                modal.find('[name="disciplina"]').val("");
                modal.find('[name="descricao"]').val("");
                modal.find('[name="cd_disciplina"]').val("");
                modal.find('[id="validation-error-edit"]').removeClass('alert alert-warning');
                modal.find('[id="validation-error-edit"]').find('p').remove();
            });

            modal.find("#form-editar-disciplina").on("submit", function (e) {
                e.preventDefault();
                $.post($('#form-editar-disciplina').attr('action'), $('#form-editar-disciplina').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error-edit').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error-edit').removeClass('alert alert-warning');
                        $('#validation-error-edit > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });

            $("#myModal").on("hide.bs.modal", function () {
                $(this).find('[name="disciplina"]').val("");
                $(this).find('[name="descricao"]').val("");
                $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
                $(this).find('[id="validation-error"]').find('p').remove();
            });
        });
    </script>

    <script>
        $(function () {
            $('.excluir-disciplina').click(function () {
                var cd_disciplina = $(this).data("id");
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá a disciplina com suas dependências",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}disciplinas/excluir/" + cd_disciplina,
                                function () {
                                    swal("Removido!", "A disciplina foi removida com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "A disciplina não foi removido :)", "error");
                    }
                });
            });

            $('.homerDemo4').click(function () {
                toastr.error('Error - This is a Homer error notification');
            });

        });


    </script>
{/block}
