<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html><head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Student Life Login</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/fontawesome/css/font-awesome.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/metisMenu/dist/metisMenu.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/animate.css/animate.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/vendor/toastr/build/toastr.min.css">

        <!-- App styles -->
        <link rel="stylesheet" href="<?= base_url() ?>assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/fonts/pe-icon-7-stroke/css/helper.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/styles/style.css">
        <link rel="stylesheet" href="<?= base_url() ?>assets/styles/static_custom.css">

        <style type="text/css"></style><style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>
    <body class="blank">
        <div class="color-line"></div>
        <div class="login-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center m-b-md">
                        <h3>Student Life - Gerenciamento Estudantil</h3>
                    </div>
                    <h3> Confirmação de Cadastro </h3>
                    <p>Olá 
                        <?php
                            if ($TIPO_USUARIO_cd_tipo_usuario == 2) {
                                echo "professor(a)" . $nm_usuario;
                            }
                            elseif ($TIPO_USUARIO_cd_tipo_usuario == 3) {
                                echo "aluno(a) " . $nm_usuario;
                            }
                        ?>
                    </p>
                    <p>Muito obrigado por se cadastrar no Student Life - Gerenciamento Estudentil.</p>
                    <p>Para concluir seu cadastro e liberar o acesso no sistema, clique no link abaixo.</p>
                    <p><a href="<?= base_url("register/confirmar/" . md5($ds_email)) ?>" >
                            Confirmar cadastro no site.</a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <strong>Student Life - Gerenciamento Estudantil</strong>
                </div>
            </div>
        </div>


        <!-- Vendor scripts -->
        <script src="<?= base_url() ?>assets/vendor/jquery/dist/jquery.min.js"></script>
        <script src="<?= base_url() ?>assets/vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="<?= base_url() ?>assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="<?= base_url() ?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="<?= base_url() ?>assets/vendor/iCheck/icheck.min.js"></script>
        <script src="<?= base_url() ?>assets/vendor/sparkline/index.js"></script>

        <!-- App scripts -->
        <script src="<?= base_url() ?>  assets/scripts/forms.js"></script>


    </body></html>
