
{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <div class="col-lg-12">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                        <a class="closebox"><i class="fa fa-times"></i></a>
                    </div>
                    Conceitos
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Nome do Aluno</th>
                                        {foreach $avaliacoes as $avaliacao}
                                        <th>{$avaliacao->nome} </th>
                                        {/foreach}



                                </tr>
                            </thead>
                            <tbody>
                                {foreach $alunos as $aluno}
                                    <tr>

                                        <td>{$aluno->nome} {$aluno->sobrenome}</td>
                                        {foreach $avaliacoes as $avaliacao}
                                            <td>
                                                <a target="_blank" href="{base_url}conceitos/a_publish/{$avaliacao->id_avaliacao}/{$aluno->id_usuario}/A" class="btn btn-default btn-xs a_publish">A</a>
                                                <a target="_blank" href="{base_url}conceitos/a_publish/{$avaliacao->id_avaliacao}/{$aluno->id_usuario}/B" class="btn btn-default btn-xs a_publish">B</a>
                                                <a target="_blank" href="{base_url}conceitos/a_publish/{$avaliacao->id_avaliacao}/{$aluno->id_usuario}/C" class="btn btn-default btn-xs a_publish">C</a>
                                                <a target="_blank" href="{base_url}conceitos/a_publish/{$avaliacao->id_avaliacao}/{$aluno->id_usuario}/D" class="btn btn-default btn-xs a_publish">D</a>
                                            </td>
                                        {/foreach}
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        {include file="templates/footer.tpl"}              
    </div>
{/block}
