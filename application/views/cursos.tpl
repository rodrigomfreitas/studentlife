{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Cursos</h3>
                    {if $sessao_dados['status'] != 3}
                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus"></i> &nbsp;Adicionar</button>
                    {/if}
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Cursos</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel" data-effect="fadeIn" data-child="hpanel">
            <div class="row">
                {if $sessao_dados['status'] != 3}
                    {if $cursos|@count < 1}
                        <div class="col-md-12">
                            <h1>Nenhum Curso Cadastrado</h1>
                        </div>
                    {/if}
                {else}
                    {if $cursos|@count < 1}
                        <div class="col-md-12">
                            <h1>Você não está matriculado em nenhum curso até o momento</h1>
                        </div>
                    {/if}
                {/if}
                {foreach $cursos as $curso}
                    <div class="col-md-3">
                        <div class="hpanel hgreen">
                            <div class="panel-body">
                                <div class="text-center">
                                    <!-- Botão de ação -->
                                    {if $sessao_dados['status'] != 3}
                                        <div class="dropdown text-right">
                                            <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
                                                <button class="text-muted btn btn-default"><i class="fa fa-gear text-primary"></i></button>
                                            </a>
                                            <ul class="dropdown-menu animated flipInX m-t-xs pull-right">
                                                <li><a href="javascript:;" class="editar-curso" data-id="{$curso->cd_curso}" ><i class="fa fa-pencil"></i>&nbsp;Editar</a></li>
                                                <li><a href="javascript:;" class="excluir-curso" data-id="{$curso->cd_curso}"><i class="fa fa-trash"></i>&nbsp;Remover</a></li>
                                            </ul>
                                        </div>
                                    {/if}
                                    <!-- Fim do Botão de ação -->
                                    <h3 class="m-b-xs">{$curso->sg_curso}</h3>
{*                                    <p class="font-bold text-success">{$curso->nm_curso|truncate:30:"...":true}</p>*}
                                    <p class="font-bold text-success">{$curso->nm_curso}</p>
                                    {*<div class="m">
                                    {$curso->ano}
                                    </div>*}

                                    <a href="{base_url}anos/{$curso->cd_curso}" class="btn btn-sm btn-success btn-block">Entrar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
    <!-- Modal de incluir curso -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Incluir Curso</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-incluir-curso" action="{base_url}cursos/incluir" method="post" class="form-horizontal">
                    <div class="modal-body">
                        <div id="validation-error"></div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Nome</label>
                            <div class="col-sm-10"><input type="text" name="nome" id="nome" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label-required">Sigla</label>
                            <div class="col-sm-3"><input type="text" name="sigla" id="sigla" class="form-control"></div>
                            <label class="col-sm-4 control-label control-label-required">Total de Semestres</label>
                            <div class="col-sm-3"><input type="number" min="1" name="semestre_total" id="semestre_total" class="form-control"></div>
                        </div>
                        {*<div class="form-group"><label class="col-sm-2 control-label control-label-required">Data Início</label>
                        <div class="col-sm-5"><input type="date" name="ano_inicio" id="ano_inicio" class="form-control"></div>
                        </div>*}
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Todos os campos são obrigatórios</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal de incluir curso -->
    <!-- Modal editar curso -->
    <div class="modal fade" id="editar-curso-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Editar Curso</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-editar-curso" action="{base_url}cursos/atualizar" method="post" class="form-horizontal">
                    <div class="modal-body">
                        <div id="validation-error-edit"></div>
                        <div class="form-group"><label class="col-sm-2 control-label control-label-required">Nome</label>
                            <div class="col-sm-10"><input type="text" name="nome" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label control-label-required">Sigla</label>
                            <div class="col-sm-3"><input type="text" name="sigla" class="form-control"></div>
                            <label class="col-sm-4 control-label control-label-required">Total de Semestres</label>
                            <div class="col-sm-3"><input type="number" min="1" name="semestre_total" class="form-control"></div>
                        </div>
                        {*<div class="form-group"><label class="col-sm-2 control-label control-label-required">Data Início</label>
                        <div class="col-sm-5"><input type="date" name="ano_inicio" class="form-control"></div>
                        </div>*}
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* Todos os campos são obrigatórios</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                    <input type="hidden" name="cd_curso" value="">
                    <input type="hidden" name="ds_caminho" value="">
                </form>
            </div>
        </div>
    </div>
    <!-- Modal de editar curso -->
{/block}
{block name="validation-script"}

    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-curso').submit(function () {
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $.post($('#form-incluir-curso').attr('action'), $('#form-incluir-curso').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 1500);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <script>
        $(document).ready(function () {

            var modal = $("#editar-curso-modal");
            $('.editar-curso').on("click", function () {
                var cd_curso = $(this).data("id");
                $.get(
                        "{base_url}cursos/editar/" + cd_curso,
                        function (curso) {
                            modal.find('[name="nome"]').val(curso.nm_curso);
                            modal.find('[name="sigla"]').val(curso.sg_curso);
                            modal.find('[name="semestre_total"]').val(curso.nr_semestre_total);
        {*                            modal.find('[name="ano_inicio"]').val(curso.dt_curso_inicio);*}
                            modal.find('[name="cd_curso"]').val(curso.cd_curso);
                            modal.find('[name="ds_caminho"]').val(curso.ds_caminho_curso);

                            modal.modal("show");

                        }
                );
            });
            modal.on("hide.bs.modal", function () {
                modal.find('[name="nome"]').val("");
                modal.find('[name="sigla"]').val("");
                modal.find('[name="semestre_total"]').val("");
        {*                modal.find('[name="ano_inicio"]').val("");*}
                modal.find('[name="cd_curso"]').val("");
                modal.find('[id="validation-error-edit"]').removeClass('alert alert-warning');
                modal.find('[id="validation-error-edit"]').find('p').remove();
            });

            modal.find("#form-editar-curso").on("submit", function (e) {
                e.preventDefault();
                $.post($('#form-editar-curso').attr('action'), $('#form-editar-curso').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error-edit').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error-edit').removeClass('alert alert-warning');
                        $('#validation-error-edit > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });

            $("#myModal").on("hide.bs.modal", function () {
                $(this).find('[name="nome"]').val("");
                $(this).find('[name="sigla"]').val("");
                $(this).find('[name="semestre_total"]').val("");
        {*                $(this).find('[name="ano_inicio"]').val("");*}
                $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
                $(this).find('[id="validation-error"]').find('p').remove();
            });
        });
    </script>

    <script>
        $(function () {
            $('.excluir-curso').click(function () {
                var cd_curso = $(this).data("id");
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá o curso com suas dependências",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}cursos/excluir/" + cd_curso,
                                function () {
                                    swal("Removido!", "O curso foi removido com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "O curso não foi removido :)", "error");
                    }
                });
            });

            $('.homerDemo4').click(function () {
                toastr.error('Error - This is a Homer error notification');
            });

        });


    </script>
{/block}
