{extends file="templates/template.tpl"}

{block name="content"}
    <div id="wrapper">

        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Dados do Usuário</h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Perfil</span>
                            </li>
                            <li>
                                <span>Editar</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="content animate-panel" data-effect="fadeIn" data-child="form-usuario">
            <form id="form-alt-usuario" class="form-usuario" action="{base_url}usuario/atualizar_usuario/{$sessao_dados['id']|md5}">
                <div class="form-group">
                    <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;Salvar</button>
                </div>
                <div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Atenção, para efetivar as alterações feitas no seu perfil clique no botão <strong>Salvar</strong></div>
                <div id="validation-error-dados"></div>
                <div class="hpanel">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true" class="font-bold"><i class="fa fa-laptop"></i> Dados</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2" aria-expanded="true" class="font-bold"><i class="fa pe-7s-pin"></i> Endereço</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3" aria-expanded="true" class="font-bold"><i class="fa fa-phone"></i> Telefones</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="true" class="font-bold"><i class="fa pe-7s-key"></i> Alterar Senha</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="hidden" name="id_usuario" value="{$dados->cd_usuario}"> 
                                        <label for="matricula">Matrícula</label>                                        
                                        <input type="text" class="form-control" id="matricula" name="matricula" 
                                               placeholder="Matrícula..."value="{$dados->nr_matricula}" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">E-mail</label>
                                        <input type="email" class="form-control" id="email" name="email" 
                                               placeholder="E-mail..." value="{$dados->ds_email}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="data_nasc">Data de Nascimento</label>
                                        <input type="date" class="form-control" id="data_nasc" name="data_nasc" 
                                               placeholder="Data de Nascimento..." value="{$dados->dt_data_nasc}">
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="nome">Nome</label>                                       
                                        <input type="text" class="form-control" id="nome" name="nome" 
                                               placeholder="Nome..."value="{$dados->nm_usuario}">
                                    </div>
                                </div>
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cpf">CPF</label>
                                        <input type="text" class="form-control cpf" id="cpf" maxlength="14" name="cpf" 
                                               placeholder="CPF..." value="{$dados->nr_cpf}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cep">CEP</label>
                                        <input type="text" class="form-control" id="cep" name="cep" 
                                               placeholder="CEP..." value="{$dados->nr_cep}">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="endereco">Endereço</label>
                                        <input type="text" class="form-control" id="endereco" name="endereco" 
                                               placeholder="Endereço..." value="{$dados->ds_endereco}">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="numero">Número</label>
                                        <input type="text" class="form-control" id="numero" name="numero" 
                                               placeholder="Número..." value="{$dados->nr_endereco}">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="complemento">Complemento</label>
                                        <input type="text" class="form-control" id="complemento" name="complemento" 
                                               placeholder="Complemento..." value="{$dados->ds_complemento}">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="bairro">Bairro</label>
                                        <input type="text" class="form-control" id="bairro" name="bairro" 
                                               placeholder="Bairro..." value="{$dados->ds_bairro}">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="cidade">Cidade</label>
                                        <input type="text" class="form-control" id="cidade" name="cidade" 
                                               placeholder="Cidade..." value="{$dados->ds_cidade}">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="estado">Estado</label>
                                        <select class="form-control" id="estado" name="estado" 
                                                placeholder="Estado...">
                                            <option value="" {if $dados->sg_estado == ''} selected="selected"{/if}></option>
                                            <option value="AC" {if $dados->sg_estado == 'AC'} selected="selected"{/if}>AC</option>
                                            <option value="AL" {if $dados->sg_estado == 'AL'} selected="selected"{/if}>AL</option>
                                            <option value="AP" {if $dados->sg_estado == 'AP'} selected="selected"{/if}>AP</option>
                                            <option value="AM" {if $dados->sg_estado == 'AM'} selected="selected"{/if}>AM</option>
                                            <option value="BA" {if $dados->sg_estado == 'BA'} selected="selected"{/if}>BA</option>
                                            <option value="CE" {if $dados->sg_estado == 'CE'} selected="selected"{/if}>CE</option>
                                            <option value="DF" {if $dados->sg_estado == 'DF'} selected="selected"{/if}>DF</option>
                                            <option value="ES" {if $dados->sg_estado == 'ES'} selected="selected"{/if}>ES</option>
                                            <option value="GO" {if $dados->sg_estado == 'GO'} selected="selected"{/if}>GO</option>
                                            <option value="MA" {if $dados->sg_estado == 'MA'} selected="selected"{/if}>MA</option>
                                            <option value="MT" {if $dados->sg_estado == 'MT'} selected="selected"{/if}>MT</option>
                                            <option value="MS" {if $dados->sg_estado == 'MS'} selected="selected"{/if}>MS</option>
                                            <option value="MG" {if $dados->sg_estado == 'MG'} selected="selected"{/if}>MG</option>
                                            <option value="PA" {if $dados->sg_estado == 'PA'} selected="selected"{/if}>PA</option>
                                            <option value="PB" {if $dados->sg_estado == 'PB'} selected="selected"{/if}>PB</option>
                                            <option value="PR" {if $dados->sg_estado == 'PR'} selected="selected"{/if}>PR</option>
                                            <option value="PE" {if $dados->sg_estado == 'PE'} selected="selected"{/if}>PE</option>
                                            <option value="PI" {if $dados->sg_estado == 'PI'} selected="selected"{/if}>PI</option>
                                            <option value="RJ" {if $dados->sg_estado == 'RJ'} selected="selected"{/if}>RJ</option>
                                            <option value="RN" {if $dados->sg_estado == 'RN'} selected="selected"{/if}>RN</option>
                                            <option value="RS" {if $dados->sg_estado == 'RS'} selected="selected"{/if}>RS</option>
                                            <option value="RO" {if $dados->sg_estado == 'RO'} selected="selected"{/if}>RO</option>
                                            <option value="RR" {if $dados->sg_estado == 'RR'} selected="selected"{/if}>RR</option>
                                            <option value="SC" {if $dados->sg_estado == 'SC'} selected="selected"{/if}>SC</option>
                                            <option value="SP" {if $dados->sg_estado == 'SP'} selected="selected"{/if}>SP</option>
                                            <option value="SE" {if $dados->sg_estado == 'SE'} selected="selected"{/if}>SE</option>
                                            <option value="TO" {if $dados->sg_estado == 'TO'} selected="selected"{/if}>TO</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="telefone">Telefone</label>
                                        <input type="text" class="form-control telefone" id="telefone" name="telefone" 
                                               placeholder="Telefone..." value="{$dados->nr_telefone}">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="celular">Celular</label>
                                        <input type="text" class="form-control celular" id="celular" name="celular" 
                                               placeholder="Celular..." value="{$dados->nr_celular}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class=" control-label">Senha Atual</label>
                                        <input type="password" name="senha_atual" id="senha_atual" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Nova Senha</label>
                                        <input type="password" name="nova_senha" id="nova_senha" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Repetir Nova Senha</label>
                                        <input type="password" name="repetir_nova_senha" id="repetir_nova_senha" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        {include file="templates/footer.tpl"}
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Alterar Senha</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente :D</small>
                </div>
                <div id="validation-error"></div>
                <form id="form-alt-senha" method="post" action="{base_url}usuario/atualizar_senha/{$sessao_dados['id']|md5}" class="form-horizontal">
                    <div class="modal-body">
                        <div class="form-group"><label class="col-sm-5 control-label">Senha Atual</label>
                            <div class="col-sm-5"><input type="password" name="senha_atual" id="senha_atual" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-5   control-label">Nova Senha</label>
                            <div class="col-sm-5"><input type="password" name="nova_senha" id="nova_senha" class="form-control"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-5 control-label">Repetir Nova Senha</label>
                            <div class="col-sm-5"><input type="password" name="repetir_nova_senha" id="repetir_nova_senha" class="form-control"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


{/block}

{block name="validation-script"}
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-alt-senha').submit(function () {
                $.post($('#form-alt-senha').attr('action'), $('#form-alt-senha').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error').addClass('alert alert-danger').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-danger');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                    else if (data.st == 3)
                    {
                        $('#validation-error').removeClass('alert alert-danger');
                        $('#validation-error > p').remove();
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });
        });

        $(document).ready(function () {
            $('#form-alt-usuario').submit(function () {
                $.post($('#form-alt-usuario').attr('action'), $('#form-alt-usuario').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error-dados').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error-dados').removeClass('alert alert-warning');
                        $('#validation-error-dados > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                    else if (data.st == 3)
                    {
                        $('#validation-error-dados').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 4)
                    {
                        $('#validation-error-dados').removeClass('alert alert-danger');
                        $('#validation-error-dados > p').remove();
                        toastr.error(data.msg);
                    }
                }, 'json');
                return false;

            });
        });

        {*$('#form-usuario').submit(function (event) {
            event.preventDefault();
            base_url = '{base_url}';
            var matricula = $('#matricula').val();
            var email = $('#email').val();
            var nome = $('#nome').val();
            var sobrenome = $('#sobrenome').val();

            if (matricula == '') {

                Alert("warning", "Atenção", "O campo Matrícula é obrigatório.");

            } else if (email == '') {

                Alert("warning", "Atenção", "O campo E-mail é obrigatório.");

            } else if (nome == '') {

                Alert("warning", "Atenção", "O campo Nome é obrigatório.");

            } else if (sobrenome = '') {

                Alert("warning", "Atenção", "O campo Sobrenome é orbrigatório.");

            }
            else {

                var dados = jQuery(this).serialize();
                console.log(dados);
                jQuery.ajax({
                    type: "POST",
                    url: base_url + "usuario/atualizar_usuario/{$sessao_dados['id']}",
                    data: dados,
                    success: function (data)
                    {
                        if (data == 1) {

                            toastr.success("Dados alterado com suscesso.");
                            setTimeout(function () {
                                location.reload();
                            }, 2000);

                        } else if (data == 2) {
                            Alert("warning", "Atenção", "Esse e-mail já consta em nosso sistema, favor insira outro e-mail.");
                        } else {
                            Alert("error", "ERRO", "Ocorreu um Erro, por favor, tente novamente.");
                        }

                    }
                });
                return false;
            }
        });*}
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            base_url = '{base_url}';
            /* Executa a requisição quando o campo CEP perder o foco */
            $('#cep').blur(function () {
                /* Configurações da requisição AJAX */
                $.ajax({
                    url: base_url + 'ajax/buscaCep', /* Classe/método que será chamado */
                    type: 'POST', /* Tipo da requisição */
                    data: 'cep=' + $('#cep').val(), /* dado que será enviado via POST */
                    dataType: 'json', /* Tipo de transmissão */
                    success: function (data) {
                        if (data.sucesso >= 1) {
                            $('#endereco').val(data.ds_endereco);
                            $('#bairro').val(data.ds_bairro);
                            $('#cidade').val(data.ds_cidade);
                            $('#estado').val(data.sg_estado);

                            $('#numero').focus();
                        } else {
                            toastr["error"]("CEP não encontrado");
                        }
                    }
                });
                return false;
            });
        });
    </script>

{/block}
