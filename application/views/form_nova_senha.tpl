<html><head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- Page title -->
        <title>Student Life Login</title>

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <!--<link rel="shortcut icon" type="image/ico" href="favicon.ico" />-->

        <!-- Vendor styles -->
        <link rel="stylesheet" href="{base_url}assets/vendor/fontawesome/css/font-awesome.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/metisMenu/dist/metisMenu.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/animate.css/animate.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" href="{base_url}assets/vendor/toastr/build/toastr.min.css">

        <!-- App styles -->
        <link rel="stylesheet" href="{base_url}assets/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
        <link rel="stylesheet" href="{base_url}assets/fonts/pe-icon-7-stroke/css/helper.css">
        <link rel="stylesheet" href="{base_url}assets/styles/style.css">
        <link rel="stylesheet" href="{base_url}assets/styles/static_custom.css">

        <style type="text/css"></style><style type="text/css">.jqstooltip { position: absolute;left: 0px;top: 0px;visibility: hidden;background: rgb(0, 0, 0) transparent;background-color: rgba(0,0,0,0.6);filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);-ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";color: white;font: 10px arial, san serif;text-align: left;white-space: nowrap;padding: 5px;border: 1px solid white;z-index: 10000;}.jqsfield { color: white;font: 10px arial, san serif;text-align: left;}</style></head>
    <body class="blank">



        <div class="color-line"></div>


        <div class="login-container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center m-b-md">
                        <h3>STUDENT LIFE</h3>
                        <small>Gerenciamento Estudantil</small>
                    </div>
                    <div class="hpanel">
                        <div class="panel-body">
                            <form id="form-nova-senha" method="post">
                                <div class="form-group">
                                    <label class="control-label" for="senha-atual">Senha atual</label>
                                    <input type="password" placeholder="******" title="Por favor insira sua senha atual" value="" name="senha-atual" id="senha-atual" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="nova-senha">Nova senha</label>
                                    <input type="password" title="Por favor, insira sua senha" placeholder="******" value="" name="nova-senha" id="nova-senha" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="repetir-nova-senha">Repetir nova senha</label>
                                    <input type="password" title="Por favor, insira sua senha" placeholder="******" value="" name="repetir-nova-senha" id="repetir-nova-senha" class="form-control">
                                </div>
                                <button class="btn btn-success btn-block" type="submit">Atualizar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <strong>Student Life</strong>  <br>  Gerenciamento Estudantil
                </div>
            </div>
        </div>


        <!-- Vendor scripts -->
        <script src="{base_url}assets/vendor/jquery/dist/jquery.min.js"></script>
        <script src="{base_url}assets/vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="{base_url}assets/vendor/slimScroll/jquery.slimscroll.min.js"></script>
        <script src="{base_url}assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="{base_url}assets/vendor/metisMenu/dist/metisMenu.min.js"></script>
        <script src="{base_url}assets/vendor/iCheck/icheck.min.js"></script>
        <script src="{base_url}assets/vendor/sparkline/index.js"></script>
        <script src="{base_url}assets/vendor/toastr/build/toastr.min.js"></script>

        <script type="text/javascript">
            function Alert(comando, texto, titulo) {
                Command: toastr[comando](titulo, texto)
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-center",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            }

            $('#form-nova-senha').submit(function (event) {
                base_url = '{base_url}';
                event.preventDefault();
                var id_usuario = {$id_usuario};
                var senha_atual = $('#senha-atual').val();
                var nova_senha = $('#nova-senha').val();
                var repetir_nova_senha = $('#repetir-nova-senha').val();
                if (senha_atual == '') {

                    Alert("warning", "ERROR", "Preencha o campo Senha atual!");
                } else if (nova_senha == '') {

                    Alert("warning", "ERROR", "Preencha o campo Nova senha!");
                } else if (repetir_nova_senha == '') {

                    Alert("warning", "ERROR", "Preencha o campo Repetir nova senha!");
                } else if (nova_senha != repetir_nova_senha) {
                    Alert("warning", "ERROR", "A não confere com a nova senha!");
                } else {

                    var dados = jQuery(this).serialize();
                    console.log(dados);
                    jQuery.ajax({
                        type: "POST",
                        url: base_url + "login/nova_senha/" + id_usuario,
                        data: dados,
                        success: function (data)
                        {
                            if (data == 1) {

                                Alert("success", "SUCESSO", "Senha alterada com sucesso");
                                setTimeout(function () {
                                    window.open('{base_url}home', '_self');
                                }, 2000);
                            } else {
                                Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                            }

                        }
                    });
                    return false;
                }
            });</script>

        <!-- App scripts -->


    </body></html>
