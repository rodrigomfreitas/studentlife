{extends file="templates/template.tpl"}
{block name="imports"}
{/block}
{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Disciplina de <strong>{$dados_disciplina->nm_disciplina}</strong></h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <a href="{base_url}anos/{$dados_disciplina->CURSO_cd_curso}">Anos</a>
                            </li>
                            <li>
                                <a href="{base_url}semestres/{$dados_disciplina->ANO_cd_ano}">Semestres</a>
                            </li>
                            <li>
                                <a href="{base_url}disciplinas/{$dados_disciplina->cd_semestre}">Disciplinas</a>
                            </li>
                            <li>
                                <span>{$dados_disciplina->nm_disciplina}</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$dados_disciplina->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row projects">
                {include file="templates/disciplina_menu.tpl"}
                <!-- Conteúdo -->
                <form id="form-incluir-aluno" action="{base_url}aluno_disciplina/incluir" method="post">
                    <div class="col-md-9">
                        <div class="hpanel">
                            <div class="panel-heading hbuilt">
                                <div class="p-xs h4">
                                    <small class="pull-right">
                                        <button class="btn btn-primary pull-right" type="submit" {if $alunosDisciplinas|@count < 1}disabled{/if}> <i class="fa fa-save"></i> &nbsp;Inserir</button>
                                    </small>                               
                                    <strong>Adicionar Alunos à Disciplina</strong>
                                </div>
                            </div>
                            <div class="panel-body">
                                {if $alunosDisciplinas|@count < 1}
                                    <div class="col-md-12">
                                        <h4>Nenhuma notificação</h4>
                                    </div>
                                {else}
                                    <div class="col-lg-12">
                                        <p>Para inserir um aluno à disciplina, clica-se no <i>check box</i> "Sim", depois no botão "Salvar"</p>
                                        <div class="hpanel">
                                            <div class="panel-body" style="display: block;">
                                                <div class="table-responsive">
                                                    <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Matrícula</th>
                                                                <th>Nome do Aluno</th>
                                                                <th>Turma</th>
                                                                <th>Inserir</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {foreach $alunosDisciplinas as $aluno}
                                                                <tr>
                                                                    <td>{$aluno->nr_matricula}</td>
                                                                    <td>{$aluno->nm_usuario}</td>
                                                                    <td>{$aluno->nr_semestre}{$aluno->nm_turno|truncate:1:"":true}/{$aluno->ano_curso}</td>
                                                                    <td>
                                                                        <div class="checkbox checkbox-success checkbox-inline">
                                                                            <input type="checkbox" id="inlineCheckbox{$aluno->USUARIO_cd_usuario}" checked value="{$aluno->USUARIO_cd_usuario}" name="cd_usuarios[]">
                                                                            <label for="cd_turma"> Sim </label>
                                                                            <input type="hidden" name="cd_turma[]" value="{$aluno->TURMA_cd_turma}">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                
                                                            {/foreach}
                                                        <input type="hidden" name="cd_disciplina" value="{$aluno->DISCIPLINA_cd_disciplina}">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="panel-footer" style="display: block;">
                                                Total de linhas: <strong>{$alunosDisciplinas|count} </strong>
                                            </div>
                                        </div>
                                    </div>
                                {/if}
                                <!-- Fim do conteúdo -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}

{block name="validation-script"}
    <script src="{base_url}assets/vendor/iCheck/icheck.min.js"></script>
        <!-- Script para incluir uma aula e o Alert toastr -->
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-aluno').submit(function () {
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $.post($('#form-incluir-aluno').attr('action'), $('#form-incluir-aluno').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        toastr.error(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                    else if (data.st == 3)
                    {
                        toastr.info(data.msg);
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <!-- Script para incluir uma aula e o Alert toastr -->
    <!-- Script para editar uma aula -->
    <script>
        $(document).ready(function () {

            $("#myModal").on("hide.bs.modal", function () {
                $(this).find('[name="cd_usuario"]').val("");
                $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
                $(this).find('[id="validation-error"]').find('p').remove();
            });
        });
    </script>
    <!-- Script para editar uma aula -->
    <!-- Script para excluir uma aula -->
    <script>
        $(function () {
            $('.excluir-professor').click(function () {
                var cd_usuario = $(this).data("id");
                var cd_disciplina = {$dados_disciplina->cd_disciplina};
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá o professor que ministra esta disciplina",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}professorDisciplina/excluir/" + cd_usuario + "/" + cd_disciplina,
                                function () {
                                    swal("Removida!", "O professor foi removido com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "O professor não foi removido :)", "error");
                    }
                });
            });
        });
    </script>
    <!-- Fim do Script para excluir uma aula -->

{/block}
