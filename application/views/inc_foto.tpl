{extends file="templates/template.tpl"}
{block name="content"}

    <div id="wrapper">

        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Foto</h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Foto</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel hgreen">
                        <div class="panel-body">
                            <form id="form-incluir-foto" action="{base_url}usuario/inserirFoto" method="post" enctype="multipart/form-data" class="form-horizontal">
                                <div class="modal-body">
                                    <div class="alert alert-info" style="margin-bottom: 25px">
                                        <p>O tamanho máximo permitido para o upload da imagem é de <strong>1024 KB</strong>.</p>
                                        <p>As extensões permitidas para o upload da imagem são: <strong>png, jpg e gif</strong>.</p>
                                    </div>
                                    {if isset($error)}
                                        <div class="alert alert-danger" style="margin-bottom: 25px">{$error['error']}</div>
                                    {/if}
                                    <input type="hidden" name="cd_usuario" value="{$sessao_dados['id']}">
                                    <div class="form-group">
                                        <label class="col-sm-1 control-label">Foto</label>
                                        <div class="col-sm-11">
                                            <input type="file" name="foto" id="foto" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {include file="templates/footer.tpl"}
    </div>
{/block}
{block name="validation-script"}

{/block}