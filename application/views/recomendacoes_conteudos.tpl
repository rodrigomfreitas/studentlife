{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Recomendação de conteúdo para estudo</h3>
                    {if $sessao_dados['status'] != 3}
                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal"> <i class="fa fa-plus"></i> &nbsp;Adicionar</button>
                    {/if}
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Recomendação</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$avaliacao[0]->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel">
            <div class="row projects">
                <!-- Conteúdo do conceito do aluno -->
                <form id="form-incluir-conceitos" method="post" action="{base_url}avaliacao_usuario/incluir">
                    <div class="col-md-12">
                        <div class="hpanel">
                            <div class="panel-body">
                                {foreach $avaliacao as $a}
                                    <div class="hpanel hred">
                                        <div class="panel-body">
                                            <span class="label label-danger pull-right">Reprovado</span>
                                            <div class="row">
                                                <div class="col-sm-12">
{*                                                    <span class="text-big pull-right">D</span>*}
                                                    <h5> {$a->nm_avaliacao}</h5>
                                                    <h6>
                                                        {$a->ds_avaliacao}
                                                    </h6>
                                                    <div class="row">
                                                        <div class="col-sm-3">
                                                            <div class="project-label">Disciplina</div>
                                                            <small>{$a->nm_disciplina}</small>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="project-label">Tipo</div>
                                                            <small>{$a->nm_tipo_avaliacao}</small>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="project-label">Data da Criação</div>
                                                            <small>{$a->dt_data_cadastro|date_format:"%d/%m/%Y"}</small>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <div class="project-label">Data Entrega Final</div>
                                                            <small>{$a->dt_data_final|date_format:"%d/%m/%Y"}</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                {/foreach}
                                <h4 class="pull-left">Materiais Básicos Fundamentais</h4>
                                <a {if $materiais|count == 0}disabled{/if} href="{base_url}recomendacoes/downloadAll/{$avaliacao[0]->cd_avaliacao}" class="btn btn-primary pull-right"> <i class="fa fa-download"></i> &nbsp;Baixar Tudo</a>
                                    <span class="clearfix"></span>
                                <div class="table-responsive">
                                    <table cellpadding="1" cellspacing="1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Arquivo</th>
                                                <th>Nome</th>
                                                <th>Descrição</th>
{*                                                <th>Palavras-chave</th>*}
                                                <th>Tipo do Documento</th>
                                                <th>Baixar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {foreach $materiais as $material}
                                                <tr>
                                                    <td>{$material->ds_arquivo}</td>
                                                    <td>{$material->ds_nomenclatura}</td>
                                                    <td>{$material->ds_descricao}</td>
{*                                                    <td>{$material->ds_palavras_chave}</td>*}
                                                    <td>{$material->ds_tipo_documento}</td>
                                                    <td class="text-center">
                                                        <a href="{base_url}recomendacoes/download/{$material->cd_documento}" class="btn btn-info btn-xs visibilidade-material" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download"><i class="fa fa-download"></i></a>
                                                    </td>
                                                </tr>
                                            {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- Fim do conteúdo do conceito do aluno -->
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}
{block name="validation-script"}

{/block}
