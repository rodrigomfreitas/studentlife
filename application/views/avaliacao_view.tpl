{extends file="templates/template.tpl"}

{block name="content"}
<div id="wrapper">
  <div class="content animate-panel">

    <div class="row">
      <div class="col-md-12">
        <div class="hpanel email-compose">
          <div class="panel-heading hbuilt">

            <div class="p-xs h4">
              {$avaliacao[0]->nome}
            </div>
          </div>
          <div class="border-top border-left border-right bg-light">
            <div class="p-m">
              <p>Tipo <b>{if $avaliacao[0]->tipo == 1} PROVA {else} TRABALHO{/if}</b></p>
              <p>Data de Entrega: <b>{$avaliacao[0]->data_final}</b></p>
            </div>
            <div class="panel-body"><a class="btn btn-success pull-right" href="{base_url}avaliacao">Voltar</a>
              {$avaliacao[0]->descricao}</div>

          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}
