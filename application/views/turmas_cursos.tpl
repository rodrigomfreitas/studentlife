{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Turmas - Cursos</h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Turmas - Cursos</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel" data-effect="fadeIn" data-child="hpanel">
            <div class="row">
                {if $cursos|@count < 1}
                    <div class="col-md-12">
                        <h1>Nenhum Curso Cadastrado</h1>
                    </div>
                {/if}

                {foreach $cursos as $curso}
                    <div class="col-md-3">
                        <div class="hpanel hgreen">
                            <div class="panel-body">
                                <div class="text-center">
                                    <h3 class="m-b-xs">{$curso->sg_curso}</h3>
{*                                    <p class="font-bold text-success">{$curso->nm_curso|truncate:30:"...":true}</p>*}
                                    <p class="font-bold text-success">{$curso->nm_curso}</p>
                                    <a {if $curso->CURSO_cd_curso == NULL} class="btn btn-sm btn-success btn-block btn-default info-curso" data-toggle="modal" data-target="#info-curso" data-id="{$curso->cd_curso}"{else}href="{base_url}turmas/ano/{$curso->cd_curso}" class="btn btn-sm btn-success btn-block"{/if}>Entrar</a>
                                    <!-- <a {if $curso->CURSO_cd_curso == NULL} class="btn btn-sm btn-success btn-block btn-default" data-container="body" data-toggle="popover" data-placement="top" data-title="Não é possível prosseguir" data-content="Para cadastrar uma Turma é preciso ter, pelo menos, um Semestre cadastrado." data-original-title="" title="" aria-describedby="popover397774"{else}href="{base_url}turmas/turmas_curso/{$curso->cd_curso}"class="btn btn-sm btn-success btn-block"{/if}>Entrar</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
        <!-- Fim do conteúdo da página -->

        <!-- Modal de Informação caso não não exista semestre em um determinado curso -->
        <div class="modal fade hmodal-info" id="info-curso" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header">
                        <h4 class="modal-title">Informações para o próximo passo</h4>
                        <small class="font-bold">Para seguir em frente leia as instruções abaixo.</small>
                    </div>
                    <div class="modal-body">
                        <p>Não foi possível prosseguir ao próximo passo, porque é preciso primeiramente cadastrar, pelo menos, um <strong>ano</strong>.<br>
                            Clique no botão <strong>Cadastrar</strong> para entrar no módulo de cadastrar ano.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <a id="url-curso" href="" class="btn btn-primary">Cadastrar</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do modal de Informação caso não não exista semestre em um determinado curso -->

        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
{/block}
{block name="validation-script"}
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script>
        $(document).ready(function () {

            var modal = $("#info-curso");
            $('.info-curso').on("click", function () {
                var cd_curso = $(this).data("id");
                 $("#url-curso").attr("href", "{base_url}anos/" + cd_curso);
            });
        });
    </script>
{/block}
