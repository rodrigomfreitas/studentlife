{extends file="templates/template.tpl"}

{block name="content"}

    <div id="wrapper">
        <!-- Cabeçalho da página-->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Anos </h3>
                    {if $sessao_dados['status'] != 3}
                        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal">  <i class="fa fa-plus"></i> &nbsp;Adicionar</button>
                    {/if}
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <a href="{base_url}cursos">Cursos</a>
                            </li>
                            <li>
                                <span>Anos</span>
                            </li>
                        </ol>
                    </div>
                    <h5>Curso de {$cursos[0]->nm_curso}</h5>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->
        <!-- Conteúdo da página -->
        <div class="content animate-panel" data-effect="fadeIn" data-child="hpanel">
            <div class="row">
                {if $sessao_dados['status'] != 3}
                    {if $anos|@count < 1}
                        <div class="col-md-12">
                            <h1>Nenhum Ano Cadastrado</h1>
                        </div>
                    {/if}
                {else}
                    {if $anos|@count < 1}
                        <div class="col-md-12">
                            <h1>Você não está matriculado em nenhum ano até o momento</h1>
                        </div>
                    {/if}
                {/if}
                {foreach $anos as $ano}
                    <div class="col-md-3">
                        <div class="hpanel hgreen">
                            <div class="panel-body text-center">
                                <!-- Botão de ação -->
                                {if $sessao_dados['status'] != 3}
                                    <div class="dropdown text-right">
                                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">
                                            <button class="text-muted btn btn-default"><i class="fa fa-gear text-primary"></i></button>
                                        </a>
                                        <ul class="dropdown-menu animated flipInX m-t-xs pull-right">
                                            <li><a href="javascript:;" class="editar-ano" data-id="{$ano->cd_ano}" ><i class="fa fa-pencil"></i>&nbsp;Editar</a></li>
                                            <li><a href="javascript:;" class="excluir-ano" data-id="{$ano->cd_ano}"><i class="fa fa-trash"></i>&nbsp;Remover</a></li>
                                        </ul>
                                    </div>
                                {/if}        
                                <!-- Fim do Botão de ação -->
                                <div class="stats-title text-center">
                                    <h3>{$ano->ano_curso}</h3>
                                </div>
                                <a href="{base_url}semestres/{$ano->cd_ano}" class="btn btn-sm btn-success btn-block">Entrar</a>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
        <!-- Fim do conteúdo da página -->
        <!-- Footer -->
        {include file="templates/footer.tpl"}
        <!-- Fim do footer -->
    </div>
    <!-- Modal de incluir ano -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Incluir Ano</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-incluir-ano" action="{base_url}anos/incluir" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_curso" id="cd_curso" value="{$cursos[0]->cd_curso}">
                    <div class="modal-body">
                        <div id="validation-error"></div>
                        <div class="form-group"><label for="dt_ano" class="col-sm-2 control-label control-label-required">Ano</label>
                            <div class="col-sm-5"><input type="date" name="dt_ano" id="dt_ano" class="form-control"></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* O campo é obrigatório</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal de incluir ano -->
    <!-- Modal de editar ano -->
    <div class="modal fade" id="editar-ano-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header text-center">
                    <h4 class="modal-title">Editar Semestre</h4>
                    <small class="font-bold">Lembre-se de Preencher todos os Campos Corretamente</small>
                </div>
                <form id="form-editar-ano" action="{base_url}anos/atualizar" method="post" class="form-horizontal">
                    <input type="hidden" name="cd_curso" value="{$cursos[0]->cd_curso}">
                    <div class="modal-body">
                        <div id="validation-error-edit"></div>
                        <div class="form-group"><label for="dt_ano" class="col-sm-2 control-label control-label-required">Ano</label>
                            <div class="col-sm-5"><input type="date" name="dt_ano" id="dt_ano" class="form-control"></div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-right"><strong>* O campo é obrigatório</strong></p>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                    <input type="hidden" name="cd_ano" value="">
                </form>
            </div>
        </div>
    </div>
    <!-- Fim do modal de editar ano -->
{/block}

{block name="validation-script"}
    <!-- Script para incluir um ano e o Alert toastr -->
    <script type="text/javascript">
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        $(document).ready(function () {
            $('#form-incluir-ano').submit(function () {
                $('button[type=submit], input[type=submit]').prop('disabled', true);
                $.post($('#form-incluir-ano').attr('action'), $('#form-incluir-ano').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('button[type=submit], input[type=submit]').prop('disabled', false);
                        $('#validation-error').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error').removeClass('alert alert-warning');
                        $('#validation-error > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                    else if (data.st == 3)
                    {
                        toastr.info(data.msg);
                    }
                }, 'json');
                return false;

            });
        });
    </script>
    <!-- Fim do Script para incluir um ano e o Alert toastr -->
    <!-- Script para editar um ano -->
    <script>
        $(document).ready(function () {

            var modal = $("#editar-ano-modal");
            $('.editar-ano').on("click", function () {
                var cd_ano = $(this).data("id");
                $.get(
                        "{base_url}anos/editar/" + cd_ano,
                        function (semestre) {
                            modal.find('[name="dt_ano"]').val(semestre.dt_ano);
                            modal.find('[name="cd_ano"]').val(semestre.cd_ano);

                            modal.modal("show");

                        }
                );
            });
            modal.on("hide.bs.modal", function () {
                modal.find('[name="dt_ano"]').val("");
                modal.find('[name="cd_ano"]').val("");
                modal.find('[id="validation-error-edit"]').removeClass('alert alert-warning');
                modal.find('[id="validation-error-edit"]').find('p').remove();
            });

            modal.find("#form-editar-ano").on("submit", function (e) {
                e.preventDefault();
                $.post($('#form-editar-ano').attr('action'), $('#form-editar-ano').serialize(), function (data) {
                    if (data.st == 0)
                    {
                        $('#validation-error-edit').addClass('alert alert-warning').html(data.msg);
                    }
                    else if (data.st == 1)
                    {
                        $('#validation-error-edit').removeClass('alert alert-warning');
                        $('#validation-error-edit > p').remove();
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                        toastr.success(data.msg);
                    }
                    else if (data.st == 2)
                    {
                        toastr.error(data.msg);
                    }
                    else if (data.st == 3)
                    {
                        toastr.info(data.msg);
                    }
                }, 'json');
                return false;

            });

            $("#myModal").on("hide.bs.modal", function () {
                $(this).find('[name="dt_ano"]').val("");
                $(this).find('[id="validation-error"]').removeClass('alert alert-warning');
                $(this).find('[id="validation-error"]').find('p').remove();
            });
        });
    </script>
    <!-- Script para editar um ano -->
    <!-- Script para excluir um ano -->
    <script>
        $(function () {
            $('.excluir-ano').click(function () {
                var cd_ano = $(this).data("id");
                swal({
                    title: "Você tem certeza?",
                    text: "Se não você removerá o ano com suas dependências",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Sim, quero remover!",
                    cancelButtonText: "Não, cancelar!",
                    closeOnConfirm: false,
                    closeOnCancel: false},
                function (isConfirm) {
                    if (isConfirm) {
                        $.get(
                                "{base_url}anos/excluir/" + cd_ano,
                                function () {
                                    swal("Removida!", "O ano foi removido com sucesso.", "success");
                                    setTimeout(function () {
                                        location.reload();
                                    }, 1000);
                                }
                        );
                    } else {
                        swal("Cancelado", "O ano não foi removido :)", "error");
                    }
                });
            });

            $('.homerDemo4').click(function () {
                toastr.error('Error - This is a Homer error notification');
            });

        });
    </script>
    <!-- Fim do Script para excluir um ano -->
{/block}
