{extends file="templates/template.tpl"}

{block name="content"}
    <div id="wrapper" style="min-height: 1860px;">

        <!-- Cabeçalho da página -->
        <div class="normalheader transition animated fadeIn small-header">
            <div class="hpanel">
                <div class="panel-body">
                    <a class="small-header-action" href="">
                        <div class="clip-header">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                    </a>
                    <h3 class="font-light m-b-xs pull-left">Envio de Mensagem</h3>
                    <div class="clearfix"></div>
                    <div id="hbreadcrumb" class="pull-right">
                        <ol class="hbreadcrumb breadcrumb">
                            <li><a href="{base_url}">Home</a></li>
                            <li>
                                <span>Envio de Mensagem</span>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fim do cabeçalho da página -->

        <div class="content animate-panel">
            <div class="row">
                <div class="col-md-12">
                    <div class="hpanel ">
                        <div class="panel-heading hbuilt">
                            Envio de Mensagem
                        </div>
                        <div class="panel-body no-padding">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="chat-discussion">
                                        {foreach $mensagens as $mensagem}
                                            {if $mensagem->cd_envia == $sessao_dados['id']}
                                                <div class="chat-message right">
                                                {else}
                                                    <div class="chat-message left">
                                                    {/if}
                                                    <img class="message-avatar" src="{if $mensagem->ds_foto == ""}{base_url}assets/images/profile.png{else}{base_url}fotos/{$mensagem->ds_foto}{/if}" alt="">
                                                    <div class="message">
                                                        <a class="message-author" href="#">{$mensagem->nm_usuario}</a>
                                                        <span class="message-date">  {$mensagem->dt_data_cad|date_format:"%d/%m/%Y"} às {$mensagem->dt_data_cad|date_format:"%H:%M:%S"}</span>
                                                        <span class="message-content {if $mensagem->fl_excluido == 0} text-success {/if}">
                                                            {$mensagem->ds_mensagem}
                                                        </span>
                                                    </div>
                                                </div>
                                            {/foreach}
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="chat-users">
                                            <div class="users-list">
                                                {foreach $usuarios as $usuario}
                                                    <div class="chat-user">
                                                        {if $usuario->cd_tipo_usuario == 3}
                                                            <span class="pull-right label label-primary">Aluno</span>
                                                        {else}
                                                            <span class="pull-right label label-success">Professor</span>
                                                        {/if}
                                                        <img class="chat-avatar" src="{if $usuario->ds_foto == ""}{base_url}assets/images/profile.png{else}{base_url}fotos/{$usuario->ds_foto}{/if}" alt="">
                                                        <div class="chat-user-name">
                                                            <a href="{base_url()}mensagem/{$usuario->cd_usuario}">{$usuario->nm_usuario}</a>
                                                        </div>
                                                    </div>
                                                {/foreach}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <form id="form-enviar-mensagem" action="{base_url}mensagem/enviar" method="post">
                                    <div class="input-group">
                                        <input id="mensagem" type="text" name="mensagem" class="form-control" placeholder="Escreva Sua mensagem aqui">
                                        <input id="id_recebe" name="cd_recebe" type="hidden" value="{$cd_enviar}">
                                        <span class="input-group-btn">
                                            <button id="botao_enviar" type="submit" class="btn btn-success">
                                                Enviar</button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            {include file="templates/footer.tpl"}
            <!-- Fim do footer -->
        </div>
    {/block}
    {block name="validation-script"}
        <script type="text/javascript">
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-center",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

            $(document).ready(function () {
                $('#form-enviar-mensagem').submit(function () {
                    $.post($('#form-enviar-mensagem').attr('action'), $('#form-enviar-mensagem').serialize(), function (data) {
                        if (data.st == 0)
                        {
{*                            $('#validation-error').addClass('alert alert-warning').html(data.msg);*}
                            toastr.error(data.msg);
                        }
                        else if (data.st == 1)
                        {
                            $('#validation-error').removeClass('alert alert-warning');
                            $('#validation-error > p').remove();
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                            toastr.success(data.msg);
                        }
                        else if (data.st == 2)
                        {
                            toastr.error(data.msg);
                        }
                        else if (data.st == 3)
                        {
                            toastr.info(data.msg);
                        }
                    }, 'json');
                    return false;
                });
            });
        </script>
    {/block}

