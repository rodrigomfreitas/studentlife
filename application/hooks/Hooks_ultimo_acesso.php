<?php

function Hooks_ultimo_acesso() {
    $CI = & get_instance();
    
    $sessao_dados['id'] = $CI->session->userdata('id');

    $CI->load->model('M_log');
    $ultimoAcesso = $CI->M_log->ultimoAcesso($sessao_dados['id']);

    $CI->smartyci->assign('ultimoAcesso', $ultimoAcesso);
    $CI->smartyci->fetch('templates/template.tpl');
}

