<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if (!function_exists('active_link')) {

    function active_link($controller) {
        $CI = & get_instance();

        $class = $CI->router->fetch_class();

        return ($class == $controller) ? 'active' : '';
    }

}

// Carrega os dados da Disciplina para inserir no cabeçalho e menu
if (!function_exists('menu_disciplina')) {

    function menu_disciplina($cd_disciplina) {
        $CI = & get_instance();
        
        $CI->load->model('M_disciplina');
        $CI->load->model('M_chave_turma');
        
        $dados_disciplina = $CI->M_disciplina->getDadosDisciplina($cd_disciplina);
//        var_dump($dados);die;
        // retorna os alunos que solicitaram cadastro em um determinada disciplina
        $totalAlunos = $CI->M_chave_turma->getTotalSolicitacoesCadTurma($cd_disciplina);
        
        $CI->smartyci->assign('dados_disciplina', $dados_disciplina);
        $CI->smartyci->assign('totalAlunos', $totalAlunos);
        $CI->smartyci->fetch('templates/disciplina_menu.tpl');
    }
    
//    function ultimoAcesso($cd_usuario) {
//        $CI = & get_instance();
//        
//        $CI->load->model('M_log');
//        $ultimoAcesso = $CI->M_log->ultimoAcesso($cd_usuario);
//        
//        $CI->smartyci->assign('ultimoAcesso', $ultimoAcesso);
//        $CI->smartyci->fetch('templates/template.tpl');
//    }

}