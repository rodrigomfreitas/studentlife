<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aluno_disciplina extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
    }

    public function index($cd_disciplina) {
        // carrega o model
        $this->load->model('M_chave_turma');

        // carrega o menu da página de disciplina interna
        menu_disciplina($cd_disciplina);

        // retorna os alunos que solicitaram cadastro em um determinada disciplina
        $alunosDisciplinas = $this->M_chave_turma->getSolicitacoesCadTurma($cd_disciplina);

        // atribui a variável ao template
        $this->smartyci->assign('alunosDisciplinas', $alunosDisciplinas);
        // exibe o template
        $this->smartyci->display('aluno_disciplina.tpl');
    }

    public function incluir() {
        $inserirAluno = array();
        $turmas = array();
        // carrega o model
        $this->load->model('M_chave_turma');
        $this->load->model('M_disciplina_usuario');
        
        // obtém os dados do form
        $data['USUARIO_cd_usuario'] = $this->input->post('cd_usuarios');
        $data['DISCIPLINA_cd_disciplina'] = $this->input->post('cd_disciplina');
        $data['TURMA_cd_turma'] = $this->input->post('cd_turma');
        
        if ($data['USUARIO_cd_usuario'] == NULL) {
            echo json_encode(array('st' => 0, 'msg' => 'Para inserir, é necessário escolher, pelo menos, um aluno.'));
            exit;
        }
        
        $alunosDisciplinas = $this->M_chave_turma->getSolicitacoesCadTurma($data['DISCIPLINA_cd_disciplina']);
        
        foreach ($alunosDisciplinas as $a) {
            $turmas[$a->USUARIO_cd_usuario] = $a->TURMA_cd_turma;
        }
        
//        for ($i = 0; $i < count($data['USUARIO_cd_usuario']); $i++) {
//            $inserirAluno[] = array('DISCIPLINA_cd_disciplina'=>$data['DISCIPLINA_cd_disciplina'],
//                                    'USUARIO_cd_usuario'=>$data['USUARIO_cd_usuario'][$i],
//                                    'TURMA_cd_turma'=>$turmas[$data['USUARIO_cd_usuario']->TURMA_cd_turma]);
//        }
//        var_dump($turmas);die;
        
        foreach ($data['USUARIO_cd_usuario'] as $a) {
            $inserirAluno[] = array('DISCIPLINA_cd_disciplina'=>$data['DISCIPLINA_cd_disciplina'],
                                    'USUARIO_cd_usuario'=>$a,
                                    'TURMA_cd_turma'=>$turmas[$a]);
        }
        
        // verifica se as solicitações foram excluídas
        // e se foi inserido os alunos à disciplina
        if($this->M_chave_turma->deleteSolicitacaoPosCadAluno($data) && $this->M_disciplina_usuario->insert($inserirAluno)) {
            echo json_encode(array('st' => 1, 'msg' => 'Alunos inseridos à disciplina com sucesso.'));
            exit;
        }
        echo json_encode(array('st' => 2, 'msg' => 'Erro ao inserir os alunos à disciplina.'));
    }

}
