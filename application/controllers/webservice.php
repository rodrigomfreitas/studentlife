<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Webservice extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_avaliacao_usuario');
    }

    public function index() {
        header("Content-type:application/json; charset=UTF-8");

        $dados = $this->M_avaliacao_usuario->listaAlunosWebservice();

        if (count($dados) > 0) {
            $resposta["alunos"] = array();

            foreach ($dados as $dado) {
                $aluno = array();
                $aluno["nome"] = $dado->nm_usuario;
                $aluno["email"] = $dado->ds_email;
                $aluno["matricula"] = $dado->nr_matricula;
//                $aluno["curso"] = $dado->nm_curso;
                $aluno["foto"] = "http://10.0.2.2/student/fotos/" . $dado->ds_foto;
                
                array_push($resposta["alunos"], $aluno);
            }
            
            echo json_encode($resposta, JSON_UNESCAPED_UNICODE);
            
//            $file = fopen("./alunos.json", "w") or die ("Impossível abrir o arquivo");
//            $alunos_json = json_encode($resposta, JSON_UNESCAPED_UNICODE);
//            fwrite($file, $alunos_json);
//            fclose($file);
        } else {
            $resposta["mensagem"] = "Nenhum aluno encontrado";

            echo json_encode($resposta);
            
        }
    }
}
