<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chave_turma extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        $this->load->model('M_chave_turma');
    }

    public function gerarChave($length = 6, $add_dashes = false, $available_sets = 'lud') {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function incluir($cd_turma) {
        header("Content-Type: application/json");

        $data['ds_chave_turma'] = $this->gerarChave();
        $data['TURMA_cd_turma'] = $cd_turma;
        $data['USUARIO_cd_usuario'] = $this->session->userdata('id');

        if ($this->verificaChave() == true) {
            echo json_encode(array('st' => 3));
            exit;
        }
        if ($this->M_chave_turma->insert($data)) {
            echo json_encode(array('st' => 1));
            exit;
        }
        echo json_encode(array('st' => 2));
    }
    
    public function verificaChave() {
        $chave_nova = $this->gerarChave();
       
        // retorna os dados de uma determinada chave
        $chave = $this->M_chave_turma->getChave($chave_nova);
        
        if (count($chave) == 1) {
            return true;
        } else {
            return false;
        }
    }
    
}
