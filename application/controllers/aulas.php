<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aulas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }

        // carrega o model
        $this->load->model('M_aula');
    }

    public function index($cd_disciplina) {
        // carrega o menu interno na página de disciplina
        menu_disciplina($cd_disciplina);

        // recuperar o status e o código do usuário na sessão
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');

        // lista a Aulas de uma determinada Disciplina
        $aulas = $this->M_aula->getAulas($cd_disciplina);

        // atribui a variável ao template
        $this->smartyci->assign('aulas', $aulas);

        // exibe o template
        $this->smartyci->display('aulas.tpl');
    }

    public function incluir() {
        // carrega a biblioteca de validação de formulários do CI
        $this->load->library('form_validation');

        // determina regras de validação aos campos do formulário para cadastrar curso
        $this->form_validation->set_rules('nr_aula', 'Número da Aula', 'required');

        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }

        $data['nr_aula'] = $this->input->post('nr_aula');
        $data['DISCIPLINA_cd_disciplina'] = $this->input->post('cd_disciplina');

        // retorna os dados da aula referente o código da aula
        $aulaCadastrada = $this->M_aula->verificaAulaCadastrada($data['nr_aula']);
        
        header('Content-Type: application/json');
        
        // verifica se a aula consta cadastrada
        if (count($aulaCadastrada) == 1 && $aulaCadastrada[0]->DISCIPLINA_cd_disciplina == $data['DISCIPLINA_cd_disciplina']) {
            echo json_encode(array('st' => 3, 'msg' => 'Este número de aula já consta no registro.'));
            exit;
        }

        // verifica se a aula foi inserida no Banco de Dados
        if ($this->M_aula->insert($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Aula adicionada com sucesso.'));
            exit;
        }

        // envia uma mensagem à view caso ocorra um erro ao adicionar uma aula
        echo json_encode(array('st' => 2, 'msg' => 'Erro ao adicionar uma aula.'));
    }

    public function editar($cd_aula = NULL) {
        header("Content-Type: application/json");

        // retorna os dados de uma determinada aula
        $aula = $this->M_aula->getAula($cd_aula);

        echo json_encode(array_shift($aula));
    }

    public function atualizar() {
        // carrega a biblioteca de validação de formulários do CI
        $this->load->library('form_validation');

        // determina regras de validação aos campos do formulário para cadastrar curso
        $this->form_validation->set_rules('nr_aula', 'Número da Aula', 'required');

        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }
        
        // obtém os dados do form
        $data['nr_aula'] = $this->input->post('nr_aula');
        $data['cd_aula'] = $this->input->post('cd_aula');
        $data['DISCIPLINA_cd_disciplina'] = $this->input->post('cd_disciplina');

        header('Content-Type: application/json');
        
        // verifica se a aula foi inserida no Banco de Dados
        if ($this->M_aula->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Aula atualizada com sucesso.'));
            exit;
        }

        // envia uma mensagem à view caso ocorra um erro ao adicionar uma aula
        echo json_encode(array('st' => 2, 'msg' => 'Erro ao atualizar uma aula.'));
    }

    public function excluir($cd_aula = NULL) {

        $data['cd_aula'] = $cd_aula;
        $data['fl_excluido'] = 1;

        // verifica se foi excluído e exibe uma mensagem
        if ($this->M_aula->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Aula excluída com sucesso.'));
        }
    }

}
