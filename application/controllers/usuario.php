<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        $this->load->model('M_usuario');
    }

    public function index($id = null) {
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }

        $dados = $this->M_usuario->getDadosUsuarioProcedure($id);
        
        $uri = $this->uri->segment(2);
//        var_dump($uri);
//        die;
//        $this->load->library('googlemaps');
//
//        $location =  "{$dados[0]->endereco}" . "," . "{$dados[0]->numero}" . ", " . "{$dados[0]->cidade}" . ", " . "{$dados[0]->estado}" ." " . "{$dados[0]->cep}";
//        $config['center'] = $location;
//        $config['zoom'] = "16";
//        $this->googlemaps->initialize($config);
//
//        $marker = array();
//        
//        $marker['position'] = $location;
//        $marker['animation']='BOUNCE';
//        $this->googlemaps->add_marker($marker);
//        $data['map'] = $this->googlemaps->create_map();
//        
//        $this->smartyci->assign($data);
        $this->smartyci->assign('uri', $uri);
        $this->smartyci->assign('dados', $dados);

        $this->smartyci->display('perfil.tpl');
    }

    public function formAlterar($id_usuario) {
        $this->output->enable_profiler(false);
        $dados_usuario = $this->M_usuario->getDadosUsuarioProcedure($id_usuario);
        // Recupera o ID da sessão do usuário
        $sessao_dados['id'] = $this->session->userdata('id');

        $this->smartyci->assign('dados', $dados_usuario);

        // Caso o ID do usuário é o mesmo da ID da sessão exibe à página para edição dos dados
        if ($id_usuario == md5($sessao_dados['id'])) {
            $this->smartyci->display('alt_perfil.tpl');
            exit;
        }

        // Se não rediriciona à página de perfil
        redirect('perfil/' . $id_usuario);
    }

    public function formAlterarFoto($cd_usuario) {
        $sessao_dados['id'] = $this->session->userdata('id');

        // Caso o ID do usuário é o mesmo da ID da sessão exibe à página para edição dos dados
        if ($cd_usuario == md5($sessao_dados['id'])) {
            $this->smartyci->display('inc_foto.tpl');
            exit;
        }

        // Se não, rediriciona à página de perfil
        redirect('perfil/' . $cd_usuario);
    }

    public function inserirFoto() {
        $data['cd_usuario'] = $this->input->post('cd_usuario');
        
        $config['upload_path'] = './fotos/';
        $config['allowed_types'] = 'gif|jpg|png|ico';
        $config['max_size'] = '1024';
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('foto')) {
            $error =  array('error' => $this->upload->display_errors());
            $this->smartyci->assign('error', $error);
            $this->formAlterarFoto(md5($data['cd_usuario']));
        } else {
            $arquivo = $this->upload->data();
            $data['ds_foto'] = $arquivo['file_name'];

            $inc = $this->M_usuario->update($data);

            if ($inc) {
                $this->session->set_userdata('foto', $data['ds_foto']);
                redirect('home');
            }
        }
    }
    
    public function validarSenhaProibidas($str) {
        $senhasProibidas = $this->M_usuario->getSenhasProibidas($str);

        if (count($senhasProibidas) >= 1) {
            $this->form_validation->set_message('validarSenhaProibidas', 'A sua {field} contém a palavra ' . '"' . $senhasProibidas[0]->ds_senhas_proibidas . '"' . ' que é proibida para se registrar.');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function validaSenha($str) {
        // aceita somente letras maiúsculas e minúsculas, pelo menos, um número e um caracter especial.
//        $pattern = "/^(?=^.{4,}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/";
        // aceita somente letras maiúsculas e minúsculas, pelo menos, um número.
        $pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/";

        if (!preg_match($pattern, $str)) {
//            $this->form_validation->set_message('validaSenha', 'A <strong>senha</strong> deve ter no mínimo <strong>seis caracteres</strong>,'
//                    . ' incluindo <strong>letras maiúsculas</strong> e <strong>minúsculas</strong> e, pelo menos,'
//                    . ' <strong>um número</strong> e <strong>um símbolo</strong>.');
            $this->form_validation->set_message('validaSenha', 'A <strong>senha</strong> deve ter no mínimo <strong>seis caracteres</strong>,'
                    . ' incluindo <strong>letras maiúsculas</strong> e <strong>minúsculas</strong> e, pelo menos,'
                    . ' <strong>um número</strong>.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function atualizar_usuario($id_usuario) {
        // Carrega e biblioteca form_validation e seta regras aos campos
        $this->load->library('form_validation');
        $this->form_validation->set_rules('matricula', 'Matrícula', 'trim|required');
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');

        // Caso os campos que seguem a regra a cima estiver inválidos é exibida mensagens de erros
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }
        
        $senha_atual = $this->input->post('senha_atual');
        
        // Caso o campo senha atual não está vazia
        if (!empty($senha_atual)) {
            
            $salt = 'St4d3ntL1f3-G3r3nc14m3nt03st4nt1l';
            $senha_atual = md5($senha_atual);
            $senhaSha1 = sha1($senha_atual.$salt);

            // Recupera os dados do usuário
            $dados = $this->M_usuario->getDadosUsuarioProcedure($id_usuario);

            // Seta as regras para validação dos campos
            $this->form_validation->set_rules('senha_atual', 'Senha Atual', 'required');
            $this->form_validation->set_rules('nova_senha', 'Nova Senha', 'required|matches[repetir_nova_senha]|min_length[6]|max_length[20]|callback_validaSenha|callback_validarSenhaProibidas');
            $this->form_validation->set_rules('repetir_nova_senha', 'Repertir Nova Senha', 'required');

            // Caso os campos que seguem a regra a cima estiver inválidos é exibida mensagens de erros
            if ($this->form_validation->run() == FALSE) {
                echo json_encode(array('st' => 3, 'msg' => validation_errors()));
                exit;
            }

            // Se a senha recuperada do banco for igual a senha atual digita no banco é realizada a alteração
            if ($dados[0]->ds_senha == $senhaSha1) {
                $data['ds_senha'] = md5($this->input->post('nova_senha'));
                $data['ds_senha'] = sha1($data['ds_senha'].$salt);
                $this->atualizar_dados($data);
                exit;
            }

            // Se não é exibida uma mensagem de erro
            echo json_encode(array('st' => 4, 'msg' => 'Senha atual não é valida.'));
            exit;
        }

        // Caso o campo senha atual não for preenchida é atualizado os dados restantes
        $this->atualizar_dados();
    }

    public function atualizar_dados($data = null) {
        $matricula = $this->input->post('matricula');
        $cpf = $this->input->post('cpf');
        $cep = $this->input->post('cep');
        $telefone = $this->input->post('telefone');
        $celular = $this->input->post('celular');

        $data['cd_usuario'] = $this->input->post('id_usuario');
        $data['nr_matricula'] = preg_replace("/[^0-9]/", "", $matricula);
        $data['nm_usuario'] = $this->input->post('nome');
        $data['nr_cpf'] = preg_replace("/[^0-9]/", "", $cpf);
        $data['nr_cep'] = preg_replace("/[^0-9]/", "", $cep);
        $data['ds_cidade'] = $this->input->post('cidade');
        $data['sg_estado'] = $this->input->post('estado');
        $data['ds_bairro'] = $this->input->post('bairro');
        $data['ds_endereco'] = $this->input->post('endereco');
        $data['nr_endereco'] = $this->input->post('numero');
        $data['ds_complemento'] = $this->input->post('complemento');
        $data['nr_telefone'] = preg_replace("/[^0-9]/", "", $telefone);
        $data['nr_celular'] = preg_replace("/[^0-9]/", "", $celular);
        $data['dt_data_nasc'] = $this->input->post('data_nasc');
//        $data['foto'] = $this->input->post('foto');
//        // atualizará o nome e a foto da sessão
//        $sessao['nome'] = $data['nome'];
//        $sessao['foto'] = $data['foto'];   
//        $this->session->set_userdata($sessao);

//        if (!empty($_FILES["foto"]["name"])) {
//
//            $config['upload_path'] = './foto/';
//            $config['allowed_types'] = 'gif|jpg|png';
//            $config['encrypt_name'] = true;
//            $this->load->library('upload', $config);
//
//            if (!$this->upload->do_upload('foto')) {
//                echo $this->upload->display_errors();
//            } else {
//                $arquivo = $this->upload->data();
//                $data['ds_foto'] = $arquivo['file_name'];
//            }
//        }
        if ($this->M_usuario->update($data)) {
            $this->session->set_userdata('nome', $data['nm_usuario']);
            echo json_encode(array('st' => 1, 'msg' => 'Dados alterado com sucesso.'));
            exit;
        }
        echo json_encode(array('st' => 2, 'msg' => 'Ocorreu um Erro, por favor, tente novamente.'));
    }

    public function buscaCep() {
        $cep = $_POST['cep'];

        $reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);

        $dados['sucesso'] = (string) $reg->resultado;
        $dados['ds_endereco'] = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
        $dados['ds_bairro'] = (string) $reg->bairro;
        $dados['ds_cidade'] = (string) $reg->cidade;
        $dados['sg_estado'] = (string) $reg->uf;

        echo json_encode($dados);
    }
    
    public function informacoesAluno($cd_disciplina, $cd_usuario) {
        $this->output->enable_profiler(FALSE);
        $this->load->model('M_disciplina');
        $this->load->model('M_avaliacao');
        
        $ordenar = array();
        date_default_timezone_set('America/Sao_Paulo');
        $data = date('Y-m-d');

        menu_disciplina($cd_disciplina);
        
        $dados = $this->M_usuario->getDadosUsuarioProcedure($cd_usuario);
        $disciplinas = $this->M_disciplina->getDisciplinasAlunoMatriculado($cd_usuario);
        $avaliacoes = $this->M_avaliacao->getAvaliacaoConceitoAluno($cd_disciplina, $cd_usuario);
        
        foreach ($disciplinas as $d) {
            $primeiro = strtoupper($d->sg_curso . "/" . $d->nr_semestre . "" . $d->nm_turno[0] . "/" . date_format(date_create($d->dt_turma), 'Y'));

            if (!isset($ordenar[$primeiro])) {
                $ordenar[$primeiro] = array();
            }

            $ordenar[$primeiro][] = $d;
        }
        
        $this->smartyci->assign('dados_aluno', $dados);
        $this->smartyci->assign('ordenar', $ordenar);
        $this->smartyci->assign('avaliacoes', $avaliacoes);
        $this->smartyci->assign('data', $data);
        $this->smartyci->display('informacoes_aluno.tpl');
    }

}
