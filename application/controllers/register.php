<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_usuario');
    }

    public function index() {
        if ($this->session->userdata('logado') == true) {
            redirect('home');
        }
        $this->smartyci->display('register.tpl');
    }

    public function validarSenhaProibidas($str) {
        $senhasProibidas = $this->M_usuario->getSenhasProibidas($str);

        if (count($senhasProibidas) >= 1) {
            $this->form_validation->set_message('validarSenhaProibidas', 'A sua {field} contém a palavra ' . '"' . $senhasProibidas[0]->ds_senhas_proibidas . '"' . ' que é proibida para se registrar.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function gerarSenhaAleatoria($length = 6, $add_dashes = false, $available_sets = 'lud') {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function gerarSenha() {
        echo $this->gerarSenhaAleatoria();
    }

    public function validaSenha($str) {
        // aceita somente letras maiúsculas e minúsculas, pelo menos, um número e um caracter especial.
//        $pattern = "/^(?=^.{4,}$)((?=.*\d)(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/";
        // aceita somente letras maiúsculas e minúsculas, pelo menos, um número.
        $pattern = "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/";

        if (!preg_match($pattern, $str)) {
//            $this->form_validation->set_message('validaSenha', 'A <strong>senha</strong> deve ter no mínimo <strong>seis caracteres</strong>,'
//                    . ' incluindo <strong>letras maiúsculas</strong> e <strong>minúsculas</strong> e, pelo menos,'
//                    . ' <strong>um número</strong> e <strong>um símbolo</strong>.');
            $this->form_validation->set_message('validaSenha', 'A <strong>senha</strong> deve ter no mínimo <strong>seis caracteres</strong>,'
                    . ' incluindo <strong>letras maiúsculas</strong> e <strong>minúsculas</strong> e, pelo menos,'
                    . ' <strong>um número</strong>.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function incluir_usuario() {
        $this->output->enable_profiler(FALSE);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('matricula', 'Matrícula', 'trim|required');
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email|matches[confirmar_email]');
        $this->form_validation->set_rules('confirmar_email', 'Confirmar E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('senha', 'Senha', 'required|matches[confirmar_senha]|min_length[6]|max_length[20]|callback_validaSenha|callback_validarSenhaProibidas');
        $this->form_validation->set_rules('confirmar_senha', 'Confirmar Senha', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
        } else {

            $matricula = $this->input->post('matricula');
            $dados['nr_matricula'] = preg_replace("/[^0-9]/", "", $matricula);
            $dados['ds_email'] = $this->input->post('email');
            $dados['nm_usuario'] = $this->input->post('nome');
            $dados['ds_senha'] = md5($this->input->post('senha'));

            // provisório para apresentação do TCC
            $dados['st_ativo'] = 0;

            $checar_email = $this->M_usuario->verificaEmail($dados['ds_email']);
            $checar_matricula = $this->M_usuario->verificaMatricula($dados['nr_matricula']);

            if (count($checar_matricula) > 0) {
                echo json_encode(array('st' => 3, 'msg' => 'A matrícula <strong>' . $dados['nr_matricula'] . '</strong> já está cadastrada, por favor, insira outra matrícula válida'));
                exit;
            }
            
            // obtém o domínio do e-mail
            $domain = explode('@', $dados['ds_email']);

            if (count($checar_email) == 0) {
                // checa se o domínio é da instituição
//                if ($domain[1] == "gmail.com") {
                if ($domain[1] == "senacrs.edu.br") {
                    $dados['TIPO_USUARIO_cd_tipo_usuario'] = 2;
                } else {
                    $dados['TIPO_USUARIO_cd_tipo_usuario'] = 3;
                }

                if ($this->M_usuario->insert($dados)) {
//                    echo json_encode(array('st' => 1, 'msg' => 'Olá <strong>' . $dados['nm_usuario'] . '</strong>, você foi cadastrado com sucesso'));
                    $this->enviar_email_confirmacao($dados);
                }
            } else {
                echo json_encode(array('st' => 2, 'msg' => 'O E-mail <strong>' . $dados['ds_email'] . '</strong> já está cadastrado, por favor, insira outro e-mail'));
            }
        }
    }

    public function enviar_email_confirmacao($dados) {
        $mensagem = $this->load->view('emails/confirmar_cadastro.php', $dados, TRUE);
        $this->load->library('email');
        $this->email->from("studentlifege@gmail.com", "Student Life Gerenciamento Estudantil");
        $this->email->to($dados['ds_email']);
        $this->email->subject('Student Life Gerenciamento Estudantil - Confirmação de Cadastro');
        $this->email->message($mensagem);
        if ($this->email->send()) {
//            $this->smartyci->display('cadastro_enviado.tpl');
            echo json_encode(array('st' => 1, 'msg' => 'Olá <strong>' . $dados['nm_usuario'] . '</strong>, você foi cadastrado com sucesso'));
        } else {
            print_r($this->email->print_debugger());
        }
    }

    public function cadastro_confirmado() {
        $this->smartyci->display('cadastro_enviado.tpl');
    }

    public function confirmar($hashEmail) {
        $dados['st_ativo'] = 1;

        if ($this->M_usuario->updateAtivo($hashEmail, $dados)) {
            $this->smartyci->display('cadastro_liberado.tpl');
        } else {
            echo "Houve um erro ao confirmar seu cadastro";
        }
    }

}
