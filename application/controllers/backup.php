<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        
        // carrega o model
        $this->load->model('M_backup');
    }

    public function index() {
        // retorna os dados do Backup
        $backups = $this->M_backup->getBackups();
        
        // atribui a variável ao template
        $this->smartyci->assign('backups', $backups);

        // exibe o template
        $this->smartyci->display('backup.tpl');
    }

    public function criarBackup() {
        header("Content-Type: application/json");
        $this->load->dbutil();

        // seta as preferencias para Backup
        $prefs = array(
            'format' => 'zip',
            'filename' => 'studentlife_db.sql'
        );

        date_default_timezone_set('America/Sao_Paulo');
        
        $backup = $this->dbutil->backup($prefs);

        $db_name = 'backup-studentlife-' . date("d-m-Y-H-i-s") . '.zip';
        $save = 'backupDB/' . $db_name;
        
        $data['ds_caminho_backup'] = $save;
        $data['nm_backup'] = $db_name;
        
        // carrega o Helper File
        $this->load->helper('file');
        write_file($save, $backup);
        
        if ($this->M_backup->insert($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Backup gerado com sucesso.'));
        }

//        // carrega o Helper Download
//        $this->load->helper('download');
//        // força o download do backup
//        force_download($db_name, $backup);
        
    }
    
    public function excluir($cd_backup = NULL) {
        $data['cd_backup'] = $cd_backup;
        $data['fl_excluido'] = 1;

        // verifica se foi excluído e exibe uma mensagem
        if ($this->M_backup->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'O Backuo foi excluído com sucesso.'));
        }
    }
    
    public function download($cd_backup) {
        // carrega o Helper 'download'
        $this->load->helper('download');
        
        // retorna dos dados do Documento
        $backup = $this->M_backup->getBackup($cd_backup);

        // atribui a variável o caminho do Documento
        $path = $backup[0]->ds_caminho_backup;
        
        // executa o download
        force_download($path, null);
    }
}
