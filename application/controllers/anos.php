<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Anos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }

        // carrega o model
        $this->load->model('M_ano');
    }

    public function index($cd_curso) {
        // carrega o model
        $this->load->model('M_curso');

        // recuperar o status e o código do usuário na sessão
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');

        if ($sessao_dados['status'] == 3) {
            // lista os anos no qual o aluno está matriculado
            $anos = $this->M_ano->getAnosAlunoMatriculado($cd_curso, $sessao_dados['id']);
//            var_dump($anos);die;
        } else {
            // lista os anos correspondente ao código do curso
            $anos = $this->M_ano->getAnos($cd_curso);
        }

        // lista os dados correspondentes ao código do curso        
        $cursos = $this->M_curso->getCurso($cd_curso);

        // atribui a variável ao template
        $this->smartyci->assign('anos', $anos);
        $this->smartyci->assign('cursos', $cursos);

        // exibe o template
        $this->smartyci->display('anos.tpl');
    }

    public function incluir() {
        // carrega a biblioteca de validação de formulários do CI
        $this->load->library('form_validation');

        // determina regras de validação aos campos do formulário para cadastrar curso
        $this->form_validation->set_rules('dt_ano', 'Ano', 'required');

        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }

        // obtém os dados do form
        $data['CURSO_cd_curso'] = $this->input->post('cd_curso');
        $data['dt_ano'] = $this->input->post('dt_ano');

        header('Content-Type: application/json');

        // verifica se o ano foi inserido no Banco de Dados
        if ($this->M_ano->insert($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Ano adicionado com sucesso.'));
            exit;
        }

        // envia uma mensagem à view caso ocorra um erro ao adicionar um ano
        echo json_encode(array('st' => 2, 'msg' => 'Erro ao adicionar um ano.'));
    }

    public function editar($cd_ano = NULL) {
        header("Content-Type: application/json");

        // retorna os dados de um determinado ano
        $ano = $this->M_ano->getAno($cd_ano);

        echo json_encode(array_shift($ano));
    }

    public function atualizar() {
        // carrega a biblioteca de validação de formulários do CI
        $this->load->library('form_validation');

        // determina regras de validação aos campos do formulário para cadastrar curso
        $this->form_validation->set_rules('dt_ano', 'Ano', 'required');

        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }
        
        // obtém os dados do form
        $data['CURSO_cd_curso'] = $this->input->post('cd_curso');
        $data['dt_ano'] = $this->input->post('dt_ano');
        $data['cd_ano'] = $this->input->post('cd_ano');

        header('Content-Type: application/json');

        // verifica se o ano foi atualizado no Banco de Dados
        if ($this->M_ano->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Ano atualizado com sucesso.'));
            exit;
        }

        // envia uma mensagem à view caso ocorra um erro ao atualizar um ano
        echo json_encode(array('st' => 2, 'msg' => 'Erro ao atualizar um ano.'));
    }
    
    public function excluir($cd_ano = NULL) {
        $data['cd_ano'] = $cd_ano;
        $data['fl_excluido'] = 1;

        // verifica se foi excluído e exibe uma mensagem
        if ($this->M_ano->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Ano excluído com sucesso.'));
        }
    }

}
