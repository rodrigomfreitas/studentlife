<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_curso');
    }

    public function index() {
        $this->output->enable_profiler(TRUE);
        // recuperar o status e o código do usuário na sessão
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');

//        $array_cd_cursos[] = "";
//        foreach ($cursos as $linha) {
//            $array_cd_cursos[] = $linha->cd_curso;
//        }
//        $turmas = $this->turmaModel->getTurmasPorCurso($array_cd_cursos);
//        $this->load->model('disciplinaModel');
//        $cursos_aluno = $this->disciplinaModel->get_cursos_aluno();
//        
        // verifica se o status é de um usuario do tipo aluno, se não, é professor ou administrador
        if ($sessao_dados['status'] == 3) {
            // lista os cursos no qual o aluno está matriculado
            $cursos = $this->M_curso->getCursosAlunoMatriculado($sessao_dados['id']);
        } else {
            // lista todos os cursos
            $cursos = $this->M_curso->getCursos();
        }
        
        // atribui a variável ao template
        $this->smartyci->assign('cursos', $cursos);
        
        // exibe o template
        $this->smartyci->display('cursos.tpl');
    }

    public function incluir() {
        $this->load->library('form_validation');

        // determina regras de validação aos campos do formulário para cadastrar curso
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('sigla', 'Sigla', 'trim|required');
        $this->form_validation->set_rules('semestre_total', 'Total de Semestres', 'trim|required');
//        $this->form_validation->set_rules('ano_inicio', 'Data Início', 'trim|required');
        
        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }

        $data['nm_curso'] = $this->input->post('nome');
        $data['sg_curso'] = $this->input->post('sigla');
        $data['nr_semestre_total'] = $this->input->post('semestre_total');
//        $data['dt_curso_inicio'] = $this->input->post('ano_inicio');

        $cursoSemAcentos = $this->tirarAcentos($data['nm_curso']);

//        $data['ds_caminho_curso'] = 'cursos_materiais/' . strtolower(str_replace(' ', '_', $cursoSemAcentos) . '_' . date('Y', strtotime($data['dt_curso_inicio'])));
        $data['ds_caminho_curso'] = 'cursos_materiais/' . strtolower(str_replace(' ', '_', $cursoSemAcentos));
        
        if ($this->criarDiretorio($data) == TRUE && $last_id = $this->M_curso->insert($data)) {
            $this->controleLog(INCLUIR, $last_id);

            echo json_encode(array('st' => 1, 'msg' => 'Curso adicionado com sucesso.'));
        }
        exit;

        echo json_encode(array('st' => 2, 'msg' => 'Ocorreu um erro! No momento não foi possível adicionar o curso.'));
    }

    public function editar($cd_curso = null) {
        header("Content-Type: application/json");

        $cursos = $this->M_curso->getCurso($cd_curso);

        echo json_encode(array_shift($cursos));
    }

    public function atualizar() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('sigla', 'Sigla', 'trim|required');
        $this->form_validation->set_rules('semestre_total', 'Total de Semestres', 'trim|required');
//        $this->form_validation->set_rules('ano_inicio', 'Data Início', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }

        $data['cd_curso'] = $this->input->post('cd_curso');
        $data['nm_curso'] = $this->input->post('nome');
        $data['sg_curso'] = $this->input->post('sigla');
        $data['nr_semestre_total'] = $this->input->post('semestre_total');
//        $data['dt_curso_inicio'] = $this->input->post('ano_inicio');
        $caminhoAntigo = $this->input->post('ds_caminho');

        $cursoSemAcentos = $this->tirarAcentos($data['nm_curso']);

//        $data['ds_caminho_curso'] = 'cursos_materiais/' . strtolower(str_replace(' ', '_', $cursoSemAcentos) . '_' . date('Y', strtotime($data['dt_curso_inicio'])));
        $data['ds_caminho_curso'] = 'cursos_materiais/' . strtolower(str_replace(' ', '_', $cursoSemAcentos));

        if ($this->renomearDiretorio($data, $caminhoAntigo) == TRUE && $this->M_curso->update($data)) {
            $this->controleLog(ALTERAR, $data['cd_curso']);

            echo json_encode(array('st' => 1, 'msg' => 'Curso atualizado com sucesso.'));
            exit;
        }

        echo json_encode(array('st' => 2, 'msg' => 'Ocorreu um erro! No momento não foi possível atualizar o curso.'));
    }

    public function excluir($cd_curso = null) {
        $data['cd_curso'] = $cd_curso;
        $data['fl_excluido'] = 1;
        if ($this->M_curso->update($data)) {
            $this->controleLog(EXCLUIR, $data['cd_curso']);

            echo json_encode(array('st' => 1, 'msg' => 'Curso excluido com sucesso.'));
        }
    }

    function tirarAcentos($string) {
        return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç)/", "/(Ç)/"), explode(" ", "a A e E i I o O u U n N c C"), $string);
    }

    public function criarDiretorio($data) {
        if (!is_dir($data['ds_caminho_curso'])) {
            mkdir($data['ds_caminho_curso'], 0755, TRUE);
        }
        return true;
    }

    public function renomearDiretorio($data, $caminhoAntigo) {
        if (is_dir($caminhoAntigo)) {
            rename($caminhoAntigo, $data['ds_caminho_curso']);
        }
        return true;
    }

    public function controleLog($acao, $cd_curso = NULL) {
        $this->load->model('M_log');
        $this->load->model('M_log_curso');

        date_default_timezone_set('America/Sao_Paulo');

        $data = date("Y-m-d");
        $hora = date("H:i:s", time());

        $dados['USUARIO_cd_usuario'] = $sessao_dados['id'] = $this->session->userdata('id');
        $dados['dt_acao'] = $data;
        $dados['hr_acao'] = $hora;

        if ($acao == INCLUIR) {
            $dados['ACAO_cd_acao'] = 3;
//            $this->M_log->insert($dados);
//            return;
            $dados['CURSO_cd_curso'] = $cd_curso;
        } else if ($acao == ALTERAR) {
            $dados['ACAO_cd_acao'] = 5;
            $dados['CURSO_cd_curso'] = $cd_curso;
        } else if ($acao == EXCLUIR) {
            $dados['ACAO_cd_acao'] = 4;
            $dados['CURSO_cd_curso'] = $cd_curso;
        }

        $this->M_log_curso->insert($dados);
    }

}
