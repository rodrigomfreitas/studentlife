<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_log');
    }

    public function index($mensagem = NULL) {
        $this->load->model('M_usuario');

        $acoes = $this->M_log->listaAcoes();
        $usuarios = $this->M_usuario->getUsuarios();

        $this->smartyci->assign('acoes', $acoes);
        $this->smartyci->assign('usuarios', $usuarios);
        $this->smartyci->assign('mensagem', $mensagem);
        $this->smartyci->display('logs.tpl');
    }

    public function validar() {
        $cd_acao = $this->input->post('cd_acao');
        $cd_usuario = $this->input->post('cd_usuario');
        $data['dt_acao_inicio'] = $this->input->post('dt_inicio');
        $data['dt_acao_final'] = $this->input->post('dt_final');

        if ($cd_acao == NULL && $cd_usuario == NULL || $cd_acao != NULL && $cd_usuario != NULL) {
            $mensagem = '<div class="alert alert-danger"><p>Preencha, pelo menos, o campo <strong>Ação</strong> ou <strong>Usuário</strong></p></div>';
            $this->index($mensagem);
        } else if ($cd_acao != NULL) {
            $this->resultadoLogsAcao($cd_acao, $data);
        } else if ($cd_usuario != NULL) {
            $this->resultadoLogsUsuario($cd_usuario, $data);
        }
    }

//    public function lista($cd_usuario) {
//        $usuariosAcoes = $this->M_log->listaAcaoUsuarios($cd_usuario);
////        var_dump($usuariosAcoes);die;
//        if (count($usuariosAcoes) > 0) {
//            $resposta["data"] = array();
//            
//            foreach ($usuariosAcoes as $u) {
//                $usuario = array();
//                $usuario[] = $u->USUARIO_cd_usuario;
//                $usuario[] = $u->nm_usuario;
//                $usuario[] = $u->ds_acao;
//                $usuario[] = $u->dt_acao;
//                $usuario[] = $u->hr_acao;
//                
//                array_push($resposta["data"], $usuario);
//            }
//            
//            echo json_encode($resposta, JSON_UNESCAPED_UNICODE);
//        }
//    }

    public function resultadoLogsAcao($cd_acao, $data = NULL) {
        $acoes = $this->M_log->listaAcao($cd_acao, $data);

        if (count($acoes) == 0) {
            $acao = $this->M_log->listaPorAcao($cd_acao);
            $this->smartyci->assign('acao', $acao);
        }
        $this->smartyci->assign('acoes', $acoes);
        $this->smartyci->display('resultado_logs_acao.tpl');
    }

    public function resultadoLogsUsuario($cd_usuario, $data = NULL) {
        $usuariosAcoes = $this->M_log->listaAcaoUsuarios($cd_usuario, $data);

        $this->smartyci->assign('usuariosAcoes', $usuariosAcoes);
        $this->smartyci->display('resultado_logs_usuario.tpl');
    }

}
