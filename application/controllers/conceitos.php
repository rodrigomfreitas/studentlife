<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Conceitos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_conceito');
    }

    public function index($id_disciplina) {
        if ($this->session->userdata('status') == 2) {
            $avaliacoes = $this->M_conceito->get_avaliacao($id_disciplina);
            $alunos = $this->M_conceito->get_avaliacoes_aluno($id_disciplina);

            $this->smartyci->assign('alunos', $alunos);
            $this->smartyci->assign('avaliacoes', $avaliacoes);
            $this->smartyci->display('conceitos.tpl');
        } else {
            $conceitos = $this->M_conceito->get_conceitos_aluno($id_disciplina);
            $this->smartyci->assign('conceitos', $conceitos);
            $this->smartyci->display('conceitos_aluno.tpl');
        }
    }

    public function a_publish($avaliacao_id, $usuario_id, $conceito) {

        echo ($this->M_conceito->a_publish($avaliacao_id, $usuario_id, $conceito));
    }

}
