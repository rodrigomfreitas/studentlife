<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recomendacoes extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }

        // carrega o model
        $this->load->model('M_recomendacao');
    }

    public function index($cd_avaliacao) {
        $this->output->enable_profiler(FALSE);
        // carrega o model
//        $this->load->model('avaliacaoModel');
        
        // retorna os dados da Avaliação
        $avaliacao = $this->M_recomendacao->getAvaliacao($cd_avaliacao);
        
        // retira as vírgulas por uma expressão regular
        $split = preg_split("/, /", $avaliacao[0]->palavras_chave_avaliacao);
        
        // obtém os documentos para recomendação através das palavras_chave de uma Avaliação
        $materiais = $this->M_recomendacao->getRecomendacaoMaterial($split);
        
        // atribui a variável ao template
        $this->smartyci->assign('avaliacao', $avaliacao);
        $this->smartyci->assign('materiais', $materiais);

        // exibe o template
        $this->smartyci->display('recomendacoes_conteudos.tpl');
    }
    
    public function download($cd_documento) {
        // carrega o Helper 'download'
        $this->load->helper('download');
        
        // retorna dos dados do Documento
        $documento = $this->M_recomendacao->getRecomendacao($cd_documento);

        // atribui a variável o caminho do Documento
        $path = $documento[0]->ds_caminho_documento;
        
        // executa o download
        force_download($path, null);
    }
    
    public function downloadAll($cd_avaliacao){
        $this->load->library('zip');
        $this->load->helper('download');
        
        // retorna os dados da Avaliação
        $avaliacao = $this->M_recomendacao->getAvaliacao($cd_avaliacao);
        
        // retira as vírgulas por uma expressão regular
        $split = preg_split("/, /", $avaliacao[0]->palavras_chave_avaliacao);
        
        // obtém os documentos para recomendação através das palavras_chave de uma Avaliação
        $materiais = $this->M_recomendacao->getRecomendacaoMaterial($split);
        
        foreach ($materiais as $m) {
            // obtém os caminhos onde constam os arquivos
            $paths = $m->ds_caminho_documento;
            // adiciona os arquivos para serem compactados em .zip
            $this->zip->add_data($paths, file_get_contents($paths));
        }
       
       // força o download do arquivo contendo o nome da Avaliação como o nome do arquivo
       $this->zip->download($avaliacao[0]->nm_avaliacao);
       
    }
}
