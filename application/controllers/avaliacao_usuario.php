<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Avaliacao_usuario extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_avaliacao_usuario');
    }

    /*
     * Método index que carrega a página com uma lista dos alunos de uma ou mais turmas
     * que foram setadas como visivéis a essa avaliação
     */

    public function index($cd_disciplina, $cd_avaliacao) {
        // carrega os dados da disciplina para o menu da disciplina interna
        menu_disciplina($cd_disciplina);

        // carrega os Conceitos dos Alunos de uma determinada Avaliação
        $this->carregarConceitos($cd_avaliacao);

        $mensagem = "";
        $cd_turma = array();
        $ordenar = array();
        date_default_timezone_set('America/Sao_Paulo');
        $data = date('Y-m-d');

        $this->load->model('M_avaliacao');
        $this->load->model('M_usuario');
        $this->load->model('M_avaliacao_turma');

        // busca as Turmas que estão marcadas como visível para uma determinada Avaliação
        $turmas = $this->M_avaliacao_turma->getAvaliacaoTurma($cd_avaliacao);
        
        // busca os dados da Avaliação
        $avaliacao = $this->M_avaliacao->getAvaliacao($cd_avaliacao);

        // cria um array de string com os Códigos das Turmas
        foreach ($turmas as $turma) {
            $cd_turma[] = $turma->TURMA_cd_turma;
        }

        // busca os Alunos que estão matriculados em uma Turma que está marcada como visível para uma determinada Avaliação
        $alunos = $this->M_usuario->alunosDasTurmasVisivilidades($cd_disciplina, $cd_turma);
//        var_dump($alunos);die;
        // ordena os Alunos pela Sigla do Curso, Número do Semestre, Turno e Ano da Turma

        foreach ($alunos as $a) {
            $primeiro = strtoupper($a->sg_curso . "/" . $a->nr_semestre . "" . $a->nm_turno[0] . "/" . date_format(date_create($a->dt_turma), 'Y'));

            if (!isset($ordenar[$primeiro])) {
                $ordenar[$primeiro] = array();
            }

            $ordenar[$primeiro][] = $a;
        }
        
        // verifica a Data Final de Entrega com o Data do Servidor
        if (strtotime($avaliacao[0]->dt_data_final) >= strtotime($data)) {
            $mensagem['msg'] = "Em Andamento";
            $mensagem['hpanel-color'] = "hblue";
            $mensagem['label-color'] = "label-info";
        } else {
            $mensagem['msg'] = "Encerrado";
            $mensagem['hpanel-color'] = "hred";
            $mensagem['label-color'] = "label-danger";
        }

        $this->smartyci->assign('avaliacao', $avaliacao);
        $this->smartyci->assign('ordenar', $ordenar);
        $this->smartyci->assign('mensagem', $mensagem);

        $this->smartyci->display('avaliacao_aluno.tpl');
    }

    public function incluir() {
        $data = array();
        
        $cd_avaliacao = $this->input->post('cd_avaliacao');
        $cd_usuario = $this->input->post('cd_usuario');
        $cd_turma = $this->input->post('cd_turma');
        $conceitos = $this->input->post('conceitos');
        
        $dados['AVALIACAO_cd_avaliacao'] = $cd_avaliacao;
        $dados['USUARIO_cd_usuario'] = $cd_usuario;
        $dados['TURMA_cd_turma'] = $cd_turma;
        $dados['ds_conceito'] = $conceitos;
        
        $quant_usuarios = count($cd_usuario);
        
        for ($i=0; $i < $quant_usuarios; $i++) {
            $data[] = array(
                'AVALIACAO_cd_avaliacao' => $cd_avaliacao,
                'USUARIO_cd_usuario' => $cd_usuario[$i],
                'TURMA_cd_turma' => $cd_turma[$i],
                'ds_conceito' => $conceitos[$i]
            );
        }
        
        $this->M_avaliacao_usuario->delete($cd_avaliacao);
        
        if ($this->M_avaliacao_usuario->insert($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Conceitos salvos com sucesso.'));
            exit;
        }

        echo json_encode(array('st' => 2, 'msg' => 'Ocorreu um erro! No momento não foi possível salvar os conceitos.'));
    }

    public function carregarConceitos($cd_avaliacao) {
        $conceitos = $this->M_avaliacao_usuario->carregarConceitos($cd_avaliacao);

        $ordenar_conceito = array();
        foreach ($conceitos as $c) {
            $ordenar_conceito[$c->USUARIO_cd_usuario] = array('USUARIO_cd_USUARIO' => $c->USUARIO_cd_usuario,
                                                              'AVALIACAO_cd_avaliacao' => $c->AVALIACAO_cd_avaliacao,
                                                              'ds_conceito' => $c->ds_conceito);
        }
//        var_dump($ordenar_conceito);die;
        $this->smartyci->assign('ordenar_conceito', $ordenar_conceito);
        $this->smartyci->assign('conceitos', $conceitos);
        $this->smartyci->fetch('avaliacao_aluno.tpl');
    }

}
