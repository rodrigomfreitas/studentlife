<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

    public function buscaCep() {
        $cep = $_POST['cep'];

        $reg = simplexml_load_file("http://cep.republicavirtual.com.br/web_cep.php?formato=xml&cep=" . $cep);

        $dados['sucesso'] = (string) $reg->resultado;
        $dados['ds_endereco'] = (string) $reg->tipo_logradouro . ' ' . $reg->logradouro;
        $dados['ds_bairro'] = (string) $reg->bairro;
        $dados['ds_cidade'] = (string) $reg->cidade;
        $dados['sg_estado'] = (string) $reg->uf;

        echo json_encode($dados);
    }

}
