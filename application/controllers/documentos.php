<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Documentos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }

        // carrega o model
        $this->load->model('M_documento');
    }

    public function index($cd_disciplina, $cd_aula) {

        // carrega o menu interno na página de disciplina
        menu_disciplina($cd_disciplina);

        // carrega o model
        $this->load->model('M_aula');
        $this->load->model('M_turma');

        // recuperar o status e o código do usuário na sessão
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');

        // retorna os materiais de uma determinada aula
        $materiais = $this->M_documento->getDocumentos($cd_aula);

        // retorna os dados referente o código da aula
        $aula = $this->M_aula->getAula($cd_aula);

        // retorna as Turmas que corresponde a Disciplina
        $turmas = $this->M_turma->getTurmasNaDisciplina($cd_disciplina);

        // atribui a variável ao template
        $this->smartyci->assign('aula', $aula);
        $this->smartyci->assign('materiais', $materiais);
        $this->smartyci->assign('turmas', $turmas);

        // exibe o template
        $this->smartyci->display('material.tpl');
    }

    function tirarAcentos($string) {
        return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç)/", "/(Ç)/"), explode(" ", "a A e E i I o O u U n N c C"), $string);
    }

    public function incluir() {
        // carrega a biblioteca de validação de formulários do CI
        $this->load->library('form_validation');

        // define as preferências para upload
        $config['upload_path'] = './material/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '20480';
        $config['encrypt_name'] = false;
        $this->load->library('upload', $config);

        header('Content-Type: application/json');

        // caso não passe pelas preferências para a validação do upload exibe as mensagens no formulário
        if (!$this->upload->do_upload('arquivo')) {
            echo json_encode(array('st' => 2, 'msg' => $this->upload->display_errors()));
            exit;
        }

        // determina regras de validação aos campos do formulário para cadastrar curso
        $this->form_validation->set_rules('ds_nomenclatura', 'Nome do material', 'required');
        $this->form_validation->set_rules('ds_descricao', 'Descrição', 'required');
        $this->form_validation->set_rules('ds_palavras_chave', 'Palavras-chave', 'required');

        header('Content-Type: application/json');

        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }

        // obtém a extensão do arquivo
        $dadosArquivo = $this->upload->data();

        // retira os acentos nas palavras-chave
        $palavrasChave = $this->input->post('ds_palavras_chave');

        // obtém os dados do form
        $data['AULA_cd_aula'] = $this->input->post('cd_aula');
        $data['ds_nomenclatura'] = $this->input->post('ds_nomenclatura');
        $data['ds_descricao'] = $this->input->post('ds_descricao');
        $data['ds_palavras_chave'] = $this->tirarAcentos($palavrasChave);

        // obtém a extensão e o nome do arquivo em $dadosArquivo = $this->upload->data()
        $data['ds_tipo_documento'] = $dadosArquivo['file_ext'];
        $data['ds_arquivo'] = $dadosArquivo['file_name'];
        $data['ds_caminho_documento'] = 'material/' . $dadosArquivo['file_name'];

        // verifica se o material foi inserido no Banco de Dados
        if ($this->M_documento->insert($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Material adicionado com sucesso.'));
            exit;
        }

        // envia uma mensagem à view caso ocorra um erro ao adicionar um material
        echo json_encode(array('st' => 3, 'msg' => 'Ocorreu um erro ao inserir o material.'));
    }

    public function editar($cd_documento) {
        header("Content-Type: application/json");

        // retorna os dados do documento
        $documentos = $this->M_documento->getDocumento($cd_documento);

        echo json_encode(array_shift($documentos));
    }

    public function atualizar() {
        // carrega a biblioteca de validação de formulários do CI
        $this->load->library('form_validation');

        header('Content-Type: application/json');

        // determina regras de validação aos campos do formulário para cadastrar curso
        $this->form_validation->set_rules('ds_nomenclatura', 'Nome do material', 'required');
        $this->form_validation->set_rules('ds_descricao', 'Descrição', 'required');
        $this->form_validation->set_rules('ds_palavras_chave', 'Palavras-chave', 'required');

        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }

        // define as preferências para upload
        $config['upload_path'] = './material/';
        $config['allowed_types'] = '*';
        $config['max_size'] = '20480';
        $config['encrypt_name'] = false;
        $this->load->library('upload', $config);

        // caso não passe pelas preferências para a validação do upload exibe as mensagens no formulário
        if (empty($_FILES['arquivo'])) {
            // retira os acentos nas palavras-chave
            $palavrasChave = $this->input->post('ds_palavras_chave');

            // obtém a ds_arquivo do form
            $data['ds_arquivo'] = $this->input->post('ds_arquivo');
            $data['ds_tipo_documento'] = $this->input->post('ds_tipo_documento');
            $data['cd_documento'] = $this->input->post('cd_documento');
            $data['AULA_cd_aula'] = $this->input->post('cd_aula');
            $data['ds_nomenclatura'] = $this->input->post('ds_nomenclatura');
            $data['ds_descricao'] = $this->input->post('ds_descricao');
            $data['ds_palavras_chave'] = $this->tirarAcentos($palavrasChave);
        }

        if ($this->upload->do_upload('arquivo')) {
            // obtém as informações do arquivo
            $dadosArquivo = $this->upload->data();

            // exclui o arquivo anterior
            $path = 'material/' . $this->input->post('ds_arquivo');
            unlink($path);

            // retira os acentos nas palavras-chave
            $palavrasChave = $this->input->post('ds_palavras_chave');

            // obtém os dados do form
            $data['cd_documento'] = $this->input->post('cd_documento');
            $data['AULA_cd_aula'] = $this->input->post('cd_aula');
            $data['ds_nomenclatura'] = $this->input->post('ds_nomenclatura');
            $data['ds_descricao'] = $this->input->post('ds_descricao');
            $data['ds_palavras_chave'] = $this->tirarAcentos($palavrasChave);

            // obtém a extensão do arquivo em $dadosArquivo = $this->upload->data()
            $data['ds_tipo_documento'] = $dadosArquivo['file_ext'];
            // obtém o nome do arquivo em $dadosArquivo = $this->upload->data()
            $data['ds_arquivo'] = $dadosArquivo['file_name'];
            $data['ds_caminho_documento'] = 'material/' . $dadosArquivo['file_name'];
        }

        header('Content-Type: application/json');

        // verifica se o material foi atualizado no Banco de Dados
        if ($this->M_documento->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Material atualizado com sucesso.'));
            exit;
        }

        // envia uma mensagem à view caso ocorra um erro ao atualizar um material
        echo json_encode(array('st' => 3, 'msg' => 'Ocorreu um erro ao atualizar o material.'));
    }

    public function incluirVisibilidadeDocumento() {
        // carrega o model
        $this->load->model('M_documento_turma');

        // obtém os dados no form
        $cd_documento = $this->input->post('cd_documento');
        $cd_turmas = $this->input->post('cd_turmas');
        $cd_disciplina = $this->input->post('cd_disciplina');

        // se estiver vazio a variável, executa a função delete para retirar a(s) visibilidade(s) do(s) documento(s) à(s) turma(s)
        if (empty($cd_turmas)) {
            $this->M_documento_turma->delete($cd_documento);
            echo json_encode(array('st' => 1, 'msg' => 'Visibilidade alterada com sucesso.'));
            exit;
        }

        // se não estiver vazio a variável, executa para excluir toda a tabela e depois insere a(s) visibilidade(s) do(s) documento(s) à(s) turma(s)
        if (!empty($cd_turmas)) {
            $this->M_documento_turma->delete($cd_documento);
            foreach ($cd_turmas as $cd_turma) {
                $data = array('DOCUMENTO_cd_documento' => $cd_documento, 'TURMA_cd_turma' => $cd_turma, 'DISCIPLINA_cd_disciplina' => $cd_disciplina);

                $this->M_documento_turma->insert($data);
            }
            echo json_encode(array('st' => 1, 'msg' => 'Visibilidade alterada com sucesso.'));
        }
    }

    public function atualizarVisibilidadeDocumento($cd_documento) {

        // carrega o model
        $this->load->model('M_documento_turma');

        header("Content-Type: application/json");

        $cd_documentos = array();

        // retorna os dados de visibilidade à(s) turma (s)
        $documento = $this->M_documento_turma->getDocumentoTurma($cd_documento);

        foreach ($documento as $doc) {
            $cd_documentos[] = $doc->TURMA_cd_turma;
        }

        echo json_encode($cd_documentos);
        die;
    }

    public function excluir($cd_documento = NULL) {

        $data['cd_documento'] = $cd_documento;
        $data['fl_excluido'] = 1;

        // retorna os dados de um determinado documento
        $documento = $this->M_documento->getDocumento($cd_documento);
        
        // exclui o arquivo anterior
        $path = 'material/' . $documento[0]->ds_arquivo;
        
        // verifica se foi excluído e exibe uma mensagem
        if ($this->M_documento->update($data)) {
            unlink($path);
            echo json_encode(array('st' => 1, 'msg' => 'Documento excluído com sucesso.'));
        }
    }

}
