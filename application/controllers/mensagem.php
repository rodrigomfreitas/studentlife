<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mensagem extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_chat');
    }

    public function index($cd_envio = null) {
//        $this->output->enable_profiler(true);
        $sessao_dados['id'] = $this->session->userdata('id');
        $cd_enviar = $this->uri->segment(2);
        $mensagens = $this->M_chat->get_mensagem($cd_envio);
        $usuarios = $this->M_chat->get_usuario();

        if (!empty($cd_envio)) {
            $mensagens = $this->M_chat->get_mensagem($cd_envio);
            $this->M_chat->update_status($cd_envio, $sessao_dados['id']);
        } else {
            $mensagens = $this->M_chat->get_mensagens_nao_lidas();
        }

        $this->smartyci->assign('cd_enviar', $cd_enviar);
        $this->smartyci->assign('mensagens', $mensagens);
        $this->smartyci->assign('usuarios', $usuarios);

        $this->smartyci->display('mensagem.tpl');
    }

    public function enviar() {
        $this->load->library('form_validation');

        // determina regras de validação aos campos do formulário para cadastrar curso
        $this->form_validation->set_rules('mensagem', 'Mensagem', 'trim|required');
        
        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }
        
        $data['cd_envia'] = $this->session->userdata('id');
        $data['cd_recebe'] = $this->input->post('cd_recebe');
        $data['ds_mensagem'] = $this->input->post('mensagem');
        
        if ($data['cd_recebe'] == NULL) {
            echo json_encode(array('st' => 3, 'msg' => 'Por favor, selecione um usuário na lista para enviar uma mensagem.'));
            exit;
        }

        if ($this->M_chat->set_mensagem($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Mensagem enviada com sucesso.'));
            exit;
        }
        echo json_encode(array('st' => 2, 'msg' => 'Erro ao enviar a mensagem.'));
    }

}
