<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {
        if ($this->session->userdata('logado') == true) {
            redirect('home');
        }
        $this->smartyci->display('login.tpl');
    }

    public function verifica_sessao() {
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
    }

    public function logar() {
        $this->load->model('M_usuario');

        // variável para encriptar a senha
        $salt = 'St4d3ntL1f3-G3r3nc14m3nt03st4nt1l';

        $email = $this->input->post('email');
        $senha = md5($this->input->post('senha'));

        $senhaSha1 = sha1($senha . $salt);

        $dados['usuario'] = $this->M_usuario->verificaUsuario($email, $senhaSha1);

        if ($email == "" || $senha == "") {
            echo json_encode(array('st' => 0, 'msg' => 'E-mail ou Senha inválidos.'));
            exit;
        }

        if (count($dados['usuario']) == 1 && $dados['usuario'][0]->st_ativo == "1") {
            $sessao['nome'] = $dados['usuario'][0]->nm_usuario;
            $sessao['id'] = $dados['usuario'][0]->cd_usuario;
            $sessao['status'] = $dados['usuario'][0]->TIPO_USUARIO_cd_tipo_usuario;
            $sessao['foto'] = $dados['usuario'][0]->ds_foto;
            $sessao['tipo_usuario'] = $dados['usuario'][0]->nm_tipo_usuario;
            $sessao['logado'] = TRUE;
            $this->session->set_userdata($sessao);

            $acao = "entrar";
            $this->controleLog($acao);

            echo json_encode(array('st' => 1, 'msg' => 'Login efetuado com sucesso. Redirecionando...'));
//            redirect(base_url("home"));
            exit;
        }

        if (count($dados['usuario']) == 1 && $dados['usuario'][0]->st_ativo == "0") {
            echo json_encode(array('st' => 2, 'msg' => 'Sua conta não está ativa. Verifique seu e-mail para ativação.'));
            exit;
        }

        $sessao['nome'] = NULL;
        $sessao['logado'] = FALSE;
        $this->session->set_userdata($sessao);
        echo json_encode(array('st' => 0, 'msg' => 'Usuário ou Senha inválidos.'));
//            redirect(base_url("login"));
    }

    public function logout() {
        $sessao['nome'] = NULL;
        $sessao['logado'] = FALSE;
        $this->session->set_userdata($sessao);

        $acao = "sair";
        $this->controleLog($acao);

        redirect(base_url("login"));
    }

    public function esqueci_minha_senha() {
        $this->smartyci->display('form_recupera_login.tpl');
    }

    public function recuperar_login() {
        $this->load->model('M_usuario');
        $this->load->helper('string');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('email', 'E-mail', 'required|valid_email');

        $email = $this->input->post('email');
        $dados ['usuario'] = $this->M_usuario->verificaEmail($email);

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
        } else if (count($dados['usuario']) == 1) {
            $dados['random'] = random_string('alnum');

            $mensagem = $this->load->view('emails/recuperar_senha.php', $dados, TRUE);
            $this->load->library('email');
            $this->email->from("macielfreitas2012@gmail.com", "StudentLife Gerenciamento Estudantil");
            $this->email->to($dados['usuario'][0]->email);
            $this->email->subject('StudentLife Gerenciamento Estudantil - Confirmação de Cadastro');
            $this->email->message($mensagem);
            if ($this->email->send()) {
                echo json_encode(array('st' => 1, 'msg' => ''));
//                $this->smartyci->display('senha_enviada.tpl');
            } else {
                print_r($this->email->print_debugger());
            }
        } else if (count($dados['usuario']) == 0) {
            echo json_encode(array('st' => 2, 'msg' => 'Esse e-mail não consta em nosso sistema.'));
        }
    }

    public function email_enviado_recuperacao() {
        $this->smartyci->display('senha_enviada.tpl');
    }

    public function form_nova_senha($cd_usuario, $senha) {
//        $this->output->enable_profiler(TRUE);
        $this->load->model('M_usuario');
        $id_usuario = $this->uri->segment(3);
        if ($this->M_usuario->updateSenha($cd_usuario, $senha)) {
            $this->smartyci->assign('id_usuario', $id_usuario);
            $this->smartyci->display('form_nova_senha.tpl');
        } else {
            echo "erro ao atualizar a senha";
        }
    }

    public function nova_senha($cd_usuario) {
        $this->load->model('M_usuario');

        $senha_atual = $this->input->post('senha-atual');

        $dados['usuario'] = $this->M_usuario->getUsuario($cd_usuario, $senha_atual);

        if (count($dados['usuario'] == 1)) {
            $nova_senha = md5($this->input->post('nova-senha'));

            if ($this->M_usuario->updateSenha($cd_usuario, $nova_senha)) {
                echo 1;
            } else {
                echo 2;
            }
        }
    }

    public function controleLog($acao = NULL) {
        $this->load->model('M_log');

        date_default_timezone_set('America/Sao_Paulo');

        $data = date("Y-m-d");
        $hora = date("H:i:s", time());

        $dados['USUARIO_cd_usuario'] = $sessao_dados['id'] = $this->session->userdata('id');
        $dados['dt_acao'] = $data;
        $dados['hr_acao'] = $hora;

        if ($acao == "entrar") {
            $dados['ACAO_cd_acao'] = 1;
        } else if ($acao == "sair") {
            $dados['ACAO_cd_acao'] = 2;
        }

        $this->M_log->insert($dados);
    }

    public function enviarSolicitacao() {
        $this->smartyci->display('chave_login_aluno.tpl');
    }

    public function incluirSolicitacaoCad() {
        $this->load->model('M_usuario');
        $this->load->model('M_chave_turma');

        // variável para encriptar a senha
        $salt = 'St4d3ntL1f3-G3r3nc14m3nt03st4nt1l';

        $email = $this->input->post('email');
        $senha = md5($this->input->post('senha'));

        $senhaSha1 = sha1($senha . $salt);

        $dados['usuario'] = $this->M_usuario->verificaUsuario($email, $senhaSha1);

        if ($email == "" || $senha == "") {
            echo json_encode(array('st' => 0, 'msg' => 'E-mail ou Senha inválidos.'));
            exit;
        }
        
        if (count($dados['usuario']) == 1 && $dados['usuario'][0]->st_ativo == "1") {
            $data['chave_turma_solicitacao_cad_ds_chave_turma'] = $this->input->post('chave_turma');
            $data['USUARIO_cd_usuario'] = $dados['usuario'][0]->cd_usuario;
            
            $chave = $this->M_chave_turma->getChave($data['chave_turma_solicitacao_cad_ds_chave_turma']);            
            
            if (count($chave) == 0) {
                echo json_encode(array('st' => 3, 'msg' => 'Por favor, insira uma <strong>Chave</strong> válida.'));
                exit;
            }
            
            $data['chave_turma_solicitacao_cad_cd_chave_turma'] = $chave[0]->cd_chave_turma;
            $cd_turma = $chave[0]->TURMA_cd_turma;
            $data_array = $this->loopDisciplina($cd_turma, $data);
            if ($this->M_chave_turma->insertSolicitacaoCadTurma($data_array)) {
                $sessao['nome'] = $dados['usuario'][0]->nm_usuario;
                $sessao['id'] = $dados['usuario'][0]->cd_usuario;
                $sessao['status'] = $dados['usuario'][0]->TIPO_USUARIO_cd_tipo_usuario;
                $sessao['foto'] = $dados['usuario'][0]->ds_foto;
                $sessao['tipo_usuario'] = $dados['usuario'][0]->nm_tipo_usuario;
                $sessao['logado'] = TRUE;
                $this->session->set_userdata($sessao);

                echo json_encode(array('st' => 1, 'msg' => 'Enviado a solicitação para incluí-lo à turma com sucesso.'));
                exit;
            }
        }

        if (count($dados['usuario']) == 1 && $dados['usuario'][0]->st_ativo == "0") {
            echo json_encode(array('st' => 2, 'msg' => 'Sua conta não está ativa. Verifique seu e-mail para ativação.'));
            exit;
        }

        $sessao['nome'] = NULL;
        $sessao['logado'] = FALSE;
        $this->session->set_userdata($sessao);
        echo json_encode(array('st' => 0, 'msg' => 'E-mail ou Senha inválidos.'));
    }
    
    public function loopDisciplina($cd_turma, $data) {
        $this->load->model('M_turma');
        
        $aluno = array();
        
        $disciplinas = $this->M_turma->getDisciplinasPorTurma($cd_turma);
        
        foreach ($disciplinas as $d) {
            $aluno[] = array('DISCIPLINA_cd_disciplina'=>$d->cd_disciplina, 
                'CHAVE_TURMA_solicitacao_cad_cd_chave_turma'=>$data['chave_turma_solicitacao_cad_cd_chave_turma'],
                'chave_turma_solicitacao_cad_ds_chave_turma'=>$data['chave_turma_solicitacao_cad_ds_chave_turma'],
                'USUARIO_cd_usuario'=>$data['USUARIO_cd_usuario']);            
        }
        return $aluno;
    }
    
    public function mensagemEnvioSolicitacao() {
        $this->smartyci->display('solicitacao_enviada.tpl');
    }

}
