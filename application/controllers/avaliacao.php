<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Avaliacao extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_avaliacao');
    }

    public function index($cd_disciplina) {
        $this->output->enable_profiler(FALSE);
        $this->load->model('M_turma');
        $this->load->model('M_avaliacao_turma');

        $turmas_visibilidade = array();

        // carrega os dados da disciplina para o menu da disciplina interna
        menu_disciplina($cd_disciplina);
        
        // retorna uma lista das Avaliações correspondente a Disciplina
        $avaliacoes = $this->M_avaliacao->getAvaliacaoDisciplina($cd_disciplina);
        // retorna os tipo de Avaliações (Prova e Trabalho)
        $tipo_avaliacao = $this->M_avaliacao->getTipoAvaliacao();
        // retorna as Turmas que corresponde a Disciplina
        $turmas = $this->M_turma->getTurmasNaDisciplina($cd_disciplina);
        // retorna as turmas que foram setadas com visibilidade a Avaliação
        $turmas_setadas = $this->M_avaliacao_turma->turmasSetadas($cd_disciplina);

        foreach ($turmas_setadas as $ts) {
            $turmas_visibilidade[$ts->AVALIACAO_cd_avaliacao] = $ts->AVALIACAO_cd_avaliacao;
        }
        
        $this->smartyci->assign('avaliacoes', $avaliacoes);
        $this->smartyci->assign('tipo_avaliacao', $tipo_avaliacao);
        $this->smartyci->assign('turmas', $turmas);
        $this->smartyci->assign('turmas_visibilidade', $turmas_visibilidade);
        $this->smartyci->display('avaliacoes.tpl');
    }
    
    function tirarAcentos($string) {
        return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/", "/(ç)/", "/(Ç)/"), explode(" ", "a A e E i I o O u U n N c C"), $string);
    }
    
    public function incluir() {
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('tipo_avaliacao', 'Tipo da Avaliação', 'trim|required');
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');
        $this->form_validation->set_rules('palavras_chave', 'Palavras-chave', 'required');
        $this->form_validation->set_rules('dt_cadastro', 'Data do Cadastro', 'trim|required');
        $this->form_validation->set_rules('dt_final', 'Data Final', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }
        
        // retira os acentos nas palavras-chave
        $palavrasChave = $this->input->post('palavras_chave');
        
        $data['DISCIPLINA_cd_disciplina'] = $this->input->post('cd_disciplina');
        $data['TIPO_AVALIACAO_cd_avaliacao'] = $this->input->post('tipo_avaliacao');
        $data['nm_avaliacao'] = $this->input->post('nome');
        $data['ds_avaliacao'] = $this->input->post('descricao');
        $data['palavras_chave_avaliacao'] = $this->tirarAcentos($palavrasChave);
        $data['dt_data_cadastro'] = $this->input->post('dt_cadastro');
        $data['dt_data_final'] = $this->input->post('dt_final');
        
        if ($this->M_avaliacao->insert($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Avaliação adicionada com sucesso.'));
        }
    }

    public function editar($cd_avaliacao = null) {
        header("Content-Type: application/json");

        $avaliacoes = $this->M_avaliacao->getAvaliacao($cd_avaliacao);
        
        echo json_encode(array_shift($avaliacoes));
    }

    public function atualizar() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('tipo_avaliacao', 'Tipo da Avaliação', 'trim|required');
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('descricao', 'Descrição', 'trim|required');
        $this->form_validation->set_rules('palavras_chave', 'Palavras-chave', 'required');
        $this->form_validation->set_rules('dt_cadastro', 'Data do Cadastro', 'trim|required');
        $this->form_validation->set_rules('dt_final', 'Data Final', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }
        
        // retira os acentos nas palavras-chave
        $palavrasChave = $this->input->post('palavras_chave');
        
        $data['DISCIPLINA_cd_disciplina'] = $this->input->post('cd_disciplina');
        $data['TIPO_AVALIACAO_cd_avaliacao'] = $this->input->post('tipo_avaliacao');
        $data['nm_avaliacao'] = $this->input->post('nome');
        $data['ds_avaliacao'] = $this->input->post('descricao');
        $data['palavras_chave_avaliacao'] = $this->tirarAcentos($palavrasChave);
        $data['dt_data_cadastro'] = $this->input->post('dt_cadastro');
        $data['dt_data_final'] = $this->input->post('dt_final');
        $data['cd_avaliacao'] = $this->input->post('cd_avaliacao');

        if ($this->M_avaliacao->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Avaliação atualizada com sucesso.'));
        }
    }

    public function incluir_visibilidade_avaliacao() {
        $this->load->model('M_avaliacao_turma');

        $cd_avaliacao = $this->input->post('cd_avaliacao');
        $cd_turmas = $this->input->post('cd_turmas');
        $cd_disciplina = $this->input->post('cd_disciplina');

        if (empty($cd_turmas)) {
            $this->M_avaliacao_turma->delete($cd_avaliacao);
            echo json_encode(array('st' => 1, 'msg' => 'Visibilidade alterada com sucesso.'));
        } else {
            $this->M_avaliacao_turma->delete($cd_avaliacao);
            foreach ($cd_turmas as $cd_turma) {
                $data = array('AVALIACAO_cd_avaliacao' => $cd_avaliacao, 'TURMA_cd_turma' => $cd_turma, 'DISCIPLINA_cd_disciplina' => $cd_disciplina);

                $this->M_avaliacao_turma->insert($data);
            }
            echo json_encode(array('st' => 1, 'msg' => 'Visibilidade alterada com sucesso.'));
        }
    }

    public function atualizar_visibilidade_avaliacao($cd_avaliacao) {
        $this->load->model('M_avaliacao_turma');

        header("Content-Type: application/json");

        $cd_avaliacoes = array();

        $avaliacao = $this->M_avaliacao_turma->getAvaliacaoTurma($cd_avaliacao);

        foreach ($avaliacao as $av) {
            $cd_avaliacoes[] = $av->TURMA_cd_turma;
        }

        echo json_encode($cd_avaliacoes);
        die;
    }

    public function excluir($cd_avaliacao = null) {
        $data['cd_avaliacao'] = $cd_avaliacao;
        $data['fl_excluido'] = 1;
        if ($this->M_avaliacao->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Avaliação excluido com sucesso.'));
        }
    }

}
