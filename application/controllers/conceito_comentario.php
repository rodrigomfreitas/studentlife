<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Conceito_comentario extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_conceito_comentario');
    }

    public function index($erro = NULL) {
        $this->output->enable_profiler(false);

        $alunos = $this->M_conceito_comentario->get_alunos();
        $cursos = $this->M_conceito_comentario->get_cursos();

        $this->smartyci->assign('alunos', $alunos);
        $this->smartyci->assign('cursos', $cursos);
        $this->smartyci->display('conceito_comentario.tpl');
    }

    public function get_alunos_de_curso($cd_curso) {
        $this->output->enable_profiler(false);

        $alunos = $this->M_conceito_comentario->get_alunos_de_curso($cd_curso);

        if (empty($alunos)) {
            return '{ "alunos[]": "Nenhum aluno encontrado" }';
        }
        echo json_encode($alunos);

        return;
    }

    public function validar() {
        $this->output->enable_profiler(false);

        $this->load->library('form_validation');

        $this->form_validation->set_rules('cd_curso', 'Curso', 'required');

        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            return $this->index($error);
        }

        $cd_aluno = $this->input->post('alunos');
        $cd_curso = $this->input->post('cd_curso');

        if (isset($cd_curso) && !isset($cd_aluno)) {
            $this->listar_conceito_curso($cd_curso);
        }
        
        if (isset($cd_aluno) && isset($cd_curso)) {
            $this->listar_conceito_aluno_curso($cd_curso, $cd_aluno);
        }
    }

    public function listar_conceito_curso($cd_curso) {
        $dados = $this->M_conceito_comentario->get_conceito_comentario($cd_curso);

        $this->output->enable_profiler(true);

        if (count($dados) == 0) {
            $curso = $this->M_conceito_comentario->get_curso($cd_curso);

            $this->smartyci->assign('curso', $curso);
        }

        $this->smartyci->assign('dados', $dados);
        $this->smartyci->display('resultado_conceito_curso.tpl');
    }

    public function listar_conceito_aluno_curso($cd_curso, $cd_aluno) {
        $this->output->enable_profiler(false);
        $dados = $this->M_conceito_comentario->get_conceito_comentario($cd_curso, $cd_aluno);

        if (count($dados) == 0) {
            $curso = $this->M_conceito_comentario->get_curso($cd_curso);

            $this->smartyci->assign('curso', $curso);
        }

        $alunos = $this->M_conceito_comentario->get_aluno_de_curso($cd_curso, $cd_aluno);

        $this->smartyci->assign('dados', $dados);
        $this->smartyci->assign('alunos', $alunos);
        $this->smartyci->display('resultado_conceito_aluno_curso.tpl');
    }

}
