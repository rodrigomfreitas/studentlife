<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Semestres extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_semestre');
    }

    public function index($cd_ano) {
        // carrega o model
        $this->load->model('M_curso');

        // recuperar o status e o código do usuário na sessão
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');

        // verifica se o status é de um usuario do tipo aluno, se não, é professor ou administrador
        if ($sessao_dados['status'] == 3) {
            // lista todos os semestres de um determinado curso no qual o aluno está matriculado
            $semestres = $this->M_semestre->getSemestresAluno($sessao_dados['id'], $cd_ano);
        } else {
            // lista todos os semestres correspondentes ao código do curso
            $semestres = $this->M_semestre->getSemestres($cd_ano);
        }

        // lista os dados do curso correspondentes ao código do ano
        $cursos = $this->M_curso->getCursoPorAno($cd_ano);

        // atribui a variável ao template
        $this->smartyci->assign('cursos', $cursos);
        $this->smartyci->assign('semestres', $semestres);

        // exibe o template
        $this->smartyci->display('semestres.tpl');
    }

    public function incluir() {
        // carrega a biblioteca de validação de formulários do CI
        $this->load->library('form_validation');

        // determina regras de validação aos campos do formulário para cadastrar curso
        $this->form_validation->set_rules('nr_semestre', 'Número do Semestre', 'required');

        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }

        // obtém os dados do form
        $data['CURSO_cd_curso'] = $this->input->post('cd_curso');
        $data['nr_semestre'] = $this->input->post('nr_semestre');
        $data['ANO_cd_ano'] = $this->input->post('cd_ano');

        // retorna os dados do semestre referente o código do semestre
        $nr_semestre = $this->M_semestre->verificarSemestreCadastrado($data['ANO_cd_ano'], $data['nr_semestre']);

        // verifica se o semestre consta cadastrado
        if (count($nr_semestre) == 1 && $nr_semestre[0]->ANO_cd_ano == $data['ANO_cd_ano']) {
            echo json_encode(array('st' => 3, 'msg' => 'Este semestre já consta no registro.'));
            exit;
        }

        // verifica se o semestre foi inserido no Banco de Dados
        if ($this->M_semestre->insert($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Semestre adicionado com sucesso.'));
            exit;
        }

        // envia uma mensagem à view caso ocorra um erro ao adicionar uma aula
        echo json_encode(array('st' => 2, 'msg' => 'Erro ao adicionar o Semestre.'));
    }

    public function editar($cd_semestre = null) {
        header("Content-Type: application/json");
        
        // retorna os dados de uma determinado semestre
        $semestres = $this->M_semestre->getSemestre($cd_semestre);

        echo json_encode(array_shift($semestres));
    }

    public function atualizar() {
        // carrega a biblioteca de validação de formulários do CI
        $this->load->library('form_validation');
        
        // determina regras de validação aos campos do formulário para cadastrar curso
        $this->form_validation->set_rules('nr_semestre', 'Número do Semestre', 'required');
        
        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }
        
        // obtém os dados do form
        $data['CURSO_cd_curso'] = $this->input->post('cd_curso');
        $data['cd_semestre'] = $this->input->post('cd_semestre');
        $data['nr_semestre'] = $this->input->post('nr_semestre');
        $data['ANO_cd_ano'] = $this->input->post('cd_ano');

        // verifica se o semestre foi atualizado no Banco de Dados
        if ($this->M_semestre->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Semestre atualizado com sucesso.'));
            exit;
        }
        
        // envia uma mensagem à view caso ocorra um erro ao adicionar uma aula
        echo json_encode(array('st' => 2, 'msg' => 'Erro ao atualizar o Semestre.'));
    }

    public function excluir($cd_turma = null) {
        $data['cd_semestre'] = $cd_turma;
        $data['fl_excluido'] = 1;
        if ($this->M_semestre->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Semestre excluído com sucesso.'));
        }
    }

}
