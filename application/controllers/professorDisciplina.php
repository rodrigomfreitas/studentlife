<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProfessorDisciplina extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        $this->load->model('M_professor_disciplina');
    }

    public function index($cd_disciplina) {
        // carrega o model
        $this->load->model('M_usuario');

        // carrega o menu interno na página de disciplina
        menu_disciplina($cd_disciplina);

        // retorna os dados dos professores que ministram em uma determinada disciplina
        $professoresDisciplina = $this->M_professor_disciplina->getProfessoresDisciplinas($cd_disciplina);

        // retorna os professores cadastrados no sistema
        $professores = $this->M_usuario->getProfessores();

        // atribui a variável ao template
        $this->smartyci->assign('professoresDisciplina', $professoresDisciplina);
        $this->smartyci->assign('professores', $professores);

        // exibe o template
        $this->smartyci->display('professor_disciplina.tpl');
    }

    public function incluir() {
        // carrega a biblioteca de validação de formulários do CI
        $this->load->library('form_validation');

        // determina regras de validação aos campos do formulário para cadastrar professor
        $this->form_validation->set_rules('cd_usuario', 'Professor', 'required');

        //caso não passe pelas regras de validação exibe as mensagens no formulário
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }

        // obtém os dados do form
        $data['USUARIO_cd_usuario'] = $this->input->post('cd_usuario');
        $data['DISCIPLINA_cd_disciplina'] = $this->input->post('cd_disciplina');

        header('Content-Type: application/json');

        // verifica se o professor foi inserido no Banco de Dados
        if ($this->M_professor_disciplina->insert($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Professor adicionado com sucesso.'));
            exit;
        }

        // envia uma mensagem à view caso ocorra um erro ao adicionar um professor
        echo json_encode(array('st' => 2, 'msg' => 'Erro ao adicionar um professor.'));
    }

    public function excluir($cd_usuario, $cd_disciplina) {
        $data['USUARIO_cd_usuario'] = $cd_usuario;
        $data['DISCIPLINA_cd_disciplina'] = $cd_disciplina;

        // verifica se foi excluído e exibe uma mensagem
        if ($this->M_professor_disciplina->delete($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Professor excluído com sucesso.'));
        }
    }
    

}
