<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Graficos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_avaliacao_usuario');
    }

    public function index($cd_disciplina, $cd_usuario) {
//        header("Content-Type: application/json");
        $this->output->enable_profiler(FALSE);
        menu_disciplina($cd_disciplina);

        // lista os conceitos correspondente a turma
        // $conceitos = $this->M_avaliacao_usuario->conceitosGrafico($cd_disciplina);
        // lista os conceitos correspondente o aluno
        $conceitos = $this->M_avaliacao_usuario->conceitosGraficoAluno($cd_disciplina, $cd_usuario);

        foreach ($conceitos as $row) {
            if ($row->ds_conceito == 'A') {
                $conceitoA[] = 10;
            }
            if ($row->ds_conceito == 'B') {
                $conceitoB[] = 8;
            }
            if ($row->ds_conceito == 'C') {
                $conceitoC[] = 7;
            }
            if ($row->ds_conceito == 'D') {
                $conceitoD[] = 6;
            }
            if ($row->ds_conceito == 'SC') {
                $conceitoSC[] = 1;
            }

            $nmAvaliacao[] = $row->nm_avaliacao;
        }

//        $lista = array("series"=>$series);

        $avaliacao = json_encode($nmAvaliacao, JSON_UNESCAPED_UNICODE);

//        foreach ($conceitos as $row) {
//            $conceitoA[] =  $row['conceitoA'];
//            $conceitoB[] = $row['conceitoB'];
//            $conceitoC[] = $row['conceitoC'];
//            $conceitoD[] = $row['conceitoD'];
//            $conceitoSC[] = $row['semConceito'];
//            
//            $nmAvaliacao[] = $row['nm_avaliacao'];
//        }
//
//        $avaliacao = json_encode($nmAvaliacao, JSON_UNESCAPED_UNICODE);
        $this->smartyci->assign('conceitoA', $conceitoA);
        $this->smartyci->assign('conceitoB', $conceitoB);
        $this->smartyci->assign('conceitoC', $conceitoC);
        $this->smartyci->assign('conceitoD', $conceitoD);
        $this->smartyci->assign('conceitoSC', $conceitoSC);
        $this->smartyci->assign('nmAvaliacao', $avaliacao);
        $this->smartyci->assign('conceitos', $conceitos);

        $this->smartyci->display('desempenho_aluno.tpl');
    }

}
