<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Disciplinas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_disciplina');
    }

    public function index($cd_semestre = NULL) {
        // carrega o model
        $this->load->model('M_semestre');
        $this->load->model('M_usuario');

        $cd_disciplina = array();
        $alunos_count = array();
        $professores = array();

        // recuperar o status e o código do usuário na sessão
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');

        // verifica se o status é de um usuario do tipo aluno, se não, é professor ou administrador
        if ($sessao_dados['status'] == 3) {
            // lista todos as disciplinas de um determinado curso no qual o aluno está matriculado
            $disciplinas = $this->M_disciplina->getDisciplinasAluno($cd_semestre, $sessao_dados['id']);
        } else {
            // lista todoas as disciplinas referente o código do semestre
            $disciplinas = $this->M_disciplina->getDisciplinas($cd_semestre);
        }
        // retorna os dados do semestre e curso
        $semestre = $this->M_semestre->getSemestre($cd_semestre);
        
        // cria um array com os IDs das disciplinas
        foreach ($disciplinas as $disc) {
            $cd_disciplina[] = $disc->cd_disciplina;

            // armazena a posição e o total de alunos de uma disciplina
            $alunos_count[$disc->cd_disciplina] = $this->M_usuario->totalAlunosMatriculadosDisciplina($disc->cd_disciplina);
            // armazena a posição e os nomes dos professores que lecionam na disciplina
            $professores[$disc->cd_disciplina] = $this->M_usuario->professoresInscritosDisciplina($disc->cd_disciplina);
        }
        // lista os professores que ministram na disciplina
//            $professores = $this->M_usuario->professoresInscritosDisciplina($cd_disciplina);

        $this->smartyci->assign('alunos_count', $alunos_count);
        $this->smartyci->assign('professores', $professores);
        $this->smartyci->assign('semestre', $semestre);
        $this->smartyci->assign('disciplinas', $disciplinas);

        $this->smartyci->display('disciplinas.tpl');
    }

    /*
     * CRUD de Disciplinas
     */

    public function incluir() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('disciplina', 'Disciplina', 'trim|required');
        $this->form_validation->set_rules('descricao', 'Descrição', 'trim|required|min_length[10]|max_length[180]');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
        } else {
            $data['SEMESTRE_cd_semestre'] = $this->input->post('cd_semestre');
            $data['nm_disciplina'] = $this->input->post('disciplina');
            $data['ds_disciplina'] = $this->input->post('descricao');

            if ($this->M_disciplina->insert($data)) {
                echo json_encode(array('st' => 1, 'msg' => 'Disciplina adicionada com sucesso.'));
            } else {
                echo json_encode(array('st' => 2, 'msg' => 'Erro ao adicionar a Disciplina.'));
            }
        }
    }

    public function editar($cd_disciplina = NULL) {
        header("Content-Type: application/json");

        $disciplinas = $this->M_disciplina->getDisciplina($cd_disciplina);

        echo json_encode(array_shift($disciplinas));
    }

    public function atualizar() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('disciplina', 'Disciplina', 'trim|required');
        $this->form_validation->set_rules('descricao', 'Descrição', 'trim|required|min_length[10]|max_length[180]');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
        } else {
            $data['SEMESTRE_cd_semestre'] = $this->input->post('cd_semestre');
            $data['cd_disciplina'] = $this->input->post('cd_disciplina');
            $data['nm_disciplina'] = $this->input->post('disciplina');
            $data['ds_disciplina'] = $this->input->post('descricao');

            if ($this->M_disciplina->update($data)) {
                echo json_encode(array('st' => 1, 'msg' => 'Disciplina atualizada com sucesso.'));
            } else {
                echo json_encode(array('st' => 2, 'msg' => 'Erro ao atualizar a Disciplina.'));
            }
        }
    }

    public function excluir($cd_disciplina = null) {
        $data['cd_disciplina'] = $cd_disciplina;
        $data['fl_excluido'] = 1;
        if ($this->M_disciplina->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Disciplina excluido com sucesso.'));
        }
    }

    /*
     * Área interna de uma Disciplina
     */

    public function disciplinaInterna($cd_disciplina = NULL) {
        $this->load->model('M_usuario');

        menu_disciplina($cd_disciplina);

        // lista os alunos matriculados na disciplina
        $alunos = $this->M_usuario->getAlunosMatriculadosDisciplina($cd_disciplina);

        $this->smartyci->assign('alunos', $alunos);
        $this->smartyci->display('disciplina_interna.tpl');
    }

}
