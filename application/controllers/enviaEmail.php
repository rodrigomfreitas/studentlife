<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EnviaEmail extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
    }
    
    public function index($cd_disciplina) {
        
        menu_disciplina($cd_disciplina);
        $this->smartyci->display('enviarEmail.tpl');
    }
}
