<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Turmas extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        $this->load->model('M_turma');
    }

    public function index() {
        $this->output->enable_profiler(FALSE);
        // carrega o model
        $this->load->model('M_curso');

        // lista somente os cursos que contém semestres cadastrado
        $cursos = $this->M_curso->getCursosComAnos();
        
        $this->smartyci->assign('cursos', $cursos);
        $this->smartyci->display('turmas_cursos.tpl');
    }

    public function turmasAno($cd_curso) {
        // carrega o model
        $this->load->model('M_ano');
        $this->load->model('M_curso');

        // lista os anos correspondente ao código do curso
        $anos = $this->M_ano->getAnosComSemestres($cd_curso);

        // lista os dados do curso
        $cursos = $this->M_curso->getCurso($cd_curso);

        // atribui a variável ao template
        $this->smartyci->assign('anos', $anos);
        $this->smartyci->assign('cursos', $cursos);

        // exibe o template
        $this->smartyci->display('turmas_anos.tpl');
    }

    public function turmasCurso($cd_ano) {
        // carrega as models
        $this->load->model('M_curso');
        $this->load->model('M_usuario');
        $this->load->model('M_semestre');

        $cd_turma = array();
        $alunos_count = array();

        // lista as turmas do curso
        $turmas = $this->M_turma->getTurmas($cd_ano);

        // cria um array com os IDs das Turmas
        foreach ($turmas as $turma) {
            $cd_turma[] = $turma->cd_turma;

            // armazena a posição e o total de alunos de uma turma
            $alunos_count[$turma->cd_turma] = $this->M_usuario->totalAlunosInscritosTurma($turma->cd_turma);
        }

        // lista os dados do curso
        $curso = $this->M_curso->getCursoPorAno($cd_ano);

        // retorna os Turnos para o preechimento dos combobox dos formulários de inclusão e edição
        $turnos = $this->M_turma->getTurnos();

        // mostra a quantidade de semestres de um curso
        $nrsemestre = $this->M_semestre->getSemestrePorAno($cd_ano);

        $this->smartyci->assign('alunos_count', $alunos_count);
        $this->smartyci->assign('curso', $curso);
        $this->smartyci->assign('turnos', $turnos);
        $this->smartyci->assign('turmas', $turmas);
        $this->smartyci->assign('nrsemestre', $nrsemestre);
        $this->smartyci->display('turmas.tpl');
    }

    public function turmasInformacoes($cd_turma) {
        $this->output->enable_profiler(FALSE);
        // carrega o model
        $this->load->model('M_avaliacao_usuario');
         $this->load->model('M_chave_turma');

        // lista todos os dados correspondente a turma (curso, semestres, disciplinas)
        $dados = $this->M_turma->getDisciplinasPorTurma($cd_turma);
        
        // lista as chaves gerdas em uma determinada turma
        $chaves = $this->M_chave_turma->getChaves($cd_turma);
        
        // lista dos conceitos correspondentes a turma
        $conceitos = $this->M_avaliacao_usuario->conceitosGraficoTurma($cd_turma);

        // lista os alunos inscritos nessa turma
        $alunos = $this->M_turma->getAlunosPorTurma($cd_turma);

        if (count($dados) > 0 && count($conceitos) > 0) {
            foreach ($conceitos as $row) {
                $conceitoA[] = $row['conceitoA'];
                $conceitoB[] = $row['conceitoB'];
                $conceitoC[] = $row['conceitoC'];
                $conceitoD[] = $row['conceitoD'];
                $conceitoSC[] = $row['semConceito'];

                $nmDisciplinas[] = $row['nm_disciplina'];
            }

            $disciplinas = json_encode($nmDisciplinas, JSON_UNESCAPED_UNICODE);


            $this->smartyci->assign('conceitoA', $conceitoA);
            $this->smartyci->assign('conceitoB', $conceitoB);
            $this->smartyci->assign('conceitoC', $conceitoC);
            $this->smartyci->assign('conceitoD', $conceitoD);
            $this->smartyci->assign('conceitoSC', $conceitoSC);
            $this->smartyci->assign('nmDisciplina', $disciplinas);
        }
        $this->smartyci->assign('conceitos', $conceitos);
        $this->smartyci->assign('dados', $dados);
        $this->smartyci->assign('chaves', $chaves);
        $this->smartyci->assign('alunos', $alunos);

        $this->smartyci->display('turmas_detalhes.tpl');
    }

    public function incluir() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('semestre', 'Semestre Atual', 'required');
        $this->form_validation->set_rules('turno', 'Turno', 'trim|required');
        $this->form_validation->set_rules('ano_turma', 'Ano da Turma', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
        } else {

            $data['CURSO_cd_curso'] = $this->input->post('cd_curso');
            $data['TURNO_cd_turno'] = $this->input->post('turno');
            $data['SEMESTRE_cd_semestre'] = $this->input->post('semestre');
            $data['dt_turma'] = $this->input->post('ano_turma');
            $data['ANO_cd_ano'] = $this->input->post('cd_ano');

            if ($this->M_turma->insert($data)) {
                echo json_encode(array('st' => 1, 'msg' => 'Turma adicionada com sucesso.'));
            }
        }
    }

    public function editar($cd_turma = NULL) {
        header("Content-Type: application/json");

        $turmas = $this->M_turma->getTurma($cd_turma);

        echo json_encode(array_shift($turmas));
    }

    public function atualizar() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('semestre', 'Semestre Atual', 'required');
        $this->form_validation->set_rules('turno', 'Turno', 'trim|required');
        $this->form_validation->set_rules('ano_turma', 'Ano da Turma', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
        } else {

            // obtém os dados do form
            $data['CURSO_cd_curso'] = $this->input->post('cd_curso');
            $data['TURNO_cd_turno'] = $this->input->post('turno');
            $data['cd_turma'] = $this->input->post('cd_turma');
            $data['SEMESTRE_cd_semestre'] = $this->input->post('semestre');
            $data['dt_turma'] = $this->input->post('ano_turma');
            $data['ANO_cd_ano'] = $this->input->post('cd_ano');

            if ($this->M_turma->update($data)) {
                echo json_encode(array('st' => 1, 'msg' => 'Turma atualizada com sucesso.'));
            }
        }
    }

    public function excluir($cd_turma = null) {
        $data['cd_turma'] = $cd_turma;
        $data['fl_excluido'] = 1;
        if ($this->M_turma->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Curso excluído com sucesso.'));
        }
    }

    
}
