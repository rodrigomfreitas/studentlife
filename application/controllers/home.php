<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
    }

    public function index() {
//        $this->output->enable_profiler(true);
        // carrega o model
        $this->load->model('M_chat');
        $this->load->model('M_avaliacao_usuario');
        $this->load->model('M_avaliacao');

        // recuperar o status e o código do usuário na sessão
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');

        // verifica se o status é de um usuario do tipo aluno
        if ($sessao_dados['status'] == 3) {
            // retorna dados referente os conceitos no qual o aluno foi reprovado
            $avisoReprovacao = $this->M_avaliacao_usuario->getConceitosRecomendacoes($sessao_dados['id']);
            // retorna os dados das Avaliações pendentes de um determinado Aluno
            $avaliacoesPendentes = $this->M_avaliacao->getAvaliacaoPendente($sessao_dados['id']);
            // atribui a variável ao template
            $this->smartyci->assign('avisoReprovacao', $avisoReprovacao);
            $this->smartyci->assign('avaliacoesPendentes', $avaliacoesPendentes);
        }

        if ($sessao_dados['status'] == 2) {
            // carrega o model
            $this->load->model('M_disciplina');

            // retorna as disciplinas no qual o professor está dando aula
            $disciplinas = $this->M_disciplina->getDisciplinaProfessor($sessao_dados['id']);

            // atribui a variável ao template
            $this->smartyci->assign('disciplinas', $disciplinas);
        }

        // lista as mensagens não lidas
        $nao_lidas = $this->M_chat->get_mensagens_nao_lidas_count($sessao_dados['id']);

        // atribui a variável ao template
        $this->smartyci->assign('nao_lidas', $nao_lidas);

        // exibe o template
        $this->smartyci->display('home.tpl');
    }

}
