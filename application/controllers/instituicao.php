<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Instituicao extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $sessao_dados['nome'] = $this->session->userdata('nome');
        $sessao_dados['status'] = $this->session->userdata('status');
        $sessao_dados['id'] = $this->session->userdata('id');
        $sessao_dados['foto'] = $this->session->userdata('foto');
        $sessao_dados['tipo_usuario'] = $this->session->userdata('tipo_usuario');

        $this->smartyci->assign('sessao_dados', $sessao_dados);
        if ($this->session->userdata('logado') == false) {
            redirect('login');
        }
        // carregando Models
        $this->load->model('M_instituicao');
        $this->load->model('M_telefone');
    }

    public function index() {
        // retorna dos dados da Instituição
        $instituicao = $this->M_instituicao->getInstituicao();
        // retorna os Telefones da Instituição
        $telefones = $this->M_telefone->getTelefones();
        
        // assign atribui variáveis ao template
        $this->smartyci->assign('telefones', $telefones);
        $this->smartyci->assign('instituicao', $instituicao);
        
        // display exbibe o template
        $this->smartyci->display('instituicao.tpl');
    }

    public function editarInstituicao($cd_instituicao = null) {
        header("Content-Type: application/json");
        
        // retorna os dados da Instituição
        $instituicao = $this->M_instituicao->getInstituicaoEditar($cd_instituicao);

        echo json_encode(array_shift($instituicao));
    }

    public function atualizar() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('codigo', 'Código', 'trim|required');
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
        $this->form_validation->set_rules('sigla', 'Sigla', 'trim|required');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('cep', 'CEP', 'trim|required');
        $this->form_validation->set_rules('bairro', 'Bairro', 'trim|required');
        $this->form_validation->set_rules('endereco', 'Endereço', 'trim|required');
        $this->form_validation->set_rules('numero', 'Número', 'trim|required');
        $this->form_validation->set_rules('cidade', 'Cidade', 'trim|required');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
            exit;
        }
        $cep = $this->input->post('cep');
        $data['cd_instituicao'] = $this->input->post('cd_instituicao');
        $data['nr_instituicao'] = $this->input->post('codigo');
        $data['nm_instituicao'] = $this->input->post('nome');
        $data['sg_instituicao'] = $this->input->post('sigla');
        $data['ds_email'] = $this->input->post('email');
        $data['nr_cep'] = preg_replace("/[^0-9]/", "", $cep);
        $data['ds_endereco'] = $this->input->post('endereco');
        $data['nr_endereco'] = $this->input->post('numero');
        $data['ds_complemento'] = $this->input->post('complemento');
        $data['ds_bairro'] = $this->input->post('bairro');
        $data['ds_cidade'] = $this->input->post('cidade');
        $data['sg_estado'] = $this->input->post('estado');

        if ($this->M_instituicao->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Instituição alterada com sucesso.'));
        }
    }

    public function inserir_telefone() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('setor', 'Setor', 'trim|required');
        $this->form_validation->set_rules('numero', 'Número', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
        } else {
            $data['INSTITUICAO_cd_instituicao'] = $this->input->post('cd_instituicao');
            $data['ds_setor'] = $this->input->post('setor');
            $numero = $this->input->post('numero');
            $data['nr_telefone'] = preg_replace("/[^0-9]/", "", $numero);
            if ($this->M_telefone->insert($data)) {
                echo json_encode(array('st' => 1, 'msg' => 'Telefone adicionado com sucesso.'));
            } else {
                echo json_encode(array('st' => 2, 'msg' => 'Ocorreu um erro! No momento não foi possível adicionar o telefone.'));
            }
        }
    }

    public function editar_telefone($cd_telefone = null) {
        header("Content-Type: application/json");

        $telefones = $this->M_telefone->getTelefonesEditar($cd_telefone);

        echo json_encode(array_shift($telefones));
    }

    public function atualizar_telefone() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('setor', 'Setor', 'trim|required');
        $this->form_validation->set_rules('numero', 'Número', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo json_encode(array('st' => 0, 'msg' => validation_errors()));
        } else {
            $data['cd_telefone'] = $this->input->post('cd_telefone');
            $data['ds_setor'] = $this->input->post('setor');
            $numero = $this->input->post('numero');
            $data['nr_telefone'] = preg_replace("/[^0-9]/", "", $numero);
            if ($this->M_telefone->update($data)) {
                echo json_encode(array('st' => 1, 'msg' => 'Telefone atualizado com sucesso.'));
            } else {
                echo json_encode(array('st' => 2, 'msg' => 'Ocorreu um erro! No momento não foi possível atualizar o telefone.'));
            }
        }
    }

    public function excluir_telefone($cd_telefone = null) {
        $data['cd_telefone'] = $cd_telefone;
        $data['fl_excluido'] = 1;
        if ($this->M_telefone->update($data)) {
            echo json_encode(array('st' => 1, 'msg' => 'Telefone excluido com sucesso.'));
        }
    }

    public function mask($mask, $string) {
        return vsprintf($mask, str_split($string));
    }

}
