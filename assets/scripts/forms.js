function Alert(comando, texto, titulo) {
    Command: toastr[comando](titulo, texto)
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
}

$('#envia_mensagem').submit(function (event) {
    event.preventDefault();
    document.getElementById("botao_enviar").disabled = true;
    var mensagem = $('#mensagem').val();
    var id_recebe = $('#id_recebe').val();

    if (mensagem == '') {
        Alert("warning", "ERROR", "Por Favor insira uma mensagem");
    } else {
        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/mensagem/enviar",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Mensagem enviada");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);


                } else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});

$('#inctel').submit(function (event) {
    event.preventDefault();
    var setor = $('#setor').val();
    var numero = $('#numero').val();

    if (setor == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Setor*");

    } else if (numero == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Telefone*");

    } else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/instituicao/inserirtelefone",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Telefone Cadastrado");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);


                } else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});

$('#alt_instituicao').submit(function (event) {
    event.preventDefault();
    var id = $('#id_instituicao').val();
    var nome_instituicao = $('#nome_instituicao').val();
    var sigla_instituicao = $('#sigla_instituicao').val();
    var endereco_instituicao = $('#endereco_instituicao').val();

    if (nome_instituicao == '') {

        Alert("warning", "ERROR", "Erro, nome invalido!");

    } else if (sigla_instituicao == '') {

        Alert("warning", "ERROR", "Erro, sigla invalido!");

    } else if (endereco_instituicao == '') {

        Alert("warning", "ERROR", "Erro, endereço invalido!");

    } else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/instituicao/atualizar",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Instituicao Atualizada");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);


                } else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});

$('#incluir_avaliacao').submit(function (event) {
    event.preventDefault();
    var disciplina_id = $('#disciplina_id').val();
    var nome = $('#nome').val();
    var tipo = $('#tipo').val();
    var data_final = $('#data_final').val();
    var descricao = $('#descricao').val();

    if (nome === '') {

        Alert("warning", "ERROR", "Erro, Nome Vazio!");

    } else if (tipo === '') {

        Alert("warning", "ERROR", "Erro, Selecione um tipo!");

    } else if (data_final === '') {

        Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");

    } else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/avaliacao/incluir",
            data: dados,
            success: function (data)
            {
                Alert("success", "SUCESSO", "avaliação Cadastrada");
                setTimeout(function () {
                    location.reload();
                }, 2000);

            }
        });
        return false;
    }
});

$('#editar_disciplina').submit(function (event) {
    event.preventDefault();
    var nome = $('#nome').val();
    var id_disciplina = $('#id_disciplina').val();

    if (nome == '') {

        Alert("warning", "ERROR", "Erro, nome invalido!");

    } else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/disciplinas/editar_disciplina",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Disciplina Alterada");
                    setTimeout(function () {
                        window.open('http://localhost/student/disciplinas/disciplina_interna/' + id_disciplina, '_self');
                    }, 2000);


                } else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});

$('#incluir_curso').submit(function (event) {
    event.preventDefault();
    var nome = $('#nome').val();
    var silga = $('#sigla').val();
    var semestre_total = $('#semestre_total').val();
    var semestre_atual = $('#semestre_atual').val();
    var ano_inicio = $('#ano_inicio').val();

    if (nome == '') {

        Alert("warning", "ERROR", "Erro, nome invalido!");

    } else if (sigla == '') {

        Alert("warning", "ERROR", "Erro, sigla invalido!");

    } else if (semestre_total == '') {

        Alert("warning", "ERROR", "Erro, semestre total invalido!");

    } else if (semestre_atual == '') {

        Alert("warning", "ERROR", "Erro, semestre atual invalido!");

    } else if (ano_inicio == '') {

        Alert("warning", "ERROR", "Erro, ano início invalido!");

    }

    else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/cursos/incluir",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Curso Cadastrado");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);


                } else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});

$('#inc_instituicao_telefone').submit(function (event) {
    event.preventDefault();
    var instituicao_id = $('#instituicao_id').val();
    var telefone = $('#telefone').val();

    if (instituicao_id == '') {

        Alert("warning", "ERROR", "Erro, nome invalido!");

    } else if (telefone == '') {

        Alert("warning", "ERROR", "Erro, telefone invalido!");

    } else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/instituicaoTelefone/incluir",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Telefone Cadastrado");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);


                } else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});

$('#alt_instituicao_telefone').submit(function (event) {
    event.preventDefault();
    var id_instituicao_telefone = $('#id_instituicao_telefone').val();
    var edit_instituicao_id = $('#edit_instituicao_id').val();
    var edit_telefone = $('#edit_telefone').val();

    if (edit_instituicao_id == '') {

        Alert("warning", "ERROR", "Erro, nome invalido!");

    } else if (edit_telefone == '') {

        Alert("warning", "ERROR", "Erro, telefone invalido!");

    } else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/instituicaoTelefone/atualizar",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Telefone Atualizado");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);


                } else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});

$('#alt_usuario').submit(function (event) {
    event.preventDefault();
    var id_usuario = $('#id_usuario').val();
    var matricula = $('#matricula').val();
    var email = $('#email').val();

    if (matricula == '') {

        Alert("warning", "ERROR", "Erro, matrícula invalida!");

    } else if (email == '') {

        Alert("warning", "ERROR", "Erro, e-mail invalido!");

    } else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/perfil/atualizar_usuario",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Dados de Acesso Atualizado");
                    setTimeout(function () {
                        //                        location.reload();
                    }, 2000);


                } else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});

//$('#alt_perfil').submit(function (event) {
//    event.preventDefault();
//    var id_perfil = $('#id_perfil').val();
//    var nome = $('#nome').val();
//    var sobrenome = $('#sobrenome').val();
//    var cpf = $('#cpf').val();
//    var cep = $('#cep').val();
//    var cidade = $('#cidade').val();
//    var estado = $('#estado').val();
//    var bairro = $('#bairro').val();
//    var endereco = $('#endereco').val();
//    var data_nasc = $('#data_nasc').val();
//
//    if (nome == '') {
//
//        Alert("warning", "ERROR", "Erro, nome invalido!");
//
//    } else if (sobrenome == '') {
//
//        Alert("warning", "ERROR", "Erro, sobrenome invalido!");
//
//    } else if (cpf == '') {
//
//        Alert("warning", "ERROR", "Erro, cpf invalido!");
//
//    } else if (cep == '') {
//
//        Alert("warning", "ERROR", "Erro, cep invalido!");
//
//    } else if (cidade == '') {
//
//        Alert("warning", "ERROR", "Erro, cidade invalida!");
//
//    } else if (estado == '') {
//
//        Alert("warning", "ERROR", "Erro, estado invalido!");
//
//    } else if (bairro == '') {
//
//        Alert("warning", "ERROR", "Erro, bairro invalido!");
//
//    } else if (endereco == '') {
//
//        Alert("warning", "ERROR", "Erro, endereço invalido!");
//
//    } else if (data_nasc == '') {
//
//        Alert("warning", "ERROR", "Erro, data de nascimento invalida!");
//
//    }
//    else {
//
//        var dados = jQuery(this).serialize();
//        console.log(dados);
//        jQuery.ajax({
//            type: "POST",
//            url: "http://localhost/student/perfil/atualizar_perfil",
//            data: dados,
//            success: function (data)
//            {
//                if (data == 1) {
//
//                    Alert("success", "SUCESSO", "Dados Pessoais Atualizado");
//                    setTimeout(function () {
//                        location.reload();
//                    }, 2000);
//
//
//                } else {
//                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
//                }
//
//            }
//        });
//        return false;
//    }
//});

$('#registerForm').submit(function (event) {
    event.preventDefault();
    var nome = $('#nome').val();
    var sobrenome = $('#sobrenome').val();
    var matricula = $('#matricula').val();
    var email = $('#email').val();
    var confirmar_email = $('#confirmar_email').val();
    var senha = $('#senha').val();
    var confirmar_senha = $('#confirmar_senha').val();

    if (matricula == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Matrícula*");

    } else if (nome == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Nome*");

    } else if (sobrenome == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Sobrenome*");

    } else if (email == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Email*");

    } else if (confirmar_email == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Confirmar Email*");

    } else if (email != confirmar_email) {

        Alert("warning", "ERROR", "E-mail de confirmação não confere");

    } else if (senha == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Senha*");

    } else if (confirmar_senha == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Confirmar Senha*");

    } else if (senha != confirmar_senha) {

        Alert("warning", "ERROR", "Senha de confirmação não confere");

    }
    else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/register/incluir_usuario",
            data: dados,
            success: function (data)
            {
                if (data == 1) {
                    Alert("warning", "Erro", "Esse E-mail já está cadastrado, por favor, insira outro e-mail");
                }
//                 else {
//                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
//                }

            }
        });
        return false;
    }
});



$('#alt_senha').submit(function (event) {
    event.preventDefault();
    var senha_atual = $('#senha_atual').val();
    var senha_nova = $('#nova_senha').val();
    var repetir_senha_nova = $('#repetir_nova_senha').val();

    if (senha_atual == '') {

        Alert("warning", "ERROR", "Erro, senha atual invalida!");

    } else if (senha_nova == '') {

        Alert("warning", "ERROR", "Erro, senha nova invalida!");

    } else if (repetir_senha_nova == '') {

        Alert("warning", "ERROR", "Erro, repetir senha nova invalida!");

    } else if (senha_nova != repetir_senha_nova) {

        Alert("warning", "ERROR", "Erro, nova senha não confere!");

    }
    else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/perfil/atualizar_senha",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Senha de Acesso Atualizado");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);

                } else if (data == 2) {
                    Alert("warning", "Erro", "Senha atual não é valida!");
                } else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});

$('#incluir_disciplina').submit(function (event) {
    event.preventDefault();
    var nome = $('#nome').val();
    var usuario_id = $('#usuario_id').val();

    if (nome == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Nome*");

    } else if (usuario_id == '') {

        Alert("warning", "ERROR", "Por Favor Preencha o Campo *Professor*");

    } else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/disciplinas/incluir",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Disciplina Cadastrada");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);


                } else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});

$('#inc_disciplina_aluno').submit(function (event) {
    event.preventDefault();
    var usuario_id = $('#usuario_id').val();

    if (usuario_id == '') {

        Alert("warning", "ERROR", "Nenhum aluno selecionado, por favor, escolha pelo menos um aluno");

    } else {

        var dados = jQuery(this).serialize();
        console.log(dados);
        jQuery.ajax({
            type: "POST",
            url: "http://localhost/student/disciplinas/incluir_disciplina_usuario",
            data: dados,
            success: function (data)
            {
                if (data == 1) {

                    Alert("success", "SUCESSO", "Aluno(s) inserido(s) na disciplina");
                    setTimeout(function () {
                        location.reload();
                    }, 2000);


                } else if (data == 2) {
                    Alert("warning", "Erro", "Nenhum aluno selecionado, por favor, escolha pelo menos um aluno");
                }

                else {
                    Alert("danger", "ERROR", "Ocorreu um Erro, por favor, tente novamente");
                }

            }
        });
        return false;
    }
});
